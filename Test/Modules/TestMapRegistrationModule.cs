#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.MapRegistration.Data;
using Skulk.Hydralize;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Skulk.Test.Modules
{
  public class TestMapRegistrationModule
  {
    private class TestMapObject
    {
      private int m_id;

      public int Id { get => m_id; set => m_id = value; }
    }

    private class TestMapObject2
    {
      private string m_name;
      private int m_count;

      public string Name { get => m_name; set => m_name = value; }
      public int Count { get => m_count; set => m_count = value; }
    }

    private class TestMessageMapObject : HydraObjMsg
    {
      public TestMessageMapObject(TestMapObject mapObject) : base(10000, 0, mapObject)
      {
        // Empty
      }

      public TestMessageMapObject(Message other) : base(other)
      {
        // Empty
      }
    }

    private class TestMessageModule : HydraModule
    {
      public Queue<TestMapObject> ObjectQueue = new Queue<TestMapObject>();

      public TestMessageModule(HydraCore parent) : base(parent, 10000, 10000)
      {
        // Empty
      }

      public override void HandleMessage(Message msg, HydraUuid uuid)
      {
        switch((UInt16)msg.GetCmd())
        {
          case 10000:
          {
            TestMessageMapObject msgMapObj = new TestMessageMapObject(msg);
            TestMapObject mapObject = msgMapObj.GetObject<TestMapObject>();
            ObjectQueue.Enqueue(mapObject);
          }
          break;

          default:
          {
            Console.WriteLine("Received unexpected message: {0}", msg.GetCmd());
          }
          break;
        }
      }
    }

    /// <summary>
    /// Tests adding and getting an object map
    /// </summary>
    [Test]
    public void TestAddingMap()
    {
      HydraSettings settings = new HydraSettings();
      HydraCore core = new HydraCore(settings);
      MapRegistrationModule mapModule = new MapRegistrationModule(core);
      core.AddModule(mapModule);
      core.Start();

      mapModule.RegisterObject(typeof(TestMapObject), (int)MapDefinitionCategories.TELEMETRY);

      MapDefinition definition = mapModule.GetMap(core.Uuid, typeof(TestMapObject).Name);
      Assert.IsNotNull(definition);
      Assert.AreEqual(1, definition.Map.TypeInfo.GetFields().Length);

      core.Stop();
      core.Join();
    }

    /// <summary>
    /// Tests adding and getting multiple object maps
    /// </summary>
    [Test]
    public void TestAddingMaps()
    {
      HydraSettings settings = new HydraSettings();
      HydraCore core = new HydraCore(settings);
      MapRegistrationModule mapModule = new MapRegistrationModule(core);
      core.AddModule(mapModule);
      core.Start();

      mapModule.RegisterObject(typeof(TestMapObject), (int)MapDefinitionCategories.TELEMETRY);
      mapModule.RegisterObject<TestMapObject2>((int)MapDefinitionCategories.TELEMETRY);

      List<MapDefinition> maps = mapModule.GetMaps(core.Uuid);
      Assert.IsNotNull(maps);
      Assert.AreEqual(2, maps.Count);

      MapDefinition definition = maps.Find(x => x.Name == typeof(TestMapObject2).Name);
      Assert.IsNotNull(definition);
      Assert.AreEqual(2, definition.Map.TypeInfo.GetFields().Length);

      core.Stop();
      core.Join();
    }

    /// <summary>
    /// Tests adding and getting a message map
    /// </summary>
    [Test]
    public void TestAddingMessageMap()
    {
      HydraSettings settings = new HydraSettings();
      HydraCore core = new HydraCore(settings);
      MapRegistrationModule mapModule = new MapRegistrationModule(core);
      core.AddModule(mapModule);
      core.Start();

      mapModule.RegisterObjectMessage<TestMapObject>(new TestMessageMapObject(new TestMapObject()), (int)MapDefinitionCategories.TELEMETRY);

      MapDefinition definition = mapModule.GetMap(core.Uuid, typeof(TestMessageMapObject).Name);
      Assert.IsNotNull(definition);
      Assert.AreEqual(1, definition.Map.TypeInfo.GetFields().Length);
      HydraMessageMap messageMap = definition.Map as HydraMessageMap;
      Assert.IsNotNull(messageMap);
      Assert.AreEqual(10000, messageMap.Code);
      Assert.AreEqual(0, messageMap.ObjectId);
      HydraTypeInfo typeInfo = messageMap.TypeInfo;
      Assert.IsNotNull(typeInfo);
      Assert.AreEqual(typeof(TestMapObject).Name, typeInfo.Name);

      Assert.AreEqual(2, mapModule.GetMaps(core.Uuid).Count);
      Assert.IsNotNull(mapModule.GetMap(core.Uuid, typeof(TestMapObject).Name));

      core.Stop();
      core.Join();
    }

    /// <summary>
    /// Test the process for requesting a message map
    /// </summary>
    [Test]
    public void TestRequestingMaps()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "server",
        UdpPort = 52505,
        TcpPort = 52600
      };
      HydraClientSettings clientSettings = new HydraClientSettings()
      {
        ApplicationName = "client",
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52600
      };

      HydraServer server = new HydraServer(serverSettings);
      HydraClient client = new HydraClient(clientSettings);
      MapRegistrationModule serverMapModule = new MapRegistrationModule(server);
      MapRegistrationModule clientMapModule = new MapRegistrationModule(client);
      server.AddModule(serverMapModule);
      client.AddModule(clientMapModule);
      server.Start();
      client.Start();

      clientMapModule.RegisterObjectMessage<TestMapObject>(new TestMessageMapObject(new TestMapObject()), (int)MapDefinitionCategories.TELEMETRY);

      Assert.IsTrue(client.WaitForHandshake(5));

      serverMapModule.RequestMap("all", client.Uuid);
      Thread.Sleep(1000);
      List<MapDefinition> maps = serverMapModule.GetMaps(client.Uuid);
      Assert.IsNotNull(maps);
      Assert.AreEqual(2, maps.Count);

      for (int i = 0; i < maps.Count; ++i)
      {
        switch (maps[i].Name)
        {
          case nameof(TestMapObject):
          {
            Assert.AreEqual(1, maps[i].Map.GetPaths().Count);
            Assert.AreEqual(1, maps[i].Map.TypeInfo.GetFields().Length);
            Assert.AreEqual("m_id", maps[i].Map.TypeInfo.GetFields()[0].Name);
            break;
          }

          case nameof(TestMessageMapObject):
          {
            var messageMap = maps[i].Map as HydraMessageMap;
            TestMessageMapObject msg = new TestMessageMapObject((TestMapObject)null);
            Assert.AreEqual(msg.GetCmd(), messageMap.Code);
            Assert.AreEqual(msg.GetObjectId(), messageMap.ObjectId);
            Assert.AreEqual(2, maps[i].CategoryIds.Count);
            Assert.IsTrue(maps[i].CategoryIds.Contains((int)MapDefinitionCategories.TELEMETRY));
            Assert.IsTrue(maps[i].CategoryIds.Contains((int)MapDefinitionCategories.MESSAGE_OBJECT));

            Assert.AreEqual(1, maps[i].Map.GetPaths().Count);
            Assert.AreEqual(1, maps[i].Map.TypeInfo.GetFields().Length);
            Assert.AreEqual("m_id", maps[i].Map.TypeInfo.GetFields()[0].Name);
            break;
          }

          default:
          {
            Assert.Fail("Unexpected object map: {0}", maps[i].Name);
            break;
          }
        }
      }

      server.Stop();
      client.Stop();
      server.Join();
      client.Join();
    }

    /// <summary>
    /// Tests the process for creating and sending a message using a message map received by request
    /// </summary>
    [Test]
    public void TestCreatingMessage()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        UdpPort = 52505,
        TcpPort = 52600
      };
      HydraClientSettings clientSettings = new HydraClientSettings()
      {
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52600
      };
      HydraServer server = new HydraServer(serverSettings);
      HydraClient client = new HydraClient(clientSettings);
      MapRegistrationModule serverMapModule = new MapRegistrationModule(server);
      MapRegistrationModule clientMapModule = new MapRegistrationModule(client);
      TestMessageModule testModule = new TestMessageModule(client);
      server.AddModule(serverMapModule);
      client.AddModule(clientMapModule);
      client.AddModule(testModule);
      server.Start();
      client.Start();

      clientMapModule.RegisterObjectMessage<TestMapObject>(new TestMessageMapObject(new TestMapObject()), (int)MapDefinitionCategories.TELEMETRY);

      Assert.IsTrue(client.WaitForHandshake(5));

      serverMapModule.RequestMap("all", client.Uuid);
      Thread.Sleep(1000);
      List<MapDefinition> maps = serverMapModule.GetMaps(client.Uuid);
      Assert.IsNotNull(maps);
      Assert.AreEqual(2, maps.Count);

      // Create message data
      MapDefinition definition = serverMapModule.GetMap(client.Uuid, typeof(TestMessageMapObject).Name);
      HydraMessageMap messageMap = definition.Map as HydraMessageMap;
      Assert.IsNotNull(messageMap);
      string path = messageMap.GetPaths()[0];
      object val = 52;
      Message message = messageMap.CreateMessage(new string[] { path }, new object[] { val });
      Assert.IsNotNull(message);
      server.SendMessage(message, client.Uuid);
      Thread.Sleep(200);
      Assert.AreEqual(1, testModule.ObjectQueue.Count);
      TestMapObject mapObject = testModule.ObjectQueue.Dequeue();
      Assert.IsNotNull(mapObject);
      Assert.AreEqual(52, mapObject.Id);

      server.Stop();
      client.Stop();
      server.Join();
      client.Join();
    }
  }
}
