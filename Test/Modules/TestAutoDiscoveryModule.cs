#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra;
using Skulk.Hydra.AutoDiscovery.Data;
using System;
using NUnit.Framework;
using System.Threading;
using System.Collections.Generic;
using System.Net;

namespace Skulk.Test.Modules
{
  [TestFixture]
  public class TestAutoDiscoveryModule : HydraUnitTest
  {
    private AutoDiscoveryModule m_serverMod;
    private AutoDiscoveryModule m_clientMod;

    public TestAutoDiscoveryModule() : base(2)
    {
      // Empty
    }

    public override void Configure()
    {
      base.Configure();
      UdpHandshake = false;
      TcpHandshake = false;

      switch (TestContext.CurrentContext.Test.Name)
      {
        case nameof(TestAutoConnect):
        {
          ServerCount = 3;
          ClientCount = 0;
          break;
        }

        default:
        {
          ServerCount = 1;
          ClientCount = 2;
          break;
        }
      }
    }

    public override void ModuleSetup()
    {
      switch (TestContext.CurrentContext.Test.Name)
      {
        case nameof(TestAutoConnect):
        {
          for (int i = 0; i < ServerCount; ++i)
          {
            var server = Servers[i];
            var discoveryModule = new AutoDiscoveryModule(server, 600, 900, new int[] { 52505, 52506, 52507 });
            discoveryModule.AutoConnect = true;
            server.AddModule(discoveryModule);
          }
          break;
        }

        default:
        {
          int[] searchPorts = new int[1];
          searchPorts[0] = Server.Settings.UdpPort;

          m_serverMod = new AutoDiscoveryModule(Server, 600, 900, searchPorts);
          m_clientMod = new AutoDiscoveryModule(Client, 2, 900, searchPorts);

          Server.AddModule(m_serverMod);
          Client.AddModule(m_clientMod);
          Clients[1].AddModule(new AutoDiscoveryModule(Clients[1], 2, 900, searchPorts));
          break;
        }
      }
    }

    [Test]
    public void TestAutoPing()
    {
      Thread.Sleep(3000);
      // Auto discovery is designed around a shared common port same machine client and server don't work with this strategy
      // only the server will get it's own pongs in this setup
      List<HydraAddressData> servers = m_clientMod.GetServers();
      Assert.IsTrue(servers.Count > 0);
      HydraAddressData serverAddress = new HydraAddressData(Server);
      Server.Log.LogInfo("Resolved local host to: '{0}'", serverAddress.Host);
      Assert.AreEqual(servers[0].Host, serverAddress.Host);
      Assert.AreEqual(servers[0].MessagePort, serverAddress.MessagePort);
      Assert.AreEqual(servers[0].StreamPort, serverAddress.StreamPort);

      // Handshake with received servers
      foreach (HydraClient client in Clients)
      {
        client.SendHandshake(servers[0].GetUdpEndPoint());
      }

      foreach (HydraClient client in Clients)
      {
        Assert.IsTrue(WaitForCondition(() => client.GetProfile(Server.Uuid) != null, 3));
      }

      foreach (HydraClient client in Clients)
      {
        client.SendMessage(new NoopMsg(), client.ServerUuid);
      }

      Thread.Sleep(100);
    }

    [Test]
    public void TestAutoConnect()
    {
      // Assume auto connect is true on the server modules
      for (int i = 0; i < ServerCount; ++i)
      {
        Assume.That(Servers[i].GetModule<AutoDiscoveryModule>().AutoConnect);
      }

      // The servers come up quickly but who pings when other servers are up is a race condition
      // Wait for connections to happen
      Assert.IsTrue(WaitForCondition(() => Servers[0].ProfileCount == 2, 5));
      Assert.IsTrue(WaitForCondition(() => Servers[1].ProfileCount == 2, 2));
      Assert.IsTrue(WaitForCondition(() => Servers[2].ProfileCount == 2, 2));

      // Check that each server knows about the others
      for (int i = 0; i < ServerCount; ++i)
      {
        var server = Servers[i];
        Assert.IsTrue(WaitForCondition(() => server.GetModule<AutoDiscoveryModule>().GetServers().Count == ServerCount, 5), "Server[{0}] count: {1} instead of expected {2}", i, server.GetModule<AutoDiscoveryModule>().GetServers().Count, ServerCount);

        for (int j = 0; j < ServerCount; ++j)
        {
          if (i != j)
          {
            Assert.IsNotNull(server.GetProfile(Servers[j].Uuid));
            Assert.IsTrue(server.GetProfile(Servers[j].Uuid).ConnectionCount > 0);
            var connection = server.GetProfile(Servers[j].Uuid).GetConnection(Hydra.Network.ConnectionTypes.MESSAGE) as Hydra.Network.UdpConnection;
            Assert.IsNotNull(connection.ClientEp);
          }
        }
      }

      Thread.Sleep(200);
    }

    [Test]
    public void RequestServerList()
    {
      Assert.IsTrue(Client.WaitForHandshake(5));
      var servers = m_clientMod.GetServers();
      m_clientMod.RequestServerList(Server.Uuid);

      // Wait for server list to be updated...
      Assert.IsTrue(WaitForCondition(() => m_clientMod.GetServers().Count == 2, 3), "Server count is: {0} instead of 2", m_clientMod.GetServers().Count);

      Thread.Sleep(200);
    }
  }
}
