#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Concurrent;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Diagnostics;
using Skulk.Hydra.Diagnostics.Data;
using Skulk.Test.Core;

namespace Skulk.Test.Modules
{
  /// <summary>
  /// Tests for the Diagnostics module
  /// </summary>
  [TestFixture]
  public class TestDiagnostics
  {
    private HydraNetwork m_network;
    private ConcurrentQueue<NetworkEvent> m_netEvents = new ConcurrentQueue<NetworkEvent>();

    [TearDown]
    public void TestTearDown()
    {
      if (m_network != null)
      {
        m_network.Stop();
        m_network.Join();
      }
    }

    /// <summary>
    /// Test the ability to get network diagnostics
    /// </summary>
    [Test]
    public void NetworkDiagnostics()
    {
      m_network = new HydraNetwork()
      {
        ServerCount = 1,
        ClientCount = 2
      };

      m_network.Init();
      m_network.Servers[0].AddModule(new DiagnosticsModule(m_network.Servers[0]));
      m_network.Clients[0].AddModule(new DiagnosticsClientModule(m_network.Clients[0]));
      m_network.Start();

      Assert.IsTrue(m_network.Clients[0].WaitForHandshake(5));
      m_network.Clients[0].GetModule<DiagnosticsClientModule>().NetworkEventReceived += HandleNetworkEvent;
      m_network.Clients[0].GetModule<DiagnosticsClientModule>().Subscribe(SubscriptionType.Network);

      // Make sure the subscription went through
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_network.Servers[0].GetModule<DiagnosticsModule>().GetSubscribers().Length == 1, 1));

      // Now connect the second client
      Assert.IsTrue(m_network.Clients[1].WaitForHandshake(5));

      // Wait for 2 events
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_netEvents.Count == 2, 5));

      // Dequeue and inspect the events
      for (int i = 0; i < 2; ++i)
      {
        Assert.IsTrue(m_netEvents.TryDequeue(out NetworkEvent netEvent));
        switch (netEvent.Event)
        {
          case NetworkEvents.ProfileAdded:
          {
            Assert.AreEqual(m_network.Clients[1].Uuid, netEvent.Profile.Uuid);
            Assert.IsNull(netEvent.Connection);
            break;
          }

          case NetworkEvents.ConnectionAdded:
          {
            Assert.AreEqual(Hydra.Network.ConnectionTypes.MESSAGE, netEvent.Connection.ConnectionType);
            Assert.IsNotNull(netEvent.Profile);
            Assert.AreEqual(m_network.Clients[1].Uuid, netEvent.Profile.Uuid);
            break;
          }

          default:
          {
            Assert.Fail("Unexpected network event: {0}", netEvent.Event);
            break;
          }
        }
      }

      // Now cause a failure in the connection and remove the profile for the second client
      m_network.Clients[1].GetProfile(m_network.Servers[0].Uuid).GetConnection(Hydra.Network.ConnectionTypes.MESSAGE).SendCloseMessage = false;
      m_network.Clients[1].GetProfile(m_network.Servers[0].Uuid).GetConnection(Hydra.Network.ConnectionTypes.MESSAGE).Close();
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_network.Clients[1].GetProfile(m_network.Servers[0].Uuid).ConnectionCount == 0, 5));
      m_network.Servers[0].GetProfile(m_network.Clients[1].Uuid).GetConnection(Hydra.Network.ConnectionTypes.MESSAGE).Close(true);
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_network.Servers[0].GetProfile(m_network.Clients[1].Uuid).ConnectionCount == 0, 5));
      m_network.Clients[1].RemoveProfile(m_network.Servers[0].Uuid);

      // Remove the profile on the server
      m_network.Servers[0].RemoveProfile(m_network.Clients[1].Uuid);
      // Wait for 3 events, the profile and connection removal and connection failure
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_netEvents.Count == 3, 5));

      // Dequeue and inspect the events
      int profileRemovedCount = 0;
      int connectionRemovedCount = 0;
      int connectionFailedCount = 0;
      for (int i = 0; i < 3; ++i)
      {
        Assert.IsTrue(m_netEvents.TryDequeue(out NetworkEvent netEvent));
        switch (netEvent.Event)
        {
          case NetworkEvents.ProfileRemoved:
          {
            ++profileRemovedCount;
            Assert.AreEqual(m_network.Clients[1].Uuid, netEvent.Profile.Uuid);
            Assert.IsNull(netEvent.Connection);
            break;
          }

          case NetworkEvents.ConnectionRemoved:
          case NetworkEvents.ConnectionFailed:
          {
            if (netEvent.Event == NetworkEvents.ConnectionFailed)
            {
              ++connectionFailedCount;
            }
            else
            {
              ++connectionRemovedCount;
            }
            Assert.AreEqual(Hydra.Network.ConnectionTypes.MESSAGE, netEvent.Connection.ConnectionType);
            Assert.IsNotNull(netEvent.Profile);
            Assert.AreEqual(m_network.Clients[1].Uuid, netEvent.Profile.Uuid);
            break;
          }

          default:
          {
            Assert.Fail("Unexpected network event: {0}", netEvent.Event);
            break;
          }
        }
      }

      Assert.AreEqual(1, profileRemovedCount);
      Assert.AreEqual(1, connectionFailedCount);
      Assert.AreEqual(1, connectionRemovedCount);
    }

    private void HandleNetworkEvent(object sender, NetworkEvent e)
    {
      m_netEvents.Enqueue(e);
    }
  }
}
