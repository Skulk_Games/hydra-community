#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;
using Skulk.MEL;
using System;
using System.Collections.Generic;

namespace Skulk.Test
{
  /// <summary>
  /// Tests for the static and dynamic message module routing
  /// </summary>
  public class TestModuleRouting
  {
    private MessageHandlerRouter m_staticHandler;
    private DynamicMessageHandler m_dynamicHandler;
    private Logger m_log;

    private class MockMessage : Message
    {
      public MockMessage(UInt16 code) : base(code, 0)
      {
        // Empty
      }
    }

    private class MockStaticModule : HydraModule
    {
      public int ReceiveCount = 0;

      public MockStaticModule(HydraCore parent) : base(parent, 10000, 10000)
      {
        // Empty
      }

      public override void HandleMessage(Message msg, HydraUuid uuid)
      {
        switch((UInt16)msg.GetCmd())
        {
          case 10000:
          {
            ++ReceiveCount;
          }
          break;
        }
      }
    }

    private class MockDynamicModule : HydraModule
    {
      public int ReceiveCount = 0;

      public MockDynamicModule(HydraCore parent) : base(parent, 10000)
      {
        // Empty
      }

      public override void HandleMessage(Message msg, HydraUuid uuid)
      {
        switch ((UInt16)msg.GetCmd())
        {
          case 10000:
          {
            ++ReceiveCount;
          }
          break;
        }
      }
    }

    [OneTimeSetUp]
    public void ClassSetUp()
    {
      m_log = new Logger("hydraTests.log", 255);
      m_log.Start(true);
    }

    [OneTimeTearDown]
    public void ClassCleanUp()
    {
      m_log.Stop();
      m_log.Join();
    }

    /// <summary>
    /// Tests message handling and routing in a regular <see cref="MessageHandlerRouter"/>
    /// </summary>
    [Test]
    public void TestStaticHandling()
    {
      MockStaticModule staticModule = new MockStaticModule(null);
      List<IModule> modules = new List<IModule>();
      modules.Add(staticModule);

      m_staticHandler = new MessageHandlerRouter(modules, m_log);

      MockMessage mockMessage = new MockMessage(10000);
      HydraUuid uuid = new HydraUuid();

      Assert.IsTrue(m_staticHandler.RouteMessage(modules, mockMessage, uuid));
      Assert.AreEqual(1, staticModule.ReceiveCount);

      mockMessage = new MockMessage(1);
      Assert.IsFalse(m_staticHandler.RouteMessage(modules, mockMessage, uuid));
      Assert.AreEqual(1, staticModule.ReceiveCount);
    }

    /// <summary>
    /// Tests adding and removing message handling to a <see cref="DynamicMessageHandler"/>
    /// </summary>
    [Test]
    public void TestDynamicHandling()
    {
      MockDynamicModule dynamicModule = new MockDynamicModule(null);
      m_dynamicHandler = new DynamicMessageHandler(m_log);
      MockMessage mockMessage = new MockMessage(10000);
      HydraUuid uuid = new HydraUuid();

      Assert.IsFalse(m_dynamicHandler.RouteMessage(mockMessage, uuid));
      m_dynamicHandler.AddHandling(10000, dynamicModule.HandleMessage);
      Assert.IsTrue(m_dynamicHandler.RouteMessage(mockMessage, uuid));
      Assert.AreEqual(1, dynamicModule.ReceiveCount);

      MockMessage badMessage = new MockMessage(1);
      Assert.IsFalse(m_dynamicHandler.RouteMessage(badMessage, uuid));

      m_dynamicHandler.RemoveHandling(10000);
      Assert.IsFalse(m_dynamicHandler.RouteMessage(mockMessage, uuid));
      Assert.AreEqual(1, dynamicModule.ReceiveCount);
    }
  }
}
