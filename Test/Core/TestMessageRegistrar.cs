﻿#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Skulk.Test.Core
{
  public class TestMessageRegistrar
  {
    /// <summary>
    /// Tests loading all code values through JSON files.
    /// </summary>
    [Test]
    public void LoadAllJson()
    {
      string json = @"{
  ""HARDCODE"": 1,
  ""Ack"": 100,
  ""Noop"": 101,
  ""NameMessage"": 102,
  ""Ping"": 103,
  ""Pong"": 104,
  ""Reply"": 105
}";

      string tmpFile = Path.GetTempFileName();
      File.Move(tmpFile, tmpFile + ".json");
      tmpFile = tmpFile + ".json";

      try
      {
        File.WriteAllText(tmpFile, json);

        Assert.AreEqual(1000, TestCodes.HARDCODE);
        Assert.AreEqual(1010, TestCodes.Ack);
        Assert.AreEqual(1011, TestCodes.Noop);
        Assert.AreEqual(1012, TestCodes.Ping);
        Assert.AreEqual(1013, TestCodes.Pong);
        Assert.AreEqual(1014, TestCodes.NameMessage);
        Assert.AreEqual(1015, TestCodes.Reply);

        TestCodes.Load<TestCodes>(tmpFile);

        Assert.AreEqual(1000, TestCodes.HARDCODE);
        Assert.AreEqual(100, TestCodes.Ack);
        Assert.AreEqual(101, TestCodes.Noop);
        Assert.AreEqual(103, TestCodes.Ping);
        Assert.AreEqual(104, TestCodes.Pong);
        Assert.AreEqual(102, TestCodes.NameMessage);
        Assert.AreEqual(105, TestCodes.Reply);

        // Load from multiple code objects
        json = @"{
  ""TestCodes"": {
    ""Ack"": 1,
    ""Noop"": 2,
    ""Ping"": 3,
    ""Pong"": 4,
    ""NameMessage"": 5,
    ""Reply"": 6
  }
}";

        File.WriteAllText(tmpFile, json);

        TestCodes.Load<TestCodes>(tmpFile);

        Assert.AreEqual(1000, TestCodes.HARDCODE);
        Assert.AreEqual(1, TestCodes.Ack);
        Assert.AreEqual(2, TestCodes.Noop);
        Assert.AreEqual(3, TestCodes.Ping);
        Assert.AreEqual(4, TestCodes.Pong);
        Assert.AreEqual(5, TestCodes.NameMessage);
        Assert.AreEqual(6, TestCodes.Reply);
      }
      finally
      {
        File.Delete(tmpFile);
      }
    }

    private class TestCodes : MessageRegistrar
    {
      public const ushort HARDCODE = 1000;
      public static ushort Ack = 1010;
      public static ushort Noop = 1011;
      public static ushort Ping = 1012;
      public static ushort Pong = 1013;
      public static ushort NameMessage = 1014;

      public static ushort Reply { get; set; } = 1015;
    }
  }
}
