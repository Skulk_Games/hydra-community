#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;
using Skulk.MEL;

namespace Skulk.Test.Core
{
  /// <summary>
  /// Contains tests for the <see cref="Hydra.HydraArgParser"/> class.
  /// </summary>
  /// <remarks>
  /// Most of the functionality of the arg parser is verified by tests for <see cref="Skulk.MEL"/> and would be redundant here.
  /// The default values and custom type converters are the focus of these tests.
  /// </remarks>
  public class TestArgParser
  {
    /// <summary>
    /// Verify the default values that are given by the parser.
    /// </summary>
    [Test]
    public void Defaults()
    {
      HydraArgParser parser = new HydraArgParser();

      Assert.AreEqual("Hydra", parser.Name);
      Assert.AreEqual("localhost", parser.ServerHost);
      Assert.AreEqual(52505, parser.ServerPort);
      Assert.AreEqual(0, parser.LocalPort);
      Assert.AreEqual(52600, parser.StreamPort);
      Assert.AreEqual((byte)(LogLevels.ERROR | LogLevels.WARNING | LogLevels.INFO), parser.LogLevel);
      Assert.AreEqual("hydra.log", parser.LogFile);
      Assert.AreEqual(false, parser.EnableConsoleLogging);
      Assert.AreEqual(1, parser.MaxClients);
      Assert.AreEqual(10, parser.KeepalivePeriod);
      Assert.AreEqual(30, parser.ProfileTimeout);
      Assert.AreEqual(false, parser.UseRouting);
      Assert.AreEqual(10, parser.MaxUpdateHz);
      Assert.AreEqual(3600, parser.StreamTimeout);
      Assert.AreEqual(false, parser.DisableUdp);
      Assert.AreEqual(null, parser.Uuid);
      Assert.AreEqual("0.0.0.0", parser.UdpBindAddress);
      Assert.AreEqual("0.0.0.0", parser.TcpBindAddress);
    }

    /// <summary>
    /// Verify the <see cref="HydraUuidStringConverter"/> works with the parser.
    /// </summary>
    [Test]
    public void Uuid()
    {
      string uuidText = "aaaaaaaa-bbbb-cccc-dddd-eeff00112233";

      HydraUuid uuid = new HydraUuid(HydraUuid.Parse(uuidText));
      HydraArgParser parser = new HydraArgParser();
      string[] args = new string[]
      {
        "-u",
        uuidText
      };
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      uuidText = "00000000-bbbb-cccc-dddd-eeff00112233";
      args = new string[]
      {
        $"-u={uuidText}"
      };
      uuid = new HydraUuid(HydraUuid.Parse(uuidText));
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      uuidText = "00000000-bbbb-1234-dddd-eeff00112233";
      args = new string[]
      {
        "--uuid",
        uuidText
      };
      uuid = new HydraUuid(HydraUuid.Parse(uuidText));
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      uuidText = "00000000-bbbb-1234-0102-eeff00112233";
      args = new string[]
      {
        $"--uuid={uuidText}"
      };
      uuid = new HydraUuid(HydraUuid.Parse(uuidText));
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      uuidText = "01020102-cccc-1234-dddd-eeff00112233";
      args = new string[]
      {
        $"--uuid=\"{uuidText}\""
      };
      uuid = new HydraUuid(HydraUuid.Parse(uuidText));
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      // Invalid args should not be used, meaning the parser UUID shouldn't change in the next cases
      args = new string[]
      {
        "--uuid",
        "this is not a uuid"
      };
      // The value has invalid characters which should cause the parsing to fail
      Assert.IsFalse(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);

      args = new string[]
      {
        "--uuid"
      };
      // The value is missing so the value is unchanged, but returns true
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(uuid, parser.Uuid);
    }
  }
}
