#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;

namespace Skulk.Test.Core
{
  /// <summary>
  /// Tests the HydraSettings class and its ability to set values on a Hydra application.
  /// </summary>
  public class TestSettings
  {
    /// <summary>
    /// Test that the UUID can be set.
    /// </summary>
    [Test]
    public void SetUuid()
    {
      HydraUuid uuid = new HydraUuid(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
      HydraSettings settings = new HydraSettings()
      {
        Uuid = uuid
      };

      HydraCore core = new HydraCore(settings);
      Assert.AreEqual(uuid, core.Uuid);
    }

    /// <summary>
    /// Test that the application name can be set.
    /// </summary>
    [Test]
    public void SetName()
    {
      string[] names = new string[]
      {
        "bob",
        "client",
        "server"
      };

      for (int i = 0; i < names.Length; ++i)
      {
        HydraSettings settings = new HydraSettings()
        {
          ApplicationName = names[i]
        };

        HydraCore core = new HydraCore(settings);
        Assert.AreEqual(names[i], core.ApplicationName);
      }
    }

    /// <summary>
    /// Test that the UDP listener can be disabled.
    /// </summary>
    [Test]
    public void DisableUdp()
    {
      HydraSettings settings = new HydraSettings()
      {
        DisableUdp = false
      };

      HydraCore core = new HydraCore(settings);
      Assert.AreEqual(0, core.Address.MessagePort);
      core.Start();
      Assert.AreNotEqual(0, core.Address.MessagePort);
      core.Stop();
      core.Join();

      settings.DisableUdp = true;
      core = new HydraCore(settings);
      Assert.AreEqual(0, core.Address.MessagePort);
      core.Start();
      Assert.AreEqual(0, core.Address.MessagePort);
      core.Stop();
      core.Join();

      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        DisableUdp = true,
        TcpPort = 52506
      };
      HydraClientSettings clientSettings = new HydraClientSettings()
      {
        DisableUdp = true,
        ServerHost = "127.0.0.1",
        ServerTcpPort = 52506,
        ServerUdpPort = 52505
      };
      HydraServer server = new HydraServer(serverSettings);
      HydraClient client = new HydraClient(clientSettings);
      server.Start();
      client.Start();

      Assert.IsTrue(client.StartTcpConnection(5000));
      Assert.IsFalse(client.SendHandshake());
      client.Stop();
      HydraClientSettings clientSettings2 = new HydraClientSettings(clientSettings);
      clientSettings2.DisableUdp = false;
      HydraClient client2 = new HydraClient(clientSettings2);
      client2.Start();
      Assert.IsTrue(client2.StartTcpConnection(5000));
      Assert.IsTrue(client2.SendHandshake());

      // Sleep to give time for a possible connection
      System.Threading.Thread.Sleep(1000);

      // Make sure no message connection was established (only TCP)
      Hydra.Network.Profile profile = client2.GetProfile(server.Uuid);
      Assert.IsNull(profile.GetConnection(Hydra.Network.ConnectionTypes.MESSAGE));

      client2.Stop();
      server.Stop();
      client.Join();
      client2.Join();
      server.Join();
    }
  }
}
