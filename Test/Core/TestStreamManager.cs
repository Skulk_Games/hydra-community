﻿#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Network;

namespace Skulk.Test.Core
{
  /// <summary>
  /// Contains tests for the <see cref="StreamManager"/>
  /// </summary>
  public class TestStreamManager : HydraUnitTest
  {
    private ConcurrentDictionary<uint, MemoryStream> m_streams = new ConcurrentDictionary<uint, MemoryStream>();
    private ConcurrentQueue<HydraStreamStatus> m_streamCompleteQueue = new ConcurrentQueue<HydraStreamStatus>();
    private ConcurrentQueue<HydraStreamStatus> m_streamUpdateQueue = new ConcurrentQueue<HydraStreamStatus>();
    private ConcurrentQueue<Stream> m_demoReceivedStreams = new ConcurrentQueue<Stream>();
    private ConcurrentQueue<StreamNegotiation> m_negotiationQueue = new ConcurrentQueue<StreamNegotiation>();
    private DemoStreamModule serverDemoModule;
    private DemoStreamModule clientDemoModule;

    /// <summary>
    /// A dictionary holding streams that can be requested by remote applications
    /// </summary>
    private Dictionary<int, MemoryStream> m_streamRequests = new Dictionary<int, MemoryStream>();

    /// <summary>
    /// Sets up the modules in use on the Hydra applications
    /// </summary>
    public override void ModuleSetup()
    {
      base.ModuleSetup();

      // Hook up StreamAuthority events
      Client.StreamAuthority.OnStreamUpdated += HandleStreamUpdated;
      Client.StreamAuthority.OnStreamCompleted += HandleStreamCompleted;
      Client.StreamAuthority.RegisterNegotiationDestination("Client", HandleStreamRequest);
      Server.StreamAuthority.RegisterNegotiationDestination("Server", HandleStreamRequest);
      Server.StreamAuthority.OnStreamUpdated += HandleStreamUpdated;
      Server.StreamAuthority.OnStreamCompleted += HandleStreamCompleted;

      if (TestContext.CurrentContext.Test.Name.Contains("External"))
      {
        serverDemoModule = new DemoStreamModule(Server);
        Server.AddModule(serverDemoModule);
        serverDemoModule.StreamReceived += HandleDemoStreamReceived;
        clientDemoModule = new DemoStreamModule(Client);
        Client.AddModule(clientDemoModule);
        clientDemoModule.StreamReceived += HandleDemoStreamReceived;
      }
    }

    /// <summary>
    /// Clean up data between tests.
    /// </summary>
    public override void CustomTestCleanup()
    {
      base.CustomTestCleanup();

      m_streams.Clear();
      m_negotiationQueue.Clear();
      m_streamCompleteQueue.Clear();
      m_streamUpdateQueue.Clear();
      m_demoReceivedStreams.Clear();
      m_streamRequests.Clear();
    }

    /// <summary>
    /// Tests sending a stream using the internal tracking of the <see cref="StreamManager"/> class.
    /// </summary>
    [Test]
    public void TestInternalSendStream()
    {
      // Test step by step with the negotiation and then sending
      Assert.IsTrue(Server.StreamAuthority.NegotiateStream("Server", "Client", Client.Uuid, HydraStreamDirections.WRITE, out uint id));

      // Wait for a completed negotiation
      Assert.IsTrue(WaitForCondition(() => m_negotiationQueue.Count > 0, 3));
      Assert.IsTrue(m_negotiationQueue.TryDequeue(out StreamNegotiation negotiation));
      Assert.IsTrue(negotiation.IsReply);
      Assert.IsTrue(negotiation.Success);

      // Now try to send a simple memory stream
      byte[] buff = new byte[500];
      Random random = new Random();
      random.NextBytes(buff);
      MemoryStream sendStream = new MemoryStream(buff);
      Assert.IsTrue(Server.SendStream(sendStream, Client.Uuid, id, negotiation.DestinationId, buff.Length));

      // Wait for the stream to complete (2 because the queue has both the send and receive completes)
      Assert.IsTrue(WaitForCondition(() => m_streamCompleteQueue.Count == 2, 3));
      for (int i = 0; i < 2; ++i)
      {
        Assert.IsTrue(m_streamCompleteQueue.TryDequeue(out HydraStreamStatus streamStatus));
        if (streamStatus.Direction == HydraStreamDirections.READ)
        {
          Assert.AreEqual(buff.Length, streamStatus.Stream.Length);
          streamStatus.Stream.Seek(0, SeekOrigin.Begin);
          for (int b = 0; b < buff.Length; ++b)
          {
            Assert.AreEqual(buff[b], streamStatus.Stream.ReadByte());
          }
        }
      }

      // Now try a direct send
      sendStream.Seek(0, SeekOrigin.Begin);
      Assert.IsTrue(Server.StreamAuthority.SendStream("Server", "Client", sendStream, Client.Uuid, out id));

      // Wait for a completed successful negotiation
      Assert.IsTrue(WaitForCondition(() => m_negotiationQueue.Count > 0, 3));
      Assert.IsTrue(m_negotiationQueue.TryDequeue(out negotiation));
      Assert.IsTrue(negotiation.Success);

      // Wait for the streams to complete
      Assert.IsTrue(WaitForCondition(() => m_streamCompleteQueue.Count == 2, 3));
      for (int i = 0; i < 2; ++i)
      {
        Assert.IsTrue(m_streamCompleteQueue.TryDequeue(out HydraStreamStatus streamStatus));
        if (streamStatus.Direction == HydraStreamDirections.READ)
        {
          Assert.AreEqual(buff.Length, streamStatus.Stream.Length);
          streamStatus.Stream.Seek(0, SeekOrigin.Begin);
          for (int b = 0; b < buff.Length; ++b)
          {
            Assert.AreEqual(buff[b], streamStatus.Stream.ReadByte());
          }
        }
      }

      // Sending with an unmatched destination should get rejected
      Assert.IsTrue(Server.StreamAuthority.NegotiateStream("Server", "Invalid", Client.Uuid, HydraStreamDirections.WRITE, out id));

      // Wait for a completed rejected negotiation
      Assert.IsTrue(WaitForCondition(() => m_negotiationQueue.Count > 0, 3));
      Assert.IsTrue(m_negotiationQueue.TryDequeue(out negotiation));
      Assert.IsFalse(negotiation.Success);
    }

    /// <summary>
    /// Tests receiving a stream internal to the <see cref="StreamManager"/>
    /// </summary>
    [Test]
    public void TestInternalReceiveStream()
    {
      // Set up a simple memory stream to request
      byte[] buff = new byte[500];
      Random random = new Random();
      random.NextBytes(buff);
      MemoryStream sendStream = new MemoryStream(buff);
      m_streamRequests.Add(1, sendStream);

      MemoryStream recvStream = new MemoryStream();
      Assert.IsTrue(Server.StreamAuthority.ReceiveStream("Client", "Server", recvStream, Client.Uuid, out uint id, StreamTransferModes.CONTINUOUS, 3600, (int)1));

      // Wait for a completed successful negotiation
      Assert.IsTrue(WaitForCondition(() => m_negotiationQueue.Count > 0, 3));
      Assert.IsTrue(m_negotiationQueue.TryDequeue(out StreamNegotiation negotiation));
      Assert.IsTrue(negotiation.Success);

      // Wait for the stream to complete (2 because the queue has both the send and receive completes)
      Assert.IsTrue(WaitForCondition(() => m_streamCompleteQueue.Count == 2, 3));
      for (int i = 0; i < 2; ++i)
      {
        Assert.IsTrue(m_streamCompleteQueue.TryDequeue(out HydraStreamStatus streamStatus));
        if (streamStatus.Direction == HydraStreamDirections.READ)
        {
          Assert.AreEqual(buff.Length, streamStatus.Stream.Length);
          streamStatus.Stream.Seek(0, SeekOrigin.Begin);
          for (int b = 0; b < buff.Length; ++b)
          {
            Assert.AreEqual(buff[b], streamStatus.Stream.ReadByte());
          }
        }
      }

      // Confirm that a rejection works by requesting an unknown stream ID
      recvStream = new MemoryStream();
      Assert.IsTrue(Server.StreamAuthority.ReceiveStream("Client", "Server", recvStream, Client.Uuid, out id, StreamTransferModes.CONTINUOUS, 3600, (int)100));

      // Wait for a rejected negotiation
      Assert.IsTrue(WaitForCondition(() => m_negotiationQueue.Count > 0, 3));
      Assert.IsTrue(m_negotiationQueue.TryDequeue(out negotiation));
      Assert.IsFalse(negotiation.Success);
    }

    /// <summary>
    /// Tests a module that handles stream data in the legacy approach external to the <see cref="StreamManager"/>.
    /// </summary>
    [Test]
    public void TestExternalStream()
    {
      // Send a stream from server to client
      byte[] buff = new byte[500];
      Random random = new Random();
      random.NextBytes(buff);
      MemoryStream sendStream = new MemoryStream(buff);
      serverDemoModule.SendStream(sendStream, Client.Uuid);

      Assert.IsTrue(WaitForCondition(() => m_demoReceivedStreams.Count == 1, 20));
      Assert.IsTrue(m_demoReceivedStreams.TryDequeue(out Stream recvStream));
      Assert.AreEqual(buff.Length, recvStream.Length);
      recvStream.Seek(0, SeekOrigin.Begin);
      for (int b = 0; b < buff.Length; ++b)
      {
        Assert.AreEqual(buff[b], recvStream.ReadByte());
      }

      // Send a stream from client to server
      sendStream.Seek(0, SeekOrigin.Begin);
      clientDemoModule.SendStream(sendStream, Server.Uuid);
      Assert.IsTrue(WaitForCondition(() => m_demoReceivedStreams.Count == 1, 20));
      Assert.IsTrue(m_demoReceivedStreams.TryDequeue(out recvStream));
      Assert.AreEqual(buff.Length, recvStream.Length);
      recvStream.Seek(0, SeekOrigin.Begin);
      for (int b = 0; b < buff.Length; ++b)
      {
        Assert.AreEqual(buff[b], recvStream.ReadByte());
      }
    }

    /// <summary>
    /// Event handler for streams handled externally through the <see cref="DemoStreamModule"/>
    /// </summary>
    /// <param name="sender">The module instance that invoked theh event</param>
    /// <param name="e">The stream that was received</param>
    private void HandleDemoStreamReceived(object sender, Stream e)
    {
      m_demoReceivedStreams.Enqueue(e);
    }

    /// <summary>
    /// Handles the event invoked when an internal stream completes
    /// </summary>
    /// <param name="sender">The <see cref="StreamManager"/> instance that invoked the event</param>
    /// <param name="e">The object describing the status of the stream</param>
    private void HandleStreamCompleted(object sender, HydraStreamStatus e)
    {
      m_streamCompleteQueue.Enqueue(e);
    }

    /// <summary>
    /// Handles the event invoked when an internal stream is updated
    /// </summary>
    /// <param name="sender">The <see cref="StreamManager"/> instance that invoked the event</param>
    /// <param name="e">The object describing the status of the stream</param>
    private void HandleStreamUpdated(object sender, HydraStreamStatus e)
    {
      m_streamUpdateQueue.Enqueue(e);
    }

    /// <summary>
    /// Handler method for negotiations to the 'Mockup' destination
    /// </summary>
    /// <param name="uuid">The UUID of the application that sent the negotiation</param>
    /// <param name="e">The negotiation object received</param>
    private void HandleStreamRequest(HydraUuid uuid, StreamNegotiation e)
    {
      HydraCore hydra;
      string localKey;
      if (uuid.Equals(Server.Uuid))
      {
        hydra = Client;
        localKey = "Client";
      }
      else
      {
        hydra = Server;
        localKey = "Server";
      }

      if (!e.IsReply)
      {

        switch (e.Direction)
        {
          case HydraStreamDirections.WRITE:
          {
            if (e.Destination == localKey)
            {
              MemoryStream stream = new MemoryStream();
              if (hydra.StreamAuthority.AcceptNegotiation(e, uuid, stream, out uint id))
              {
                m_streams.TryAdd(id, stream);
              }
            }
          }
          break;

          case HydraStreamDirections.READ:
          {
            if (e.Source == localKey)
            {
              // Expect the tag to be an int indicating an ID for the requested data
              bool accepted = false;
              if (e.Tag != null && e.Tag is int)
              {
                int id = (int)e.Tag;
                if (m_streamRequests.TryGetValue(id, out MemoryStream stream))
                {
                  if (hydra.StreamAuthority.AcceptNegotiation(e, uuid, stream, out uint localId))
                  {
                    m_streams.TryAdd(localId, stream);
                    accepted = true;
                  }
                }
              }

              if (!accepted)
              {
                hydra.StreamAuthority.RejectNegotiation(e, uuid);
              }
            }
          }
          break;
        }
      }
      else
      {
        // Handle a reply by sticking it in the queue
        m_negotiationQueue.Enqueue(e);
      }
    }

    /// <summary>
    /// A demo module that implements handling for stream data.
    /// </summary>
    class DemoStreamModule : HydraModule
    {
      private Dictionary<uint, Stream> m_activeStreams = new Dictionary<uint, Stream>();
      public event EventHandler<Stream> StreamReceived;
      private const ushort SEND_STREAM_ID = 10001;
      private const ushort ACK_STREAM_ID = 10002;

      public DemoStreamModule(HydraCore hydra) : base(hydra, Tuple.Create(SEND_STREAM_ID, nameof(SEND_STREAM_ID)), Tuple.Create(ACK_STREAM_ID, nameof(ACK_STREAM_ID)))
      {
        // Empty
      }

      public void SendStream(MemoryStream stream, HydraUuid uuid)
      {
        // Reserve stream and kick of message
        if (Parent.StreamAuthority.ReserveStreamId(this, out uint id))
        {
          m_activeStreams.Add(id, stream);
          Parent.SendMessage(new HydraObjMsg(SEND_STREAM_ID, 0, id, false, false), uuid);
        }
      }

      public override void HandleMessage(Message msg, HydraUuid uuid)
      {
        switch (msg.Command)
        {
          case SEND_STREAM_ID:
          {
            // Set up a receiving stream
            if (Parent.StreamAuthority.ReserveStreamId(this, out uint id))
            {
              m_activeStreams.Add(id, new MemoryStream());
              HydraObjMsg objMsg = new HydraObjMsg(msg);
              uint remoteId = (uint)objMsg.GetObjectInfo().GetRaw(0);
              Parent.SendMessage(new HydraObjMsg(ACK_STREAM_ID, 0, new StreamDescription() { LocalId = id, RemoteId = remoteId }, false, false), uuid);
            }
            break;
          }

          case ACK_STREAM_ID:
          {
            HydraObjMsg objMsg = new HydraObjMsg(msg);
            StreamDescription description = objMsg.GetObject<StreamDescription>();
            if (m_activeStreams.TryGetValue(description.RemoteId, out Stream stream))
            {
              Parent.SendStream(stream, uuid, description.RemoteId, description.LocalId, stream.Length);
            }
            break;
          }
        }
      }

      public override void HandleStream(uint streamId, byte[] buffer, int offset, HydraUuid uuid, int count = -1)
      {
        if (m_activeStreams.TryGetValue(streamId, out Stream stream))
        {
          stream.Write(buffer, offset, count == -1 ? buffer.Length - offset : count);
        }
      }

      public override void EndStream(HydraStreamInfo streamInfo, HydraUuid uuid)
      {
        if (streamInfo.Direction == HydraStreamDirections.READ)
        {
          if (m_activeStreams.TryGetValue(streamInfo.Id, out Stream stream))
          {
            StreamReceived?.Invoke(this, stream);
            m_activeStreams.Remove(streamInfo.Id);
          }
        }
      }

      class StreamDescription
      {
        public uint LocalId;
        public uint RemoteId;
      }
    }
  }
}
