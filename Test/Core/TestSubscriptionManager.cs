#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Skulk.Hydra;
using Skulk.Hydra.Network;
using NUnit.Framework;

namespace Skulk.Test.Core
{
  /// <summary>
  /// Tests for the <see cref="SubscriptionManager"/> class
  /// </summary>
  [TestFixture]
  public class TestSubscriptionManager
  {
    private HydraNetwork m_network;

    [TearDown]
    public void TestTearDown()
    {
      if (m_network != null)
      {
        m_network.Stop();
        m_network.Join();
      }
    }

    [SetUp]
    public void SetUp()
    {
      m_network = new HydraNetwork();
      m_network.Init();
      m_network.Start();
    }

    /// <summary>
    /// Test adding and removing subscribers
    /// </summary>
    [Test]
    public void AddRemove()
    {
      SubscriptionManager subManager = new SubscriptionManager(m_network.Servers[0]);

      // Removing non-existent UUID should fail
      HydraUuid[] uuids = new HydraUuid[3];
      uuids[0] = new HydraUuid();
      Assert.IsFalse(subManager.Remove(uuids[0]));

      // Add a subscriber
      Assert.IsTrue(subManager.Add(uuids[0]));

      // Double add should fail
      Assert.IsFalse(subManager.Add(uuids[0]));

      HydraUuid[] subs = subManager.GetSubscribers();
      Assert.IsNotNull(subs);
      Assert.AreEqual(1, subs.Length);
      Assert.AreEqual(uuids[0], subs[0]);

      uuids[1] = new HydraUuid();
      Assert.IsTrue(subManager.Add(uuids[1]));
      uuids[2] = new HydraUuid();
      Assert.IsTrue(subManager.Add(uuids[2]));

      subs = subManager.GetSubscribers();
      Assert.IsNotNull(subs);
      Assert.AreEqual(uuids.Length, subs.Length);
      for (int i = 0; i < subs.Length; ++i)
      {
        Assert.AreEqual(uuids[i], subs[i]);
      }

      for (int i = 0; i < uuids.Length; ++i)
      {
        Assert.IsTrue(subManager.Remove(uuids[i]));
        Assert.IsFalse(subManager.Remove(uuids[i]));

        subs = subManager.GetSubscribers();
        Assert.IsNotNull(subs);
        Assert.AreEqual(uuids.Length - (i + 1), subs.Length);
        for (int j = 0; j < subs.Length; ++j)
        {
          Assert.AreEqual(uuids[i + j + 1], subs[j]);
        }
      }
    }

    /// <summary>
    /// Test sending messages to subscribers
    /// </summary>
    [Test]
    public void Send()
    {
      m_network.Stop();
      m_network.Join();
      m_network = new HydraNetwork();
      m_network.ClientCount = 3;
      m_network.Init();

      // Add simple modules to all clients and set as the default
      for (int i = 0; i < m_network.Clients.Length; ++i)
      {
        SimpleModule simple = new SimpleModule(m_network.Clients[i]);
        m_network.Clients[i].AddModule(simple);
        m_network.Clients[i].DefaultModule = simple;
      }

      m_network.Start();
      m_network.HandshakeClients(true, false);

      SubscriptionManager subManager = new SubscriptionManager(m_network.Servers[0]);
      for (int i = 0; i < m_network.ClientCount; ++i)
      {
        subManager.Add(m_network.Clients[i].Uuid);
      }

      Message msg = new Message(10000, 0);
      subManager.SendMessage(msg);

      for (int i = 0; i < m_network.Clients.Length; ++i)
      {
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_network.Clients[i].GetModule<SimpleModule>().Queue.Count == 1, 5));
      }
    }
  }
}
