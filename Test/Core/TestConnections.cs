#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Skulk.Hydra;
using Skulk.Hydralize;
using Skulk.Hydralize.Encoding;
using System.Threading;
using Skulk.Hydra.Network;
using System.Security.Cryptography;
using System.Net;
using System.Runtime;

namespace Skulk.Test
{
  public class TestConnections : HydraUnitTest
  {
    private int m_profilesAdded = 0;
    private int m_profilesRemoved = 0;
    private int m_udpConnectionsAdded = 0;
    private int m_clientUdpConnections = 0;
    private bool m_sendOnAdd = false;

    private int m_tcpConnectionsAdded = 0;
    private int m_clientTcpConnections = 0;

    private List<TinyEncryption> m_transcoders = new List<TinyEncryption>();

    private class TestMessage : Message
    {
      public TestMessage() : base(10000, 0)
      {
        // Empty
      }
    }

    private class TestStreamInfo
    {
      private UInt32 m_remoteId;
      private UInt32 m_id;
      private long m_length;
      private StreamTransferModes m_transferMode;

      public uint RemoteId { get => m_remoteId; set => m_remoteId = value; }
      public uint Id { get => m_id; set => m_id = value; }
      public long Length { get => m_length; set => m_length = value; }
      public StreamTransferModes TransferMode { get => m_transferMode; set => m_transferMode = value; }
    }

    private class TestStreamMsg : HydraObjMsg
    {
      public TestStreamMsg(UInt32 localId, long length, StreamTransferModes transferModes = StreamTransferModes.CONTINUOUS) : base(10001, 0, new TestStreamInfo() { Id = localId, Length = length, TransferMode = transferModes }, true)
      {
        // Empty
      }

      public TestStreamMsg(TestStreamInfo ack) : base(10001, 1, ack, true)
      {
        // Empty
      }

      public TestStreamMsg(Message other) : base(other)
      {
        // Empty
      }
    }

    private class LargeInfo
    {
      private int m_id;
      private byte[] m_data;

      public int Id { get => m_id; set => m_id = value; }
      public byte[] Data { get => m_data; set => m_data = value; }
    }

    private class LargeMsg : HydraObjMsg
    {
      public LargeMsg(LargeInfo info) : base(10002, 0, info, true)
      {
        // Empty
      }

      public LargeMsg(Message other) : base(other)
      {
        // Empty
      }
    }

    private class TesterModule : HydraModule
    {
      public int MessageCount = 0;
      private Dictionary<UInt32, Stream> m_streams = new Dictionary<uint, Stream>();
      private long m_receivedCount;

      public Dictionary<uint, Stream> Streams { get => m_streams; set => m_streams = value; }
      public long ReceivedCount { get => m_receivedCount; set => m_receivedCount = value; }
      public Queue<LargeMsg> LargeMsgQueue { get; set; } = new Queue<LargeMsg>();

      public TesterModule(HydraCore parent) : base(parent, 10000, 10001, 10002)
      {
        // Empty
      }

      public void SendTestMessage(HydraUuid uuid)
      {
        Parent.SendMessage(new TestMessage(), uuid);
      }

      public void SendStream(Stream sendStream, HydraUuid uuid, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
      {
        if (Parent.StreamAuthority.ReserveStreamId(this, out uint localId))
        {
          Parent.SendMessage(new TestStreamMsg(localId, sendStream.Length, transferMode), uuid);
          m_streams.Add(localId, sendStream);
        }
        else
        {
          Parent.Log?.LogError("Failed to reserve an ID for a new test stream");
        }
      }

      public override void HandleMessage(Message msg, HydraUuid uuid)
      {
        switch((UInt16)msg.GetCmd())
        {
          case 10000:
          {
            ++MessageCount;
          }
          break;

          case 10001:
          {
            TestStreamMsg objMsg = new TestStreamMsg(msg);
            TestStreamInfo remoteInfo = objMsg.GetObject<TestStreamInfo>();

            switch(objMsg.GetObjectId())
            {
              case 0:
              {
                // Sent to me
                if (Parent.StreamAuthority.ReserveStreamId(this, out uint id))
                {
                  // Create memory stream to receive
                  MemoryStream memStream = new MemoryStream((int)remoteInfo.Length);
                  m_streams.Add(id, memStream);
                  TestStreamInfo streamInfo = new TestStreamInfo();
                  streamInfo.Id = id;
                  streamInfo.RemoteId = remoteInfo.Id;
                  streamInfo.TransferMode = remoteInfo.TransferMode;
                  streamInfo.Length = remoteInfo.Length;
                  Parent.SendMessage(new TestStreamMsg(streamInfo), uuid);
                }
              }
              break;

              case 1:
              {
                // Response for me
                if (m_streams.TryGetValue(remoteInfo.RemoteId, out Stream stream))
                {
                  if (stream.Length == remoteInfo.Length)
                  {
                    if (!Parent.SendStream(stream, uuid, remoteInfo.RemoteId, remoteInfo.Id, stream.Length, remoteInfo.TransferMode))
                    {
                      Parent.Log.LogError("Failed to set up a test stream");
                    }
                  }
                  else
                  {
                    Parent.Log.LogError("Received test stream length does not match the length of the stream being sent, aborting...");
                    Parent.StreamAuthority.ReleaseStreamId(remoteInfo.RemoteId);
                  }
                }
                else
                {
                  Parent.Log.LogError("Unable to find a test stream with ID: {0}", remoteInfo.RemoteId);
                }
              }
              break;
            }
          }
          break;

          case 10002:
          {
            LargeMsg largeMsg = new LargeMsg(msg);
            this.LargeMsgQueue.Enqueue(largeMsg);
            break;
          }
        }
      }

      public override void HandleStream(UInt32 streamId, byte[] buffer,  int offset, HydraUuid uuid, int count = -1)
      {
        if (count == -1)
        {
          count = buffer.Length - offset;
        }

        if (m_streams.ContainsKey(streamId))
        {
          m_streams[streamId].Write(buffer, offset, count);
          m_receivedCount += count;
        }
        else
        {
          Parent.Log.LogError("No stream exists for received stream segment with ID: {0}", streamId);
        }
      }
    }

    public override void Configure()
    {
      base.Configure();
      KeepAlivePeriod = 4;
      ClientTimeoutPeriod = 5;
      UdpHandshake = false;
      TcpHandshake = false;

      // Tests named 'Encoded' will get message transcoders
      if (TestContext.CurrentContext.Test.Name.Contains("Encoded"))
      {
        byte[] key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        TinyEncryption tea = new TinyEncryption(key);
        m_transcoders.Add(tea);
        ServerMessageTranscoders = new TranscoderPair[] { new TranscoderPair(tea.GetEncryptor, tea.GetDecryptor) };
        ClientTranscoders = new TranscoderPair[ClientCount];
        for (int i = 0; i < ClientCount; ++i)
        {
          ClientTranscoders[i] = new TranscoderPair(tea.GetEncryptor, tea.GetDecryptor);
        }

        var aes = Aes.Create();
        aes.Key = key;
        byte[] iv = new byte[] { 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        aes.IV = iv;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        TranscoderPair streamTranscorder = new TranscoderPair(aes.CreateEncryptor, aes.CreateDecryptor);
        ServerStreamTranscoders = new TranscoderPair[] { streamTranscorder };
        ClientStreamTranscoders = new TranscoderPair[ClientCount];
        for (int i = 0; i < ClientCount; ++i)
        {
          ClientStreamTranscoders[i] = new TranscoderPair(aes.CreateEncryptor, aes.CreateDecryptor);
        }
      }
      else
      {
        ServerStreamTranscoders = null;
        ClientStreamTranscoders = null;
        ClientTranscoders = null;
        ServerMessageTranscoders = null;
      }

      if (TestContext.CurrentContext.Test.Name.Contains("Keepalive"))
      {
        ClientTimeoutPeriod = 10;
        KeepAlivePeriod = 3.3;
      }
      else
      {
        ClientTimeoutPeriod = 5;
        KeepAlivePeriod = 4;
      }
    }

    public override void ModuleSetup()
    {
      // No modules - core testing
      Server.ProfileManager.OnProfileAdded += OnProfileAdded;
      Server.ProfileManager.OnProfileRemoved += OnProfileRemoved;
      Server.ProfileManager.OnConnectionAdded += OnServerConnectionAdded;
      Client.ProfileManager.OnConnectionAdded += OnClientConnectionAdded;

      Server.AddModule(new TesterModule(Server));
      Client.AddModule(new TesterModule(Client));
    }

    public override void CustomTestCleanup()
    {
      m_profilesAdded = 0;
      m_profilesRemoved = 0;
      m_udpConnectionsAdded = 0;
      m_tcpConnectionsAdded = 0;
      m_clientUdpConnections = 0;
      m_clientTcpConnections = 0;
      m_sendOnAdd = false;
    }

    /// <summary>
    /// Tests that UDP connection added events are triggered correctly
    /// </summary>
    [Test]
    public void TestUdpAddedEvent()
    {
      Client.SendHandshake();

      // Check server side
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null, 3), "Timed out waiting for the client to connect");
      Assert.IsTrue(WaitForCondition(() => m_udpConnectionsAdded == 1, 3), "Timed out waiting for server UDP connection added event");

      // Check client side
      Assert.IsTrue(WaitForCondition(() => m_clientUdpConnections == 1, 3), "Timed out waiting for client UDP connection added event");
    }

    /// <summary>
    /// Tests that sending on a UDP connection added event works
    /// </summary>
    [Test]
    public void TestSendingOnUdpAddedEvent()
    {
      m_sendOnAdd = true;
      Client.SendHandshake();

      Assert.IsTrue(WaitForCondition(() => Client.GetModule<TesterModule>().MessageCount == 1, 3), "Timed out waiting for new message");
    }

    /// <summary>
    /// Tests the MaintainUdp property on a HydraClient
    /// </summary>
    [Test]
    public void TestMaintainUdp()
    {
      // Should cause UDP to be connected after a timeout period
      Client.MaintainUdp = true;
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Assert.IsNotNull(Client.ServerUuid);

      // Should stay connected...
      Thread.Sleep((int)(ClientTimeoutPeriod * 1100));
      Assert.IsNotNull(Client.GetProfile(Client.ServerUuid));
    }

    /// <summary>
    /// Tests sending a stream over a UDP connection
    /// </summary>
    [Test]
    public void TestUdpStream()
    {
      Assert.IsTrue(Client.WaitForHandshake(5), "Failed to handshake");

      byte[] buffer = new byte[32 * 1024];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = (byte)rand.Next(0, 256);
      }
      MemoryStream memStream = new MemoryStream(buffer);

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid);

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 0, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length, 10));

      // Check received values
      bool match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            if (buffer[i] != stream.ReadByte())
            {
              Assert.Fail();
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);
    }

    /// <summary>
    /// Tests sending a stream over a TCP connection
    /// </summary>
    [Test]
    public void TestTcpStream()
    {
      Assert.IsTrue(Client.StartTcpConnection());

      byte[] buffer = new byte[32 * 1024];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = (byte)rand.Next(0, 256);
      }
      MemoryStream memStream = new MemoryStream(buffer);

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid);

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 0, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length, 10));

      // Check received values
      bool match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            if (buffer[i] != stream.ReadByte())
            {
              Assert.Fail();
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);


      // Now send segmented
      memStream.Seek(0, SeekOrigin.Begin);
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid, StreamTransferModes.SEGMENTED);
      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 1, 5));
      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length * 2, 10));
      // Check received values
      match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            if (buffer[i] != stream.ReadByte())
            {
              Assert.Fail();
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);
    }

    /// <summary>
    /// Tests relatively small consecutive TCP streams. Ensures parsing of messages and switching to streams
    /// is handled correctly even when the received buffer contains a mix of stream and message data.
    /// </summary>
    [Test]
    public void TestShortTcpStreams()
    {
      Assert.IsTrue(Client.StartTcpConnection());
      Random rand = new Random(DateTime.UtcNow.Millisecond);

      byte[][] buffer = new byte[10][];
      MemoryStream[] streams = new MemoryStream[10];
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = new byte[256];
        rand.NextBytes(buffer[i]);
        streams[i] = new MemoryStream(buffer[i]);
      }

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));

      for (int i = 0; i < streams.Length; ++i)
      {
        Client.GetModule<TesterModule>().SendStream(streams[i], Client.ServerUuid);
      }

      // Wait for server to receive each stream
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count == streams.Length, 120));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length * buffer[0].Length, 10));

      // Check received values
      bool match = false;
      for (int i = 1; i < Server.GetModule<TesterModule>().Streams.Count + 1; ++i)
      {
        Stream receivedStream = Server.GetModule<TesterModule>().Streams[(uint)i];
        Assert.AreEqual(receivedStream.Length, buffer[i - 1].Length, "received stream length: {0} and buffer length: {1} for stream: {2} differ", receivedStream.Length, buffer[i - 1].Length, i);
        if (receivedStream.Length == buffer[i - 1].Length)
        {
          receivedStream.Seek(0, SeekOrigin.Begin);
          for (int j = 0; j < receivedStream.Length; ++j)
          {
            Assert.AreEqual(buffer[i - 1][j], receivedStream.ReadByte(), "byte in stream {0} differs at index {1}", i, j);
          }
          match = true;
        }
        else
        {
          Assert.Fail();
        }
      }

      Assert.IsTrue(match);
    }

    /// <summary>
    /// Tests sending a stream over an encoded UDP connection
    /// </summary>
    [Test]
    public void TestUdpEncodedStream()
    {
      Client.SendHandshake();

      byte[] buffer = new byte[32 * 1024];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = (byte)rand.Next(0, 256);
      }
      MemoryStream memStream = new MemoryStream(buffer);

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid);

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 0, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length, 10));

      // Check received values
      bool match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            if (buffer[i] != stream.ReadByte())
            {
              Assert.Fail();
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);
    }

    /// <summary>
    /// Tests sending a stream over an encoded TCP connection
    /// </summary>
    [Test]
    public void TestTcpEncodedStream()
    {
      Assert.IsTrue(Client.StartTcpConnection());

      byte[] buffer = new byte[32 * 1024];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = (byte)rand.Next(0, 256);
      }
      MemoryStream memStream = new MemoryStream(buffer);

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid);

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 0, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length, 10));

      // Check received values
      bool match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            int byteVal = stream.ReadByte();
            if (buffer[i] != byteVal)
            {
              Assert.Fail("buffer[{0}] = {1}, not read value: {2}", i, buffer[i], byteVal);
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);

      // Send segmented now
      memStream.Seek(0, SeekOrigin.Begin);
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid, StreamTransferModes.SEGMENTED);

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 1, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length * 2, 10));

      // Check received values
      match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            int byteVal = stream.ReadByte();
            if (buffer[i] != byteVal)
            {
              Assert.Fail("buffer[{0}] = {1}, not read value: {2}", i, buffer[i], byteVal);
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);
    }

    [Test]
    public void TestTcpAddedEvent()
    {
      Assert.IsTrue(Client.StartTcpConnection());

      // Check server side
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null, 3), "Timed out waiting for the client to connect");
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 1, 3), "Timed out waiting for server TCP connection added event");

      // Check client side
      Assert.IsTrue(WaitForCondition(() => m_clientTcpConnections == 1, 3), "Timed out waiting for client TCP connection added event, count is: {0}", m_clientTcpConnections);
    }

    [Test]
    public void TestHandshakeSerialization()
    {
      HandshakeInfo info = new HandshakeInfo(new HydraUuid(), "test", HydraModes.SERVER, new HydraAddress("localhost", 52505, 52600), 52, 0);
      byte[] raw = Hydralizer.Hydralize(info, true, false);
      int offset = 0;
      HandshakeInfo result = new HandshakeInfo();
      Hydralizer.Dehydralize(result, raw, ref offset);
      Assert.AreEqual(info.Port, result.Port);
      Assert.AreEqual(info.Uuid, result.Uuid);
      Assert.AreEqual(info.RemoteKey, result.RemoteKey);
      Assert.AreEqual(info.LocalKey, result.LocalKey);
      Assert.AreEqual(info.Address, result.Address);
    }

    [Test]
    public void TestThrottledEncodedTcpConnection()
    {
      TestThrottledTcpConnection();
    }

    [Test]
    public void TestThrottledTcpConnection()
    {
      // Test basic operation
      Server.CreatingTcpConnection += CreateThrottledConnection;
      Client.CreatingTcpConnection += CreateThrottledConnection;

      Assert.IsTrue(Client.StartTcpConnection());
      // Actual send rate should be around 16KB/4 = 4KBs = 32Kbps

      byte[] buffer = new byte[4096 * 10];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < buffer.Length; ++i)
      {
        buffer[i] = (byte)rand.Next(0, 256);
      }
      MemoryStream memStream = new MemoryStream(buffer);
      var sw = new System.Diagnostics.Stopwatch();

      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null));
      Client.GetModule<TesterModule>().SendStream(memStream, Client.ServerUuid);
      sw.Start();

      // Wait for server to get the stream started
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().Streams.Count > 0, 5));

      // Wait for all expected bytes
      Assert.IsTrue(WaitForCondition(() => Server.GetModule<TesterModule>().ReceivedCount == buffer.Length, 10));

      sw.Stop();

      // Sends 16K sleeps 4 seconds, 16 K sleeps 4 seconds then the last 8K or so right after that. With about 500 milliseconds
      // overhead that leaves us with expecting to receive all the data on the server around 8500ms. The timing is pretty fragile though.
      Assert.IsTrue(Math.Abs(8500 - sw.ElapsedMilliseconds) < 500, "Unexpected transmit time: {0}ms", sw.ElapsedMilliseconds);

      // Check received values, just to make sure there are no problems with the connection data
      bool match = false;
      foreach (var stream in Server.GetModule<TesterModule>().Streams.Values)
      {
        if (stream.Length == buffer.Length)
        {
          stream.Seek(0, SeekOrigin.Begin);
          for (int i = 0; i < stream.Length; ++i)
          {
            if (buffer[i] != stream.ReadByte())
            {
              Assert.Fail();
            }
          }
          match = true;
        }
      }

      Assert.IsTrue(match);
    }

    /// <summary>
    /// Test that a UDP connection will maintain itself sending keepalives
    /// </summary>
    [Test]
    public void UdpKeepalive()
    {
      Assert.IsTrue(Client.WaitForHandshake(5));
      Assert.IsTrue(Client.GetProfile(Server.Uuid) != null);
      Assert.IsFalse(WaitForCondition(() => Client.GetProfile(Server.Uuid) == null, ClientTimeoutPeriod * 1.5));
    }

    /// <summary>
    /// Test that a TCP connection will maintain itself sending keepalives
    /// </summary>
    [Test]
    public void TcpKeepalive()
    {
      Assert.IsTrue(Client.StartTcpConnection(5000));
      Assert.IsTrue(Client.GetProfile(Server.Uuid) != null);

      // Check some values
      TcpConnection tcp = Client.GetProfile(Server.Uuid).GetConnection(ConnectionTypes.STREAM) as TcpConnection;
      Assert.IsNotNull(tcp);
      Assert.IsTrue(tcp.CanRead);
      Assert.IsTrue(tcp.CanWrite);
      Assert.IsTrue(tcp.CanStream);
      Assert.AreNotEqual(0, tcp.LocalPort);
      Assert.AreEqual(4096, tcp.MaxSendsPerLoop);
      tcp.MaxSendsPerLoop = 2048;
      Assert.AreEqual(2048, tcp.MaxSendsPerLoop);
      tcp.MaxSendsPerLoop = 4096;
      Assert.AreEqual(4096, tcp.MaxSendsPerLoop);

      Assert.IsFalse(WaitForCondition(() => Client.GetProfile(Server.Uuid) == null, ClientTimeoutPeriod * 1.5));
    }

    /// <summary>
    /// Test sending and receiving extended messages over a TCP connection.
    /// </summary>
    [Test]
    public void TcpExtendedMessages()
    {
      Assert.IsTrue(Client.StartTcpConnection(5000));
      Assert.IsTrue(Client.GetProfile(Server.Uuid) != null);

      Random rand = new Random();
      for (int i = 0; i < 3; ++i)
      {
        LargeInfo large = new LargeInfo()
        {
          Id = 10,
          Data = new byte[500000]
        };
        rand.NextBytes(large.Data);

        LargeMsg largeMsg = new LargeMsg(large);
        Server.SendMessage(largeMsg, Client.Uuid);

        TesterModule clientTester = Client.GetModule<TesterModule>();
        Assert.IsTrue(WaitForCondition(() => clientTester.LargeMsgQueue.Count > 0, 3));
        Assert.IsTrue(clientTester.LargeMsgQueue.TryDequeue(out LargeMsg recvLargeMsg));
        LargeInfo recvLarge = recvLargeMsg.GetObject<LargeInfo>();
        Assert.IsNotNull(recvLarge);
        Assert.AreEqual(large.Id, recvLarge.Id);
        Assert.AreEqual(large.Data, recvLarge.Data);

        // The specific number doesn't really matter, just that it's incrementing but this test is pretty deterministic
        // that we'll be the next messages
        Assert.AreEqual(i + 1, recvLargeMsg.GetId());
      }
    }

    /// <summary>
    /// Test sending and receiving extended messages over a UDP connection.
    /// </summary>
    [Test]
    public void UdpExtendedMessages()
    {
      // We have to throttle otherwise we'd just drop most of the large message
      Server.CreatingUdpConnection += CreateThrottledUdpConnection;

      Assert.IsTrue(Client.WaitForHandshake(5));
      Assert.IsTrue(Client.GetProfile(Server.Uuid) != null);

      Random rand = new Random();
      for (int i = 0; i < 3; ++i)
      {
        LargeInfo large = new LargeInfo()
        {
          Id = 10,
          Data = new byte[500000]
        };
        rand.NextBytes(large.Data);

        LargeMsg largeMsg = new LargeMsg(large);
        Server.SendMessage(largeMsg, Client.Uuid);

        TesterModule clientTester = Client.GetModule<TesterModule>();

        // Should take about 5s to transfer because we're throttling to prevent dropping data
        Assert.IsFalse(WaitForCondition(() => clientTester.LargeMsgQueue.Count > 0, 3));
        Assert.IsTrue(WaitForCondition(() => clientTester.LargeMsgQueue.Count > 0, 5));
        Assert.IsTrue(clientTester.LargeMsgQueue.TryDequeue(out LargeMsg recvLargeMsg));
        LargeInfo recvLarge = recvLargeMsg.GetObject<LargeInfo>();
        Assert.IsNotNull(recvLarge);
        Assert.AreEqual(large.Id, recvLarge.Id);
        Assert.AreEqual(large.Data, recvLarge.Data);

        // Only really important thing is that this increases with each message, but it should be pretty deterministic since we start trying to send
        // and keepalives are weaved in between each of our messages due to the throttling time. But this can be changed to only check for increasing
        // if needed
        Assert.AreEqual(i * 2, recvLargeMsg.GetId());
      }
    }

    /// <summary>
    /// Creates a standard UDP connection but sets it throttled to 1Mbps.
    /// </summary>
    /// <param name="server">The server creating the connection.</param>
    /// <param name="info">The handshake information for the connection.</param>
    /// <param name="ep">The remote endpoint to connect with.</param>
    /// <returns>The newly created throttled UDP connection.</returns>
    private UdpConnection CreateThrottledUdpConnection(HydraServer server, HandshakeInfo info, System.Net.IPEndPoint ep)
    {
      UdpConnection connection = new UdpConnection(server.Settings.UdpBindAddress, ep, 0, 0, 1, -1, server.Settings.MessageEncoder, server.Settings.KeepAlivePeriod, 1000000);
      connection.StreamTimeout = server.Settings.StreamTimeout;
      return connection;
    }

    private TcpConnection CreateThrottledConnection(HydraServer server, System.Net.Sockets.Socket socket)
    {
      return new ThrottledTcpConnection(socket, 0, -1, 1, server.StreamEncoder, server.Settings.KeepAlivePeriod, 1000);
    }

    private TcpConnection CreateThrottledConnection(HydraClient client, System.Net.Sockets.Socket socket)
    {
      return new ThrottledTcpConnection(socket, 0, -1, 1, client.StreamEncoder, client.Settings.KeepAlivePeriod, 1000);
    }

    private void OnProfileAdded(Profile profile)
    {
      ++m_profilesAdded;
    }

    private void OnProfileRemoved(Profile profile)
    {
      ++m_profilesRemoved;
    }

    private void OnServerConnectionAdded(Connection connection)
    {
      if (connection.ConnectionType == ConnectionTypes.MESSAGE)
      {
        ++m_udpConnectionsAdded;
      }
      else if (connection.ConnectionType == ConnectionTypes.STREAM)
      {
        ++m_tcpConnectionsAdded;
      }

      if (m_sendOnAdd)
      {
        Server.SendMessage(new TestMessage(), connection.Profile.Uuid);
      }
    }

    private void OnClientConnectionAdded(Connection connection)
    {
      if (connection.ConnectionType == ConnectionTypes.MESSAGE)
      {
        ++m_clientUdpConnections;
      }
      else if (connection.ConnectionType == ConnectionTypes.STREAM)
      {
        ++m_clientTcpConnections;
      }
    }
  }
}
