#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Network;

namespace Skulk.Test
{
  public class TestHydraRouter
  {
    private class MockHydra : HydraCore
    {
      public MockHydra()
        : base(
            new HydraSettings()
            {
              ApplicationName = "Mock Hydra",
              UdpPort = 0,
              LogLevel = MEL.LogLevels.ALL,
              LogFile = "mockHydra.log",
              EnableConsoleLog = true,
              UseRouting = true
            })
      {
        // Empty
      }

      public HydraProfileManager GetProfileManager()
      {
        return m_profileManager;
      }

      public HydraRouter GetRouter()
      {
        return m_profileManager.Router;
      }
    }

    public TestHydraRouter()
    {
      // Empty
    }

    /// <summary>
    /// Tests some basic method calls on a <see cref="HydraRouter"/>
    /// </summary>
    [Test]
    public void TestAddingRoute()
    {
      MockHydra hydra = new MockHydra();
      hydra.Start();

      HydraRouter router = hydra.GetRouter();
      Assert.IsNotNull(router);
      HydraUuid destination = new HydraUuid();
      HydraRouteMapNode mapNode = new HydraRouteMapNode()
      {
        Uuid = hydra.Uuid,
        Profiles = new List<HydraUuid>(new HydraUuid[] { destination })
      };
      router.SetMapNode(mapNode);
      mapNode = router.RouteMap.GetNode(hydra.Uuid);
      Assert.IsNotNull(mapNode);
      Assert.AreEqual(hydra.Uuid, mapNode.Uuid);

      HydraUuid[] uuids = mapNode.Profiles.ToArray();
      Assert.IsNotNull(uuids);
      Assert.AreEqual(1, uuids.Length);
      Assert.AreEqual(destination, uuids[0]);

      HydraUuid route = router.RouteMap.SearchRoute(hydra.Uuid, destination);
      Assert.IsNotNull(route);
      Assert.AreEqual(destination, route);

      hydra.Stop();
      hydra.Join();
    }

    [Test]
    public void TestRouteDiscovery()
    {
      HydraNodeSettings nodeSettings = new HydraNodeSettings()
      {
        ApplicationName = "Node 1",
        UdpPort = 52505,
        TcpPort = 52610,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraNode1.log",
        UseRouting = true
      };
      HydraNode node1 = new HydraNode(nodeSettings);

      nodeSettings = new HydraNodeSettings(nodeSettings)
      {
        ApplicationName = "Node 2",
        UdpPort = 52506,
        TcpPort = 52611,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraNode2.log"
      };
      HydraNode node2 = new HydraNode(nodeSettings);

      nodeSettings = new HydraNodeSettings(nodeSettings)
      {
        ApplicationName = "Node 3",
        UdpPort = 52507,
        TcpPort = 52612,
        LogFile = "hydraNode3.log"
      };
      HydraNode node3 = new HydraNode(nodeSettings);

      try
      {
        node1.Start();
        node2.Start();
        node3.Start();

        node1.SendHandshake(node2.Address.GetUdpEndPoint());
        node2.SendHandshake(node3.Address.GetUdpEndPoint());

        HydraProfileManager profileManager =
          typeof(HydraNode).GetField("m_profileManager",
          System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.FlattenHierarchy).GetValue(node3)
          as HydraProfileManager;
        Assert.IsNotNull(profileManager);

        // Let network settle
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => profileManager.Router.RouteMap.Nodes.Count == 3, 20));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => profileManager.Router.RouteMap.GetMissingNodes().Count == 0, 5));

        HydraRouteMapNode mapNode2 = profileManager.Router.RouteMap.GetNode(node2.Uuid);
        Assert.IsNotNull(mapNode2);
        Assert.AreEqual(2, mapNode2.Profiles.Count);
        Assert.AreEqual(3, profileManager.Router.RouteMap.Nodes.Count);
        Assert.IsNotNull(profileManager.Router.RouteMap.GetNode(node1.Uuid));
        Assert.IsNotNull(profileManager.Router.RouteMap.GetNode(node2.Uuid));
        Assert.IsNotNull(profileManager.Router.RouteMap.GetNode(node3.Uuid));
      }
      finally
      {
        Thread.Sleep(500);
        node1?.Stop();
        node2?.Stop();
        node3?.Stop();
        node1?.Join();
        node2?.Join();
        node3?.Join();
      }
    }
  }
}
