#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Messages;
using Skulk.Hydra.Network;

namespace Skulk.Test
{
  public class TestMeshNetworking : HydraNodeUnitTest
  {
    private int m_latencyCheckCount = 0;

    public TestMeshNetworking() : base(5)
    {
      // Empty
    }

    protected override void Configure()
    {
      base.Configure();

      // Don't automatically handshake with all nodes
      TcpStart = false;
      UdpHandshake = false;
      AddAllStaticNodes = true;
      KeepAlivePeriod = 1;

      m_latencyCheckCount = 0;
    }

    [Test]
    public void TestLinearRouting()
    {
      // Allow for 25ms of delay on processing, sending and receiving along 5 nodes. In other words, we allow up to 5ms of delay per node.
      double allowedLatency = 0.025;

      for (int i = 0; i < Nodes.Length - 1; ++i)
      {
        Assert.IsTrue(Nodes[i].StartTcpConnection(Nodes[i + 1].Address));
      }

      for (int i = 0; i < Nodes.Length - 1; ++i)
      {
        // Make sure the Nagle algorithm is off it can introduce 40ms of delay for small packets on each connection
        // Turn it off by setting NoDelay to true so that we're actually testing the latency of the library itself
        (Nodes[i].GetProfile(Nodes[i + 1].Uuid).GetConnection(ConnectionTypes.STREAM) as TcpConnection).Socket.NoDelay = true;
        (Nodes[i + 1].GetProfile(Nodes[i].Uuid).GetConnection(ConnectionTypes.STREAM) as TcpConnection).Socket.NoDelay = true;
      }

      HydraRouteMap map = Nodes[0].GetNodeMap();
      UInt32 time = 0;
      while (!map.ContainsMapNode(Nodes[Nodes.Length - 1].Uuid) && time < 15000)
      {
        Thread.Sleep(100);
        time += 100;
      }
      Assert.IsTrue(map.ContainsMapNode(Nodes[Nodes.Length - 1].Uuid), "Node: {0} missing from Node: {1} routing map.", Nodes[Nodes.Length - 1].Uuid, Nodes[0].Uuid);

      HydraUuid farUuid = Nodes[Nodes.Length - 1].Uuid;

      // Attempt to form virtual connection between furthest nodes
      Nodes[0].SendHandshake(farUuid);

      // Give time for handshake process to occur
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => Nodes[0].GetProfile(farUuid) != null, 3));

      // Check the latency
      Connection connection = Nodes[0].GetProfile(farUuid).GetConnection(ConnectionTypes.STREAM);
      Nodes[0].GetProfile(farUuid).OnLatencyUpdated += HandleLatencyUpdated;
      Nodes[0].GetProfile(farUuid).StartLatencyCheck(connection.Id);

      // Wait for latency
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_latencyCheckCount == 1, 5));
      Console.WriteLine("Virtual connection latency: {0}", connection.Latency);
      Assert.IsTrue(connection.Latency <= allowedLatency, "Latency was higher than expected: {0}", connection.Latency);

      // Now just sit and watch the network maintain itself for a bit
      Thread.Sleep((int)(1000 * ClientTimeoutPeriod));

      // Check the latency again
      Nodes[0].GetProfile(farUuid).StartLatencyCheck(connection.Id);

      // Wait for latency
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_latencyCheckCount == 2, 5));
      Console.WriteLine("Virtual connection latency: {0}", connection.Latency);
      Assert.IsTrue(connection.Latency <= allowedLatency, "Latency was higher than expected: {0}", connection.Latency);

      // Check the latency again
      Nodes[0].GetProfile(farUuid).StartLatencyCheck(connection.Id);

      // Wait for latency
      Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_latencyCheckCount == 3, 5));
      Console.WriteLine("Virtual connection latency: {0}", connection.Latency);
      Assert.IsTrue(connection.Latency <= allowedLatency, "Latency was higher than expected: {0}", connection.Latency);
    }

    private void HandleLatencyUpdated(Connection connection)
    {
      ++m_latencyCheckCount;
    }
  }
}
