#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Skulk.Hydra;
using Skulk.Hydralize.Encoding;
using NUnit.Framework;

namespace Skulk.Test.Core
{
  /// <summary>
  /// A utility class for testing that manages a network of Hydra servers and clients
  /// </summary>
  public class HydraNetwork
  {
    private int m_serverCount = 1;
    private HydraServerSettings m_serverTemplateSettings;
    private TranscoderPair m_serverMessageTranscoder;
    private TranscoderPair m_serverStreamTranscoder;
    private HydraServer[] m_servers;

    private int m_clientCount = 1;
    private HydraClientSettings m_clientTemplateSettings;
    private TranscoderPair m_clientMessageTranscoder;
    private TranscoderPair m_clientStreamTranscoder;
    private HydraClient[] m_clients;

    private double m_keepAlivePeriod = 5;
    private double m_profileTimeout = 15;
    private bool m_useRouting = false;

    public int ServerCount { get => m_serverCount; set => m_serverCount = value; }
    public HydraServerSettings ServerTemplateSettings { get => m_serverTemplateSettings; set => m_serverTemplateSettings = value; }
    public TranscoderPair ServerMessageTranscoder { get => m_serverMessageTranscoder; set => m_serverMessageTranscoder = value; }
    public TranscoderPair ServerStreamTranscoder { get => m_serverStreamTranscoder; set => m_serverStreamTranscoder = value; }
    public HydraServer[] Servers { get => m_servers; set => m_servers = value; }
    public int ClientCount { get => m_clientCount; set => m_clientCount = value; }
    public HydraClientSettings ClientTemplateSettings { get => m_clientTemplateSettings; set => m_clientTemplateSettings = value; }
    public TranscoderPair ClientMessageTranscoder { get => m_clientMessageTranscoder; set => m_clientMessageTranscoder = value; }
    public TranscoderPair ClientStreamTranscoder { get => m_clientStreamTranscoder; set => m_clientStreamTranscoder = value; }
    public HydraClient[] Clients { get => m_clients; set => m_clients = value; }
    public double KeepAlivePeriod { get => m_keepAlivePeriod; set => m_keepAlivePeriod = value; }
    public double ProfileTimeout { get => m_profileTimeout; set => m_profileTimeout = value; }
    public bool UseRouting { get => m_useRouting; set => m_useRouting = value; }

    public void Init()
    {
      if (m_servers == null)
      {
        m_serverTemplateSettings ??= new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = m_clientCount + m_serverCount,
          UdpPort = 52500,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = m_keepAlivePeriod,
          ProfileTimeout = m_profileTimeout,
          UseRouting = m_useRouting
        };

        m_servers = new MockHydraServer[m_serverCount];
        for (int i = 0; i < m_serverCount; ++i)
        {
          HydraServerSettings serverSettings = new HydraServerSettings(m_serverTemplateSettings);
          serverSettings.ApplicationName += i.ToString();
          serverSettings.MessageEncoder = m_serverMessageTranscoder?.Clone();
          serverSettings.StreamEncoder = m_serverStreamTranscoder?.Clone();
          serverSettings.TcpPort += i;
          serverSettings.UdpPort += i;
          m_servers[i] = new MockHydraServer(serverSettings);
          m_servers[i].Log.LogInfo("HydraTestServer[{0}] UUID: {1}", i, m_servers[i].Uuid);
        }
      }
      else
      {
        m_serverCount = m_servers.Length;
      }

      if (m_clients == null)
      {
        m_clientTemplateSettings ??= new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52500,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = m_keepAlivePeriod,
          ProfileTimeout = m_profileTimeout,
          UseRouting = m_useRouting,
          MaxClients = 2
        };

        m_clients = new MockHydraClient[m_clientCount];
        for (int i = 0; i < m_clientCount; ++i)
        {
          HydraClientSettings clientSettings = new HydraClientSettings(m_clientTemplateSettings);
          clientSettings.ApplicationName += i.ToString();
          clientSettings.MessageEncoder = m_clientMessageTranscoder?.Clone();
          clientSettings.StreamEncoder = m_clientStreamTranscoder?.Clone();
          m_clients[i] = new MockHydraClient(clientSettings);
          m_clients[i].Log.LogInfo("HydraClient {0} UUID: {1}", i, m_clients[i].Uuid);
        }
      }
      else
      {
        m_clientCount = m_clients.Length;
      }
    }

    public void Start()
    {
      for (int i = 0; i < m_serverCount; ++i)
      {
        m_servers[i].Start();
      }

      for (int i = 0; i < m_clientCount; ++i)
      {
        m_clients[i].Start();
      }
    }

    public void Stop()
    {
      for (int i = 0; i < m_serverCount; ++i)
      {
        m_servers[i].Stop();
      }

      for (int i = 0; i < m_clientCount; ++i)
      {
        m_clients[i].Stop();
      }
    }

    public void Join()
    {
      for (int i = 0; i < m_serverCount; ++i)
      {
        m_servers[i].Join();
      }

      for (int i = 0; i < m_clientCount; ++i)
      {
        m_clients[i].Join();
      }
    }

    public void HandshakeClients(bool udp, bool tcp)
    {
      for (int i = 0; i < m_clientCount; ++i)
      {
        if (udp)
        {
          Assert.IsTrue(m_clients[i].WaitForHandshake(5));
        }

        if (tcp)
        {
          Assert.IsTrue(m_clients[i].WaitForTcpConnection(5));
        }
      }
    }
  }
}
