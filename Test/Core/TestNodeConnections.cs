#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.AutoDiscovery.Data;
using Skulk.Hydra.Network;

namespace Skulk.Test
{
  public class TestNodeConnections : HydraNodeUnitTest
  {
    public TestNodeConnections() : base(5)
    {
      // Empty
    }

    protected override void Configure()
    {
      base.Configure();

      // Don't automatically handshake with all nodes
      TcpStart = false;
      UdpHandshake = false;
    }

    /// <summary>
    /// Tests the process of discovering and connecting to other nodes
    /// </summary>
    [Test]
    public void TestDiscovery()
    {
      // AutoDiscoveryModule sends pings on startup but later nodes may not be up and running when earlier nodes start by pinging
      // check that all are running first
      int time = 0;
      for (int i = 0; i < Nodes.Length; ++i)
      {
        time = 0;
        while (!Nodes[i].Running && time < 1000)
        {
          Thread.Sleep(100);
          time += 100;
        }
      }

      List<AutoDiscoveryModule> autoDiscoveryModules = new List<AutoDiscoveryModule>();
      // Now ensure pings are sent out (again) when all nodes are running
      for (int i = 0; i < Nodes.Length; ++i)
      {
        autoDiscoveryModules.Add(Nodes[i].GetModule<AutoDiscoveryModule>());
        autoDiscoveryModules[autoDiscoveryModules.Count - 1].Ping();
      }

      for (int i = 0; i < autoDiscoveryModules.Count; ++i)
      {
        // Wait for all pings and pongs to come through
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => autoDiscoveryModules[i].GetServers().Count == Nodes.Length, 5));
        List<HydraAddressData> addresses = autoDiscoveryModules[i].GetServers(true);
        for (int addrIdx = 0; addrIdx < addresses.Count; ++addrIdx)
        {
          Console.WriteLine("Server address {0}: {1}", addrIdx, addresses[addrIdx].ToString());
        }

        // All nodes should know of all other nodes
        for (int nodeIndex = 0; nodeIndex < Nodes.Length; ++nodeIndex)
        {
          Assert.IsTrue(addresses.Contains(new HydraAddressData(Nodes[nodeIndex])), "Addresses for node: {0} does not contain node {1} address: {2}", i, nodeIndex, Nodes[nodeIndex].Address);
        }
      }
    }
  }
}
