#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra;
using System;
using NUnit.Framework;
using System.Threading;
using Skulk.Hydralize.Encoding;
using Skulk.Hydra.Network;
using System.Net.Sockets;
using System.Net;

namespace Skulk.Test
{
  public class MockHydraServer : HydraServer
  {
    public event Func<HydraServer, Socket, TcpConnection> CreatingTcpConnection;
    public event Func<HydraServer, HandshakeInfo, IPEndPoint, UdpConnection> CreatingUdpConnection;

    public MockHydraServer(HydraServerSettings settings) : base(settings)
    {
      // Empty
    }

    protected override TcpConnection CreateTcpConnection(Socket socket)
    {
      if (CreatingTcpConnection != null)
      {
        return CreatingTcpConnection(this, socket);
      }
      else
      {
        return base.CreateTcpConnection(socket);
      }
    }

    protected override UdpConnection CreateUdpConnection(HandshakeInfo handshake, IPEndPoint endPoint = null)
    {
      if (CreatingUdpConnection != null)
      {
        return CreatingUdpConnection(this, handshake, endPoint);
      }
      else
      {
        return base.CreateUdpConnection(handshake, endPoint);
      }
    }
  }

  public class MockHydraClient : HydraClient
  {
    public event Func<HydraClient, Socket, TcpConnection> CreatingTcpConnection;

    public MockHydraClient(HydraClientSettings settings) : base(settings)
    {
      // Empty
    }

    protected override TcpConnection CreateTcpConnection(Socket socket)
    {
      if (CreatingTcpConnection != null)
      {
        return CreatingTcpConnection(this, socket);
      }
      else
      {
        return base.CreateTcpConnection(socket);
      }
    }
  }

  public class HydraUnitTest
  {
    private MockHydraServer m_server;
    private MockHydraServer[] m_servers;
    private MockHydraClient m_client;
    private MockHydraClient[] m_clients;
    private int m_serverCount = 1;
    private int m_clientCount = 1;
    private double m_keepAlivePeriod = 10;
    private double m_clientTimeoutPeriod = 30;
    private bool m_udpHandshake = true;
    private bool m_tcpHandshake = true;
    private bool m_useRouting = false;
    private TranscoderPair[] m_serverMessageTranscoders;
    private TranscoderPair[] m_serverStreamTranscoders;
    private TranscoderPair[] m_clientMessageTranscoders;
    private TranscoderPair[] m_clientStreamTranscoders;

    /// <summary>
    /// The Hydra server to use for testing
    /// </summary>
    public MockHydraServer Server { get => m_server; set => m_server = value; }

    /// <summary>
    /// The array of Hydra servers
    /// </summary>
    public MockHydraServer[] Servers { get => m_servers; set => m_servers = value; }

    /// <summary>
    /// The primary client to use in testing
    /// </summary>
    public MockHydraClient Client { get => m_client; set => m_client = value; }

    /// <summary>
    /// The array of Hydra clients
    /// </summary>
    public MockHydraClient[] Clients { get => m_clients; set => m_clients = value; }

    /// <summary>
    /// The number of servers to set up
    /// </summary>
    public int ServerCount { get => m_serverCount; set => m_serverCount = value; }

    /// <summary>
    /// The number of Hydra clients
    /// </summary>
    public int ClientCount { get => m_clientCount; set => m_clientCount = value; }

    /// <summary>
    /// The keep alive period in seconds for the Hydra server
    /// </summary>
    public double KeepAlivePeriod { get => m_keepAlivePeriod; set => m_keepAlivePeriod = value; }

    /// <summary>
    /// The client timeout period in seconds for the Hydra server
    /// </summary>
    public double ClientTimeoutPeriod { get => m_clientTimeoutPeriod; set => m_clientTimeoutPeriod = value; }

    /// <summary>
    /// Indicates if clients should send a UDP handshake on test startup
    /// </summary>
    public bool UdpHandshake { get => m_udpHandshake; set => m_udpHandshake = value; }

    /// <summary>
    /// Indicates if clients should send start a TCP connection and handshake on test startup
    /// </summary>
    public bool TcpHandshake { get => m_tcpHandshake; set => m_tcpHandshake = value; }

    /// <summary>
    /// Indicates if applications should support message routing
    /// </summary>
    public bool UseRouting { get => m_useRouting; set => m_useRouting = value; }

    /// <summary>
    /// The transcoder pair to use on the server for messages
    /// </summary>
    public TranscoderPair[] ServerMessageTranscoders { get => m_serverMessageTranscoders; set => m_serverMessageTranscoders = value; }
    public TranscoderPair[] ClientTranscoders { get => m_clientMessageTranscoders; set => m_clientMessageTranscoders = value; }
    public TranscoderPair[] ServerStreamTranscoders { get => m_serverStreamTranscoders; set => m_serverStreamTranscoders = value; }
    public TranscoderPair[] ClientStreamTranscoders { get => m_clientStreamTranscoders; set => m_clientStreamTranscoders = value; }

    /// <summary>
    /// Initializes a new HydraUnitTest with the specified number of clients
    /// </summary>
    /// <param name="clientCount">The number of clients to initialize</param>
    public HydraUnitTest(int clientCount = 1)
    {
      m_clientCount = clientCount;
    }

    [SetUp]
    public void TestSetup()
    {
      Configure();
      HydraServerSettings templateServerSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        MaxClients = m_clientCount + m_serverCount,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = m_keepAlivePeriod,
        ProfileTimeout = m_clientTimeoutPeriod,
        UseRouting = m_useRouting,
      };

      m_servers = new MockHydraServer[m_serverCount];
      for (int i = 0; i < m_serverCount; ++i)
      {
        HydraServerSettings serverSettings = new HydraServerSettings(templateServerSettings);
        serverSettings.ApplicationName += i.ToString();
        serverSettings.MessageEncoder = m_serverMessageTranscoders?[i];
        serverSettings.StreamEncoder = m_serverStreamTranscoders?[i];
        serverSettings.TcpPort += i;
        serverSettings.UdpPort += i;
        m_servers[i] = new MockHydraServer(serverSettings);
        m_servers[i].Log.LogInfo("HydraTestServer[{0}] UUID: {1}", i, m_servers[i].Uuid);
      }

      if (m_servers.Length > 0)
      {
        m_server = m_servers[0];
      }

      m_clients = new MockHydraClient[m_clientCount];
      HydraClientSettings templateClientSettings = new HydraClientSettings()
      {
        ApplicationName = "HydraTestClient",
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraClient.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = m_keepAlivePeriod,
        ProfileTimeout = m_clientTimeoutPeriod,
        UseRouting = m_useRouting,
        MaxClients = 2
      };

      for (int i = 0; i < m_clientCount; ++i)
      {
        HydraClientSettings clientSettings = new HydraClientSettings(templateClientSettings);
        clientSettings.ApplicationName += i.ToString();
        clientSettings.MessageEncoder = m_clientMessageTranscoders != null ? m_clientMessageTranscoders[i] : null;
        clientSettings.StreamEncoder = m_clientStreamTranscoders != null ? m_clientStreamTranscoders[i] : null;
        clientSettings.UdpPort = 52500 + i;
        m_clients[i] = new MockHydraClient(clientSettings);
        m_clients[i].Log.LogInfo("HydraClient {0} UUID: {1}", i, m_clients[i].Uuid);
      }

      if (m_clients.Length > 0)
      {
        m_client = m_clients[0];
      }
      ModuleSetup();

      for (int i = 0; i < m_serverCount; ++i)
      {
        m_servers[i].Start();
      }

      for (int i = 0; i < m_clientCount; ++i)
      {
        m_clients[i].Start();
        if (m_udpHandshake)
        {
          Assert.IsTrue(m_clients[i].WaitForHandshake(5));
        }
        if (m_tcpHandshake)
        {
          Assert.IsTrue(m_clients[i].StartTcpConnection(5000));
        }
      }

      CustomTestSetup();
    }

    [TearDown]
    public void TestCleanup()
    {
      if (m_servers != null)
      {
        for (int i = 0; i < m_serverCount; ++i)
        {
          if (m_servers[i] != null)
          {
            m_servers[i].Stop();
            m_servers[i].Join();
          }
        }
      }

      if (m_clients != null)
      {
        for (int i = 0; i < m_clientCount; ++i)
        {
          if (m_clients[i] != null)
          {
            m_clients[i].MaintainUdp = false;
            m_clients[i].MaintainStream(false);
            m_clients[i].Stop();
          }
        }

        for (int i = 0; i < m_clientCount; ++i)
        {
          m_clients[i]?.Join();
        }
      }

      CustomTestCleanup();
      Thread.Sleep(100);
    }

    /// <summary>
    /// Configure Hydra settings here, this will be executed before the Hydra applications are constructed
    /// </summary>
    public virtual void Configure()
    {
      // Empty
    }

    public virtual void ModuleSetup()
    {
      // Empty
    }

    public virtual void CustomTestSetup()
    {
      // Empty
    }

    public virtual void CustomTestCleanup()
    {
      // Empty
    }

    /// <summary>
    /// Evaluates a condition periodically until it is true or the timeout elapses
    /// </summary>
    /// <param name="comparison">The comparison operation to poll</param>
    /// <param name="timeout">The max time in seconds to wait for the comparison to evaluate to true</param>
    /// <param name="pollPeriod">The period in seconds to sleep between comparison polls</param>
    /// <returns>True if the condition evaluated to true otherwise false if the timeout elapsed</returns>
    public static bool WaitForCondition(Func<bool> comparison, double timeout = 15, double pollPeriod = 0.1)
    {
      double time = 0;
      while (!comparison.Invoke() && time < timeout)
      {
        Thread.Sleep((int)(pollPeriod * 1000));
        time += pollPeriod;
      }

      return time < timeout;
    }
  }
}
