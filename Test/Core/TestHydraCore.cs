#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Network;

namespace Skulk.Test.Core
{
  internal class SimpleModule : HydraModule
  {
    private Queue<Tuple<HydraUuid, Message>> m_queue = new Queue<Tuple<HydraUuid, Message>>();

    public Queue<Tuple<HydraUuid, Message>> Queue { get => m_queue; }

    public SimpleModule(HydraCore hydra) : base(hydra)
    {
      // Empty
    }

    public override void HandleMessage(Message msg, HydraUuid uuid)
    {
      lock (m_queue)
      {
        m_queue.Enqueue(new Tuple<HydraUuid, Message>(uuid, msg));
      }
    }
  }

  internal class SimpleModule2 : SimpleModule
  {
    public SimpleModule2(HydraCore hydra) : base(hydra)
    {
      // Empty
    }
  }

  internal class SimpleModule3 : SimpleModule
  {
    public SimpleModule3(HydraCore hydra) : base(hydra)
    {
      // Empty
    }
  }

  /// <summary>
  /// Tests for methods implemented on HydraCore
  /// </summary>
  public class TestHydraCore
  {
    private Queue<Message> m_messageQueue = new Queue<Message>();
    private Queue<Connection> m_addedConnections = new Queue<Connection>();
    private Queue<Connection> m_removedConnections = new Queue<Connection>();
    private Queue<Connection> m_failedConnections = new Queue<Connection>();

    [SetUp]
    public void TestSetUp()
    {
      m_messageQueue.Clear();
      m_addedConnections.Clear();
      m_removedConnections.Clear();
      m_failedConnections.Clear();
    }

    /// <summary>
    /// Test that DNS hostnames can resolve and failed resolutions return null.
    /// </summary>
    /// <remarks>
    /// Unfortunately there's no easy way to test this without assuming a connection of some
    /// sort so this test expects an internet connection to google.
    /// </remarks>
    [Test]
    public void DnsResolution()
    {
      HydraSettings defaults = new HydraSettings();
      HydraCore core = new HydraCore(defaults);

      // Directly interpret an IP
      var address = core.ResolveHostname("127.0.0.1");
      Assert.IsNotNull(address);
      Assert.AreEqual(new System.Net.IPAddress(new byte[] { 127, 0, 0, 1 }), address);

      var googleAddress = core.ResolveHostname("google.com");
      Assert.IsNotNull(googleAddress);

      var unknownAddress = core.ResolveHostname("NotAKnownHost");
      Assert.IsNull(unknownAddress);
    }

    /// <summary>
    /// Test that the max clients value can be set and read as well as it works and limits the number of profiles
    /// </summary>
    [Test]
    public void MaxClients()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        MaxClients = 2,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false,
      };
      HydraServer server = new HydraServer(serverSettings);

      HydraClientSettings templateClientSettings = new HydraClientSettings()
      {
        ApplicationName = "HydraTestClient",
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraClient.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false,
        MaxClients = 2
      };

      int clientCount = 3;
      HydraClient[] clients = new HydraClient[clientCount];
      for (int i = 0; i < clientCount; ++i)
      {
        HydraClientSettings clientSettings = new HydraClientSettings(templateClientSettings);
        clientSettings.ApplicationName += i.ToString();
        clientSettings.UdpPort = 52500 + i;
        clients[i] = new MockHydraClient(clientSettings);
        clients[i].MaintainUdp = false;
        clients[i].MaintainStream(false);
        clients[i].Log.LogInfo("HydraClient {0} UUID: {1}", i, clients[i].Uuid);
      }

      // Start the server
      server.Start();
      for (int i = 0; i < clientCount; ++i)
      {
        clients[i].Start();
      }

      Assert.AreEqual(2, server.MaxClients);

      // Attempt to connect the first 2 clients
      Assert.IsTrue(clients[0].WaitForHandshake(5));
      Assert.IsTrue(clients[1].WaitForHandshake(5));

      // The third one should be rejected because of max clients
      Assert.IsFalse(clients[2].WaitForHandshake(5));

      for (int i = 0; i < clientCount; ++i)
      {
        clients[i].Stop();
        clients[i].Join();
      }

      server.Stop();
      server.Join();
    }

    /// <summary>
    /// Test that the server port can be set and read
    /// </summary>
    [Test]
    public void ServerPort()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        MaxClients = 2,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false,
      };
      HydraServer server = new HydraServer(serverSettings);
      Assert.AreEqual(serverSettings.UdpPort, server.ServerPort);
      server.Start();
      Assert.AreEqual(serverSettings.UdpPort, server.ServerPort);
      server.Stop();
      server.Join();
    }

    /// <summary>
    /// Test that unhandled messages will be sent to the default module
    /// </summary>
    [Test]
    public void DefaultModule()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        MaxClients = 2,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false,
      };
      HydraServer server = new HydraServer(serverSettings);
      SimpleModule simple = new SimpleModule(server);
      server.AddModule(simple);
      Assert.IsNull(server.DefaultModule);
      server.DefaultModule = simple;
      Assert.AreEqual(simple, server.DefaultModule);
      server.Start();

      HydraClientSettings clientSettings = new HydraClientSettings()
      {
        ApplicationName = "HydraTestClient",
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraClient.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false,
        MaxClients = 2
      };

      HydraClient client = new HydraClient(clientSettings);
      client.MaintainUdp = false;
      client.MaintainStream(false);
      client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
      client.Start();
      Assert.IsTrue(client.WaitForHandshake(5));

      // Send some made up messages that will be unhandled
      Message msg1 = new Message(10000, 0);
      Message msg2 = new Message(10001, 10);
      byte[] data = new byte[10];
      msg2.SetPayload(data);
      client.SendMessage(msg1, server.Uuid);
      client.SendMessage(msg2);

      bool received = false;
      double time = 0;
      while (!received && time < 5)
      {
        lock (simple.Queue)
        {
          received = simple.Queue.Count == 2;
        }

        if (!received)
        {
          System.Threading.Thread.Sleep(100);
          time += 0.1;
        }
      }

      if (!received)
      {
        Assert.Fail("Default module failed to receive 2 messages");
      }
      else
      {
        lock (simple.Queue)
        {
          var pair = simple.Queue.Dequeue();
          Assert.AreEqual(client.Uuid, pair.Item1);
          Assert.AreEqual(msg1.Command, pair.Item2.Command);
          Assert.AreEqual(msg1.GetLength(), pair.Item2.GetLength());
          pair = simple.Queue.Dequeue();
          Assert.AreEqual(client.Uuid, pair.Item1);
          Assert.AreEqual(msg2.Command, pair.Item2.Command);
          Assert.AreEqual(msg2.GetLength(), pair.Item2.GetLength());
        }
      }

      server.Stop();
      server.Join();
      client.Stop();
      client.Join();
    }

    /// <summary>
    /// Test the <see cref="HydraCore.OnMessageReceived"/> callback
    /// </summary>
    [Test]
    public void OnMessageReceived()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
        };
        server = new HydraServer(serverSettings);
        server.ProfileManager.OnMessageReceived += HandleMessages;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        // Send some made up messages that will be unhandled
        Message msg1 = new Message(10000, 0);
        Message msg2 = new Message(10001, 10);
        byte[] data = new byte[10];
        msg2.SetPayload(data);
        client.SendMessage(msg1, server.Uuid);
        client.SendMessage(msg2);
        Message msg3 = new NoopMsg();
        client.SendMessage(msg3);

        bool received = false;
        double time = 0;
        while (!received && time < 5)
        {
          lock (m_messageQueue)
          {
            received = m_messageQueue.Count == 3;
          }

          if (!received)
          {
            System.Threading.Thread.Sleep(100);
            time += 0.1;
          }
        }

        Assert.IsTrue(received);
        if (received)
        {
          lock (m_messageQueue)
          {
            var msg = m_messageQueue.Dequeue();
            Assert.AreEqual(msg1.Command, msg.Command);
            Assert.AreEqual(msg1.GetLength(), msg.GetLength());
            msg = m_messageQueue.Dequeue();
            Assert.AreEqual(msg2.Command, msg.Command);
            Assert.AreEqual(msg2.GetLength(), msg.GetLength());
            msg = m_messageQueue.Dequeue();
            Assert.AreEqual(msg3.Command, msg.Command);
            Assert.AreEqual(msg3.GetLength(), msg.GetLength());
          }
        }
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Test transferring a client between Hydra servers
    /// </summary>
    [Test]
    public void TransferClient()
    {
      HydraServer server1 = null;
      HydraServer server2 = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
        };
        HydraServerSettings server1Settings = new HydraServerSettings(serverSettings);
        server1 = new HydraServer(server1Settings);
        HydraServerSettings server2Settings = new HydraServerSettings(serverSettings);
        server2Settings.UdpPort = 52506;
        server2Settings.TcpPort = 52601;
        server2 = new HydraServer(server2Settings);
        server1.Start();
        server2.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        // Now transfer to server 2 without a TCP connection
        HydraTransferData transferData = new HydraTransferData(server2.Address.Host, server2.Address.MessagePort, server2.Address.StreamPort, false);
        HydraTransferMsg transferMsg = new HydraTransferMsg(transferData);
        server1.SendMessage(transferMsg, client.Uuid);

        // Wait for server1 to lose connection
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => server1.ProfileCount == 0, 5));

        // Wait for server2 to get a connection
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => server2.ProfileCount == 1, 5));

        // Check that the client is set for the correct server now
        Assert.AreEqual(server2.Uuid, client.ServerUuid);

        // Confirm that no TCP stream was started
        Assert.AreEqual(1, client.GetProfile(server2.Uuid).ConnectionCount);

        // Now transfer back to server 1, but use a TCP connection and use constructor initialization to exercise the properties
        transferData = new HydraTransferData()
        {
          HostName = server1.Address.Host,
          Port = server1.Address.MessagePort,
          StreamPort = server1.Address.StreamPort,
          StartStream = true
        };
        transferMsg = new HydraTransferMsg(transferData);
        server2.SendMessage(transferMsg, client.Uuid);

        // Wait for server2 to lose connection and server1 to get one
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => server2.ProfileCount == 0, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => server1.ProfileCount == 1, 5));

        // Check that the client is set for the correct server now
        Assert.AreEqual(server1.Uuid, client.ServerUuid);

        // Wait for a UDP and TCP connection
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => client.GetProfile(server1.Uuid).ConnectionCount == 2, 5));

        transferMsg = new HydraTransferMsg((HydraTransferData)null);
        server1.Log.LogDebug("Sending null transfer data message");
        server1.SendMessage(transferMsg, client.Uuid);

        // Confirm nothing happened with invalid data
        Assert.IsFalse(HydraUnitTest.WaitForCondition(() => client.GetProfile(server1.Uuid) == null, 2));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => client.GetProfile(server1.Uuid).ConnectionCount == 2, 1));

        // Send a message to the server to confirm that servers don't transfer
        transferData = new HydraTransferData()
        {
          HostName = server2.Address.Host,
          Port = server2.Address.MessagePort,
          StreamPort = server2.Address.StreamPort,
          StartStream = true
        };
        transferMsg = new HydraTransferMsg(transferData);
        client.SendMessage(transferMsg, server1.Uuid);

        // Wait for a second and confirm the client is still connected
        Assert.IsFalse(HydraUnitTest.WaitForCondition(() => client.GetProfile(server1.Uuid) == null, 2));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => client.GetProfile(server1.Uuid).ConnectionCount == 2, 1));
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        server1?.Stop();
        server1?.Join();
        server2?.Stop();
        server2?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Test the ConnectionAdded, ConnectionRemoved, and ConnectionFailed callbacks with UDP connections
    /// </summary>
    [Test]
    public void UdpConnectionCallbacks()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        server.ProfileManager.OnMessageReceived += HandleMessages;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.ProfileManager.OnConnectionAdded += HandleConnectionAdded;
        client.ProfileManager.OnConnectionRemoved += HandleConnectionRemoved;
        client.ProfileManager.OnConnectionFailed += HandleConnectionFailed;
        client.Start();

        // Confirm no connections
        Assert.AreEqual(0, m_addedConnections.Count);
        Assert.AreEqual(0, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        Assert.IsTrue(client.WaitForHandshake(5));

        // Confirm an added connection
        Assert.AreEqual(1, m_addedConnections.Count);
        Assert.AreEqual(0, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        Assert.IsTrue(client.RemoveProfile(server.Uuid));

        // Now expect 1 removed connection
        Assert.AreEqual(1, m_addedConnections.Count);
        Assert.AreEqual(1, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        // Remove the profile on the server and handshake again
        server.RemoveProfile(client.Uuid);
        Assert.IsTrue(client.WaitForHandshake(5));

        // Now expect another added connection
        Assert.AreEqual(2, m_addedConnections.Count, "Connection count {0} does not match expected count {1} after handshaking a new one", m_addedConnections.Count, 2);
        Assert.AreEqual(1, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        // Now intentionally stop the server side connection without sending anything to the client. This should cause errors on the client connection
        Connection connection = server.GetProfile(client.Uuid).GetConnection(ConnectionTypes.MESSAGE);
        connection.SendCloseMessage = false;
        connection.Stop();

        // Wait for server connection to end
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => connection.Ended, 3));

        // Send a message to force threads to wake up and fail
        client.SendMessage(new NoopMsg(), server.Uuid);

        if (Environment.OSVersion.Platform == PlatformID.Unix)
        {
          // Linux doesn't cause an exception on a closed localhost UDP remote endpoint, so we need to cause one
          client.GetProfile(server.Uuid).GetConnection(ConnectionTypes.MESSAGE).Close(true);
        }

        // A failed connection also gets removed so expect 1 more of each
        // Wait for the failure
        Console.WriteLine("Waiting for failure: {0}", DateTime.Now.ToString("O"));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_failedConnections.Count == 1, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_removedConnections.Count == 2, 5));
        Assert.AreEqual(2, m_addedConnections.Count, "Added connection count {0} does not match expected count {1}", m_addedConnections.Count, 2);
        Assert.AreEqual(2, m_removedConnections.Count, "Removed connection count {0} does not match expected count {1}", m_removedConnections.Count, 2);
        Assert.AreEqual(1, m_failedConnections.Count);
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        System.Threading.Thread.Sleep(200);
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Test the ConnectionAdded, ConnectionRemoved, and ConnectionFailed callbacks with TCP connections
    /// </summary>
    [Test]
    public void TcpConnectionCallbacks()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        server.ProfileManager.OnMessageReceived += HandleMessages;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.ProfileManager.OnConnectionAdded += HandleConnectionAdded;
        client.ProfileManager.OnConnectionRemoved += HandleConnectionRemoved;
        client.ProfileManager.OnConnectionFailed += HandleConnectionFailed;
        client.Start();

        // Confirm no connections
        Assert.AreEqual(0, m_addedConnections.Count);
        Assert.AreEqual(0, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        Assert.IsTrue(client.StartTcpConnection(3000));

        // Confirm an added connection
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_addedConnections.Count == 1, 1));
        Assert.AreEqual(0, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        Assert.IsTrue(client.RemoveProfile(server.Uuid));

        // Now expect 1 removed connection
        Assert.AreEqual(1, m_addedConnections.Count);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_removedConnections.Count == 1, 1));
        Assert.AreEqual(0, m_failedConnections.Count);

        Assert.IsTrue(client.StartTcpConnection(3000));

        // Now expect another added connection
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_addedConnections.Count == 2, 1));
        Assert.AreEqual(1, m_removedConnections.Count);
        Assert.AreEqual(0, m_failedConnections.Count);

        // Now intentionally stop the server side connection without sending anything to the client. This should cause errors on the client connection
        Connection connection = server.GetProfile(client.Uuid).GetConnection(ConnectionTypes.STREAM);
        connection.SendCloseMessage = false;
        connection.Stop();

        // Wait for the connection to stop on the server
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => (server.GetProfile(client.Uuid)?.ConnectionCount ?? 0) == 0, 5));

        // Send a message to force threads to wake up and fail
        for (int i = 0; i < 3; ++i)
        {
          if (!HydraUnitTest.WaitForCondition(() => m_failedConnections.Count == 1, 1))
          {
            client.SendMessage(new NoopMsg(), server.Uuid);
          }
          else
          {
            break;
          }
        }

        // A failed connection also gets removed so expect 1 more of each
        // Wait for the failure
        Console.WriteLine("Waiting for failure: {0}", DateTime.Now.ToString("O"));
        HydraUnitTest.WaitForCondition(() => m_failedConnections.Count == 1, 5);

        // The connection may count as failed or just removed depending on when the closure is caught
        Assert.IsTrue(m_failedConnections.Count < 2);

        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => m_removedConnections.Count == 2, 1));
        Assert.AreEqual(2, m_addedConnections.Count);
        Assert.AreEqual(2, m_removedConnections.Count);
        Assert.Less(m_failedConnections.Count, 2);
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        System.Threading.Thread.Sleep(200);
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Tests the ability to set the localhost value on a Hydra application
    /// </summary>
    [Test]
    public void SetUdpBind()
    {
      HydraServer server = null;
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        UdpBindAddress = "localhost",
        MaxClients = 2,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = false,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false
      };
      server = new HydraServer(serverSettings);
      Assert.AreEqual("127.0.0.1", server.Address.Host);
      serverSettings.UdpBindAddress = "127.0.0.1";
      server = new HydraServer(serverSettings);
      Assert.AreEqual(serverSettings.UdpBindAddress, server.Address.Host);
    }

    /// <summary>
    /// Test the behavior of Hydralizer indexing when a Hydra application is constructed
    /// </summary>
    [Test]
    public void HydralizerIndexing()
    {
      try
      {
        HydraServer server = null;
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);

        // Confirm MEL is indexed as 1
        Assert.IsFalse(Hydralize.Hydralizer.AddSupportedAssembly(1, typeof(HydraAddress).Assembly));
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(1, typeof(MEL.AppLog).Assembly));

        // Confirm Hydra is indexed as 2
        Assert.IsFalse(Hydralize.Hydralizer.AddSupportedAssembly(2, typeof(MEL.AppLog).Assembly));
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(2, typeof(HydraAddress).Assembly));

        // Put MEL as 1 and Test as 2
        Hydralize.Hydralizer.DefaultContext.Clear();
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(1, typeof(MEL.AppLog).Assembly));
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(2, typeof(TestHydraCore).Assembly));

        // Expect constructor to throw since the indexing has the test assembly as 2 instead of Hydra
        Assert.Throws(typeof(Exception), () => server = new HydraServer(serverSettings));

        // Put Hydra as 1 and MEL as 2
        Hydralize.Hydralizer.DefaultContext.Clear();
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(1, typeof(HydraAddress).Assembly));
        Assert.IsTrue(Hydralize.Hydralizer.AddSupportedAssembly(2, typeof(MEL.AppLog).Assembly));

        // Expect constructor to throw since the indexing has Hydra as 1 instead of MEL
        Assert.Throws(typeof(Exception), () => server = new HydraServer(serverSettings));
      }
      finally
      {
        Hydralize.Hydralizer.DefaultContext.Clear();
      }
    }

    /// <summary>
    /// Test sending a message to a direct IP and port
    /// </summary>
    [Test]
    public void DirectSend()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        SimpleModule simple = new SimpleModule(server);
        server.DefaultModule = simple;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.Start();

        // Send an anonymous message directly without handshaking
        Message anon = new Message(11111, 0, (ushort)MessageFlagValues.ANONYMOUS);
        Assert.IsTrue(client.SendMessage(anon, server.Address.Host, server.Address.MessagePort));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 1, 3));

        // Send non anonymous message over main UDP, expect it to be dropped
        Message dummy = new Message(11111, 0);
        Assert.IsTrue(client.SendMessage(dummy, server.Address.Host, server.Address.MessagePort));
        Assert.IsFalse(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 2));

        Assert.IsTrue(client.WaitForHandshake(5));

        // Can send direct even after handshaking too, but only after verifying as a new source and using the direct connection port
        Profile profile = client.GetProfile(server.Uuid);
        int port = (profile.GetConnection(ConnectionTypes.MESSAGE) as UdpConnection).ClientEp.Port;
        ProfileInfo profileInfo = new ProfileInfo(client.Uuid, profile.RemoteKey);
        SetUdpSourceMsg setUdpSourceMsg = new SetUdpSourceMsg(profileInfo);

        // Confirm the message is rejected initially
        Assert.IsTrue(client.SendMessage(dummy, server.Address.Host, port));
        Assert.IsFalse(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 3));

        // Set our main UDP port as the new source and try again
        Assert.IsTrue(client.SendMessage(setUdpSourceMsg, server.Address.Host, port));
        Assert.IsTrue(client.SendMessage(dummy, server.Address.Host, port));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 3));

        // Set the source back to the actual connection
        client.SendMessage(setUdpSourceMsg, server.Uuid);
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Tests attempts at sending invalid messages
    /// </summary>
    [Test]
    public void InvalidMessages()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        SimpleModule simple = new SimpleModule(server);
        server.DefaultModule = simple;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        Message msg = new Message(12121, 0);

        // null UUID should mean the msg goes nowhere
        client.SendMessage(msg, (HydraUuid)null);

        // null message should mean it gets dropped and not sent
        client.SendMessage(null, server.Uuid);

        // null message should get dropped on direct send too
        client.SendMessage(null, server.Address.Host, server.Address.MessagePort);

        Assert.IsFalse(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 1, 3));

        client.Stop();
        clientSettings.DisableUdp = true;
        client = new HydraClient(clientSettings);
        client.Start();

        Assert.IsFalse(client.NetworkBroadcast(msg, 52000));
        Assert.IsFalse(client.SendMessage(msg, server.Address.Host, server.Address.MessagePort));
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Test saving and loading UUID values
    /// </summary>
    [Test]
    public void SavingUuids()
    {
      HydraServerSettings serverSettings = new HydraServerSettings()
      {
        ApplicationName = "HydraTestServer",
        MaxClients = 2,
        UdpPort = 52505,
        TcpPort = 52600,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraServer.log",
        EnableConsoleLog = true,
        KeepAlivePeriod = 3,
        ProfileTimeout = 5,
        UseRouting = false
      };

      // Random UUID
      HydraServer server = new HydraServer(serverSettings);
      HydraUuid uuid = server.Uuid;

      // Another random UUID
      server = new HydraServer(serverSettings);
      Assert.AreNotEqual(uuid, server.Uuid);
      uuid = server.Uuid;
      Assert.IsTrue(server.SaveUuid());

      // Loads saved UUID
      server = new HydraServer(serverSettings);
      Assert.AreEqual(uuid, server.Uuid);
      Assert.IsTrue(server.ClearUuid());

      // Random UUID again after clearing
      server = new HydraServer(serverSettings);
      Assert.AreNotEqual(uuid, server.Uuid);
    }

    /// <summary>
    /// Tests retrieving the UUIDs of active profiles
    /// </summary>
    [Test]
    public void GetUuids()
    {
      HydraServer server = null;
      int clientCount = 3;
      HydraClient[] clients = new HydraClient[clientCount];

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 10,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);

        HydraClientSettings templateClientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        for (int i = 0; i < clientCount; ++i)
        {
          HydraClientSettings clientSettings = new HydraClientSettings(templateClientSettings);
          clientSettings.ApplicationName += i.ToString();
          clientSettings.UdpPort = 52500 + i;
          clients[i] = new MockHydraClient(clientSettings);
          clients[i].MaintainUdp = false;
          clients[i].MaintainStream(false);
          clients[i].Log.LogInfo("HydraClient {0} UUID: {1}", i, clients[i].Uuid);
        }

        // Start the server
        server.Start();
        for (int i = 0; i < clientCount; ++i)
        {
          clients[i].Start();
          Assert.IsTrue(clients[i].WaitForHandshake(5));

          // Make sure all the clients are in the array
          HydraUuid[] uuids = server.GetUuids();
          Assert.AreEqual(i + 1, uuids.Length);
          for (int j = 0; j < i + 1; ++j)
          {
            bool found = false;
            for (int k = 0; k < uuids.Length; ++k)
            {
              if (clients[j].Uuid.Equals(uuids[k]))
              {
                found = true;
                break;
              }
            }

            Assert.IsTrue(found, "Client UUID: {0} not found in array", clients[i].Uuid);
          }
        }
      }
      finally
      {
        for (int i = 0; i < clientCount; ++i)
        {
          clients[i]?.Stop();
          clients[i]?.Join();
        }

        server?.Stop();
        server?.Join();
      }
    }

    /// <summary>
    /// Tests dynamic message handling
    /// </summary>
    [Test]
    public void DynamicHandling()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        SimpleModule simple = new SimpleModule(server);
        server.AddModule(simple);
        SimpleModule2 simple2 = new SimpleModule2(server);
        server.AddModule(simple2);
        SimpleModule3 simple3 = new SimpleModule3(server);
        server.AddModule(simple3);
        server.DefaultModule = simple;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.MaintainUdp = false;
        client.MaintainStream(false);
        client.Log.LogInfo("HydraClient UUID: {0}", client.Uuid);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        // Make a basic message with code 10000
        Message msg = new Message(10000, 0);

        client.SendMessage(msg);
        // Currently 10000 is unhandled so wait for it to go to the default module
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 1, 5));

        // Now set dynamic handling to simple2
        server.AddDynamicHandling(10000, simple2.HandleMessage);
        server.Log.LogDebug("Added dynamic handling for 10000");
        client.SendMessage(msg);

        // Make sure simple2 gets the message and simple doesn't
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 1, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 1, 5));

        Assert.IsTrue(server.RemoveDynamicHandling(10000));
        Assert.IsFalse(server.RemoveDynamicHandling(10000));
        client.SendMessage(msg);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 1, 1));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple3.Queue.Count == 0, 1));

        server.AddDynamicHandling(10000, simple3.HandleMessage);
        client.SendMessage(msg);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple3.Queue.Count == 1, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 1));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 1, 1));

        server.AddDynamicHandling(10001, simple2.HandleMessage);
        Message msg2 = new Message(10001, 0);
        client.SendMessage(msg);
        client.SendMessage(msg2);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple3.Queue.Count == 2, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 2, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 2, 1));

        Assert.IsTrue(server.RemoveDynamicHandling(10000));
        client.SendMessage(msg);
        client.SendMessage(msg2);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 3, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 3, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple3.Queue.Count == 2, 1));

        Assert.IsTrue(server.RemoveDynamicHandling(10001));
        client.SendMessage(msg);
        client.SendMessage(msg2);
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == 5, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple2.Queue.Count == 3, 1));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple3.Queue.Count == 2, 1));
      }
      catch (Exception ex)
      {
        Assert.Fail("{0}", ex);
      }
      finally
      {
        System.Threading.Thread.Sleep(200);
        server?.Stop();
        server?.Join(3000);
        client?.Stop();
        client?.Join(3000);
      }
    }

    /// <summary>
    /// Receives messages and puts them in the message queue on this class
    /// </summary>
    /// <param name="message">The message received</param>
    /// <param name="uuid">The UUID of the sender</param>
    private void HandleMessages(Message message, HydraUuid uuid)
    {
      lock (m_messageQueue)
      {
        m_messageQueue.Enqueue(message);
      }
    }

    private void HandleConnectionAdded(Connection connection)
    {
      lock (m_addedConnections)
      {
        m_addedConnections.Enqueue(connection);
      }
      Console.WriteLine("Connection added on profile: {0}", connection.Profile.Uuid);
    }

    private void HandleConnectionRemoved(Connection connection)
    {
      lock (m_removedConnections)
      {
        m_removedConnections.Enqueue(connection);
      }
      Console.WriteLine("Connection removed on profile: {0}", connection.Profile.Uuid);
    }

    private void HandleConnectionFailed(Connection connection)
    {
      lock (m_failedConnections)
      {
        m_failedConnections.Enqueue(connection);
      }
      Console.WriteLine("Connection failed on profile: {0}", connection.Profile.Uuid);
    }
  }
}
