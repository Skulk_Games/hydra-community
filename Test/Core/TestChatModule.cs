#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra;
using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Test
{
  public class ChatInfo
  {
    private string m_msg;
    private string m_user;
    private DateTime m_time;

    public string Msg { get => m_msg; set => m_msg = value; }
    public string User { get => m_user; set => m_user = value; }
    public DateTime Time { get => m_time; set => m_time = value; }
  }

  public class ChatMsg : HydraObjMsg
  {
    public ChatMsg(ChatInfo chatInfo) : base(10101, 0, chatInfo, true)
    {
      // Empty
    }

    public ChatMsg(Message other) : base(other)
    {
      // Empty
    }
  }

  /// <summary>
  /// A simple test module that handles chat messages
  /// </summary>
  public class TestChatModule : HydraModule
  {
    private Queue<ChatInfo> m_chatQueue = new Queue<ChatInfo>();

    public event EventHandler<ChatInfo> MessageReceived;

    public TestChatModule(HydraCore hydra) : base(hydra, 10101)
    {
      // Empty
    }

    public ChatInfo GetChat()
    {
      lock (m_chatQueue)
      {
        if (m_chatQueue.Count > 0)
        {
          return m_chatQueue.Dequeue();
        }
      }

      return null;
    }

    public void SendChat(string user, string msg, HydraUuid destination)
    {
      ChatInfo info = new ChatInfo()
      {
        User = user,
        Msg = msg,
        Time = DateTime.Now
      };
      SendChat(info, destination);
    }

    public void SendChat(ChatInfo chatInfo, HydraUuid destination)
    {
      Parent.SendMessage(new ChatMsg(chatInfo), destination);
    }

    public override void HandleMessage(Message msg, HydraUuid uuid)
    {
      switch((UInt16)msg.GetCmd())
      {
        case 10101:
        {
          ChatMsg chatMsg = new ChatMsg(msg);
          ChatInfo chatInfo = chatMsg.GetObject<ChatInfo>();
          if (chatInfo != null)
          {
            lock (m_chatQueue)
            {
              m_chatQueue.Enqueue(chatInfo);
            }

            try
            {
              MessageReceived?.Invoke(this, chatInfo);
            }
            catch (Exception ex)
            {
              Parent.Log?.LogError("Exception in MessageReceived event: {0}", ex.Message);
            }
          }
        }
        break;

        default:
        {
          Parent.Log?.LogError("Received unhandled message with code: {0}", msg.GetCmd());
        }
        break;
      }
    }
  }
}
