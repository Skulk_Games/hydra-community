#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.Test
{
  /// <summary>
  /// Tests for verifying the latency check feature on connections
  /// </summary>
  public class TestHydraLatency : HydraUnitTest
  {
    /// <summary>
    /// Latency should always be less than 10ms (my personal decision) round trip or we need to optimize the network path.
    /// The connection threads now use Poll to be more responsive to received messages and the PollPeriod in settings just affects the max time they Poll
    /// for SelectRead mode before checking the send queue and stop flag. So some overhead will be be inherent in the latency but most of the time should now
    /// be wire time (although this is a localhost test)
    /// </summary>
    private const double MAX_ALLOWED_LATENCY = 0.01;

    /// <summary>
    /// The number of latency check responses that have been received
    /// </summary>
    private int m_latencyCheckCount = 0;

    public override void Configure()
    {
      base.Configure();
      KeepAlivePeriod = 4;
      ClientTimeoutPeriod = 5;
      UdpHandshake = true;
      TcpHandshake = true;
    }

    public override void ModuleSetup()
    {
      // No modules - core testing
    }

    public override void CustomTestCleanup()
    {
      // Clean up code
    }

    /// <summary>
    /// Verifies that getting latency values for connections works and is a reasonable value
    /// </summary>
    [Test]
    public void TestConnectionLatency()
    {
      Profile profile = Client.GetProfile(Server.Uuid);
      profile.OnLatencyUpdated += HandleLatencyUpdate;
      // Get the connections ordered by message priority
      Connection[] connections = profile.ConnectionManager.GetConnections(ConnectionTypes.MESSAGE);
      int checkCount = m_latencyCheckCount;
      for (int i = 0; i < connections.Length; ++i)
      {
        profile.StartLatencyCheck(connections[i].Id);
        Assert.IsTrue(WaitForCondition(() => m_latencyCheckCount == checkCount + 1, 10), "Timed out waiting for latency on connection {0}, count is: {1}", connections[i].Id, m_latencyCheckCount);
        ++checkCount;
        Client.Log?.LogInfo("Latency for connection: {0} is {1}", connections[i].Id, connections[i].Latency);
        Assert.IsTrue(connections[i].Latency < MAX_ALLOWED_LATENCY, "Connection {0}'s Latency: {1}s exceeded max of {2}s", i, connections[i].Latency, MAX_ALLOWED_LATENCY);
      }
    }

    /// <summary>
    /// Verifies that getting latency values for connections works and the average response is reasonable
    /// </summary>
    [Test]
    public void TestAverageConnectionLatency()
    {
      // Get the connections ordered by message priority
      Profile profile = Client.GetProfile(Server.Uuid);
      profile.OnLatencyUpdated += HandleLatencyUpdate;
      Connection[] connections = profile.ConnectionManager.GetConnections(ConnectionTypes.MESSAGE);
      int sends = 10;
      double[] latencies = new double[sends];
      int checkCount = m_latencyCheckCount;
      for (int connectionId = 0; connectionId < connections.Length; ++connectionId)
      {
        Connection connection = connections[connectionId];
        for (int send = 0; send < sends; ++send)
        {
          profile.StartLatencyCheck(connection.Id);
          Assert.IsTrue(WaitForCondition(() => m_latencyCheckCount == checkCount + 1, 10), "Timed out waiting for latency on connection {0}", connection.Id);
          ++checkCount;
          Client.Log?.LogInfo("Latency for connection: {0} is {1}", connection.Id, connection.Latency);
          latencies[send] = connection.Latency;
        }

        double avg = 0;
        for (int i = 0; i < sends; ++i)
        {
          Console.WriteLine("Latency[{0}]={1}", i, latencies[i]);
          avg += latencies[i];
        }
        avg /= latencies.Length;
        Client.Log?.LogInfo("Connection[{0}]: Average latency is: {1}", connectionId, avg);
        Assert.IsTrue(avg < MAX_ALLOWED_LATENCY, "Connection[{0}] average latency: {1} exceeded max of {2}", connectionId, avg, MAX_ALLOWED_LATENCY);
      }
    }

    private void HandleLatencyUpdate(Connection connection)
    {
      ++m_latencyCheckCount;
    }
  }
}
