#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.Network;

namespace Skulk.Test.Core
{
  [TestFixture]
  public class TestProfileManager
  {
    /// <summary>
    /// Tests getting profiles by name
    /// </summary>
    [Test]
    public void GetProfilesByName()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        List<Profile> profiles = client.ProfileManager.GetProfiles(serverSettings.ApplicationName);
        Assert.AreEqual(1, profiles.Count);
        Assert.AreEqual(server.Uuid, profiles[0].Uuid);
        Assert.AreEqual(serverSettings.ApplicationName, profiles[0].Name);

        profiles = client.ProfileManager.GetProfiles("bob");
        Assert.AreEqual(0, profiles.Count);

        profiles = client.ProfileManager.GetProfiles(null);
        Assert.AreEqual(0, profiles.Count);

        profiles = client.ProfileManager.GetProfiles(serverSettings.ApplicationName, HydraModes.SERVER);
        Assert.AreEqual(1, profiles.Count);
        Assert.AreEqual(server.Uuid, profiles[0].Uuid);
        Assert.AreEqual(serverSettings.ApplicationName, profiles[0].Name);

        profiles = client.ProfileManager.GetProfiles(serverSettings.ApplicationName, HydraModes.CLIENT);
        Assert.AreEqual(0, profiles.Count);

        profiles = client.ProfileManager.GetProfiles(serverSettings.ApplicationName, HydraModes.NODE);
        Assert.AreEqual(0, profiles.Count);

        // Server side
        profiles = server.ProfileManager.GetProfiles(clientSettings.ApplicationName);
        Assert.AreEqual(1, profiles.Count);
        Assert.AreEqual(client.Uuid, profiles[0].Uuid);
        Assert.AreEqual(clientSettings.ApplicationName, profiles[0].Name);

        profiles = server.ProfileManager.GetProfiles(clientSettings.ApplicationName, HydraModes.CLIENT);
        Assert.AreEqual(1, profiles.Count);
        Assert.AreEqual(client.Uuid, profiles[0].Uuid);
        Assert.AreEqual(clientSettings.ApplicationName, profiles[0].Name);

        profiles = server.ProfileManager.GetProfiles(clientSettings.ApplicationName, HydraModes.SERVER);
        Assert.AreEqual(0, profiles.Count);

        profiles = server.ProfileManager.GetProfiles(clientSettings.ApplicationName, HydraModes.NODE);
        Assert.AreEqual(0, profiles.Count);

        profiles = server.ProfileManager.GetProfiles("fred");
        Assert.AreEqual(0, profiles.Count);

        profiles = server.ProfileManager.GetProfiles(null);
        Assert.AreEqual(0, profiles.Count);
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    /// <summary>
    /// Tests the <see cref="HydraProfileManager.IsConnected(HydraUuid)"/> method
    /// </summary>
    [Test]
    public void IsConnected()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);

        // Before a connection we should not be connected
        Assert.IsFalse(client.ProfileManager.IsConnected(server.Uuid));
        Assert.IsFalse(server.ProfileManager.IsConnected(client.Uuid));

        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        // Now we should show as connected
        Assert.IsTrue(client.ProfileManager.IsConnected(server.Uuid));
        Assert.IsTrue(server.ProfileManager.IsConnected(client.Uuid));

        Assert.IsTrue(client.WaitForTcpConnection(3));

        // Still show as connected
        Assert.IsTrue(client.ProfileManager.IsConnected(server.Uuid));
        Assert.IsTrue(server.ProfileManager.IsConnected(client.Uuid));

        // Close the UDP connection
        client.GetProfile(server.Uuid).GetConnection(ConnectionTypes.MESSAGE).Stop();
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => client.GetProfile(server.Uuid).ConnectionCount == 1, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => server.GetProfile(client.Uuid).ConnectionCount == 1, 3));

        // Still show as connected
        Assert.IsTrue(client.ProfileManager.IsConnected(server.Uuid));
        Assert.IsTrue(server.ProfileManager.IsConnected(client.Uuid));

        // Close the TCP connection
        client.GetProfile(server.Uuid).GetConnection(ConnectionTypes.STREAM).Stop();
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => (client.GetProfile(server.Uuid)?.ConnectionCount ?? 0) == 0, 5));
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => (server.GetProfile(client.Uuid)?.ConnectionCount ?? 0) == 0, 3));

        // No more connections, we should not be connected
        Assert.IsFalse(client.ProfileManager.IsConnected(server.Uuid));
        Assert.IsFalse(server.ProfileManager.IsConnected(client.Uuid));
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }

    [Test]
    public void SendOnConnectionType()
    {
      HydraServer server = null;
      HydraClient client = null;

      try
      {
        HydraServerSettings serverSettings = new HydraServerSettings()
        {
          ApplicationName = "HydraTestServer",
          MaxClients = 2,
          UdpPort = 52505,
          TcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraServer.log",
          EnableConsoleLog = true,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false
        };
        server = new HydraServer(serverSettings);
        SimpleModule simple = new SimpleModule(server);
        server.AddModule(simple);
        server.DefaultModule = simple;
        server.Start();

        HydraClientSettings clientSettings = new HydraClientSettings()
        {
          ApplicationName = "HydraTestClient",
          UdpPort = 0,
          ServerHost = "localhost",
          ServerUdpPort = 52505,
          ServerTcpPort = 52600,
          LogLevel = MEL.LogLevels.ALL,
          LogFile = "hydraClient.log",
          EnableConsoleLog = false,
          KeepAlivePeriod = 3,
          ProfileTimeout = 5,
          UseRouting = false,
          MaxClients = 2
        };

        client = new HydraClient(clientSettings);
        client.Start();
        Assert.IsTrue(client.WaitForHandshake(5));

        Profile serverProfile = client.ProfileManager.GetProfile(server.Uuid);
        Assert.IsNotNull(serverProfile);
        Message msg = new Message(11111, 0);

        // Send over UDP messaging connection
        Assert.IsTrue(serverProfile.SendMessage(msg, ConnectionTypes.MESSAGE));

        // There is no stream (TCP) connection
        Assert.IsFalse(serverProfile.SendMessage(msg, ConnectionTypes.STREAM));

        int expectedCount = 1;
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == expectedCount, 5));

        // Start a TCP connection
        Assert.IsTrue(client.StartTcpConnection(5000));

        // Send over UDP messaging connection
        Assert.IsTrue(serverProfile.SendMessage(msg, ConnectionTypes.MESSAGE));
        ++expectedCount;

        // Send over TCP stream
        Assert.IsTrue(serverProfile.SendMessage(msg, ConnectionTypes.STREAM));
        ++expectedCount;

        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == expectedCount, 5));

        // Stop the UDP connection
        serverProfile.GetConnection(ConnectionTypes.MESSAGE).Stop();
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => serverProfile.ConnectionCount == 1, 5));

        // Fail to send over UDP messaging connection, we stopped it
        Assert.IsFalse(serverProfile.SendMessage(msg, ConnectionTypes.MESSAGE));

        // Send over TCP stream
        Assert.IsTrue(serverProfile.SendMessage(msg, ConnectionTypes.STREAM));
        ++expectedCount;

        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => simple.Queue.Count == expectedCount, 5));

        // Stop the TCP connection
        serverProfile.GetConnection(ConnectionTypes.STREAM).Stop();
        Assert.IsTrue(HydraUnitTest.WaitForCondition(() => serverProfile.ConnectionCount == 0, 5));

        // Fail to send over UDP messaging connection, we stopped it
        Assert.IsFalse(serverProfile.SendMessage(msg, ConnectionTypes.MESSAGE));

        // Fail to send over TCP stream, we stopped it too
        Assert.IsFalse(serverProfile.SendMessage(msg, ConnectionTypes.STREAM));
      }
      finally
      {
        server?.Stop();
        server?.Join();
        client?.Stop();
        client?.Join();
      }
    }
  }
}
