#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Skulk.Hydra.Messages;
using System.Threading;
using Skulk.Hydra.Network;
using Skulk.Hydralize;

namespace Skulk.Test
{
  public class TestHydraRouting
  {
    private HydraServer m_server;
    private HydraClient m_client1;
    private HydraClient m_client2;

    [OneTimeSetUp]
    public void ClassSetUp()
    {
      HydraServerSettings settings = new HydraServerSettings()
      {
        ApplicationName = "Server",
        KeepAlivePeriod = 10,
        MaxClients = 5,
        MaxUpdateHz = 30,
        MessageEncoder = null,
        StreamEncoder = null,
        Mode = HydraModes.SERVER,
        StreamTimeout = 3600,
        ProfileTimeout = 30,
        TcpPort = 52700,
        UdpPort = 52505,
        UseRouting = true,
        LogFile = "hydraServer.log",
        LogLevel = MEL.LogLevels.ALL,
        EnableConsoleLog = true
      };
      m_server = new HydraServer(settings);

      HydraClientSettings clientSettings = new HydraClientSettings()
      {
        ApplicationName = "Client1",
        MaxUpdateHz = 30,
        UdpPort = 0,
        ServerHost = "localhost",
        ServerUdpPort = 52505,
        ServerTcpPort = 52700,
        UseRouting = true,
        LogFile = "hydraClient1.log",
        LogLevel = MEL.LogLevels.ALL,
        EnableConsoleLog = true
      };
      m_client1 = new HydraClient(clientSettings);
      m_client1.AddModule(new TestChatModule(m_client1));

      clientSettings = new HydraClientSettings(clientSettings)
      {
        ApplicationName = "Client2",
        LogFile = "hydraClient2.log"
      };
      m_client2 = new HydraClient(clientSettings);
      m_client2.AddModule(new TestChatModule(m_client2));
    }

    [TearDown]
    public void TestCleanUp()
    {
      m_server.Stop();
      m_client1.Stop();
      m_client2.Stop();

      m_server.Join();
      m_client1.Join();
      m_client2.Join();
    }

    /// <summary>
    /// Tests hydralizing a routing table since it makes use of some uncommon features with hydralizing dictionaries
    /// </summary>
    [Test]
    public void HydralizeTable()
    {
      HydraRoutingTable table = new HydraRoutingTable();
      Assert.IsTrue(table.Add(new HydraRouteInfo(m_client1.Uuid, new HydraRoutingEntry(m_server.Uuid, 0))));

      byte[] raw = Hydralizer.Hydralize(table, true, true);
      int offset = 0;
      HydraRoutingTable result = (HydraRoutingTable)Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(result);
      HydraUuid[] uuids = result.GetDestinations();
      Assert.IsNotNull(uuids);
      Assert.AreEqual(1, uuids.Length);
      Assert.AreEqual(m_client1.Uuid, uuids[0]);
    }

    [Test]
    public void TestClientRouting()
    {
      try
      {
        m_server.Start();
        m_client1.Start();
        m_client2.Start();

        Assert.IsTrue(m_client1.WaitForHandshake(5));
        Assert.IsTrue(m_client2.WaitForHandshake(5));
        Thread.Sleep(500);
        m_client2.SendHandshake(m_client1.Uuid);
        Thread.Sleep(500);
        Assert.IsNotNull(m_client2.GetProfile(m_client1.Uuid));
        m_client2.SendMessage(new NoopMsg(), m_client1.Uuid);
        Thread.Sleep(100);

        m_client2.GetModule<TestChatModule>().SendChat("Client 2", "Hello", m_client1.Uuid);
        Thread.Sleep(300);
        int messageCount = 0;
        for (int i = 0; i < 20 && messageCount < 1; ++i)
        {
          ChatInfo chatInfo = m_client1.GetModule<TestChatModule>().GetChat();
          if (chatInfo != null)
          {
            ++messageCount;
            Assert.AreEqual("Client 2", chatInfo.User);
            Assert.AreEqual("Hello", chatInfo.Msg);
            Assert.IsTrue(DateTime.Now - chatInfo.Time < new TimeSpan(0, 0, 3));
          }
          else
          {
            Thread.Sleep(50);
          }
        }

        Assert.AreEqual(1, messageCount);
      }
      finally
      {
        Thread.Sleep(500);
        m_server.Stop();
        m_client1.Stop();
        m_client2.Stop();

        m_server.Join();
        m_client1.Join();
        m_client2.Join();
      }
    }
  }
}
