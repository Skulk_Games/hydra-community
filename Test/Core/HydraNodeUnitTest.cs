#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;
using Skulk.Hydra.AutoDiscovery.Data;
using Skulk.Hydralize.Encoding;

namespace Skulk.Test
{
  public class HydraNodeUnitTest
  {
    private HydraNode[] m_nodes;
    private double m_keepAlivePeriod = 10;
    private double m_clientTimeoutPeriod = 30;
    private bool m_udpHandshake = false;
    private bool m_tcpStart = false;
    TranscoderPair m_encoder = null;
    TranscoderPair m_streamEncoder = null;
    private int m_port = 52500;
    private int m_streamPort = 52600;
    private double m_discoveryPeriod = 300;
    private double m_discoveryTimeout = 600;
    private int[] m_discoveryPorts = null;
    private bool m_useRouting = true;
    private bool m_addAllStaticNodes = false;

    public HydraNode[] Nodes { get => m_nodes; set => m_nodes = value; }
    public double KeepAlivePeriod { get => m_keepAlivePeriod; set => m_keepAlivePeriod = value; }
    public double ClientTimeoutPeriod { get => m_clientTimeoutPeriod; set => m_clientTimeoutPeriod = value; }
    public bool UdpHandshake { get => m_udpHandshake; set => m_udpHandshake = value; }
    public bool TcpStart { get => m_tcpStart; set => m_tcpStart = value; }
    public TranscoderPair Encoder { get => m_encoder; set => m_encoder = value; }
    public TranscoderPair StreamEncoder { get => m_streamEncoder; set => m_streamEncoder = value; }
    public int Port { get => m_port; set => m_port = value; }
    public int StreamPort { get => m_streamPort; set => m_streamPort = value; }
    public double DiscoveryPeriod { get => m_discoveryPeriod; set => m_discoveryPeriod = value; }
    public double DiscoveryTimeout { get => m_discoveryTimeout; set => m_discoveryTimeout = value; }
    public int[] DiscoveryPorts { get => m_discoveryPorts; set => m_discoveryPorts = value; }
    public bool UseRouting { get => m_useRouting; set => m_useRouting = value; }
    public bool AddAllStaticNodes { get => m_addAllStaticNodes; set => m_addAllStaticNodes = value; }

    public HydraNodeUnitTest(int nodeCount = 2)
    {
      m_nodes = new HydraNode[nodeCount];
    }

    [SetUp]
    public void TestSetup()
    {
      // Configure member values
      Configure();

      StaticNodeEndPoint[] eps = new StaticNodeEndPoint[m_nodes.Length];
      int[] discoveryPorts = new int[m_nodes.Length];
      for (int i = 0; i < m_nodes.Length; ++i)
      {
        eps[i] = new StaticNodeEndPoint(new HydraAddressData(null, "localhost", m_port + i, m_streamPort + i, HydraModes.NODE));
        discoveryPorts[i] = m_port + i;
      }

      // Initialize each node
      HydraNodeSettings baseSettings = new HydraNodeSettings()
      {
        ApplicationName = "HydraNode",
        UdpPort = m_port,
        MaxClients = m_nodes.Length,
        TcpPort = m_streamPort,
        DiscoveryPeriod = m_discoveryPeriod,
        DiscoveryTimeout = m_discoveryTimeout,
        MessageEncoder = m_encoder,
        StreamEncoder = m_streamEncoder,
        LogLevel = MEL.LogLevels.ALL,
        LogFile = "hydraNode",
        EnableConsoleLog = true,
        KeepAlivePeriod = m_keepAlivePeriod,
        ProfileTimeout = m_clientTimeoutPeriod,
        DiscoveryPorts = m_discoveryPorts ?? discoveryPorts,
        UseRouting = m_useRouting
      };
      for (int i = 0; i < m_nodes.Length; ++i)
      {
        HydraNodeSettings nodeSettings = new HydraNodeSettings(baseSettings)
        {
          ApplicationName = "HydraNode" + i.ToString(),
          UdpPort = m_port + i,
          TcpPort = m_streamPort + i,
          LogFile = "hydraNode" + i.ToString() + ".log"
        };

        if (!m_addAllStaticNodes)
        {
          eps = null;
        }

        m_nodes[i] = new HydraNode(nodeSettings, eps);
      }

      // Setup modules
      ModuleSetup();

      // Start each node
      for (int i = 0; i < m_nodes.Length; ++i)
      {
        m_nodes[i].Start();

        for (int j = 0; j < m_nodes.Length; ++j)
        {
          if (j == i)
          {
            continue;
          }

          if (m_udpHandshake)
          {
            m_nodes[i].SendHandshake(m_nodes[j].Address.GetUdpEndPoint());
          }

          if (m_tcpStart)
          {
            m_nodes[i].StartTcpConnection(m_nodes[j].Address);
          }
        }
      }

      CustomSetup();
    }

    [TearDown]
    public void TestCleanup()
    {
      for (int i = 0; i < m_nodes.Length; ++i)
      {
        m_nodes[i]?.Stop();
        m_nodes[i]?.Join();
      }

      CustomTestCleanup();
    }

    protected virtual void Configure()
    {
      // Empty
    }

    protected virtual void ModuleSetup()
    {
      // Empty
    }

    protected virtual void CustomSetup()
    {
      // Empty
    }

    protected virtual void CustomTestCleanup()
    {
      // Empty
    }
  }
}
