#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using NUnit.Framework;
using System.Threading;
using Skulk.Hydra;
using Skulk.Hydra.Network;

namespace Skulk.Test
{
  [TestFixture]
  public class TestReconnect : HydraUnitTest
  {
    private int m_profilesAdded = 0;
    private int m_profilesRemoved = 0;
    private int m_tcpConnectionsAdded = 0;
    private int m_udpConnectionsAdded = 0;

    public override void Configure()
    {
      base.Configure();
      UdpHandshake = false;
      TcpHandshake = false;
      ClientCount = 1;
      KeepAlivePeriod = 4;
      ClientTimeoutPeriod = 5;
    }

    public override void ModuleSetup()
    {
      // No modules - core testing
      Server.ProfileManager.OnProfileAdded += OnProfileAdded;
      Server.ProfileManager.OnProfileRemoved += OnProfileRemoved;
      Server.ProfileManager.OnConnectionAdded += OnConnectionAdded;
    }

    public override void CustomTestCleanup()
    {
      m_profilesAdded = 0;
      m_profilesRemoved = 0;
      m_tcpConnectionsAdded = 0;
      m_udpConnectionsAdded = 0;
    }

    [Test]
    public void TestUdpReconnect()
    {
      // Need to start up with a connection
      Client.SendHandshake();
      Assert.IsTrue(WaitForUdpConnection(Client));

      Client.Stop();
      Client.Join();
      Assert.IsTrue(WaitForUdpDisconnect(Client, 1));
      Assert.IsTrue(WaitForCondition(() => m_profilesRemoved == 1));
      Client.MaintainUdp = true;
      Client.Settings.KeepAlivePeriod = 2;
      Client.Start();
      Assert.IsTrue(WaitForUdpConnection(Client));

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    [Test]
    public void TestUdpServerReconnect()
    {
      // Need to start up with a connection
      Client.SendHandshake();
      Assert.IsTrue(WaitForUdpConnection(Client));
      Assert.AreEqual(1, m_profilesAdded);

      Server.Stop();
      Server.Join();
      Assert.IsTrue(WaitForUdpDisconnect(Server, 1));
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid == null, 10));
      Client.MaintainUdp = true;
      Server.Start();
      Assert.IsTrue(WaitForUdpConnection(Client));

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    /// <summary>
    /// Tests that a failed connection on the client will cause an immediate attempt to reconnect
    /// </summary>
    [Test]
    public void TestImmediateUdpReconnect()
    {
      // Need to start up with a connection
      Client.SendHandshake();
      Assert.IsTrue(WaitForUdpConnection(Client));
      Assert.AreEqual(1, m_profilesAdded);
      Assert.IsTrue(WaitForCondition(() => m_udpConnectionsAdded == 1, 5));

      // Close and fail the connection
      Client.MaintainUdp = true;
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null, 5));
      Client.GetProfile(Client.ServerUuid).GetConnection(ConnectionTypes.MESSAGE).Close(true);
      
      // A failed connection should trigger an immediate attempt to reconnect
      // so within a second the connection should be back.
      Assert.IsTrue(WaitForCondition(() => m_udpConnectionsAdded == 2, 1));

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    [Test]
    public void TestTcpReconnect()
    {
      Assert.IsTrue(Client.StartTcpConnection());
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 1, 5));
      Client.Stop();
      Client.Join();
      Assert.IsTrue(WaitForCondition(() => Client.ProfileCount == 0, 2));
      Assert.IsTrue(WaitForCondition(() => m_profilesRemoved == 1, 6));
      Client.MaintainStream(true);
      Client.TcpPeriod = 2;
      Client.Start();
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 2, 5));

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    [Test]
    public void TestTcpServerReconnect()
    {
      Assert.IsTrue(Client.StartTcpConnection());
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 1, 5));
      Server.Stop();
      Server.Join();
      Assert.IsTrue(WaitForCondition(() => Client.ProfileCount == 0, 6));
      Assert.IsTrue(WaitForCondition(() => m_profilesRemoved == 1, 6));
      Client.MaintainStream(true);
      Client.TcpPeriod = 2;
      Server.Start();
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 2, 5));
      Thread.Sleep(100);

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    /// <summary>
    /// Tests that a failed TCP connection on a client will cause an immediate attempt to reconnect
    /// </summary>
    [Test]
    public void TestImmediateTcpReconnect()
    {
      // Ensure TCP connections are added
      Assert.IsTrue(Client.StartTcpConnection());
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 1, 5), "Failed to connect to TCP at start, TCP connection count is {0}", m_tcpConnectionsAdded);
      Assert.IsTrue(WaitForCondition(() => Client.ServerUuid != null, 3));
      Assert.IsTrue(WaitForCondition(() => Client.GetProfile(Client.ServerUuid) != null, 3));
      Assert.IsTrue(WaitForCondition(() => Client.GetProfile(Client.ServerUuid).GetConnection(ConnectionTypes.STREAM) != null, 3));

      // Ensure the client is set to maintain TCP connections
      Client.MaintainStream(true);
      Connection originalConnection = Client.GetProfile(Client.ServerUuid).GetConnection(ConnectionTypes.STREAM);
      uint originalId = originalConnection.Id;

      // Trigger exception in TCP connection
      originalConnection.Close(true);

      // Expect original connection to be removed immediately
      Assert.IsTrue(WaitForCondition(() => Client.GetProfile(Client.ServerUuid).GetConnection(originalId) == null, 0.5));

      // Expect a new connection to be immediately attempted and made in less than a second
      Assert.IsTrue(WaitForCondition(() => m_tcpConnectionsAdded == 2, 1));
      Assert.IsTrue(WaitForCondition(() => Client.GetProfile(Client.ServerUuid).GetConnection(ConnectionTypes.STREAM) != null, 1));

      Client.MaintainUdp = false;
      Client.MaintainStream(false);
    }

    public bool WaitForUdpConnection(HydraCore hydra, double timeout = 15)
    {
      double time = 0;
      while (hydra.ProfileCount == 0 && time < timeout)
      {
        Thread.Sleep(100);
        time += 0.1;
      }

      return time < timeout;
    }

    public bool WaitForUdpDisconnect(HydraCore hydra, double timeout = 15)
    {
      double time = 0;
      while (hydra.ProfileCount != 0 && time < timeout)
      {
        Thread.Sleep(100);
        time += 0.1;
      }

      return time < timeout;
    }

    private void OnProfileAdded(Profile profile)
    {
      Console.WriteLine("Added profile: {0}", profile.Uuid);
      ++m_profilesAdded;
    }

    private void OnProfileRemoved(Profile profile)
    {
      ++m_profilesRemoved;
    }

    private void OnConnectionAdded(Connection connection)
    {
      if (connection.ConnectionType == ConnectionTypes.STREAM)
      {
        Console.WriteLine("TCP connection added");
        ++m_tcpConnectionsAdded;
      }
      else
      {
        ++m_udpConnectionsAdded;
      }
    }
  }
}
