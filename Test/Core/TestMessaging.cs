#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skulk.Hydra;
using System.Threading;
using Skulk.Hydra.Network;
using Skulk.Test.Core;

namespace Skulk.Test
{
  [TestFixture]
  public class TestMessaging : HydraUnitTest
  {
    int m_connections = 0;

    public TestMessaging() : base(1)
    {
      // Empty
    }

    public override void CustomTestCleanup()
    {
      m_connections = 0;
    }

    public override void Configure()
    {
      base.Configure();
      TcpHandshake = true;
      UdpHandshake = false;
      KeepAlivePeriod = 2;
      ClientTimeoutPeriod = 10;
    }

    public override void ModuleSetup()
    {
      base.ModuleSetup();
      Client.ProfileManager.OnConnectionAdded += OnConnectionAdded;
      Client.AddModule(new SimpleModule(Client));
      Client.DefaultModule = Client.GetModule<SimpleModule>();
    }

    [Test]
    public void TestTcpMessaging()
    {
      // TODO: figure out a good message for testing this automatically
      Assert.IsTrue(Client.WaitForTcpConnection(5));
      Assert.IsTrue(WaitForCondition(() => m_connections == 1, 5));
      // Manually check output logs to see if error was handled by HydraCore handler
      Client.SendMessage(new HydraErrorMsg(new HydraErrorInfo(HydraErrors.INVALID_PORT)), Client.ServerUuid);
      Client.SendMessage(new NoopMsg(), Client.ServerUuid);
      Thread.Sleep(1000);
    }

    [Test]
    public void TestTcpRouting()
    {
      //Client.ConfigureDefaultPaths(HydraPaths.TCP);
      // All keep alives should be going over TCP now
      Thread.Sleep(5000);
    }

    /// <summary>
    /// Tests sending messages to the local application.
    /// </summary>
    [Test]
    public void LocalMessages()
    {
      // Send a noop, this gets automatically handled and ACK'd by the core logic
      var noop = new NoopMsg();
      Client.SendMessage(noop, Client.Uuid);

      // Send another (unhandled) type of message that will go to the default module
      Message msg1 = new Message(10000, 0);
      Client.SendMessage(msg1, Client.Uuid);

      // Confirm we got the message on the default module
      var simple = Client.GetModule<SimpleModule>();
      Assert.IsNotNull(simple);
      Assert.AreEqual(1, simple.Queue.Count);
      Tuple<HydraUuid, Message> msgPair = simple.Queue.Dequeue();
      Assert.AreEqual(msg1.Command, msgPair.Item2.Command);
      Assert.AreEqual(Client.Uuid, msgPair.Item1);
    }

    private void OnConnectionAdded(Connection connection)
    {
      Console.WriteLine("Connection added");
      ++m_connections;
    }
  }
}
