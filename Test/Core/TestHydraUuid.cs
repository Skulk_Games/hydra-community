#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;

namespace Skulk.Test.Core
{
  public class TestHydraUuid
  {
    [Test]
    public void CopyConstructor()
    {
      HydraUuid uuid1 = new HydraUuid();
      Assert.IsNotNull(uuid1.Uuid);
      Assert.AreEqual(16, uuid1.Uuid.Length);

      HydraUuid uuid2 = new HydraUuid(uuid1);
      Assert.IsNotNull(uuid2.Uuid);
      Assert.AreEqual(16, uuid2.Uuid.Length);

      for (int i = 0; i < uuid1.Uuid.Length; ++i)
      {
        Assert.AreEqual(uuid1.Uuid[i], uuid2.Uuid[i]);
      }
    }

    [Test]
    public void ParseUuid()
    {
      HydraUuid uuid = new HydraUuid();
      HydraUuid parseUuid = new HydraUuid(HydraUuid.Parse(uuid.ToString()));
      Assert.AreEqual(uuid, parseUuid);

      Assert.IsNull(HydraUuid.Parse(""));
      Assert.IsNull(HydraUuid.Parse("ababababab"));
      Assert.IsNull(HydraUuid.Parse("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaag"));
      Assert.IsNotNull(HydraUuid.Parse("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"));
    }
  }
}
