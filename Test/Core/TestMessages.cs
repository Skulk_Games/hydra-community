#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Skulk.Test.Core
{
  /// <summary>
  /// Tests for the <see cref="Message"/> class
  /// </summary>
  public class TestMessages
  {
    /// <summary>
    /// Test the message command code
    /// </summary>
    [Test]
    public void CommandCode()
    {
      ushort code = 1;
      Message msg = new Message(code, 0);
      Assert.AreEqual(code, msg.Command);
      Assert.AreEqual(code, msg.GetCmd());

      code = 501;
      msg.SetCmd(code);
      Assert.AreEqual(code, msg.Command);
      Assert.AreEqual(code, msg.GetCmd());

      code = 15550;
      msg.Command = code;
      Assert.AreEqual(code, msg.Command);
      Assert.AreEqual(code, msg.GetCmd());
    }

    /// <summary>
    /// Tests deserializing a partial message and the value of the PartialIndex as it progresses
    /// </summary>
    [Test]
    public void PartialIndexParsing()
    {
      ushort code = 10;
      byte[] data = new byte[13];
      Random rand = new Random();
      rand.NextBytes(data);

      Message msg = new Message(code, (ushort)data.Length);
      msg.SetPayload(data);
      byte[] raw = msg.Serialize();
      Message parseMsg = new Message();

      uint chunk1Length = (uint)(Message.HEADER_SIZE + Message.GetSyncLength() + 1);

      Assert.IsFalse(parseMsg.Deserialize(raw, 0, chunk1Length, out uint leftOverIndex));
      Assert.AreEqual((uint)(Message.HEADER_SIZE + Message.GetSyncLength()), leftOverIndex);
      Assert.IsTrue(parseMsg.IsPartial);
      Assert.AreEqual(0, parseMsg.PartialIndex);
      uint index = leftOverIndex;
      Assert.IsFalse(parseMsg.Deserialize(raw, (uint)leftOverIndex, 1, out leftOverIndex));
      Assert.AreEqual(index + 1, leftOverIndex);
      Assert.IsTrue(parseMsg.IsPartial);
      Assert.AreEqual(1, parseMsg.PartialIndex);
      Assert.IsTrue(parseMsg.Deserialize(raw, chunk1Length, (uint)(raw.Length - chunk1Length), out leftOverIndex));
      Assert.AreEqual(raw.Length, leftOverIndex);
      Assert.IsFalse(parseMsg.IsPartial);
      Assert.AreEqual(-1, parseMsg.PartialIndex);
    }

    /// <summary>
    /// Test using the extended message header
    /// </summary>
    [Test]
    public void TestExtendedMessages()
    {
      byte[] data = new byte[500000];
      Random rand = new Random();
      rand.NextBytes(data);

      List<Message> messages = Message.MakeSequence(1, data);
      Assert.AreEqual(data.Length / Message.MAX_PAYLOAD_SIZE + 1, messages.Count);
      int totalLength = 0;
      for (int i = 0; i < messages.Count; ++i)
      {
        Assert.AreEqual(1, messages[i].GetCmd());
        Assert.IsTrue(messages[i].IsExtended());
        Assert.AreEqual(messages.Count, messages[i].GetCount());
        Assert.AreEqual(i + 1, messages[i].GetIndex());
        int offset = (int)(i * Message.MAX_PAYLOAD_SIZE);
        totalLength += messages[i].GetLength();
        for (int j = 0; j < messages[i].GetLength(); ++j)
        {
          Assert.AreEqual(messages[i].Payload[j], data[offset + j]);
        }
      }

      Assert.AreEqual(data.Length, totalLength);
    }

    /// <summary>
    /// Test serializing extended messages
    /// </summary>
    [Test]
    public void TestExtendedMessageSerialization()
    {
      byte[] data = new byte[100000];
      Random rand = new Random();
      rand.NextBytes(data);

      List<Message> messages = Message.MakeSequence(10, data);
      System.IO.MemoryStream stream = new System.IO.MemoryStream();
      foreach (Message msg in messages)
      {
        stream.Write(msg.Serialize());
      }

      // Now try to deserialize from a stream
      List<Message> receivedMsgs = Message.Deserialize(stream.GetBuffer());

      Assert.AreEqual(messages.Count, receivedMsgs.Count);
      for (int i = 0; i < receivedMsgs.Count; ++i)
      {
        Assert.AreEqual(messages[i].GetCmd(), receivedMsgs[i].GetCmd());
        Assert.AreEqual(messages[i].IsExtended(), receivedMsgs[i].IsExtended());
        Assert.AreEqual(messages[i].GetLength(), receivedMsgs[i].GetLength());
        Assert.AreEqual(messages[i].GetIndex(), receivedMsgs[i].GetIndex());
        Assert.AreEqual(messages[i].GetCount(), receivedMsgs[i].GetCount());

        // Check data
        for (int j = 0; j < receivedMsgs[i].GetLength(); ++j)
        {
          Assert.AreEqual(messages[i].Payload[j], receivedMsgs[i].Payload[j]);
        }
      }
    }

    /// <summary>
    /// Test serializing a message with a large payload automatically into a sequence.
    /// </summary>
    [Test]
    public void TestExtendedSerialization()
    {
      byte[] data = new byte[300000];
      Random rand = new Random();
      rand.NextBytes(data);

      Message msg = new Message(1, 1);
      msg.SetId(1);
      msg.SetPayload(data);

      byte[] rawSequence = msg.Serialize();
      Assert.IsTrue(rawSequence.Length > data.Length);

      // Deserialize a sequence
      List<Message> messages = Message.Deserialize(rawSequence);

      Assert.AreEqual(Math.Ceiling(data.Length / (double)Message.MAX_PAYLOAD_SIZE), messages.Count);
      for (int i = 0; i < messages.Count; ++i)
      {
        Assert.IsTrue(messages[i].IsExtended());
        Assert.AreEqual(msg.GetCmd(), messages[i].GetCmd());
        Assert.AreEqual(msg.GetId(), messages[i].GetId());
        Assert.AreEqual(Math.Min(data.Length - (i * Message.MAX_PAYLOAD_SIZE), Message.MAX_PAYLOAD_SIZE), messages[i].GetLength());
        Assert.AreEqual(i + 1, messages[i].GetIndex());
        Assert.AreEqual(messages.Count, messages[i].GetCount());

        // Check data
        for (int j = 0; j < messages[i].GetLength(); ++j)
        {
          Assert.AreEqual(data[(i * Message.MAX_PAYLOAD_SIZE) + j], messages[i].Payload[j]);
        }
      }
    }

    class MsgData
    {
      private int m_id;
      private string m_name;

      public int Id { get => m_id; set => m_id = value; }
      public string Name { get => m_name; set => m_name = value; }
    }

    /// <summary>
    /// Tests setting and getting the message flags
    /// </summary>
    [Test]
    public void TestFlags()
    {
      MsgData data = new MsgData()
      {
        Id = 23,
        Name = "Bob"
      };

      // Build a message from scratch and set the flag manually
      byte[] raw = Hydralize.Hydralizer.Hydralize(data);
      Message msg = new Message(11110, (ushort)raw.Length, (ushort)MessageFlagValues.HYDRALIZED_OBJECT);
      msg.SetPayload(raw);
      Assert.IsTrue(msg.IsHydralized());

      // The obj message should set the flag automatically
      HydraObjMsg objMsg = new HydraObjMsg(11111, 0, data, false, false);
      Assert.IsTrue(objMsg.IsHydralized());

      Tuple<MessageFlagValues, Func<Message, bool>>[] checks = new Tuple<MessageFlagValues, Func<Message, bool>>[]
      {
        Tuple.Create<MessageFlagValues, Func<Message, bool>>(MessageFlagValues.ANONYMOUS, (x) => x.IsAnonymous()),
        Tuple.Create<MessageFlagValues, Func<Message, bool>>(MessageFlagValues.GUARANTEED, (x) => x.IsGuaranteed()),
        Tuple.Create<MessageFlagValues, Func<Message, bool>>(MessageFlagValues.EXTENDED, (x) => x.IsExtended()),
        Tuple.Create<MessageFlagValues, Func<Message, bool>>(MessageFlagValues.HYDRALIZED_OBJECT, (x) => x.IsHydralized()),
      };

      // Test all the combinations of known flags
      msg = new Message(100, 0);
      for (int i = 0; i < checks.Length; ++i)
      {
        msg.SetFlags(0);
        Assert.AreEqual(0, msg.GetFlags());
        msg.SetFlags((ushort)checks[i].Item1);
        Assert.AreEqual((ushort)checks[i].Item1, msg.GetFlags());
        Assert.IsTrue(checks[i].Item2.Invoke(msg), "Check failed for: {0}", checks[i].Item1);

        for (int j = 0; j < checks.Length; ++j)
        {
          if (i == j)
          {
            continue;
          }

          ushort flags = msg.GetFlags();
          flags |= (ushort)checks[j].Item1;
          msg.SetFlags(flags);
          Assert.AreEqual(flags, msg.GetFlags());
          for (int k = 0; k <= j; ++k)
          {
            Assert.IsTrue(checks[k].Item2.Invoke(msg), "Check failed for: {0}, with flags: {1}", checks[k].Item1, flags);
          }

          if (i > j)
          {
            Assert.IsTrue(checks[i].Item2.Invoke(msg));
          }
        }
      }
    }

    /// <summary>
    /// Test setting and getting the message ID
    /// </summary>
    [Test]
    public void TestId()
    {
      Message msg = new Message(10000, 0);

      Random rand = new Random();
      for (int i = 0; i < 10; ++i)
      {
        ushort id = (ushort)rand.Next(UInt16.MaxValue);
        msg.SetId(id);
        Assert.AreEqual(id, msg.GetId());
        Message parseMsg = new Message();
        byte[] raw = msg.Serialize();
        Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out uint index));
        Assert.AreEqual(id, parseMsg.GetId());
      }
    }

    /// <summary>
    /// Test the message ToString method
    /// </summary>
    [Test]
    public void TestToString()
    {
      Message msg = new Message(0, 0);
      string msgString = msg.ToString();
      // Sync bytes and all 0's for the length of the header
      Assert.AreEqual("[ 55  AB  00  00  00  00  00  00  00  00  00  01 ]", msgString);
      msg.Command = 10;
      msgString = msg.ToString();
      Assert.AreEqual("[ 55  AB  00  0A  00  00  00  00  00  00  00  0B ]", msgString);
      msg.SetLength(2);
      msgString = msg.ToString();
      Assert.AreEqual("[ 55  AB  00  0A  00  02  00  00  00  00  00  0D  00  00 ]", msgString);
      Message.SetSync(new byte[] { 0xAA, 0xBB });
      msgString = msg.ToString();
      Assert.AreEqual("[ AA  BB  00  0A  00  02  00  00  00  00  00  0D  00  00 ]", msgString);
      Message.SetSync(new byte[] { 0x11, 0x22, 0x33, 0x44 });
      msgString = msg.ToString();
      Assert.AreEqual("[ 11  22  33  44  00  0A  00  02  00  00  00  00  00  0D  00  00 ]", msgString);
      Message.SetSync(null);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  00  00  00  00  0D  00  00 ]", msgString);
      msg.SetFlags((ushort)(MessageFlagValues.ANONYMOUS | MessageFlagValues.GUARANTEED));
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  05  00  00  00  12  00  00 ]", msgString);
      msg.SetFlags((ushort)MessageFlagValues.EXTENDED);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  08  00  00  00  15  00  00  00  00  00  00 ]", msgString);
      msg.SetIndex(3);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  08  00  00  00  15  00  03  00  00  00  00 ]", msgString);
      msg.SetCount(16);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  08  00  00  00  15  00  03  00  10  00  00 ]", msgString);
      msg.SetPayload(0, 255);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  08  00  00  00  15  00  03  00  10  FF  00 ]", msgString);
      msg.SetPayload(1, 0xBB);
      msgString = msg.ToString();
      Assert.AreEqual("[ 00  0A  00  02  00  08  00  00  00  15  00  03  00  10  FF  BB ]", msgString);

      // Set sync back to default
      Message.SetSync(new byte[] { 0x55, 0xAB });
    }

    /// <summary>
    /// Tests setting payload values
    /// </summary>
    [Test]
    public void SetPayload()
    {
      Message msg = new Message(100, 100);
      byte[] data = new byte[100];
      Random rand = new Random();
      rand.NextBytes(data);
      msg.SetPayload(data);
      for (int i = 0; i < data.Length; ++i)
      {
        Assert.AreEqual(data[i], msg.Payload[i]);
      }

      for (int t = 0; t < 3; ++t)
      {
        if (t == 1)
        {
          msg.SetPayload(null);
        }
        else if (t == 2)
        {
          msg.SetPayload(new byte[1]);
        }

        rand.NextBytes(data);
        for (ushort i = 0; i < data.Length; ++i)
        {
          msg.SetPayload(i, data[i]);
          Assert.AreEqual(data[i], msg.Payload[i]);
        }
      }

      for (int t = 0; t < 3; ++t)
      {
        if (t == 1)
        {
          msg.SetPayload(null);
        }
        else if (t == 2)
        {
          msg.SetPayload(new byte[1]);
        }

        rand.NextBytes(data);
        msg.SetPayload(data, 0, 0, (ushort)data.Length);
        for (int i = 0; i < data.Length; ++i)
        {
          Assert.AreEqual(data[i], msg.Payload[i]);
        }
      }

      for (int t = 0; t < 3; ++t)
      {
        if (t == 1)
        {
          msg.SetPayload(null);
        }
        else if (t == 2)
        {
          msg.SetPayload(new byte[1]);
        }

        rand.NextBytes(data);
        List<byte> listData = new List<byte>(data);
        msg.SetPayload(listData, 0, 0, (ushort)listData.Count);
        for (int i = 0; i < data.Length; ++i)
        {
          Assert.AreEqual(listData[i], msg.Payload[i]);
        }
      }
    }

    /// <summary>
    /// Test setting the sync bytes for messages
    /// </summary>
    [Test]
    public void SetSync()
    {
      ushort payloadLength = 10;
      Message msg = new Message(1, payloadLength);
      Message parseMsg = new Message();
      byte[] raw = msg.Serialize();
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out uint index));

      byte[] sync = null;
      Message.SetSync(sync);
      Assert.IsNull(Message.GetSync());
      Assert.AreEqual(0, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA, 0xBB };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA, 0xBB, 0xCC };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA, 0xBB, 0xCC, 0xDD };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      sync = new byte[] { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };
      Message.SetSync(sync);
      Enumerable.SequenceEqual(sync, Message.GetSync());
      Assert.AreEqual(sync.Length, Message.GetSyncLength());
      raw = msg.Serialize();
      Assert.AreEqual(Message.HEADER_SIZE + sync.Length + payloadLength, raw.Length);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));

      Message.SetSync(new byte[] { 0x55, 0xAB });
    }

    /// <summary>
    /// Test setting and calculating the checksum by deserializing valid and invalid checksum values
    /// </summary>
    [Test]
    public void TestChecksum()
    {
      Message msg = new Message(10, 5, (ushort)MessageFlagValues.ANONYMOUS);
      Message parseMsg = new Message();
      byte[] raw = msg.Serialize();

      // Check deserializing a good message
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out uint index));

      // Corrupt the checksum
      int syncLength = Message.GetSyncLength();
      int offset = (int)(Message.CHECKSUM_OFFSET + syncLength);
      ushort checksum = msg.GetCheckSum();
      Hydralize.Hydralizer.Put(raw, checksum + 1, ref offset, false);
      Assert.IsFalse(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));
      Assert.AreEqual(0, index);

      // Corrupt the other way
      offset = (int)(Message.CHECKSUM_OFFSET + syncLength);
      Hydralize.Hydralizer.Put(raw, checksum - 1, ref offset, false);
      Assert.IsFalse(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));
      Assert.AreEqual(0, index);

      // Put the correct checksum back
      offset = (int)(Message.CHECKSUM_OFFSET + syncLength);
      Hydralize.Hydralizer.Put(raw, checksum, ref offset, false);
      Assert.IsTrue(parseMsg.Deserialize(raw, 0, (uint)raw.Length, out index));
      Assert.AreEqual(raw.Length, index);
    }

    /// <summary>
    /// Tests deserializing valid and invalid headers
    /// </summary>
    [Test]
    public void DeserializeHeader()
    {
      Message msg = new Message(123, 0);
      Message parsedMsg = new Message();

      // Use no sync to focus on just header deserialization
      Message.SetSync(null);

      byte[] raw = msg.Serialize();
      Assert.IsTrue(parsedMsg.Deserialize(raw, 0, (uint)raw.Length, out uint index));

      byte[] small = new byte[raw.Length - 1];
      Array.Copy(raw, 0, small, 0, small.Length);
      Assert.IsFalse(parsedMsg.Deserialize(small, 0, (uint)small.Length, out index));

      byte[] large = new byte[raw.Length + 1];
      Array.Copy(raw, 0, large, 0, raw.Length);
      Assert.IsTrue(parsedMsg.Deserialize(large, 0, (uint)large.Length, out index));

      large = new byte[raw.Length + 100];
      Array.Copy(raw, 0, large, 100, raw.Length);
      Assert.IsFalse(parsedMsg.Deserialize(large, 0, (uint)large.Length, out index));

      // Use a sync again to show we can parse the header once we know where it is
      Message.SetSync(new byte[] { 0x55, 0xAB });
      Array.Copy(Message.GetSync(), 0, large, 100 - Message.GetSyncLength(), Message.GetSyncLength());
      Assert.IsTrue(parsedMsg.Deserialize(large, 0, (uint)large.Length, out index));
    }

    /// <summary>
    /// Test finding sync patterns to deserialize messages
    /// </summary>
    [Test]
    public void TestSyncing()
    {
      Message msg = new Message(123, 0);
      Message parsedMsg = new Message();

      // Set a stupid repeating sync to test some extra paths in sync matching logic
      Message.SetSync(new byte[] { 0xAA, 0xAA, 0xBB, 0xBB });

      byte[] raw = msg.Serialize();
      byte[] repeating = new byte[raw.Length + 10];
      Array.Copy(raw, 0, repeating, 3, raw.Length);
      for (int i = 0; i < 3; ++i)
      {
        repeating[i] = 0xAA;
      }

      Assert.IsTrue(parsedMsg.Deserialize(repeating, 0, (uint)repeating.Length, out uint index));

      // Set a non repeating sync but throw in some tricky partial matches
      Message.SetSync(new byte[] { 0xAA, 0xBB, 0xCC });
      raw = msg.Serialize();
      repeating = new byte[raw.Length + 10];
      Array.Copy(raw, 0, repeating, 5, raw.Length);
      repeating[4] = 0xBB;
      repeating[3] = 0xAA;
      repeating[2] = 0xAA;
      repeating[1] = 0xAA;
      Assert.IsTrue(parsedMsg.Deserialize(repeating, 0, (uint)repeating.Length, out index));

      // Now for good measure test some parsing with random noise in front of the message
      // This could fail very very rarely since the random noise could generate the sync pattern and then
      // a message should fail to parse since it will most likely fail the checksum at that point but it's a small
      // enough chance that I don't think we need to worry about it
      for (int i = 0; i < 10; ++i)
      {
        byte[] data = new byte[raw.Length + 100];
        Random rand = new Random();
        rand.NextBytes(data);
        Array.Copy(raw, 0, data, 100, raw.Length);
        Assert.IsTrue(parsedMsg.Deserialize(data, 0, (uint)data.Length, out index));
        Assert.AreEqual(data.Length, index);
      }

      Message.SetSync(new byte[] { 0x55, 0xAB });
    }
  }
}
