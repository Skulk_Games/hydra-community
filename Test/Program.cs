using NUnitLite;

namespace NUnitLite.Tests
{
  public static class Program
  {
    public static int Main(string[] args)
    {
      return new AutoRun().Execute(args);
    }
  }
}
