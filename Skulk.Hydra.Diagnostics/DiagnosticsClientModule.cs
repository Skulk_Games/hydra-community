#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Diagnostics.Data;
using Skulk.Hydra.Diagnostics.Messages;
using Skulk.MEL;
using System;

namespace Skulk.Hydra.Diagnostics
{
  /// <summary>
  /// Module for subscribing to remote diagnostic events
  /// </summary>
  public class DiagnosticsClientModule : HydraModule
  {
    /// <summary>
    /// A value indicating whether diagnostic event information will be logged
    /// </summary>
    private bool m_logDiagnostics;

    /// <summary>
    /// The path to the file where diagnostic information will be logged
    /// </summary>
    private string m_diagnosticsFile;

    /// <summary>
    /// The diagnostic logger instance
    /// </summary>
    private Logger m_logger;

    /// <summary>
    /// Event invoked when a network event is received
    /// </summary>
    public event EventHandler<NetworkEvent> NetworkEventReceived;

    /// <summary>
    /// Initializes this module with the specified values
    /// </summary>
    /// <param name="hydra">The parent application</param>
    /// <param name="enableDiagnosticLogging">A value indicating whether diagnostic event information should be logged</param>
    /// <param name="diagnosticLog">The path to the file where diagnostic info will be logged</param>
    public DiagnosticsClientModule(HydraCore hydra, bool enableDiagnosticLogging = false, string diagnosticLog = "diagnostics.log") : base(hydra)
    {
      m_logDiagnostics = enableDiagnosticLogging;
      m_diagnosticsFile = diagnosticLog;
      SetMessageHandler(DiagnosticCodes.NetworkEvent, HandleNetworkEvent, "NetworkEvent");
    }

    /// <inheritdoc/>
    public override void ModuleStartup()
    {
      base.ModuleStartup();

      if (m_logDiagnostics)
      {
        if (m_logger != null)
        {
          m_logger.Stop();
          m_logger.Join();
        }

        m_logger = new Logger(m_diagnosticsFile, 255);
        m_logger.EnableConsole = false;
        m_logger.Start();
      }
    }

    /// <inheritdoc/>
    public override void ModuleShutdown()
    {
      base.ModuleShutdown();

      if (m_logger != null)
      {
        m_logger.Stop();
        m_logger.Join();
        m_logger = null;
      }
    }

    /// <summary>
    /// Sends a subscription message to the server for the specified event type
    /// </summary>
    /// <param name="subType">The type of event to subscribe to</param>
    /// <param name="uuid">The UUID of the application to send the message to, if null the first known server UUID will be used</param>
    public void Subscribe(SubscriptionType subType, HydraUuid uuid = null)
    {
      if (uuid == null)
      {
        uuid = Parent.ServerUuid;
      }

      SubscriptionInfo subInfo = new SubscriptionInfo()
      {
        Type = subType
      };

      Parent.SendMessage(new SubscriptionMsg(subInfo), uuid);
    }

    /// <summary>
    /// Handles a received <see cref="NetworkEventMsg"/>
    /// </summary>
    /// <param name="message">The received network event message</param>
    /// <param name="uuid">The UUID of the application that sent the message</param>
    private void HandleNetworkEvent(Message message, HydraUuid uuid)
    {
      NetworkEventMsg eventMsg = new NetworkEventMsg(message);
      NetworkEvent netEvent = eventMsg.GetObject<NetworkEvent>();
      if (netEvent != null)
      {
        m_logger?.LogInfo("{0}", netEvent);
        try
        {
          NetworkEventReceived?.Invoke(this, netEvent);
        }
        catch (Exception ex)
        {
          Parent.Log.LogError("Exception in NetworkEventReceived event handler: '{0}'", ex.Message);
        }
      }
    }
  }
}
