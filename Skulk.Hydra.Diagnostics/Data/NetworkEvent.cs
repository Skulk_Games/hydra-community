#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using System;

namespace Skulk.Hydra.Diagnostics.Data
{
  /// <summary>
  /// Enumeration of the types of network events
  /// </summary>
  public enum NetworkEvents
  {
    /// <summary>
    /// A connection was added
    /// </summary>
    ConnectionAdded,

    /// <summary>
    /// A connection was removed
    /// </summary>
    ConnectionRemoved,

    /// <summary>
    /// A connection failed and closed
    /// </summary>
    ConnectionFailed,

    /// <summary>
    /// A new profile was added
    /// </summary>
    ProfileAdded,

    /// <summary>
    /// A profile was removed
    /// </summary>
    ProfileRemoved
  }

  /// <summary>
  /// Contains information about a network event
  /// </summary>
  public class NetworkEvent
  {
    /// <summary>
    /// The type of the event
    /// </summary>
    private NetworkEvents m_event;

    /// <summary>
    /// Information about the profile involved in the event
    /// </summary>
    private ProfileDiagnostics m_profile;

    /// <summary>
    /// Information about the connection involved in the event
    /// </summary>
    private ConnectionDiagnostics m_connection;

    /// <summary>
    /// Default constructor.
    /// </summary>
    public NetworkEvent()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this object with the specified event and connection information.
    /// </summary>
    /// <param name="netEvent">The type of network event that occurred.</param>
    /// <param name="connection">The connection related to the event.</param>
    public NetworkEvent(NetworkEvents netEvent, Connection connection) : this(netEvent)
    {
      m_profile = connection.Profile.GetDiagnostics();
      m_connection = connection.GetDiagnostics();
    }

    public NetworkEvent(NetworkEvents netEvent, Profile profile) : this(netEvent)
    {
      m_profile = profile.GetDiagnostics();
      // m_connection will be left null for profile events
    }

    /// <summary>
    /// Initializes the event type.
    /// </summary>
    /// <param name="netEvent">The type of the network event.</param>
    private NetworkEvent(NetworkEvents netEvent)
    {
      m_event = netEvent;
    }

    /// <summary>
    /// The type of event being described
    /// </summary>
    public NetworkEvents Event { get => m_event; set => m_event = value; }

    /// <summary>
    /// Information about the profile involved in the event
    /// </summary>
    public ProfileDiagnostics Profile { get => m_profile; set => m_profile = value; }

    /// <summary>
    /// Information about the connection involved in the event, if applicable
    /// </summary>
    public ConnectionDiagnostics Connection { get => m_connection; set => m_connection = value; }

    /// <summary>
    /// Creates a string representation of the event information
    /// </summary>
    /// <returns>The string representation of this event</returns>
    public override string ToString()
    {
      return String.Format("Network Event\nType: {0}\n{1}{2}", Event, Profile, Connection?.ToString() ?? "");
    }
  }
}
