#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Diagnostics.Data;
using Skulk.Hydra.Diagnostics.Messages;
using Skulk.Hydra.Network;

namespace Skulk.Hydra.Diagnostics
{
  /// <summary>
  /// A module that exposes diagnostic information of the parent Hydra application.
  /// </summary>
  public class DiagnosticsModule : HydraModule
  {
    /// <summary>
    /// Manages subscribers
    /// </summary>
    private SubscriptionManager networkSubscriptions;

    /// <summary>
    /// Initializes this module
    /// </summary>
    /// <param name="hydra">The parent application</param>
    public DiagnosticsModule(HydraCore hydra) : base(hydra)
    {
      networkSubscriptions = new SubscriptionManager(hydra);
      hydra.ProfileManager.OnProfileAdded += HandleProfileAdded;
      hydra.ProfileManager.OnConnectionAdded += HandleConnectionAdded;
      hydra.ProfileManager.OnProfileRemoved += HandleProfileRemoved;
      hydra.ProfileManager.OnConnectionRemoved += HandleConnectionRemoved;
      hydra.ProfileManager.OnConnectionFailed += HandleConnectionFailed;

      SetMessageHandler(DiagnosticCodes.Subscription, HandleSubscription, "DiagnosticSubscription");
    }

    /// <summary>
    /// Gets an array of the UUIDs of subscribers
    /// </summary>
    /// <returns>An array of the UUIDs for subscribed applications</returns>
    public HydraUuid[] GetSubscribers()
    {
      return networkSubscriptions.GetSubscribers();
    }

    /// <summary>
    /// Removes subscriptions for the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the subscriber to remove</param>
    private void RemoveSubscriptions(HydraUuid uuid)
    {
      networkSubscriptions.Remove(uuid);
    }

    /// <summary>
    /// Handles a received <see cref="SubscriptionMsg"/>
    /// </summary>
    /// <param name="message">The message received</param>
    /// <param name="uuid">The UUID of the application that sent the message</param>
    private void HandleSubscription(Message message, HydraUuid uuid)
    {
      SubscriptionMsg subscriptionMsg = new SubscriptionMsg(message);
      SubscriptionInfo subscriptionInfo = subscriptionMsg.GetObject<SubscriptionInfo>();
      if (subscriptionInfo != null)
      {
        switch (subscriptionInfo.Type)
        {
          case SubscriptionType.Network:
          {
            networkSubscriptions.Add(uuid);
            break;
          }

          default:
          {
            Parent.Log.LogError("Unknown diagnostic subscription type: {0}", subscriptionInfo.Type);
            break;
          }
        }
      }
      else
      {
        Parent.Log.LogError("Failed to get subscription object from diagnostic subscription message from UUID: {0}", uuid);
      }
    }

    /// <summary>
    /// Handles the event for added profiles
    /// </summary>
    /// <param name="profile">The profile that was added</param>
    private void HandleProfileAdded(Profile profile)
    {
      NetworkEvent netEvent = new NetworkEvent(NetworkEvents.ProfileAdded, profile);
      NetworkEventMsg msg = new NetworkEventMsg(netEvent);
      networkSubscriptions.SendMessage(msg);
    }

    /// <summary>
    /// Handles the event for added connections
    /// </summary>
    /// <param name="connection">The connection that was added</param>
    private void HandleConnectionAdded(Connection connection)
    {
      NetworkEvent netEvent = new NetworkEvent(NetworkEvents.ConnectionAdded, connection);
      NetworkEventMsg msg = new NetworkEventMsg(netEvent);
      networkSubscriptions.SendMessage(msg);
    }

    /// <summary>
    /// Handles the event for removed profiles
    /// </summary>
    /// <param name="profile">The profile that was removed</param>
    private void HandleProfileRemoved(Profile profile)
    {
      RemoveSubscriptions(profile.Uuid);
      NetworkEvent netEvent = new NetworkEvent(NetworkEvents.ProfileRemoved, profile);
      NetworkEventMsg msg = new NetworkEventMsg(netEvent);
      networkSubscriptions.SendMessage(msg);
    }

    /// <summary>
    /// Handles the event for removed connections
    /// </summary>
    /// <param name="connection">The connection that was removed</param>
    private void HandleConnectionRemoved(Connection connection)
    {
      NetworkEvent netEvent = new NetworkEvent(NetworkEvents.ConnectionRemoved, connection);
      NetworkEventMsg msg = new NetworkEventMsg(netEvent);
      networkSubscriptions.SendMessage(msg);
    }

    /// <summary>
    /// Handles the event for connections that failed and closed
    /// </summary>
    /// <param name="connection">The connection that failed</param>
    private void HandleConnectionFailed(Connection connection)
    {
      Parent.Log.LogDebug("Handling failed connection for profile: {0}, connection: {1}", connection.Profile.Uuid, connection.Id);
      NetworkEvent netEvent = new NetworkEvent(NetworkEvents.ConnectionFailed, connection);
      NetworkEventMsg msg = new NetworkEventMsg(netEvent);
      networkSubscriptions.SendMessage(msg);
    }
  }
}
