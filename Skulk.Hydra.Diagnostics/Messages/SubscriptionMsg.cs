#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Diagnostics.Data;

namespace Skulk.Hydra.Diagnostics.Messages
{
  /// <summary>
  /// A message used to subscribe to diagnostic events
  /// </summary>
  public class SubscriptionMsg : HydraObjMsg
  {
    /// <summary>
    /// Initializes a new message with the specified subscription information
    /// </summary>
    /// <param name="subscription">The object containing the subscription information</param>
    public SubscriptionMsg(SubscriptionInfo subscription) : base(DiagnosticCodes.Subscription, 0, subscription, true)
    {
      // Empty
    }

    /// <summary>
    /// Initializes this message with the data in the specified message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public SubscriptionMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
