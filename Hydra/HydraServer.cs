#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Skulk.Hydra
{
  /// <summary>
  /// A server implementation of a Hydra application
  /// </summary>
  public class HydraServer : HydraCore
  {
    private TcpListener m_tcpListener;
    private Thread m_listenerThread;
    private int m_listenPollPeriod = 50;

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraServer"/> class
    /// </summary>
    /// <param name="settings">The application configuration settings</param>
    public HydraServer(HydraServerSettings settings)
      : base(settings)
    {
      Address.StreamPort = settings.TcpPort;

      if (!IPAddress.TryParse(settings.TcpBindAddress, out IPAddress listenAddress))
      {
        Log?.LogError("Failed to parse address: '{0}', listening on 0.0.0.0 instead", settings.TcpBindAddress);
        listenAddress = IPAddress.Parse("0.0.0.0");
      }

      m_tcpListener = new TcpListener(listenAddress, settings.TcpPort);
    }

    /// <summary>
    /// Starts the <see cref="HydraServer"/> listener and run threads
    /// </summary>
    /// <param name="background">Indicates whether the run thread should run as a background thread</param>
    public override void Start(bool background = false)
    {
      base.Start(background);
      if (m_listenerThread == null || !m_listenerThread.IsAlive)
      {
        m_listenerThread = new Thread(ListenLoop)
        {
          IsBackground = true
        };
        m_listenerThread.Start();
      }
    }

    /// <summary>
    /// Blocks waiting to join the server threads
    /// </summary>
    public override void Join()
    {
      base.Join();
      m_listenerThread?.Join();
    }

    /// <summary>
    /// Blocks waiting to join each application thread up to a specified timeout
    /// </summary>
    /// <param name="millisecondTimeout">The time in milliseconds to wait to join on each application thread</param>
    /// <returns>True if all threads ended, false otherwise</returns>
    public override bool Join(int millisecondTimeout)
    {
      bool ended = base.Join(millisecondTimeout);
      return m_listenerThread?.Join(millisecondTimeout) ?? true && ended;
    }

    /// <summary>
    /// Waits for a handshake on a new <see cref="TcpConnection"/> object
    /// </summary>
    /// <param name="arg">A new <see cref="TcpConnection"/> object to wait for a handshake on</param>
    public void AwaitHandshake(object arg)
    {
      TcpConnection client = (TcpConnection)arg;
      if (client.ReceiveHandshake(5000, out HandshakeInfo info))
      {
        bool success;
        Log.LogDebug("Received handshake for UUID: {0} and Key: {1}", info.Uuid, info.RemoteKey);
        info.Address.Host = client.RemoteEndPoint.Address.ToString();
        if (info.RemoteKey == 0)
        {
          success = AddConnection(client, info, true);
        }
        else
        {
          success = AddConnection(client, info, false);
        }

        if (!success)
        {
          // Failed to add
          Log.LogError("Adding connection failed");
        }
        else
        {
          Profile profile = GetProfile(info.Uuid);
          Log?.LogDebug("Successfully handshaked, sending ACK, adding stream connection to profile UUID: {0}", info.Uuid);
          HydraHandshakeAckMsg ack = new HydraHandshakeAckMsg(new HandshakeInfo(Uuid, ApplicationName, HydraMode, Address, profile.Key, info.LocalKey));
          client.Write(ack);
          client.Start(true);
          return;
        }
      }

      Log?.LogError("Failed to handshake with new TCP connection, closing...");
      client.Stop();
      client.Close();
    }

    /// <summary>
    /// The TCP listener loop that accepts new client connections
    /// </summary>
    protected void ListenLoop()
    {
      try
      {
        m_tcpListener.Start(10);
        while (!StopRun)
        {
          if (m_tcpListener.Pending())
          {
            Socket client = m_tcpListener.AcceptSocket();
            client.Blocking = true;
            client.ReceiveTimeout = 100;
            TcpConnection tcpConnection = CreateTcpConnection(client);
            Thread streamThread = new Thread(AwaitHandshake);
            streamThread.Start(tcpConnection);
          }
          else
          {
            Thread.Sleep(m_listenPollPeriod);
          }
        }
      }
      catch (System.Exception ex)
      {
        Log.LogError("Exception in listen loop: '{0}'\n{1}", ex.Message, ex.StackTrace);
      }
      
      m_tcpListener.Stop();
    }
  }
}
