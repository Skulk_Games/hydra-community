#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.AutoDiscovery.Data;
using Skulk.Hydra.Network;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace Skulk.Hydra
{
  /// <summary>
  /// A <see cref="HydraCore"/> implementation for applications in a mesh network
  /// </summary>
  public class HydraNode : HydraServer
  {
    /// <summary>
    /// Internally used for determining the time to wait before retrying to start a TCP stream
    /// </summary>
    private double m_tcpTime = 0;

    /// <summary>
    /// The period in seconds that re-establishing a TCP stream should be attempted
    /// </summary>
    protected const double STREAM_RETRY_PERIOD = 10;

    /// <summary>
    /// A list containing the Addresses for streams to maintain
    /// </summary>
    private List<HydraAddress> m_maintainStreams = new List<HydraAddress>();

    /// <summary>
    /// A list of addresses currently in the process of connecting
    /// </summary>
    private List<HydraAddress> m_pendingConnections = new List<HydraAddress>();

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraNode"/> class with the specified settings
    /// </summary>
    /// <param name="nodeSettings">The configuration settings for this node</param>
    /// <param name="nodes">The endpoints of known nodes in the network</param>
    public HydraNode(
      HydraNodeSettings nodeSettings,
      params StaticNodeEndPoint[] nodes)
      : base(nodeSettings)
    {
      AutoDiscoveryModule discoveryModule = new AutoDiscoveryModule(this, nodeSettings.DiscoveryPeriod, nodeSettings.DiscoveryTimeout, nodeSettings.DiscoveryPorts, nodes);
      AddModule(discoveryModule);
    }

    /// <summary>
    /// Realtime update processing that handles reconnecting to streams that lost connection
    /// </summary>
    /// <param name="updateTime">The time in seconds since the last update</param>
    protected override void Update(UpdateTime updateTime)
    {
      base.Update(updateTime);

      lock (m_maintainStreams)
      {
        if (m_maintainStreams.Count > 0)
        {
          m_tcpTime += updateTime.DeltaSeconds;
          if (m_tcpTime >= Settings.KeepAlivePeriod)
          {
            m_tcpTime = 0;
            for (int i = 0; i < m_maintainStreams.Count; ++i)
            {
              Profile profile = m_profileManager.GetProfile(m_maintainStreams[i]);
              if (profile == null || !profile.HasConnection(ConnectionTypes.STREAM))
              {
                Thread connectThread = new Thread(ThreadedStartTcpConnection);
                connectThread.Start(m_maintainStreams[i]);
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Starts a TCP stream connection to another node
    /// </summary>
    /// <param name="address">The address information for the application to connect with</param>
    /// <param name="timeoutMs">The max time in milliseconds to wait for a valid handshake on the connection</param>
    /// <returns>True if the stream successfully established, false otherwise</returns>
    public bool StartTcpConnection(HydraAddress address, int timeoutMs = 3000)
    {
      try
      {
        Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        client.Connect(address.GetTcpEndPoint());
        client.Blocking = true;
        TcpConnection tcpConnection = new TcpConnection(client, 0, -1, 1, StreamEncoder, Settings.KeepAlivePeriod);
        Profile profile = m_profileManager.GetProfile(address);
        HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(new HandshakeInfo(Uuid, ApplicationName, HydraMode, Address, profile?.RemoteKey ?? 0));
        tcpConnection.Write(handshakeMsg);
        if (tcpConnection.ReceiveHandshake(timeoutMs, out HandshakeInfo info))
        {
          bool success;
          if (m_profileManager.Contains(info.Uuid))
          {
            success = AddConnection(tcpConnection, info.Uuid);
          }
          else
          {
            success = AddConnection(tcpConnection, info, true);
          }

          if (success)
          {
            tcpConnection.Start(true);
            return true;
          }
          else
          {
            Log?.LogError("Failed to add newly handshaked TCP connection on UUID: {0}, stopping and closing connection", info.Uuid);
          }
        }
        else
        {
          Log.LogError("Failed to handshake new TCP connection, stopping and closing connection");
        }

        tcpConnection.Stop();
        tcpConnection.Close();
      }
      catch (Exception ex)
      {
        Log?.LogError("Exception while starting TCP connection: {0}", ex.Message);
        Log.LogDebug("Full Exception: {0}", ex.ToString());
      }

      return false;
    }

    /// <summary>
    /// Gets the <see cref="HydraRouteMap"/> instance used for routing messages to nodes
    /// </summary>
    /// <returns>The <see cref="HydraRouteMap"/> instance in use on this node or null if routing is disabled.</returns>
    public HydraRouteMap GetNodeMap()
    {
      return m_profileManager.Router?.RouteMap;
    }

    /// <summary>
    /// A thread compatible and safe method that starts the TCP connection method
    /// </summary>
    private void ThreadedStartTcpConnection(object arg)
    {
      HydraAddress address = arg as HydraAddress;
      if (address == null)
      {
        Log?.LogError("Address is null, can't start connection");
      }

      bool start = false;
      lock (m_pendingConnections)
      {
        if (m_pendingConnections.Contains(address))
        {
          return;
        }
        else
        {
          m_pendingConnections.Add(address);
          start = true;
        }
      }

      if (start)
      {
        Log.LogTrace("Threaded starting TCP connection");
        StartTcpConnection(address);

        lock (m_pendingConnections)
        {
          m_pendingConnections.Remove(address);
        }
      }
    }
  }
}
