#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra.Messages
{
  /// <summary>
  /// A message sent to test a connections latency
  /// </summary>
  public class HydraLatencyMsg : Message
  {
    /// <summary>
    /// The size of this message payload in bytes
    /// </summary>
    public const int MESSAGE_SIZE = 4;
    private const int CHECK_ID_OFFSET = 0;
    private const int LATENCY_MS_OFFSET = 1;
    private const int TYPE_FLAG_OFFSET = 3;

    /// <summary>
    /// Enumeration of the latency check types
    /// </summary>
    public enum CheckType : byte
    {
      /// <summary>
      /// An unset latency check type
      /// </summary>
      NULL,

      /// <summary>
      /// A latency check that is being initiated
      /// </summary>
      INITIATED,

      /// <summary>
      /// A response to an initiated latency check
      /// </summary>
      RESPONSE
    }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraLatencyMsg()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this message with the data from another message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public HydraLatencyMsg(Message other) : base(other)
    {
      // Empty
    }

    /// <summary>
    /// Initalizes this <see cref="HydraLatencyMsg"/> with the specified latency value payload
    /// </summary>
    /// <param name="latencyMs">The latency value to set in this message</param>
    /// <param name="checkId">The ID of this latency check</param>
    /// <param name="type">The type of this latency check message</param>
    public HydraLatencyMsg(UInt16 latencyMs, byte checkId, CheckType type = CheckType.INITIATED) : base(HydraCodes.HydraLatencyTest, MESSAGE_SIZE)
    {
      SetLatencyMs(latencyMs);
      SetCheckId(checkId);
      SetCheckType(type);
    }

    /// <summary>
    /// Sets the ID of this latency check
    /// </summary>
    /// <param name="id">The ID of the latency check</param>
    public void SetCheckId(byte id)
    {
      int offset = CHECK_ID_OFFSET;
      Hydralizer.Put(m_aPayload, id, ref offset, false);
    }

    /// <summary>
    /// Gets the ID of the latency check in this message
    /// </summary>
    /// <returns>The latency check ID in this message</returns>
    public byte GetCheckId()
    {
      int offset = CHECK_ID_OFFSET;
      return Hydralizer.GetByte(m_aPayload, ref offset);
    }

    /// <summary>
    /// Sets the latency time in milliseconds stored in this message
    /// </summary>
    /// <param name="latencyMs">The latency time value in milliseconds to set</param>
    public void SetLatencyMs(UInt16 latencyMs)
    {
      int offset = LATENCY_MS_OFFSET;
      Hydralizer.Put(m_aPayload, latencyMs, ref offset, false);
    }

    /// <summary>
    /// Gets the latency time in milliseconds set in this message
    /// </summary>
    /// <returns>The latency time value in this message</returns>
    public UInt16 GetLatencyMs()
    {
      int offset = LATENCY_MS_OFFSET;
      return Hydralizer.GetUInt16(m_aPayload, ref offset);
    }

    /// <summary>
    /// Sets the type of this latency check message
    /// </summary>
    /// <param name="type">The type of this latency check message</param>
    public void SetCheckType(CheckType type)
    {
      int offset = TYPE_FLAG_OFFSET;
      Hydralizer.Put(m_aPayload, (byte)type, ref offset, false);
    }

    /// <summary>
    /// Gets the latency check type of this message
    /// </summary>
    /// <returns>The latency check type of this message</returns>
    public CheckType GetCheckType()
    {
      int offset = TYPE_FLAG_OFFSET;
      return (CheckType)Hydralizer.GetByte(m_aPayload, ref offset);
    }
  }
}
