#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// An class defining the built-in Hydra message codes
  /// </summary>
  public class HydraCodes : MessageRegistrar
  {
    #region CoreMessages

    /// <summary>
    /// A null command code - not used, indicates an error or unset message
    /// </summary>
    public const UInt16 Null = 0;

    /// <summary>
    /// A noop command code - used for testing dummy messages
    /// </summary>
    public const UInt16 Noop = 1;

    /// <summary>
    /// A noop response message
    /// </summary>
    public const UInt16 NoopAck = 2;

    /// <summary>
    /// A Hydra error message
    /// </summary>
    public const UInt16 HydraError = 3;

    /// <summary>
    /// A Hydra handshake message
    /// </summary>
    public const UInt16 HydraHandshake = 4;

    /// <summary>
    /// A Hydra handshake ack message
    /// </summary>
    public const UInt16 HydraHandshakeAck = 5;

    /// <summary>
    /// A Hydra start TCP stream message, indicates the beginning of stream data
    /// </summary>
    public const UInt16 HydraStreamStart = 6;

    /// <summary>
    /// A Hydra end TCP stream message, indicates the end of stream data
    /// </summary>
    public const UInt16 HydraStreamEnd = 7;

    /// <summary>
    /// A Hydra stream segment message, contains a segment of the stream data
    /// </summary>
    public const UInt16 HydraStreamSegment = 8;

    /// <summary>
    /// A Hydra transfer message that directs a client to a new server
    /// </summary>
    public const UInt16 HydraTransfer = 9;

    /// <summary>
    /// A Hydra message sent to update the allowed UDP source for a connection
    /// </summary>
    public const UInt16 HydraSetUdpSource = 10;

    /// <summary>
    /// A Hydra message that contains raw encoded bytes
    /// </summary>
    public const UInt16 HydraEncodedData = 11;

    /// <summary>
    /// A Hydra message sent when a connection is closing
    /// </summary>
    public const UInt16 HydraCloseConnection = 12;

    /// <summary>
    /// A Hydra message sent when an application profile is closing
    /// </summary>
    public const UInt16 HydraCloseProfile = 13;

    /// <summary>
    /// A Hydra message that contains a message to route
    /// </summary>
    public const UInt16 HydraRouteMessage = 14;

    /// <summary>
    /// A Hydra message that requests or sends route entry info
    /// </summary>
    public const UInt16 HydraRouteEntry = 15;

    /// <summary>
    /// A Hydra message sent to test a connection's latency
    /// </summary>
    public const UInt16 HydraLatencyTest = 16;

    /// <summary>
    /// A Hydra message sent to negotiate a stream
    /// </summary>
    public const UInt16 HydraStreamNegotiation = 17;

    #endregion

    // Module message codes are not constants. This allows them to be reconfigured
    // at runtime, but does mean that simple switch statements can no longer be used
    // when handling messages.

    #region MapRegistrationMessages

    /// <summary>
    /// A message that requests object maps
    /// </summary>
    public static UInt16 HydraRequestMap = 100;

    /// <summary>
    /// A message containing object map data
    /// </summary>
    public static UInt16 HydraObjectMap = 101;

    #endregion

    #region AutoDiscoveryMessages

    /// <summary>
    /// A message to search for available Hydra servers on a network
    /// </summary>
    public static UInt16 HydraDiscoveryPing = 200;

    /// <summary>
    /// A message sent in response giving information about a Hydra server on the network
    /// </summary>
    public static UInt16 HydraDiscoveryPong = 201;

    /// <summary>
    /// A message sent to request a server list from another Hydra application
    /// </summary>
    public static UInt16 HydraDiscoveryRequest = 202;

    /// <summary>
    /// A message containing a list of known servers
    /// </summary>
    public static UInt16 HydraDiscoveryList = 203;

    #endregion
  }
}
