#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Net;

namespace Skulk.Hydra
{
  /// <summary>
  /// Class that describes a message and destination
  /// </summary>
  public class MessageInfo
  {
    private Message message;
    private IPEndPoint m_ep;
    private int m_client;

    /// <summary>
    /// Initializes a new instance of the <see cref="MessageInfo"/> class
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="ep">The endpoint to send the message to</param>
    /// <param name="client">The ID of the client to send the message to, -1 if unused</param>
    public MessageInfo(Message message, IPEndPoint ep, int client = -1)
    {
      Message = message;
      m_ep = ep;
      m_client = client;
    }

    /// <summary>
    /// The message to send
    /// </summary>
    public Message Message { get => message; set => message = value; }

    /// <summary>
    /// The IP endpoint destination for the message
    /// </summary>
    public IPEndPoint Ep { get => m_ep; set => m_ep = value; }

    /// <summary>
    /// The ID of the destination client, or -1 if unused
    /// </summary>
    public int Client { get => m_client; set => m_client = value; }
  }
}
