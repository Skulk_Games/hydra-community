#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra.Messages
{
  /// <summary>
  /// A message that wraps another message allowing it to be routed through multiple application hops
  /// </summary>
  public class RoutedMessage : Message
  {
    /// <summary>
    /// The size of the routed message data on top of the message bytes
    /// </summary>
    private const int BASE_MESSAGE_SIZE = 32;

    /// <summary>
    /// The byte index of the source UUID information
    /// </summary>
    private const int SOURCE_INDEX = 0;

    /// <summary>
    /// The byte index of the destination UUID information
    /// </summary>
    private const int DESTINATION_INDEX = 16;

    /// <summary>
    /// The byte index of the routed message bytes
    /// </summary>
    private const int MESSAGE_INDEX = 32;

    /// <summary>
    /// Initializes this routed message with the specified values
    /// </summary>
    /// <param name="source">The UUID of the application to set as the source for this message</param>
    /// <param name="destination">The UUID of the application to set as the destination for this message</param>
    /// <param name="message">The raw bytes of the message to route</param>
    public RoutedMessage(HydraUuid source, HydraUuid destination, byte[] message) : base(HydraCodes.HydraRouteMessage, (UInt16)(BASE_MESSAGE_SIZE + message?.Length ?? 0))
    {
      if (message.Length + BASE_MESSAGE_SIZE > UInt16.MaxValue)
      {
        throw new Exception("Routed message bytes array is too large, can't sent more than 65,535 bytes in a message");
      }

      SetSource(source);
      SetDestination(destination);
      SetMessage(message);
    }

    /// <summary>
    /// Initializes this routed message with the specified values
    /// </summary>
    /// <param name="source">The UUID of the application to set as the source for this message</param>
    /// <param name="destination">The UUID of the application to set as the destination for this message</param>
    /// <param name="message">The message to route</param>
    public RoutedMessage(HydraUuid source, HydraUuid destination, Message message) : this(source, destination, message.Serialize())
    {
      // Empty
    }

    /// <summary>
    /// Initializes this routed message data with data from another message
    /// </summary>
    /// <param name="other">The message to get this message data from</param>
    public RoutedMessage(Message other) : base(other)
    {
      // Empty
    }

    /// <summary>
    /// Sets the source <see cref="HydraUuid"/> in this message
    /// </summary>
    /// <param name="uuid">The UUID to set as the source for this message</param>
    public void SetSource(HydraUuid uuid)
    {
      Array.Copy(uuid.Uuid, 0, m_aPayload, SOURCE_INDEX, HydraUuid.UUID_LENGTH);
    }

    /// <summary>
    /// Gets the <see cref="HydraUuid"/> of the source application for this message
    /// </summary>
    /// <returns>The <see cref="HydraUuid"/> of the source application</returns>
    public HydraUuid GetSource()
    {
      byte[] uuidBytes = new byte[HydraUuid.UUID_LENGTH];
      Array.Copy(m_aPayload, SOURCE_INDEX, uuidBytes, 0, uuidBytes.Length);
      return new HydraUuid(uuidBytes);
    }

    /// <summary>
    /// Sets the destination <see cref="HydraUuid"/> in this message
    /// </summary>
    /// <param name="uuid">The UUID to set as the destination for this message</param>
    public void SetDestination(HydraUuid uuid)
    {
      Array.Copy(uuid.Uuid, 0, m_aPayload, DESTINATION_INDEX, HydraUuid.UUID_LENGTH);
    }

    /// <summary>
    /// Gets the <see cref="HydraUuid"/> of the destination application for this message
    /// </summary>
    /// <returns>The <see cref="HydraUuid"/> of the destination application</returns>
    public HydraUuid GetDestination()
    {
      byte[] uuidBytes = new byte[HydraUuid.UUID_LENGTH];
      Array.Copy(m_aPayload, DESTINATION_INDEX, uuidBytes, 0, uuidBytes.Length);
      return new HydraUuid(uuidBytes);
    }

    /// <summary>
    /// Gets the message contained in this routed message
    /// </summary>
    /// <returns>The routed message</returns>
    public Message GetMessage()
    {
      Message message = new Message();
      if (message.Deserialize(m_aPayload, MESSAGE_INDEX, (UInt16)(m_aPayload.Length - MESSAGE_INDEX), out uint remainder))
      {
        return message;
      }

      return null;
    }

    /// <summary>
    /// Sets the message bytes in this routed message
    /// </summary>
    /// <param name="message">The bytes of the message to route</param>
    private void SetMessage(byte[] message)
    {
      if (message.Length == m_aPayload.Length - BASE_MESSAGE_SIZE)
      {
        Array.Copy(message, 0, m_aPayload, MESSAGE_INDEX, m_aPayload.Length - BASE_MESSAGE_SIZE);
      }
    }
  }
}
