#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using Skulk.Hydralize;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Skulk.Hydra
{
  /// <summary>
  /// Special flag values used in the message
  /// </summary>
  public enum MessageFlagValues : UInt16
  {
    /// <summary>
    /// Indicates the message is anonymous and does not need a handshaked client
    /// </summary>
    ANONYMOUS = 0x01,

    /// <summary>
    /// Indicates the message contains a hydralized object
    /// </summary>
    HYDRALIZED_OBJECT = 0x02,

    /// <summary>
    /// Indicates this message should guarantee delivery by a message guarantor
    /// </summary>
    GUARANTEED = 0x04,

    /// <summary>
    /// Indicates this message is part of an extended message with payload data split across multiple messages
    /// </summary>
    EXTENDED = 0x08
  }

  /// <summary>
  /// The base class for a simple Hydra message
  /// </summary>
  public class Message
  {
    /// <summary>
    /// The max message size in bytes (used to determine buffer sizes)
    /// </summary>
    public const uint MAX_MESSAGE_SIZE = MAX_PAYLOAD_SIZE + HEADER_SIZE + EXTENDED_HEADER_SIZE;

    /// <summary>
    /// The max payload size in bytes
    /// </summary>
    /// <remarks>
    /// Bad things happen if this is larger than 65505 on TCP and the max for UDP datagrams is 65497.
    /// This value was chosen as an arbitrary number considering the UDP and TCP constraints while allowing for some margin.
    /// </remarks>
    public const uint MAX_PAYLOAD_SIZE = 65000;

    /// <summary>
    /// The size of the message header in bytes
    /// </summary>
    public const uint HEADER_SIZE = 10;

    /// <summary>
    /// The byte offset of the command value in a message header
    /// </summary>
    public const uint CMD_OFFSET = 0;

    /// <summary>
    /// The byte offset of the length value in a message header
    /// </summary>
    public const uint LENGTH_OFFSET = 2;

    /// <summary>
    /// The byte offset of the flags value in a message header
    /// </summary>
    public const uint FLAGS_OFFSET = 4;

    /// <summary>
    /// The byte offset of the message ID value in the message header
    /// </summary>
    public const UInt32 ID_OFFSET = 6;

    /// <summary>
    /// The byte offset of the checksum value in a message header
    /// </summary>
    public const uint CHECKSUM_OFFSET = 8;

    /// <summary>
    /// The number of bytes in the extended header
    /// </summary>
    public const uint EXTENDED_HEADER_SIZE = 4;

    /// <summary>
    /// The byte offset of the message index in an extended header
    /// </summary>
    public const uint EXTENDED_INDEX_OFFSET = 10;

    /// <summary>
    /// The byte offset of the message count in an extended header
    /// </summary>
    public const uint EXTENDED_COUNT_OFFSET = 12;

    /// <summary>
    /// The sync bytes used for messages
    /// </summary>
    private static byte[] ms_aSync = new byte[] { 0x55, 0xab };

    /// <summary>
    /// The header byte array for this message
    /// </summary>
    protected byte[] m_aHeader = null;

    /// <summary>
    /// The payload byte array for this message
    /// </summary>
    protected byte[] m_aPayload = null;

    /// <summary>
    /// The partially filled message index used when deserializing a less than full message
    /// </summary>
    private int m_partial = -1;

    /// <summary>
    /// The command code of this message
    /// </summary>
    public UInt16 Command { get => GetCmd(); set => SetCmd(value); }

    /// <summary>
    /// Gets the array of payload bytes in this message
    /// </summary>
    public byte[] Payload { get => m_aPayload; }

    /// <summary>
    /// Gets the partial index for this message
    /// </summary>
    public int PartialIndex { get => m_partial; }

    /// <summary>
    /// Attempts to deserialize messages from an array of bytes
    /// </summary>
    /// <param name="aBytes">The array of bytes to deserialize into messages</param>
    /// <returns>The list of messages deserialized from the array of bytes</returns>
    public static List<Message> Deserialize(byte[] aBytes)
    {
      List<Message> parsedMessages = new List<Message>();
      bool success = true;
      UInt32 offset = 0;
      while (success && offset < aBytes.Length)
      {
        success = false;

        if (FindSync(aBytes, offset, (uint)(aBytes.Length - offset), out offset))
        {
          Message msg = new Message();
          if (DeserializeHeader(msg, aBytes, offset, (uint)(aBytes.Length - offset), out offset))
          {
            if (msg.IsExtended())
            {
              if (!DeserializeExtendedHeader(msg, aBytes, offset, (uint)(aBytes.Length - offset), out offset))
              {
                break;
              }
            }

            if (DeserializePayload(msg, aBytes, offset, (uint)(aBytes.Length - offset), out offset))
            {
              success = true;
              parsedMessages.Add(msg);
            }
          }
        }
      }

      return parsedMessages;
    }

    /// <summary>
    /// Attempts to deserialize messages from an array of bytes
    /// </summary>
    /// <param name="aBytes">The array of bytes to deserialize into messages</param>
    /// <param name="offset">The index in the array to begin deserializing at</param>
    /// <param name="count">The total number of bytes to use while deserializing</param>
    /// <returns>The list of messages deserialized from the array of bytes</returns>
    public static List<Message> Deserialize(byte[] aBytes, uint offset, uint count)
    {
      List<Message> parsedMessages = new List<Message>();
      bool success = true;
      while (success && offset < aBytes.Length)
      {
        success = false;

        if (FindSync(aBytes, offset, count, out offset))
        {
          Message msg = new Message();
          if (DeserializeHeader(msg, aBytes, offset, count - offset, out offset))
          {
            if (msg.IsExtended())
            {
              if (!DeserializeExtendedHeader(msg, aBytes, offset, count - offset, out offset))
              {
                break;
              }
            }

            if (DeserializePayload(msg, aBytes, offset, count - offset, out offset))
            {
              success = true;
              parsedMessages.Add(msg);
            }
          }
        }
      }

      return parsedMessages;
    }

    /// <summary>
    /// Reads bytes from the specified socket until the sync pattern is found or there is no more data available
    /// </summary>
    /// <param name="socket">The socket on which to search for the sync pattern</param>
    /// <returns>True if the sync was found, false otherwise</returns>
    private static bool FindSync(Socket socket)
    {
      if (ms_aSync != null)
      {
        Queue<byte> buffer = new Queue<byte>(ms_aSync.Length);
        int syncIndex = 0;
        byte[] tmpByte = new byte[1];
        while (syncIndex < ms_aSync.Length)
        {
          int streamByte = -1;
          if (buffer.Count > 0)
          {
            streamByte = buffer.Dequeue();
          }
          else
          {
            if (socket.Available > 0)
            {
              if (socket.Receive(tmpByte) == tmpByte.Length)
              {
                streamByte = tmpByte[0];
              }
              else
              {
                streamByte = -1;
              }
            }
            else
            {
              streamByte = -1;
            }
          }

          if (streamByte != -1)
          {
            if (ms_aSync[syncIndex] == streamByte)
            {
              ++syncIndex;
            }
            else if (syncIndex > 0)
            {
              for (int i = 1; i < syncIndex; ++i)
              {
                buffer.Enqueue(ms_aSync[i]);
              }
            }
          }
          else
          {
            // End of stream
            return false;
          }
        }

        return syncIndex == ms_aSync.Length;
      }
      else
      {
        return true;
      }
    }

    private static bool FindSync(byte[] aBytes, UInt32 offset, uint length, out UInt32 index)
    {
      if (ms_aSync != null)
      {
        // Find sync
        int syncIndex = 0;
        for (index = offset; index < aBytes.Length && index < offset + length && syncIndex < ms_aSync.Length; index++)
        {
          if (aBytes[(int)index] == ms_aSync[syncIndex])
          {
            syncIndex++;
          }
          else if (syncIndex > 0)
          {
            index -= (uint)(syncIndex);
            syncIndex = 0;
          }
        }

        return syncIndex == ms_aSync.Length;
      }
      else
      {
        index = offset;
        return true;
      }
    }

    /// <summary>
    /// Attempts to deserialize the header for the specified message
    /// </summary>
    /// <param name="msg">The message to deserialize the header for</param>
    /// <param name="aBytes">The byte array to deserialize the header from</param>
    /// <param name="offset">The index of the first byte to use for the header data</param>
    /// <param name="length">The number of bytes that can be used in the buffer</param>
    /// <param name="index">The index of the first byte in the buffer not used for header data</param>
    /// <returns>True if the header was successfully parsed, false otherwise</returns>
    private static bool DeserializeHeader(Message msg, byte[] aBytes, UInt32 offset, uint length, out UInt32 index)
    {
      int localIndex = (int)offset;

      if (length >= HEADER_SIZE)
      {
        msg.SetCmd(Hydralizer.GetUInt16(aBytes, ref localIndex));
        msg.SetLength(Hydralizer.GetUInt16(aBytes, ref localIndex));
        msg.SetFlags(Hydralizer.GetUInt16(aBytes, ref localIndex));
        msg.SetId(Hydralizer.GetUInt16(aBytes, ref localIndex));
        msg.SetCheckSum(Hydralizer.GetUInt16(aBytes, ref localIndex));
        index = (uint)localIndex;
        if (msg.CalculateCheckSum() == msg.GetCheckSum())
        {
          msg.InitPayload();
          return true;
        }
        else
        {
          return false;
        }
      }

      index = offset;
      return false;
    }

    /// <summary>
    /// Attempts to deserialize extended header data for the specified message
    /// </summary>
    /// <param name="msg">The message where deserialized data will be written</param>
    /// <param name="aBytes">The bytes to deserialize</param>
    /// <param name="offset">The index of the first byte to use when deserializing</param>
    /// <param name="length">The max number of bytes that can be used from the buffer</param>
    /// <param name="index">The index of the first byte in the buffer not used for the extended header data</param>
    /// <returns>True if a valid extended header was deserialized, false otherwise</returns>
    private static bool DeserializeExtendedHeader(Message msg, byte[] aBytes, UInt32 offset, uint length, out UInt32 index)
    {
      index = offset;
      if (length >= EXTENDED_HEADER_SIZE)
      {
        int localIndex = (int)index;
        msg.SetIndex(Hydralizer.GetUInt16(aBytes, ref localIndex));
        msg.SetCount(Hydralizer.GetUInt16(aBytes, ref localIndex));
        index = (uint)localIndex;
        return msg.GetIndex() <= msg.GetCount();
      }

      return false;
    }

    /// <summary>
    /// Attempts to deserialize the message payload from an array of bytes.
    /// </summary>
    /// <param name="msg">The message that needs to deserialize the payload.</param>
    /// <param name="aBytes">The array of bytes containing the payload bytes to read.</param>
    /// <param name="offset">The index of the first payload byte to read.</param>
    /// <param name="length">The max number of bytes that can be used from the buffer.</param>
    /// <param name="index">The index of the first byte that was unused.</param>
    /// <returns>True if the entire payload was deserialized; otherwise, false.</returns>
    private static bool DeserializePayload(Message msg, byte[] aBytes, UInt32 offset, uint length, out UInt32 index)
    {
      if (length >= msg.GetLength())
      {
        msg.SetPayload(aBytes, offset, 0, msg.GetLength());
        index = offset + msg.GetLength();
        return true;
      }

      index = offset;
      return false;
    }

    /// <summary>
    /// Attempts to deserialize the payload bytes for a message directly from a socket
    /// </summary>
    /// <param name="message">The message to deserialize the payload bytes in</param>
    /// <param name="socket">The socket to read the bytes from</param>
    /// <returns>True if all payload bytes were set, false otherwise.</returns>
    private static bool DeserializePayload(Message message, Socket socket)
    {
      if (message.IsPartial)
      {
        int left = message.m_aPayload.Length - message.m_partial;
        int count = Math.Min(socket.Available, left);
        int length = socket.Receive(message.m_aPayload, message.m_partial, count, SocketFlags.None);
        if (length == left)
        {
          message.ClearPartial();
          return true;
        }
        else
        {
          message.m_partial += length;
          return false;
        }
      }
      else
      {
        int count = Math.Min(socket.Available, message.m_aPayload.Length);
        int length = socket.Receive(message.m_aPayload, 0, count, SocketFlags.None);
        if (length == message.m_aPayload.Length)
        {
          return true;
        }
        else
        {
          message.m_partial = length;
          return false;
        }
      }
    }

    /// <summary>
    /// Sets the sync bytes to use for all messages
    /// </summary>
    /// <param name="aSync">The array of sync bytes to use in messages</param>
    public static void SetSync(byte[] aSync)
    {
      ms_aSync = aSync;
    }

    /// <summary>
    /// Gets the array of sync bytes
    /// </summary>
    /// <returns>The array of sync bytes</returns>
    public static byte[] GetSync()
    {
      return ms_aSync;
    }

    /// <summary>
    /// Gets the length of the sync pattern in bytes
    /// </summary>
    /// <returns>The number of bytes in the sync pattern</returns>
    public static int GetSyncLength()
    {
      if (ms_aSync != null)
      {
        return ms_aSync.Length;
      }

      return 0;
    }

    /// <summary>
    /// Makes a sequence of messages that will contain the specified data
    /// </summary>
    /// <param name="cmd">The command code to set in each message</param>
    /// <param name="data">The raw data to include in the sequence of messages</param>
    /// <param name="offset">The index of the first byte to use in the raw data array</param>
    /// <param name="length">The number of bytes to send in the sequence</param>
    /// <param name="flags">The flags to set on each message (The extended flag will be set regardless of this value)</param>
    /// <returns>The list of messages made for the sequence or null if an invalid parameter value was set</returns>
    public static List<Message> MakeSequence(UInt16 cmd, byte[] data, int offset = 0, int length = -1, UInt16 flags = 0)
    {
      if (length == -1)
      {
        length = data.Length - offset;
      }

      if (length < 0 || offset < 0)
      {
        return null;
      }

      int msgCount = (int)(length / MAX_PAYLOAD_SIZE);
      if (length - (msgCount * MAX_PAYLOAD_SIZE) > 0)
      {
        ++msgCount;
      }

      List<Message> messages = new List<Message>(msgCount);
      int dataIndex = offset;
      for (int i = 0; i < msgCount; ++i)
      {
        ushort segmentLength = (ushort)Math.Min(length, MAX_PAYLOAD_SIZE);
        Message segmentMsg = new Message(cmd, segmentLength, (ushort)(flags | (ushort)MessageFlagValues.EXTENDED));
        segmentMsg.SetIndex((ushort)(i + 1));
        segmentMsg.SetCount((ushort)(msgCount));
        segmentMsg.SetPayload(data, (uint)dataIndex, 0, segmentLength);
        messages.Add(segmentMsg);
        dataIndex += segmentLength;
        length -= segmentLength;
      }

      return messages;
    }

    /// <summary>
    /// Default constructor
    /// </summary>
    public Message() : this(HydraCodes.Null, 0)
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new Message with the command and length values specified
    /// </summary>
    /// <param name="cmd">The command code to use in this message</param>
    /// <param name="length">The length of the payload data in this message</param>
    /// <param name="flags">The special flags for this message</param>
    public Message(UInt16 cmd, UInt16 length, UInt16 flags = 0)
    {
      SetFlags(flags);
      SetCmd(cmd);
      SetLength(length);
      InitPayload();
    }

    /// <summary>
    /// Initializes this message with the data from another message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public Message(Message other)
    {
      m_aHeader = other.m_aHeader;
      m_aPayload = other.m_aPayload;
      m_partial = other.m_partial;
    }

    /// <summary>
    /// Serializes this message into an array of bytes.
    /// </summary>
    /// <remarks>
    /// The array of bytes may contain multiple (extended) messages. For size limited connections <see cref="SerializeSequence"/> should be considered.
    /// </remarks>
    /// <returns>The array of serialized bytes</returns>
    public virtual byte[] Serialize()
    {
      int syncLength = GetSyncLength();
      int payloadLength = Math.Max(this.Payload?.Length ?? 0, GetLength());

      // Automatically serialize large payloads into a sequence of extended messages
      if (payloadLength > MAX_PAYLOAD_SIZE)
      {
        // Make a serialized sequence
        int segmentCount = (ushort)Math.Ceiling(payloadLength / (double)MAX_PAYLOAD_SIZE);
        int totalSize = (int)(segmentCount * (syncLength + HEADER_SIZE + EXTENDED_HEADER_SIZE) + payloadLength);
        byte[] sequenceBytes = new byte[totalSize];

        int putIndex = 0;
        int remainingBytes = payloadLength;
        SetFlags((ushort)(GetFlags() | (ushort)MessageFlagValues.EXTENDED));
        SetCount((ushort)segmentCount);
        for (int i = 0; i < segmentCount; ++i)
        {
          // Put sequence
          if (syncLength > 0)
          {
            Array.Copy(ms_aSync, 0, sequenceBytes, putIndex, syncLength);
            putIndex += syncLength;
          }

          // The payload length for this segment
          ushort segmentLength = (ushort)Math.Min(remainingBytes, MAX_PAYLOAD_SIZE);

          // Put header
          SetLength(segmentLength);
          SetIndex((ushort)(i + 1));
          Array.Copy(m_aHeader, 0, sequenceBytes, putIndex, m_aHeader.Length);
          putIndex += m_aHeader.Length;

          // Put payload
          int payloadIndex = payloadLength - remainingBytes;
          if (this.Payload != null && this.Payload.Length >= payloadIndex + segmentLength)
          {
            Array.Copy(this.Payload, payloadIndex, sequenceBytes, putIndex, segmentLength);
          }
          else
          {
            for (int j = 0; j < segmentLength; ++j)
            {
              if (this.Payload == null || this.Payload.Length < payloadIndex + j)
              {
                // Don't set anything leave all 0's
                break;
              }
              else
              {
                sequenceBytes[putIndex + j] = this.Payload[payloadIndex + j];
              }
            }
          }

          putIndex += segmentLength;
          remainingBytes -= segmentLength;
        }

        return sequenceBytes;
      }

      // Regular single message
      byte[] msg = new byte[syncLength + m_aHeader.Length + payloadLength];
      if (syncLength > 0)
      {
        Array.Copy(GetSync(), 0, msg, 0, syncLength);
      }

      Array.Copy(m_aHeader, 0, msg, syncLength, m_aHeader.Length);

      if (payloadLength > 0)
      {
        if (m_aPayload == null || m_aPayload.Length != payloadLength)
        {
          InitPayload();
        }

        Array.Copy(m_aPayload, 0, msg, syncLength + m_aHeader.Length, payloadLength);
      }

      return msg;
    }

    /// <summary>
    /// Serializes this message into an array of arrays contained the serialized bytes for messages.
    /// </summary>
    /// <returns>The array of arrays contained serialized bytes for each message</returns>
    public virtual byte[][] SerializeSequence()
    {
      int syncLength = GetSyncLength();
      int payloadLength = Math.Max(this.Payload?.Length ?? 0, GetLength());
      byte[][] msgSequenceBytes;

      // Automatically serialize large payloads into a sequence of extended messages
      if (payloadLength > MAX_PAYLOAD_SIZE)
      {
        // Make a serialized sequence
        int segmentCount = (ushort)Math.Ceiling(payloadLength / (double)MAX_PAYLOAD_SIZE);
        msgSequenceBytes = new byte[segmentCount][];
        int totalSize = (int)(segmentCount * (syncLength + HEADER_SIZE + EXTENDED_HEADER_SIZE) + payloadLength);

        SetFlags((ushort)(GetFlags() | (ushort)MessageFlagValues.EXTENDED));
        SetCount((ushort)segmentCount);
        int remainingBytes = payloadLength;
        for (int i = 0; i < segmentCount; ++i)
        {
          // The payload length for this segment
          ushort segmentLength = (ushort)Math.Min(remainingBytes, MAX_PAYLOAD_SIZE);
          int segmentSize = (int)(syncLength + HEADER_SIZE + EXTENDED_HEADER_SIZE + segmentLength);
          msgSequenceBytes[i] = new byte[segmentSize];
          int putIndex = 0;

          // Put sequence
          if (syncLength > 0)
          {
            Array.Copy(ms_aSync, 0, msgSequenceBytes[i], putIndex, syncLength);
            putIndex += syncLength;
          }

          // Put header
          SetLength(segmentLength);
          SetIndex((ushort)(i + 1));
          Array.Copy(m_aHeader, 0, msgSequenceBytes[i], putIndex, m_aHeader.Length);
          putIndex += m_aHeader.Length;

          // Put payload
          int payloadIndex = payloadLength - remainingBytes;
          if (this.Payload != null && this.Payload.Length >= payloadIndex + segmentLength)
          {
            Array.Copy(this.Payload, payloadIndex, msgSequenceBytes[i], putIndex, segmentLength);
          }
          else
          {
            for (int j = 0; j < segmentLength; ++j)
            {
              if (this.Payload == null || this.Payload.Length < payloadIndex + j)
              {
                // Don't set anything leave all 0's
                break;
              }
              else
              {
                msgSequenceBytes[i][putIndex + j] = this.Payload[payloadIndex + j];
              }
            }
          }

          remainingBytes -= segmentLength;
        }

        return msgSequenceBytes;
      }

      // Regular single message
      msgSequenceBytes = new byte[1][];
      byte[] msg = new byte[syncLength + m_aHeader.Length + payloadLength];
      msgSequenceBytes[0] = msg;
      if (syncLength > 0)
      {
        Array.Copy(GetSync(), 0, msg, 0, syncLength);
      }

      Array.Copy(m_aHeader, 0, msg, syncLength, m_aHeader.Length);

      if (payloadLength > 0)
      {
        if (m_aPayload == null || m_aPayload.Length != payloadLength)
        {
          InitPayload();
        }

        Array.Copy(m_aPayload, 0, msg, syncLength + m_aHeader.Length, payloadLength);
      }

      return msgSequenceBytes;
    }

    /// <summary>
    /// Clears the partial index
    /// </summary>
    public void ClearPartial()
    {
      m_partial = -1;
    }

    /// <summary>
    /// Indicates if this message is partially set
    /// </summary>
    public bool IsPartial
    {
      get
      {
        return m_partial != -1;
      }
    }

    /// <summary>
    /// Sets the message command code
    /// </summary>
    /// <param name="cmd">The message code value to set</param>
    public void SetCmd(UInt16 cmd)
    {
      int offset = (int)CMD_OFFSET;
      Hydralizer.Put(m_aHeader, cmd, ref offset, false);
      SetCheckSum(CalculateCheckSum());
    }

    /// <summary>
    /// Gets the message command code
    /// </summary>
    /// <returns>The message command code</returns>
    public UInt16 GetCmd()
    {
      int offset = (int)CMD_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Sets the length of the message
    /// </summary>
    /// <param name="length">The length value to set in this message's header</param>
    public void SetLength(UInt16 length)
    {
      int offset = (int)LENGTH_OFFSET;
      Hydralizer.Put(m_aHeader, length, ref offset, false);
      SetCheckSum(CalculateCheckSum());
    }

    /// <summary>
    /// Gets the length value in the header of this message
    /// </summary>
    /// <returns>The value of the length field in this message</returns>
    public UInt16 GetLength()
    {
      int offset = (int)LENGTH_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Sets the flags bitmask in this message
    /// </summary>
    /// <param name="flags">The bitmask values to set</param>
    public void SetFlags(UInt16 flags)
    {
      uint length = (flags & (ushort)MessageFlagValues.EXTENDED) != 0 ? HEADER_SIZE + EXTENDED_HEADER_SIZE : HEADER_SIZE;
      if (m_aHeader == null)
      {
        m_aHeader = new byte[length];
      }
      else if (m_aHeader.Length != length)
      {
        byte[] newHeader = new byte[length];
        Array.Copy(m_aHeader, newHeader, Math.Min(length, m_aHeader.Length));
        m_aHeader = newHeader;
      }

      int offset = (int)FLAGS_OFFSET;
      Hydralizer.Put(m_aHeader, flags, ref offset, false);
      SetCheckSum(CalculateCheckSum());
    }

    /// <summary>
    /// Gets the flags in this message
    /// </summary>
    /// <returns>The flags bitmask in this message</returns>
    public UInt16 GetFlags()
    {
      int offset = (int)FLAGS_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Sets the extended message index in this message
    /// </summary>
    /// <param name="index">The index of this message in the extended message</param>
    public void SetIndex(UInt16 index)
    {
      int offset = (int)EXTENDED_INDEX_OFFSET;
      Hydralizer.Put(m_aHeader, index, ref offset, false);
    }

    /// <summary>
    /// Gets the message index from the extended header in this message
    /// </summary>
    /// <returns>The index of this message</returns>
    public UInt16 GetIndex()
    {
      int offset = (int)EXTENDED_INDEX_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Sets the extended message count in this message
    /// </summary>
    /// <param name="index">The number of messages in the extended message</param>
    public void SetCount(UInt16 index)
    {
      int offset = (int)EXTENDED_COUNT_OFFSET;
      Hydralizer.Put(m_aHeader, index, ref offset, false);
    }

    /// <summary>
    /// Gets the message count from the extended header in this message
    /// </summary>
    /// <returns>The count of total message segments in this extended message</returns>
    public UInt16 GetCount()
    {
      int offset = (int)EXTENDED_COUNT_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Determines if the anonymous bit is set in the message flags
    /// </summary>
    /// <returns>True if the anonymous bit is set in this message's flags</returns>
    public bool IsAnonymous()
    {
      UInt16 mask = (UInt16)MessageFlagValues.ANONYMOUS;
      return (GetFlags() & mask) != 0;
    }

    /// <summary>
    /// Determines if the hydralized object bit is set in the message flags
    /// This indicates the message contains a hydralized object
    /// </summary>
    /// <returns>True if the hydralized object bit is set in the message's flags</returns>
    public bool IsHydralized()
    {
      UInt16 mask = (UInt16)MessageFlagValues.HYDRALIZED_OBJECT;
      return (GetFlags() & mask) != 0;
    }

    /// <summary>
    /// Determines if the guaranteed bit is set in the message flags
    /// This indicates the message guarantees delivery and should be acknowledged
    /// </summary>
    /// <returns>True if the guaranteed bit is set in the message's flags</returns>
    public bool IsGuaranteed()
    {
      UInt16 mask = (UInt16)MessageFlagValues.GUARANTEED;
      return (GetFlags() & mask) != 0;
    }

    /// <summary>
    /// Determines if the extended bit is set in the message flags
    /// This indicates the message data is extended across multiple message segments
    /// </summary>
    /// <returns>True if the extended bit is set in the message flags</returns>
    public bool IsExtended()
    {
      UInt16 mask = (UInt16)MessageFlagValues.EXTENDED;
      return (GetFlags() & mask) != 0;
    }

    /// <summary>
    /// Gets the message ID value in the header
    /// </summary>
    /// <returns>The message ID value</returns>
    public UInt16 GetId()
    {
      int offset = (int)ID_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Sets the message ID for this message
    /// </summary>
    /// <param name="id">The message ID to set</param>
    public void SetId(UInt16 id)
    {
      int offset = (int)ID_OFFSET;
      Hydralizer.Put(m_aHeader, id, ref offset, false);
      SetCheckSum(CalculateCheckSum());
    }

    /// <summary>
    /// Gets the checksum value in the header
    /// </summary>
    /// <returns>The checksum value</returns>
    public UInt16 GetCheckSum()
    {
      int offset = (int)CHECKSUM_OFFSET;
      return Hydralizer.GetUInt16(m_aHeader, ref offset);
    }

    /// <summary>
    /// Calculates the header checksum for this message
    /// </summary>
    /// <returns>The checksum value</returns>
    public UInt16 CalculateCheckSum()
    {
      UInt16 chkSum = 0;
      for (int i = 0; i < HEADER_SIZE - sizeof(UInt16); i++)
      {
        chkSum += m_aHeader[i];
      }

      // Add 1 so that an all 0 buffer doesn't pass checksum validation
      return (ushort)(chkSum + 1);
    }

    /// <summary>
    /// Sets the message payload data to be the array indicated.
    /// </summary>
    /// <param name="aBytes">The array containing the payload bytes.</param>
    public void SetPayload(byte[] aBytes)
    {
      m_aPayload = aBytes;
    }

    /// <summary>
    /// Copies the bytes from the specified array into the payload byte array.
    /// </summary>
    /// <param name="aBytes">The array of bytes to copy into the payload.</param>
    /// <param name="offset">The index in the specified array of the first byte to copy.</param>
    /// <param name="destOffset">The index in the payload array to start copying to.</param>
    /// <param name="length">The number of bytes to copy.</param>
    public void SetPayload(byte[] aBytes, UInt32 offset, UInt16 destOffset, UInt16 length)
    {
      if (m_aPayload == null || m_aPayload.Length != GetLength())
      {
        InitPayload();
      }


      if (offset + length <= aBytes.Length)
      {
        if (destOffset + length <= GetLength())
        {
          Array.Copy(aBytes, offset, m_aPayload, destOffset, length);
        }
      }
    }

    /// <summary>
    /// Sets the payload bytes with the specified data
    /// </summary>
    /// <param name="aBytes">A list of byte values to set in the payload</param>
    /// <param name="offset">The offset in the list of bytes to begin setting values at</param>
    /// <param name="destOffset">The offset in the payload to begin setting values at</param>
    /// <param name="length">The number of bytes to set</param>
    public void SetPayload(List<byte> aBytes, UInt32 offset, UInt16 destOffset, UInt16 length)
    {
      if (m_aPayload == null || m_aPayload.Length != GetLength())
      {
        InitPayload();
      }

      if (offset + length <= aBytes.Count)
      {
        if (destOffset + length <= GetLength())
        {
          for (int i = 0; i < length; i++)
          {
            SetPayload((UInt16)(i + destOffset), aBytes[(int)(offset + i)]);
          }
        }
      }
    }

    /// <summary>
    /// Sets a byte value in the payload
    /// </summary>
    /// <param name="offset">The offset in the payload to set the value</param>
    /// <param name="value">The value to set in the payload</param>
    public void SetPayload(UInt16 offset, byte value)
    {
      if (m_aPayload == null || m_aPayload.Length != GetLength())
      {
        InitPayload();
      }

      if (offset < GetLength())
      {
        m_aPayload[offset] = value;
      }
    }

    /// <summary>
    /// Attempts to deserialize this message from data on the specified socket
    /// </summary>
    /// <param name="socket">The socket to read message data from</param>
    /// <returns>True if the entire message deserialized, false if only partial</returns>
    public bool Deserialize(Socket socket)
    {
      if (socket != null)
      {
        if (IsPartial)
        {
          return DeserializePayload(this, socket);
        }
        else
        {
          // A new message
          if (FindSync(socket))
          {
            if (socket.Available >= HEADER_SIZE)
            {
              byte[] aHeader = new byte[HEADER_SIZE];
              if (socket.Receive(aHeader, 0, (int)HEADER_SIZE, SocketFlags.None) != HEADER_SIZE)
              {
                return false;
              }
              if (DeserializeHeader(this, aHeader, 0, (uint)aHeader.Length, out UInt32 index))
              {
                if (IsExtended())
                {
                  if (socket.Receive(aHeader, 0, (int)EXTENDED_HEADER_SIZE, SocketFlags.None) == EXTENDED_HEADER_SIZE)
                  {
                    if (!DeserializeExtendedHeader(this, aHeader, 0, EXTENDED_HEADER_SIZE, out index))
                    {
                      return false;
                    }
                  }
                  else
                  {
                    return false;
                  }
                }

                return DeserializePayload(this, socket);
              }
            }
          }

          return false;
        }
      }

      return false;
    }

    /// <summary>
    /// Attempts to parse the byte array into a valid message.
    /// </summary>
    /// <param name="aBytes">The raw message bytes received.</param>
    /// <param name="offset">The index in the array of the first byte to use when deserializing</param>
    /// <param name="length">The number of bytes to parse from the array.</param>
    /// <param name="leftoverIndex">The index of the first byte not used in this message</param>
    /// <returns>True if a valid message was parsed, False otherwise.</returns>
    public bool Deserialize(byte[] aBytes, UInt32 offset, UInt32 length, out uint leftoverIndex)
    {
      if (length == 0)
      {
        leftoverIndex = offset;
        return false;
      }

      if (IsPartial)
      {
        int payloadLeft = GetLength() - m_partial;
        if (payloadLeft > length)
        {
          // Still partial
          SetPayload(aBytes, offset, (UInt16)m_partial, (UInt16)length);
          m_partial += (int)length;
          leftoverIndex = offset + length;
          return false;
        }
        else
        {
          // Full message
          SetPayload(aBytes, offset, (UInt16)m_partial, (UInt16)payloadLeft);

          if (payloadLeft == length)
          {
            leftoverIndex = offset + length;
          }
          else
          {
            leftoverIndex = (uint)(offset + payloadLeft);
          }

          // Message finished
          ClearPartial();
          return true;
        }
      }
      else
      {
        // A new message
        UInt32 index = offset;
        if (FindSync(aBytes, offset, length, out index))
        {
          if (DeserializeHeader(this, aBytes, index, length - (index - offset), out index))
          {
            if (IsExtended())
            {
              if (!DeserializeExtendedHeader(this, aBytes, index, length - (index - offset), out index))
              {
                leftoverIndex = offset;
                return false;
              }
            }

            if (DeserializePayload(this, aBytes, index, length - (index - offset), out index))
            {
              // Success
              leftoverIndex = index;
              return true;
            }
            else
            {
              m_partial = 0;
              leftoverIndex = index;
              return false;
            }
          }
        }

        leftoverIndex = offset;
        return false;
      }
    }

    /// <summary>
    /// Creates a string representation of this message
    /// </summary>
    /// <returns>The string representation of this message</returns>
    public override string ToString()
    {
      byte[] aBytes = Serialize();
      string str = "[";
      for (int i = 0; i < aBytes.Length; i++)
      {
        str += string.Format(" {0:X2} ", aBytes[i]);
      }

      str += "]";
      return str;
    }

    /// <summary>
    /// Initializes the payload byte array matching this message's length
    /// </summary>
    protected void InitPayload()
    {
      m_aPayload = new byte[GetLength()];
    }

    /// <summary>
    /// Sets the checksum value for this message
    /// </summary>
    /// <param name="chkSum">The checksum value to set</param>
    private void SetCheckSum(UInt16 chkSum)
    {
      int offset = (int)CHECKSUM_OFFSET;
      Hydralizer.Put(m_aHeader, chkSum, ref offset, false);
    }
  }
}
