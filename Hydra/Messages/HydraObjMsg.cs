#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// A message that can transfer data from an object
  /// </summary>
  public class HydraObjMsg : Message
  {
    /// <summary>
    /// Base length of this message before adding object data
    /// </summary>
    public const int BASE_MESSAGE_LENGTH = 4;

    /// <summary>
    /// The byte offset of the object Id
    /// </summary>
    public const int OBJECT_ID_OFFSET = 0;

    /// <summary>
    /// The byte offset of the object data
    /// </summary>
    public const int OBJECT_OFFSET = 4;

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraObjMsg"/> class
    /// </summary>
    /// <param name="msgCode">The message code to use</param>
    /// <param name="objectId">An identifier value indicating the type of object being sent</param>
    /// <param name="obj">The object containing data to send in the message</param>
    /// <param name="lite">Indicates if the message should be hydralized in lite mode</param>
    /// <param name="indexed">Indicates if the message should be indexed when it is hydralized</param>
    /// <param name="flags">Special flag values for this message, the hydralized bit will be set no matter what</param>
    public HydraObjMsg(UInt16 msgCode, int objectId, object obj, bool lite = false, bool indexed = false, UInt16 flags = 0) : base(msgCode, BASE_MESSAGE_LENGTH, (UInt16)(flags | (UInt16)MessageFlagValues.HYDRALIZED_OBJECT))
    {
      if (obj != null)
      {
        SetObject(obj, lite, indexed);
      }
      else
      {
        SetLength(BASE_MESSAGE_LENGTH);
        InitPayload();
      }

      SetObjectId(objectId);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraObjMsg"/> class
    /// </summary>
    /// <param name="msgCode">The message code to use</param>
    /// <param name="objectId">An identifier value indicating the type of object being sent</param>
    /// <param name="info">The <see cref="HydralizeInfo"/> object containing the object data to send</param>
    /// <param name="flags">Special flag values for this message, the hydralized bit will be set in addition to these flags</param>
    public HydraObjMsg(UInt16 msgCode, int objectId, HydralizeInfo info, UInt16 flags = 0) : base(msgCode, BASE_MESSAGE_LENGTH, (UInt16)(flags | (UInt16)MessageFlagValues.HYDRALIZED_OBJECT))
    {
      if (info != null)
      {
        SetObjectInfo(info);
      }
      else
      {
        SetLength(BASE_MESSAGE_LENGTH);
        InitPayload();
      }

      SetObjectId(objectId);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraObjMsg"/> with data from another message
    /// </summary>
    /// <param name="other">The message containing the data to use for this message</param>
    public HydraObjMsg(Message other) : base(other)
    {
      // Empty
    }

    /// <summary>
    /// Sets the object identifier value in this message
    /// </summary>
    /// <param name="objectId">The object identifier value</param>
    public void SetObjectId(int objectId)
    {
      int offset = OBJECT_ID_OFFSET;
      Hydralizer.Put(m_aPayload, objectId, ref offset, false);
    }

    /// <summary>
    /// Gets the object identifier value in this message
    /// </summary>
    /// <returns>The object identifier value</returns>
    public int GetObjectId()
    {
      int offset = OBJECT_ID_OFFSET;
      return Hydralizer.GetInt32(m_aPayload, ref offset);
    }

    /// <summary>
    /// Sets the object contained within this message
    /// </summary>
    /// <param name="obj">The hydralizeable object</param>
    /// <param name="lite">Indicates the hydralize mode to use</param>
    /// <param name="indexed">Indicates if the object should be indexed</param>
    public void SetObject(object obj, bool lite, bool indexed)
    {
      byte[] aObj = Hydralizer.Hydralize(obj, lite, indexed);
      SetObjectBytes(aObj);
    }

    /// <summary>
    /// Sets the object data contained within this message from the <see cref="HydralizeInfo"/> specified
    /// </summary>
    /// <param name="info">The object data to set in this message</param>
    public void SetObjectInfo(HydralizeInfo info)
    {
      byte[] aObj = info.Hydralize();
      SetObjectBytes(aObj);
    }

    /// <summary>
    /// Gets the object in this message, this will only work for indexed messages
    /// </summary>
    /// <returns>The IHydralize object retrieved from the message or null if the object was unable to be retrieved</returns>
    public object GetObject()
    {
      HydralizeInfo info = GetObjectInfo();
      return Hydralizer.Dehydralize(info);
    }

    /// <summary>
    /// Gets the object data and loads it into the <see cref="IHydralize"/> object indicated
    /// </summary>
    /// <param name="obj">The object to load the message data into</param>
    public void GetObject(IHydralize obj)
    {
      int offset = OBJECT_OFFSET;
      Hydralizer.Dehydralize(obj, m_aPayload, ref offset);
    }

    /// <summary>
    /// Attempts to get the object in this message and cast or load the data into the <see cref="IHydralize"/> object specified
    /// </summary>
    /// <typeparam name="T">The type of object to cast or load the message data into</typeparam>
    /// <returns>The object retrieved from the message or the object default value if an error occurred</returns>
    public T GetObject<T>() where T : new()
    {
      HydralizeInfo info = GetObjectInfo();

      // If Indexed simply cast
      if (info.Indexed)
      {
        try
        {
          return (T)Hydralizer.Dehydralize(info);
        }
        catch
        {
          return default(T);
        }
      }
      else
      {
        T result = new T();
        object obj = result;
        Hydralizer.LoadObjectInfo(ref obj, info);
        return result;
      }
    }

    /// <summary>
    /// Gets the HydralizeInfo for the object in this message
    /// </summary>
    /// <returns>The HydralizeInfo describing the object in this message</returns>
    public HydralizeInfo GetObjectInfo()
    {
      HydralizeInfo info = new HydralizeInfo();
      int offset = OBJECT_OFFSET;
      info.Dehydralize(m_aPayload, ref offset);
      return info;
    }

    /// <summary>
    /// Sets the object data payload with the specified bytes
    /// </summary>
    /// <param name="objBytes">The object data payload bytes to set in this message</param>
    private void SetObjectBytes(byte[] objBytes)
    {
      int len = objBytes.Length + BASE_MESSAGE_LENGTH;
      if (this.Payload == null || this.Payload.Length != len)
      {
        int objId = GetObjectId();
        SetLength((UInt16)(BASE_MESSAGE_LENGTH + objBytes.Length));
        byte[] payload = new byte[len];
        Array.Copy(objBytes, 0, payload, BASE_MESSAGE_LENGTH, objBytes.Length);
        SetPayload(payload);
        SetObjectId(objId);
      }
      else
      {
        SetPayload(objBytes, 0, OBJECT_OFFSET, (UInt16)objBytes.Length);
      }
    }
  }
}
