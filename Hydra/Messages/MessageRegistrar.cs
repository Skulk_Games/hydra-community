﻿#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.IO;
using System.Reflection;

namespace Skulk.Hydra.Messages
{
  /// <summary>
  /// A class that provides utilities for loading message codes from file
  /// </summary>
  public class MessageRegistrar
  {
    /// <summary>
    /// Loads the fields and properties on this class that match values specified in a JSON file.
    /// </summary>
    /// <param name="filepath">The path to a json file specifying values to load.</param>
    /// <param name="typeInstance">The object instance to use if values are not static.</param>
    public static void Load<T>(string filepath, MessageRegistrar typeInstance = null)
    {
      try
      {
        Type type = typeInstance == null ? typeof(T) : typeInstance.GetType();
        if (Path.GetExtension(filepath).ToLower() == ".json")
        {
          Hydralize.JSON.JsonObject jsonObj = new Hydralize.JSON.JsonObject();
          string json = File.ReadAllText(filepath);
          int index = 0;
          if (jsonObj.Read(json, ref index))
          {
            // Search for fields/properties on this class that match keys
            foreach (var pair in jsonObj.Elements)
            {
              if (pair.Value.TokenValue.TokenType == Hydralize.JSON.JsonTokenType.Object)
              {
                if (pair.Key == type.Name || pair.Key == type.FullName)
                {
                  LoadObject(pair.Value.TokenValue as Hydralize.JSON.JsonObject, type, typeInstance);
                  break;
                }
              }
              else if (pair.Value.TokenValue.TokenType == Hydralize.JSON.JsonTokenType.Value)
              {
                LoadObject(jsonObj, type, typeInstance);
                break;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception loading message codes: '{0}'", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
      }
    }

    private static void LoadObject(Hydralize.JSON.JsonObject codes, Type registrarType, object typeInstance)
    {
      // Search for fields/properties on this class that match keys
      foreach (var pair in codes.Elements)
      {
        if (pair.Value.TokenValue.TokenType == Hydralize.JSON.JsonTokenType.Value)
        {
          var field = registrarType.GetField(pair.Key, BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
          if (field != null)
          {
            object instance = field.IsStatic ? null : typeInstance;

            if (!field.IsLiteral && field.IsStatic || typeInstance != null)
            {
              field.SetValue(instance, (ushort)(ulong)(pair.Value.TokenValue as Hydralize.JSON.JsonValue).Value);
            }
          }
          else
          {
            var prop = registrarType.GetProperty(pair.Key);
            if (prop != null)
            {
              var setMethod = prop.GetSetMethod();
              if (setMethod != null)
              {
                object instance = setMethod.IsStatic ? null : typeInstance;
                if (prop.GetGetMethod().IsStatic || typeInstance != null)
                {
                  prop.SetValue(instance, (ushort)(ulong)(pair.Value.TokenValue as Hydralize.JSON.JsonValue).Value);
                }
              }
            }
          }
        }
      }
    }
  }
}
