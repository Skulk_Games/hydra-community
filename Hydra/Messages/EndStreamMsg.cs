#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

namespace Skulk.Hydra
{
  /// <summary>
  /// A message sent after a stream finishes transferring
  /// </summary>
  public class EndStreamMsg : HydraObjMsg
  {
    /// <summary>
    /// Initializes this message with the object data specified
    /// </summary>
    /// <param name="streamData">The object data describing the stream that ended</param>
    public EndStreamMsg(HydraStreamInfo streamData) : base(HydraCodes.HydraStreamEnd, 0, streamData, true, true)
    {
      // Empty
    }

    /// <summary>
    /// Initializes this message with the data from another message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public EndStreamMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
