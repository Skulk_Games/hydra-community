#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

namespace Skulk.Hydra
{
  /// <summary>
  /// Message sent to acknowledge a handshake
  /// </summary>
  public class HydraHandshakeAckMsg : HydraObjMsg
  {
    /// <summary>
    /// Initializes a HydraHandshakeAckMsg with the information provided.
    /// </summary>
    /// <param name="ack">The ack information</param>
    public HydraHandshakeAckMsg(HandshakeInfo ack) : base(HydraCodes.HydraHandshakeAck, 0, ack, true)
    {
      // Empty
    }

    /// <summary>
    /// Initializes a HydraHanshakeAckMsg from the raw message buffers indicated.
    /// </summary>
    /// <param name="other">The message to use for initializing data.</param>
    public HydraHandshakeAckMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
