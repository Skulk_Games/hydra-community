#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;

namespace Skulk.Hydra.Messages
{
  /// <summary>
  /// A hydra message that requests or sends route entry information
  /// </summary>
  public class HydraRouteEntryMsg : HydraObjMsg
  {
    /// <summary>
    /// The object ID that indicates the message is a request
    /// </summary>
    public const int REQUEST_ID = 0;

    /// <summary>
    /// The object ID that indicates the message contains an entry
    /// </summary>
    public const int ENTRY_ID = 1;

    /// <summary>
    /// Initializes this message as a request for getting route entries
    /// </summary>
    public HydraRouteEntryMsg() : base(HydraCodes.HydraRouteEntry, REQUEST_ID, null)
    {
      // Empty
    }

    /// <summary>
    /// Initializes this message with data describing a route entry
    /// </summary>
    /// <param name="node">The routing node information to send in this message</param>
    public HydraRouteEntryMsg(HydraRouteMapNode node) : base(HydraCodes.HydraRouteEntry, ENTRY_ID, node, true, true)
    {
      // Empty
    }

    /// <summary>
    /// Initializes this message with the data from another message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public HydraRouteEntryMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
