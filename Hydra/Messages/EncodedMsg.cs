#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// A message that wraps encoded data
  /// </summary>
  public class EncodedMsg : Message
  {
    /// <summary>
    /// Initializes a new message with the specified array of bytes as it's payload
    /// </summary>
    /// <param name="crypto">The encoded array of bytes</param>
    public EncodedMsg(byte[] crypto) : base()
    {
      if (crypto.Length > UInt16.MaxValue)
      {
        throw new Exception("Encoded byte array too large, can't send more than 65,535 bytes in a message");
      }

      m_aHeader = new byte[HEADER_SIZE];
      SetCmd(HydraCodes.HydraEncodedData);
      SetLength((UInt16)crypto.Length);
      SetFlags(0);
      m_aPayload = crypto;
    }

    /// <summary>
    /// Initializes this message with data taken from another message
    /// </summary>
    /// <param name="other">The message containing the data to use in this message</param>
    public EncodedMsg(Message other) : base(other)
    {
      // Empty
    }

    /// <summary>
    /// Gets a reference to the array of encoded bytes in this message
    /// </summary>
    /// <returns>The array of encoded bytes in this message</returns>
    public byte[] GetEncodedBytes()
    {
      return m_aPayload;
    }
  }
}
