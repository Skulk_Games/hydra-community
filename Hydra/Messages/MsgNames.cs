#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Skulk.Hydra.Messages
{
  /// <summary>
  /// Utility class for associating a name with message command codes
  /// </summary>
  public static class MsgNames
  {
    /// <summary>
    /// Dictionary used to associated a name with a message code
    /// </summary>
    private static Dictionary<ushort, string> m_nameLookup = new Dictionary<ushort, string>();

    /// <summary>
    /// Adds the specified command code associated with the indicated message name for lookup
    /// </summary>
    /// <param name="cmdCode">The command code of the message</param>
    /// <param name="name">The name for the message</param>
    public static void Add(ushort cmdCode, string name)
    {
      if (!m_nameLookup.ContainsKey(cmdCode))
      {
        m_nameLookup.Add(cmdCode, name);
      }
      else
      {
        m_nameLookup[cmdCode] = name;
      }
    }

    /// <summary>
    /// Adds the public constant ushort message codes defined on a type to the name lookup
    /// </summary>
    /// <param name="type">The type containing the constant message codes</param>
    public static void AddConstants(Type type)
    {
      FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
      foreach (FieldInfo field in fields)
      {
        if (field.FieldType == typeof(ushort))
        {
          Add((ushort)field.GetValue(null), field.Name);
        }
      }

      PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
      foreach (PropertyInfo property in properties)
      {
        if (property.PropertyType == typeof(ushort))
        {
          Add((ushort)property.GetValue(null), property.Name);
        }
      }
    }

    /// <summary>
    /// Gets the name of the message with the specified code
    /// </summary>
    /// <param name="cmdCode">The command code to get the name of</param>
    /// <returns>The name of the message with the specified code or the command code as a string if not found</returns>
    public static string Get(ushort cmdCode)
    {
      if (m_nameLookup.TryGetValue(cmdCode, out string name))
      {
        return name;
      }

      return cmdCode.ToString();
    }
  }
}
