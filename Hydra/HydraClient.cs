#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Skulk.Hydra
{
  /// <summary>
  /// Implements a Hydra application as a client
  /// </summary>
  public class HydraClient : HydraCore
  {
    /// <summary>
    /// Indicates if a TCP stream should be maintained automatically
    /// </summary>
    private bool m_maintainStream = false;

    /// <summary>
    /// Indicates whether a stream connection has recently failed
    /// </summary>
    private bool m_streamFailed;

    /// <summary>
    /// Internally used for determining the time to wait before retrying to start a TCP stream
    /// </summary>
    private double m_tcpTime = 0;

    /// <summary>
    /// The period in seconds at which the TCP stream will attempt to reconnect
    /// </summary>
    private double m_tcpPeriod = 10;

    /// <summary>
    /// flag used to indicate if the TCP connection is in the process of starting 
    /// </summary>
    private bool m_connectionStarting;

    // UDP reconnection fields
    /// <summary>
    /// Indicates whether UDP should attempt to reconnect when disconnected
    /// </summary>
    private bool m_maintainUdp;

    /// <summary>
    /// Indicates whether the UDP connection has recently failed
    /// </summary>
    private bool m_udpFailed;

    /// <summary>
    /// The running time counter for UDP reconnections
    /// </summary>
    private double m_udpTime;

    /// <summary>
    /// Address information for the server
    /// </summary>
    private HydraAddress m_serverAddress;

    /// <summary>
    /// The period in seconds that re-establishing a TCP stream should be attempted
    /// </summary>
    public double TcpPeriod { get => m_tcpPeriod; set => m_tcpPeriod = value; }

    /// <summary>
    /// Indicates if the application should attempt to reconnect if UDP becomes disconnected
    /// </summary>
    public bool MaintainUdp { get => m_maintainUdp; set => m_maintainUdp = value; }

    /// <summary>
    /// The address for the server
    /// </summary>
    public HydraAddress ServerAddress { get => m_serverAddress; set => m_serverAddress = value; }

    /// <summary>
    /// Initializes a new instance of a <see cref="HydraClient"/> class with the specified values
    /// </summary>
    /// <param name="settings">The application configuration settings</param>
    public HydraClient(HydraClientSettings settings) : base(settings)
    {
      IPAddress ip = ResolveHostname(settings.ServerHost);
      m_serverAddress = new HydraAddress(ip.ToString(), settings.ServerUdpPort, settings.ServerTcpPort);
      ProfileManager.OnConnectionFailed += HandleConnectionFailure;
    }

    /// <summary>
    /// A default implementation for sending messages to the server
    /// </summary>
    /// <param name="message">The message to send to the server</param>
    public void SendMessage(Message message)
    {
      if (ServerUuid != null)
      {
        SendMessage(message, ServerUuid);
      }
    }

    /// <summary>
    /// Client method that sends a handshake to the default server
    /// </summary>
    /// <param name="localKey">The key for the local profile associated with the remote server</param>
    /// <param name="remoteKey">The profile key to use when handshaking, use 0 for a new profile</param>
    /// <returns>True if the handshake message was enqueued; otherwise, false.</returns>
    public virtual bool SendHandshake(ulong localKey = 0, ulong remoteKey = 0)
    {
      return SendHandshake(m_serverAddress.GetUdpEndPoint(), localKey, remoteKey);
    }

    /// <summary>
    /// Sends UDP handshake messages and waits for a successful UDP handshake attempt
    /// </summary>
    /// <param name="timeout">The max time in seconds to wait for the handshake</param>
    /// <param name="key">The profile key to use when handshaking, use 0 for a new profile connection</param>
    /// <returns>True if the handshake succeeded, false on a timeout</returns>
    public bool WaitForHandshake(double timeout = 60, ulong key = 0)
    {
      double time = 0;
      List<Profile> profiles = null;
      while (time < timeout && !StopRun)
      {
        profiles = m_profileManager.GetProfiles(HydraModes.SERVER);
        if (profiles.Count > 0)
        {
          if (profiles[0].HasConnection(ConnectionTypes.MESSAGE))
          {
            return true;
          }
        }

        SendHandshake(0, key);
        Thread.Sleep(500);
        time += 0.5;
      }

      profiles = m_profileManager.GetProfiles(HydraModes.SERVER);
      if (profiles.Count > 0)
      {
        if (profiles[0].HasConnection(ConnectionTypes.MESSAGE))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Sends TCP handshake connections and waits for a successful attempt
    /// </summary>
    /// <param name="timeout">The max time in seconds to attempt to connect</param>
    /// <returns>True if the TCP connection was successful, false on a timeout</returns>
    public bool WaitForTcpConnection(double timeout = 60)
    {
      Stopwatch sw = Stopwatch.StartNew();
      List<Profile> profiles = new List<Profile>();
      while (sw.Elapsed.TotalSeconds < timeout && !StopRun)
      {
        profiles = m_profileManager.GetProfiles(HydraModes.SERVER);
        if (profiles.Count > 0)
        {
          if (profiles[0].HasConnection(ConnectionTypes.STREAM))
          {
            return true;
          }
        }

        if (StartTcpConnection())
        {
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Sets whether a TCP connection should attempt to be maintained
    /// </summary>
    /// <param name="maintain">If true the application will attempt to maintain a TCP connection with the server</param>
    public void MaintainStream(bool maintain)
    {
      m_maintainStream = maintain;
    }

    /// <summary>
    /// Starts a TCP stream connection to a HydraServer
    /// </summary>
    /// <returns>True if the stream successfully established, false otherwise</returns>
    public bool StartTcpConnection(int timeoutMs = 3000)
    {
      m_connectionStarting = true;
      try
      {
        Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        client.Blocking = true;
        client.Connect(m_serverAddress.Host, m_serverAddress.StreamPort);
        TcpConnection tcpConnection = CreateTcpConnection(client);
        Profile profile = m_profileManager.GetProfile(ServerUuid);
        HandshakeInfo handshakeInfo = new HandshakeInfo(Uuid, ApplicationName, HydraMode, Address, profile?.Key ?? 0, profile?.RemoteKey ?? 0);
        HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(handshakeInfo);
        tcpConnection.Write(handshakeMsg);

        if (!StopRun && tcpConnection.ReceiveHandshake(timeoutMs, out HandshakeInfo info, Log))
        {
          bool success;
          if (m_profileManager.Contains(info.Uuid))
          {
            success = AddConnection(tcpConnection, info.Uuid);
          }
          else
          {
            success = AddConnection(tcpConnection, info, true);
          }

          if (success)
          {
            m_connectionStarting = false;
            tcpConnection.Start(true);
            return true;
          }
          else
          {
            Log?.LogError("Failed to add newly handshaked TCP connection on UUID: {0}, stopping and closing connection", info.Uuid);
          }
        }
        else
        {
          Log.LogError("Failed to handshake new TCP connection, stopping and closing connection");
        }

        tcpConnection.Stop();
        tcpConnection.Close();
      }
      catch (Exception ex)
      {
        Log?.LogError("Exception while starting TCP connection: {0}", ex.Message);
        Log.LogDebug("Full Exception: {0}", ex.ToString());
      }

      m_connectionStarting = false;
      return false;
    }

    /// <summary>
    /// Handles transferring a client to a new server
    /// </summary>
    /// <param name="transferData">The transfer data</param>
    protected override void HandleHydraTransfer(object transferData)
    {
      base.HandleHydraTransfer(transferData);
      HydraTransferData data = transferData as HydraTransferData;
      if (data != null && !string.IsNullOrEmpty(data.HostName))
      {
        m_serverAddress.Host = data.HostName;
        m_serverAddress.MessagePort = data.Port;
        m_serverAddress.StreamPort = data.StreamPort;

        WaitForHandshake(5);
        if (data.StartStream)
        {
          StartTcpConnection(5000);
        }
      }
    }

    /// <summary>
    /// Handles core hydra timing based responses and actions
    /// </summary>
    /// <param name="updateTime">The time in seconds since the last update call</param>
    protected override void Update(UpdateTime updateTime)
    {
      base.Update(updateTime);
      if (m_maintainStream)
      {
        List<Profile> profiles = m_profileManager.GetProfiles(HydraModes.SERVER);
        if (profiles.Count < 1 || !profiles[0].HasConnection(ConnectionTypes.STREAM))
        {
          m_tcpTime += updateTime.DeltaSeconds;
          if (m_tcpTime >= m_tcpPeriod || m_streamFailed)
          {
            m_streamFailed = false;
            if (!m_connectionStarting)
            {
              m_connectionStarting = true;
              Thread thread = new Thread(ThreadedStartTcpConnection)
              {
                IsBackground = true
              };
              thread.Start();
            }
            m_tcpTime = 0;
          }
        }
      }

      if (m_maintainUdp)
      {
        m_udpTime += updateTime.DeltaSeconds;
        if (m_udpTime >= Settings.KeepAlivePeriod || m_udpFailed)
        {
          m_udpFailed = false;
          Log?.LogDebug("Checking UDP state");
          m_udpTime = 0;
          Profile profile = m_profileManager.GetProfile(ServerUuid);
          if (profile == null)
          {
            Log?.LogDebug("Profile null for server address, sending handshake...");
            SendHandshake(m_serverAddress.GetUdpEndPoint(), 0, 0);
          }
          else if (profile.GetConnection(ConnectionTypes.MESSAGE) == null)
          {
            Log?.LogDebug("Sending handshake for server profile with local key: {0} and remote key: {1}", profile.Key, profile.RemoteKey);
            SendHandshake(m_serverAddress.GetUdpEndPoint(), profile.Key, profile.RemoteKey);
          }
          else
          {
            Log?.LogTrace("Active UDP connection");
          }
        }
      }
    }

    /// <summary>
    /// Performs automatic handling of received Hydra error messages
    /// </summary>
    /// <param name="error">The error received</param>
    /// <param name="uuid">The UUID of the connection that sent the error</param>
    /// <returns>True if the error was handled, false otherwise</returns>
    protected override bool HandleHydraError(HydraErrorInfo error, HydraUuid uuid)
    {
      if (!base.HandleHydraError(error, uuid))
      {
        switch (error.Error)
        {
          case HydraErrors.NO_OPEN_TCP_CONNECTION:
          {
            // Don't start multiple times - set flag to prevent duplicate error messages from starting connections
            if (!m_connectionStarting)
            {
              m_connectionStarting = true;
              Thread thread = new Thread(ThreadedStartTcpConnection)
              {
                IsBackground = true
              };
              thread.Start();
            }
          }
          break;

          case HydraErrors.INVALID_HANDSHAKE_KEY:
          {
            Log?.LogInfo("Used an invalid key when connecting, removing server profile due to invalid key.");
            RemoveProfile(ServerUuid);
          }
          break;

          default:
            return false;
        }
      }

      return true;
    }

    /// <summary>
    /// A thread compatible method that starts the TCP connection method
    /// </summary>
    private void ThreadedStartTcpConnection()
    {
      Log.LogTrace("Threaded starting TCP connection");
      StartTcpConnection();
    }

    private void HandleConnectionFailure(Connection connection)
    {
      switch (connection.ConnectionType)
      {
        case ConnectionTypes.MESSAGE:
        {
          Log?.LogInfo("UDP connection failed, setting flag");
          m_udpFailed = true;
        }
        break;

        case ConnectionTypes.STREAM:
        {
          Log?.LogInfo("TCP connection failed, setting stream failure flag");
          m_streamFailed = true;
        }
        break;
      }

      Log?.LogDebug("Connection failed with a profile with local key: {0} and remote key: {1}", connection.Profile.Key, connection.Profile.RemoteKey);
    }
  }
}
