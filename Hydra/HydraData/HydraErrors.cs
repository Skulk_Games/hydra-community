#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Standard Hydra Error codes.
  /// </summary>
  public enum HydraErrors : UInt32
  {
    /// <summary>
    /// Indicates no error
    /// </summary>
    NULL,

    /// <summary>
    /// The client ID was not valid.
    /// </summary>
    INVALID_CLIENT_ID,

    /// <summary>
    /// The port number was not valid.
    /// </summary>
    INVALID_PORT,

    /// <summary>
    /// The action required a TCP connection, but none was found.
    /// </summary>
    NO_OPEN_TCP_CONNECTION,

    /// <summary>
    /// A stream is required, but no stream has been setup.
    /// </summary>
    NO_STREAM_SETUP,

    /// <summary>
    /// A handshake requesting an ID used an invalid key
    /// </summary>
    INVALID_HANDSHAKE_KEY
  }
}
