#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;

namespace Skulk.Hydra
{
  /// <summary>
  /// Data class containing information for transferring a client to another server
  /// </summary>
  public class HydraTransferData : HydralizeObject
  {
    private string m_hostname;
    private int m_port;
    private int m_streamPort;
    private bool m_startStream;

    /// <summary>
    /// The hostname of the new server
    /// </summary>
    public string HostName { get => m_hostname; set => m_hostname = value; }

    /// <summary>
    /// The port to connect on for UDP messaging
    /// </summary>
    public int Port { get => m_port; set => m_port = value; }

    /// <summary>
    /// The port to connect on for TCP streaming
    /// </summary>
    public int StreamPort { get => m_streamPort; set => m_streamPort = value; }

    /// <summary>
    /// Indicates if the TCP stream should start automatically once transferred
    /// </summary>
    public bool StartStream { get => m_startStream; set => m_startStream = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraTransferData()
    {
      // Empty
    }

    /// <summary>
    /// Constructs this class with the data specified
    /// </summary>
    /// <param name="hostname">The server hostname</param>
    /// <param name="port">The server UDP port</param>
    /// <param name="streamPort">The server TCP port</param>
    /// <param name="startStream">Indicates if the TCP stream should automatically start once transferred</param>
    public HydraTransferData(string hostname, int port, int streamPort, bool startStream)
    {
      m_hostname = hostname;
      m_port = port;
      m_streamPort = streamPort;
      m_startStream = startStream;
    }
  }
}
