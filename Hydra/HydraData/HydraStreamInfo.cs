#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Enumeration of the different directions of Hydra streams
  /// </summary>
  public enum HydraStreamDirections : byte
  {
    /// <summary>
    /// A default null mode, used to indicate an unset value.
    /// </summary>
    NULL,

    /// <summary>
    /// The mode where data is read from the network stream and written to a local stream.
    /// </summary>
    READ,

    /// <summary>
    /// The mode where data is written to the network stream and read from a local stream.
    /// </summary>
    WRITE
  }

  /// <summary>
  /// Enumeration of the modes used when transferring stream data
  /// </summary>
  public enum StreamTransferModes : byte
  {
    /// <summary>
    /// An unset stream transfer mode
    /// </summary>
    NULL,

    /// <summary>
    /// The stream data will be sent in message segments
    /// </summary>
    SEGMENTED,

    /// <summary>
    /// The stream data will be sent raw following the start message
    /// </summary>
    CONTINUOUS
  }

  /// <summary>
  /// Class containing the information describing a stream
  /// </summary>
  public class HydraStreamInfo : HydralizeObject
  {
    /// <summary>
    /// The unique identifier for the stream being transferred
    /// </summary>
    private UInt32 m_id;

    /// <summary>
    /// The total length of the stream being sent
    /// </summary>
    private Int64 m_length;

    /// <summary>
    /// The mode indicating the way the stream data will be transferred
    /// </summary>
    private StreamTransferModes m_mode;

    /// <summary>
    /// The direction of the stream transfer
    /// </summary>
    private HydraStreamDirections m_direction;

    /// <summary>
    /// Gets or sets the unique identifier of a stream
    /// </summary>
    public uint Id { get => m_id; set => m_id = value; }

    /// <summary>
    /// Gets or sets the total length of the stream being transferred
    /// </summary>
    public long Length { get => m_length; set => m_length = value; }

    /// <summary>
    /// Gets or sets the mode of transfer, which indicates the way data is transferred
    /// </summary>
    public StreamTransferModes Mode { get => m_mode; set => m_mode = value; }

    /// <summary>
    /// Gets or sets the direction of the stream transfer
    /// </summary>
    public HydraStreamDirections Direction { get => m_direction; set => m_direction = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraStreamInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraStreamInfo"/> class with the specified values
    /// </summary>
    /// <param name="id">The ID of the stream</param>
    /// <param name="length">The total length in bytes of the stream</param>
    /// <param name="mode">The transfer mode to use</param>
    /// <param name="direction">The direction of the transfer</param>
    public HydraStreamInfo(UInt32 id, long length, StreamTransferModes mode, HydraStreamDirections direction)
    {
      m_id = id;
      m_length = length;
      m_mode = mode;
      m_direction = direction;
    }
  }
}
