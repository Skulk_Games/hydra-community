#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.IO;

namespace Skulk.Hydra
{
  /// <summary>
  /// Describes a stream for transfer over a connection
  /// </summary>
  public class ConnectionStreamInfo
  {
    private HydraStreamInfo m_remoteStreamInfo;
    private HydraStreamInfo m_localStreamInfo;
    private Stream m_stream;
    private long m_transferCount;
    private UInt32 m_index;

    /// <summary>
    /// The information describing the remote receiving stream
    /// </summary>
    public HydraStreamInfo RemoteStreamInfo { get => m_remoteStreamInfo; set => m_remoteStreamInfo = value; }

    /// <summary>
    /// The information describing the local sending stream
    /// </summary>
    public HydraStreamInfo LocalStreamInfo { get => m_localStreamInfo; set => m_localStreamInfo = value; }

    /// <summary>
    /// The actual stream to transfer on a connection
    /// </summary>
    public Stream Stream { get => m_stream; set => m_stream = value; }

    /// <summary>
    /// The number of bytes transferred
    /// </summary>
    public long TransferCount { get => m_transferCount; set => m_transferCount = value; }

    /// <summary>
    /// The current segment index the stream is on
    /// </summary>
    public UInt32 Index { get => m_index; set => m_index = value; }

    /// <summary>
    /// Initializes the connection stream information with the specified values
    /// </summary>
    /// <param name="remoteInfo">The object describing the remote stream information</param>
    /// <param name="localInfo">The object describig the local stream information</param>
    /// <param name="stream">The stream containing the data to transfer</param>
    public ConnectionStreamInfo(HydraStreamInfo remoteInfo, HydraStreamInfo localInfo, Stream stream)
    {
      m_remoteStreamInfo = remoteInfo;
      m_localStreamInfo = localInfo;
      m_stream = stream;
      m_transferCount = 0;
      m_index = 0;
    }
  }
}
