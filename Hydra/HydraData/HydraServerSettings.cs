#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

namespace Skulk.Hydra
{
  /// <summary>
  /// Configuration settings for a <see cref="HydraServer"/> application
  /// </summary>
  public class HydraServerSettings : HydraSettings
  {
    private int m_tcpPort = 52600;
    private string m_tcpBindAddress = "0.0.0.0";

    /// <summary>
    /// The TCP port the server should listen on.
    /// </summary>
    public int TcpPort { get => m_tcpPort; set => m_tcpPort = value; }

    /// <summary>
    /// The IP address to bind to for TCP connections.
    /// </summary>
    public string TcpBindAddress { get => m_tcpBindAddress; set => m_tcpBindAddress = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraServerSettings()
    {
      Mode = HydraModes.SERVER;
    }

    /// <summary>
    /// Initializes this server settings with a copy of another settings
    /// </summary>
    /// <param name="serverSettings">The server settings to copy</param>
    public HydraServerSettings(HydraServerSettings serverSettings) : base(serverSettings)
    {
      m_tcpPort = serverSettings.m_tcpPort;
      m_tcpBindAddress = serverSettings.m_tcpBindAddress;
    }
  }
}
