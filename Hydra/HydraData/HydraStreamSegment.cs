#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Contains data describing a segment of stream data
  /// </summary>
  public class HydraStreamSegment : HydralizeObject
  {
    /// <summary>
    /// The ID of the stream this data is a part of
    /// </summary>
    private UInt32 m_id;

    /// <summary>
    /// The index of this data segment in the complete stream
    /// </summary>
    private UInt32 m_index;

    /// <summary>
    /// The data for this segment of the stream
    /// </summary>
    private byte[] m_data;

    /// <summary>
    /// Gets or sets the ID of the stream this data is a part of
    /// </summary>
    public uint Id { get => m_id; set => m_id = value; }

    /// <summary>
    /// Gets or sets the index of this segment in the complete stream
    /// </summary>
    public uint Index { get => m_index; set => m_index = value; }

    /// <summary>
    /// Gets or sets the data for this segment
    /// </summary>
    public byte[] Data { get => m_data; set => m_data = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraStreamSegment()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a stream segment instance with the specified values
    /// </summary>
    /// <param name="id">The ID of the stream</param>
    /// <param name="index">The index of this segment</param>
    /// <param name="data">The array of raw data bytes</param>
    public HydraStreamSegment(UInt32 id, UInt32 index, byte[] data)
    {
      m_id = id;
      m_index = index;
      m_data = data;
    }
  }
}
