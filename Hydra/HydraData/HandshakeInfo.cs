#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using Skulk.Hydralize;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Contains information used in Hydra handshake messages
  /// </summary>
  public class HandshakeInfo : HydralizeObject
  {
    private HydraUuid m_uuid;
    private HydraModes m_mode;
    private UInt64 m_localKey;
    private UInt64 m_remoteKey;
    private int m_port;
    HydraAddress m_address;
    private string m_name;

    /// <summary>
    /// The ID of the source profile
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The key value for the local profile associated with the remote destination
    /// </summary>
    public UInt64 LocalKey { get => m_localKey; set => m_localKey = value; }

    /// <summary>
    /// A key value used to associate this connection with a remote active data path
    /// </summary>
    public UInt64 RemoteKey { get => m_remoteKey; set => m_remoteKey = value; }

    /// <summary>
    /// The assigned port for the connection
    /// </summary>
    public int Port { get => m_port; set => m_port = value; }

    /// <summary>
    /// The mode of the application that sent this info
    /// </summary>
    public HydraModes Mode { get => m_mode; set => m_mode = value; }

    /// <summary>
    /// The address information of the handshaking application
    /// </summary>
    public HydraAddress Address { get => m_address; set => m_address = value; }

    /// <summary>
    /// The name of the application sending this handshake info
    /// </summary>
    public string Name { get => m_name; set => m_name = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HandshakeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this instance with the specified values
    /// </summary>
    /// <param name="uuid">The UUID of the Hydra application sending the handshake info</param>
    /// <param name="name">The name of the application sending this info</param>
    /// <param name="mode">The mode of the application sending this info</param>
    /// <param name="address">The address information for the application that sent this info</param>
    /// <param name="localKey">The key value for the local profile associated with the remote destination</param>
    /// <param name="remoteKey">A key value used to associate this connection with another data path</param>
    /// <param name="port">The port assigned to the client connection</param>
    public HandshakeInfo(HydraUuid uuid, string name, HydraModes mode, HydraAddress address, UInt64 localKey = 0, UInt64 remoteKey = 0, int port = -1)
    {
      m_uuid = uuid;
      m_name = name;
      m_mode = mode;
      m_localKey = localKey;
      m_remoteKey = remoteKey;
      m_port = port;
      m_address = address;
    }
  }
}
