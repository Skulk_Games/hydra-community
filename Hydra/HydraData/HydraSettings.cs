#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize.Encoding;
using Skulk.MEL;

namespace Skulk.Hydra
{
  /// <summary>
  /// A class containing the configurable settings for running a Hydra application
  /// </summary>
  public class HydraSettings
  {
    // Core local settings
    private string m_applicationName = "Hydra";
    private int m_udpPort = 0;
    private TranscoderPair m_messageEncoder = null;
    private TranscoderPair m_streamEncoder = null;
    private HydraModes m_mode = HydraModes.CLIENT;
    private int m_maxClients = 10;
    private LogLevels m_logLevel = LogLevels.ERROR | LogLevels.WARNING | LogLevels.INFO;
    private string m_logFile = "hydra.log";
    private bool m_enableConsoleLog = true;
    private double m_keepAlivePeriod = 10;
    private double m_profileTimeout = 30;
    private bool m_useRouting = false;
    private double m_maxUpdateHz = 10;
    private double m_streamTimeout = 3600;
    private bool m_disableUdp = false;
    private HydraUuid m_uuid = null;
    private string m_messageCodeFile = null;
    private string m_udpBindAddress = "0.0.0.0";

    /// <summary>
    /// The name to use for the Hydra application
    /// </summary>
    public string ApplicationName { get => m_applicationName; set => m_applicationName = value; }

    /// <summary>
    /// The UDP port to listen on
    /// </summary>
    public int UdpPort { get => m_udpPort; set => m_udpPort = value; }

    /// <summary>
    /// The encoder to use for message based connections
    /// </summary>
    public TranscoderPair MessageEncoder { get => m_messageEncoder; set => m_messageEncoder = value; }

    /// <summary>
    /// The encoder to use for stream based connections
    /// </summary>
    public TranscoderPair StreamEncoder { get => m_streamEncoder; set => m_streamEncoder = value; }

    /// <summary>
    /// The mode this application is running in
    /// </summary>
    public HydraModes Mode { get => m_mode; set => m_mode = value; }

    /// <summary>
    /// The max number of clients allowed on this application
    /// </summary>
    public int MaxClients { get => m_maxClients; set => m_maxClients = value; }

    /// <summary>
    /// The logging level to use for the application
    /// </summary>
    public LogLevels LogLevel { get => m_logLevel; set => m_logLevel = value; }

    /// <summary>
    /// The file to use for writing log messages
    /// </summary>
    public string LogFile { get => m_logFile; set => m_logFile = value; }

    /// <summary>
    /// Indicates if log messages should be printed to the system console
    /// </summary>
    public bool EnableConsoleLog { get => m_enableConsoleLog; set => m_enableConsoleLog = value; }

    /// <summary>
    /// The time in seconds between sending keep alive messages when a connection is silent
    /// </summary>
    public double KeepAlivePeriod { get => m_keepAlivePeriod; set => m_keepAlivePeriod = value; }

    /// <summary>
    /// The time in seconds before a profile should be removed due to inactivity
    /// </summary>
    public double ProfileTimeout { get => m_profileTimeout; set => m_profileTimeout = value; }

    /// <summary>
    /// Indicates whether this application should support routing messages
    /// </summary>
    public bool UseRouting { get => m_useRouting; set => m_useRouting = value; }

    /// <summary>
    /// The max rate that real time Updates can be called
    /// </summary>
    public double MaxUpdateHz { get => m_maxUpdateHz; set => m_maxUpdateHz = value; }

    /// <summary>
    /// The max time in seconds a stream will be tracked before it should be removed
    /// </summary>
    public double StreamTimeout { get => m_streamTimeout; set => m_streamTimeout = value; }

    /// <summary>
    /// Indicates if UDP listening should be disabled on this application.
    /// </summary>
    public bool DisableUdp { get => m_disableUdp; set => m_disableUdp = value; }

    /// <summary>
    /// A manually specified UUID to use on the Hydra application.
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The path to a file used to specify custom message code values.
    /// </summary>
    public string MessageCodeFile { get => m_messageCodeFile; set => m_messageCodeFile = value; }

    /// <summary>
    /// The IP to bind the UDP listener to.
    /// </summary>
    public string UdpBindAddress { get => m_udpBindAddress; set => m_udpBindAddress = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraSettings()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this instance with copied values from another settings instance
    /// </summary>
    /// <param name="settings">The settings to copy</param>
    public HydraSettings(HydraSettings settings)
    {
      m_applicationName = settings.m_applicationName;
      m_udpPort = settings.m_udpPort;
      m_messageEncoder = settings.m_messageEncoder;
      m_streamEncoder = settings.m_streamEncoder;
      m_mode = settings.m_mode;
      m_maxClients = settings.m_maxClients;
      m_logLevel = settings.m_logLevel;
      m_logFile = settings.m_logFile;
      m_enableConsoleLog = settings.m_enableConsoleLog;
      m_keepAlivePeriod = settings.m_keepAlivePeriod;
      m_profileTimeout = settings.m_profileTimeout;
      m_useRouting = settings.m_useRouting;
      m_maxUpdateHz = settings.m_maxUpdateHz;
      m_streamTimeout = settings.m_streamTimeout;
      m_disableUdp = settings.m_disableUdp;
      m_uuid = settings.m_uuid != null ? new HydraUuid(settings.m_uuid) : null;
      m_udpBindAddress = settings.m_udpBindAddress;
    }
  }
}
