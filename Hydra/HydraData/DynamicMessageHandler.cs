#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL;
using System;
using System.Collections.Generic;

namespace Skulk.Hydra
{
  /// <summary>
  /// A <see cref="MessageHandlerRouter"/> implementation that supports dynamically adding and removing handling for messages
  /// </summary>
  /// <remarks>
  /// Introduces locking overhead to support multi-threaded access. As such the base <see cref="MessageHandlerRouter"/> class should be used
  /// if this feature is not needed
  /// </remarks>
  public class DynamicMessageHandler : MessageHandlerRouter
  {
    private object m_cacheLock = new object();

    /// <summary>
    /// Default constructor
    /// </summary>
    public DynamicMessageHandler(ILogger log = null) : base(log)
    {
      // Empty
    }

    /// <summary>
    /// Routes a message to its handling HydraModule.
    /// </summary>
    /// <param name="modules">The collection of modules to search for handling HydraModules</param>
    /// <param name="msg">The message to route.</param>
    /// <param name="client">The ID of the client that received the message.</param>
    /// <returns>True if the message was routed, false if no handling HydraModule was found.</returns>
    public override bool RouteMessage(IEnumerable<IModule> modules, Message msg, HydraUuid client)
    {
      lock (m_cacheLock)
      {
        return base.RouteMessage(modules, msg, client);
      }
    }

    /// <summary>
    /// Routes a message to its cached handling HydraModule.
    /// </summary>
    /// <param name="msg">The message to route.</param>
    /// <param name="client">The ID of the client that received the message.</param>
    /// <returns>True if the message was routed, false if no handling HydraModule was found.</returns>
    public bool RouteMessage(Message msg, HydraUuid client)
    {
      lock (m_cacheLock)
      {
        return base.RouteCached(msg, client) == true;
      }
    }

    /// <summary>
    /// Adds message handling for the specified message code
    /// </summary>
    /// <param name="messageCode">The message code to handle</param>
    /// <param name="handler">The handler to route messages with the matching code to</param>
    public void AddHandling(UInt16 messageCode, MessageClientCallback handler)
    {
      lock (m_cacheLock)
      {
        Cache(messageCode, handler);
      }
    }

    /// <summary>
    /// Removes message handling for the specified message code
    /// </summary>
    /// <param name="messageCode">The message code to remove handling for</param>
    /// <returns>True if the handling was removed, otherwise false</returns>
    public bool RemoveHandling(UInt16 messageCode)
    {
      lock (m_cacheLock)
      {
        return Uncache(messageCode);
      }
    }
  }
}
