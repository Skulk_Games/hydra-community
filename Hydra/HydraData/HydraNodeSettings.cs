#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Configuration settings for a <see cref="HydraNode"/> application
  /// </summary>
  public class HydraNodeSettings : HydraServerSettings
  {
    private double m_discoveryPeriod = 300;
    private double m_discoveryTimeout = 900;
    private int[] m_discoveryPorts = null;

    /// <summary>
    /// The period in seconds between sending discovery pings
    /// </summary>
    public double DiscoveryPeriod { get => m_discoveryPeriod; set => m_discoveryPeriod = value; }

    /// <summary>
    /// The time in seconds without a response before a node entry is removed from the discovery list
    /// </summary>
    public double DiscoveryTimeout { get => m_discoveryTimeout; set => m_discoveryTimeout = value; }

    /// <summary>
    /// The port numbers to broadcast discovery messages on, if null the local application's UDP port will be used
    /// </summary>
    public int[] DiscoveryPorts { get => m_discoveryPorts; set => m_discoveryPorts = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraNodeSettings()
    {
      Mode = HydraModes.NODE;
    }

    /// <summary>
    /// Initializes this instance with a copy of another settings
    /// </summary>
    /// <param name="nodeSettings">The node settings to copy</param>
    public HydraNodeSettings(HydraNodeSettings nodeSettings) : base(nodeSettings)
    {
      m_discoveryPeriod = nodeSettings.m_discoveryPeriod;
      m_discoveryTimeout = nodeSettings.m_discoveryTimeout;
      if (nodeSettings.m_discoveryPorts != null)
      {
        m_discoveryPorts = new int[nodeSettings.m_discoveryPorts.Length];
        Array.Copy(nodeSettings.m_discoveryPorts, m_discoveryPorts, m_discoveryPorts.Length);
      }
    }
  }
}
