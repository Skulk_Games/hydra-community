#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Defines a command code and message handler pair
  /// </summary>
  public class HydraMessageHandler
  {
    private UInt16 m_cmdCode;
    private string m_messageName;
    private MessageClientCallback m_handler;

    /// <summary>
    /// The command code associated with the handler
    /// </summary>
    public ushort CmdCode { get => m_cmdCode; set => m_cmdCode = value; }

    /// <summary>
    /// The name of the message associated with the command code.
    /// </summary>
    public string MessageName { get => m_messageName; set => m_messageName = value; }

    /// <summary>
    /// The message received handler callback
    /// </summary>
    public MessageClientCallback Handler { get => m_handler; set => m_handler = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraMessageHandler()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this HydrMessageHandler with the specified command code and handler delegate
    /// </summary>
    /// <param name="cmdCode">The command code of a message to handle</param>
    /// <param name="handler">The delegate to invoke when handling a message with the specified command code</param>
    /// <param name="name">The name of the message associated with the command code</param>
    public HydraMessageHandler(UInt16 cmdCode, MessageClientCallback handler, string name = null)
    {
      m_cmdCode = cmdCode;
      m_handler = handler;
      m_messageName = name;
    }

    /// <summary>
    /// Compares the message code of another <see cref="HydraMessageHandler"/> to determine equality
    /// </summary>
    /// <param name="obj">The <see cref="HydraMessageHandler"/> to compare</param>
    /// <returns>True if the other object is a <see cref="HydraMessageHandler"/> and specifies the same command code; otherwise, false</returns>
    public override bool Equals(object obj)
    {
      HydraMessageHandler otherHandler = obj as HydraMessageHandler;
      if (otherHandler != null)
      {
        return otherHandler.m_cmdCode == m_cmdCode;
      }

      return false;
    }

    /// <summary>
    /// Gets a hash code for this instance
    /// </summary>
    /// <returns>The command code</returns>
    public override int GetHashCode()
    {
      return m_cmdCode;
    }
  }
}
