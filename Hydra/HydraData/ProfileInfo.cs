#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra
{
  /// <summary>
  /// Class containing information relating to a Profile
  /// </summary>
  public class ProfileInfo : HydralizeObject
  {
    private HydraUuid m_uuid;
    private UInt64 m_key;

    /// <summary>
    /// The UUID of a profile to connect with
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The key of a profile to connect with
    /// </summary>
    public ulong Key { get => m_key; set => m_key = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public ProfileInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProfileInfo"/> class with the specified values
    /// </summary>
    /// <param name="uuid">The UUID of a profile</param>
    /// <param name="key">The authentication key of a profile</param>
    public ProfileInfo(HydraUuid uuid, UInt64 key)
    {
      m_uuid = uuid;
      m_key = key;
    }
  }
}
