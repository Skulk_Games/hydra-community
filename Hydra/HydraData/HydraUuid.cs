#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using Skulk.MEL.Utils;
using System;
using System.ComponentModel;

namespace Skulk.Hydra
{
  /// <summary>
  /// Manages an array of bytes that indicate the UUID for a Hydra application
  /// </summary>
  [TypeConverter(typeof(HydraUuidStringConverter))]
  public class HydraUuid : IHydralize
  {
    /// <summary>
    /// The length of the UUID in bytes
    /// </summary>
    public const int UUID_LENGTH = 16;

    /// <summary>
    /// The raw UUID byte array
    /// </summary>
    private byte[] m_uuid;

    /// <summary>
    /// The raw UUID 16 bytes
    /// </summary>
    public byte[] Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// Initializes a signature with a random value
    /// </summary>
    public HydraUuid()
    {
      Generate();
    }

    /// <summary>
    /// Initializes a new UUID with the specified values
    /// </summary>
    /// <param name="uuid">The array of UUID bytes</param>
    /// <remarks>The length of the array should match the UUID constant length</remarks>
    public HydraUuid(byte[] uuid)
    {
      m_uuid = new byte[UUID_LENGTH];
      Array.Copy(uuid, m_uuid, UUID_LENGTH);
    }

    /// <summary>
    /// Initializes this HydraUuid with a copied value from another instance
    /// </summary>
    /// <param name="other">The UUID instance to copy</param>
    public HydraUuid(HydraUuid other) : this(other.m_uuid)
    {
      // Empty
    }

    /// <summary>
    /// Determines whether the UUID bytes are considered less than another
    /// </summary>
    /// <param name="a">The first UUID to compare</param>
    /// <param name="b">The second UUID to compare</param>
    /// <returns>True if a is less than b</returns>
    public static bool operator<(HydraUuid a, HydraUuid b)
    {
      if (a == null)
      {
        return b != null;
      }
      else if (b == null)
      {
        return false;
      }
      else
      {
        for (int i = 0; i < UUID_LENGTH; ++i)
        {
          if (a.m_uuid[i] == b.m_uuid[i])
          {
            continue;
          }
          else
          {
            return a.m_uuid[i] < b.m_uuid[i];
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Determines whether the UUID bytes are considered greater than another
    /// </summary>
    /// <param name="a">The first UUID to compare</param>
    /// <param name="b">The second UUID to compare</param>
    /// <returns>True if a is greater than b</returns>
    public static bool operator>(HydraUuid a, HydraUuid b)
    {
      if (a == null)
      {
        return false;
      }
      else if (b == null)
      {
        return a != null;
      }
      else
      {
        for (int i = 0; i < UUID_LENGTH; ++i)
        {
          if (a.m_uuid[i] == b.m_uuid[i])
          {
            continue;
          }
          else
          {
            return a.m_uuid[i] > b.m_uuid[i];
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Generaes a random UUID value for this class instance
    /// </summary>
    public void Generate()
    {
      Int64 time = DateTime.UtcNow.ToBinary();
      ChanceUtils.Reseed();
      UInt64 rand = ChanceUtils.RandUInt64(0, UInt64.MaxValue);
      m_uuid = new byte[16];
      byte[] timeBytes = BitConverter.GetBytes(time);
      byte[] randBytes = BitConverter.GetBytes(rand);
      for (int i = 0; i < sizeof(UInt64); ++i)
      {
        m_uuid[i] = timeBytes[i];
        m_uuid[i + 8] = randBytes[i];
      }
    }

    /// <summary>
    /// Gets a hash code for this uuid
    /// </summary>
    /// <returns>The hash code value for this instance</returns>
    public override int GetHashCode()
    {
      int hash = 0;
      for (int i = 0; i < m_uuid.Length; ++i)
      {
        hash += m_uuid[i];
      }
      return hash;
    }

    /// <summary>
    /// Determines if two <see cref="HydraUuid"/> contain equivalent signatures
    /// </summary>
    /// <param name="obj">The <see cref="HydraUuid"/> to compare</param>
    /// <returns>True if the object is an equivalent signature</returns>
    public override bool Equals(object obj)
    {
      HydraUuid other = obj as HydraUuid;
      if (other != null)
      {
        if (m_uuid.Length == other.m_uuid.Length)
        {
          for (int i = 0; i < m_uuid.Length; ++i)
          {
            if (m_uuid[i] != other.m_uuid[i])
            {
              return false;
            }
          }
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Returns the UUID value in a standard UUID string format
    /// </summary>
    /// <returns>The UUID string</returns>
    public override string ToString()
    {
      return string.Format(
        "{0:x2}{1:x2}{2:x2}{3:x2}-{4:x2}{5:x2}-{6:x2}{7:x2}-{8:x2}{9:x2}-{10:x2}{11:x2}{12:x2}{13:x2}{14:x2}{15:x2}",
        m_uuid[0], m_uuid[1], m_uuid[2], m_uuid[3], m_uuid[4], m_uuid[5], m_uuid[6], m_uuid[7], m_uuid[8], m_uuid[9],
        m_uuid[10], m_uuid[11], m_uuid[12], m_uuid[13], m_uuid[14], m_uuid[15]);
    }

    /// <summary>
    /// Attempts to parse the UUID bytes from the specified string
    /// </summary>
    /// <param name="uuidText">The string containing the UUID hex value</param>
    /// <returns>The array of UUID bytes parsed or null if an error occurred</returns>
    public static byte[] Parse(string uuidText)
    {
      try
      {
        uuidText = uuidText.Trim();
        uuidText = uuidText.Replace("-", "");
        if (uuidText.Length == (UUID_LENGTH * 2))
        {
          byte[] bytes = new byte[UUID_LENGTH];
          for (int i = 0; i < UUID_LENGTH; ++i)
          {
            bytes[i] = byte.Parse(uuidText.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
          }

          return bytes;
        }
      }
      catch
      {
        // Empty
      }

      return null;
    }

    /// <inheritdoc />
    public void GetObjectData(HydralizeInfo info)
    {
      info.AddInfo("uuid", m_uuid);
    }

    /// <inheritdoc />
    public void LoadObjectData(HydralizeInfo info)
    {
      m_uuid = info.Get<byte[]>("uuid", 0);
    }
  }
}
