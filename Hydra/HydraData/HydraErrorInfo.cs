#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;

namespace Skulk.Hydra
{
  /// <summary>
  /// A simple base class for Hydra error information
  /// </summary>
  public class HydraErrorInfo : HydralizeObject
  {
    private HydraErrors m_error;

    /// <summary>
    /// The type of hydra error.
    /// </summary>
    public HydraErrors Error { get => m_error; set => m_error = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraErrorInfo()
    {
      // Empty
    }

    /// <summary>
    /// Constructor for a Hydra error with the specified error.
    /// </summary>
    /// <param name="error">The type of error</param>
    public HydraErrorInfo(HydraErrors error)
    {
      m_error = error;
    }
  }
}
