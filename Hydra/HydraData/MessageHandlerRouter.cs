#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Skulk.Hydra
{
  /// <summary>
  /// Utility class that routes messages to their handling HydraMessageHandler delegates and caches the pairs.
  /// </summary>
  public class MessageHandlerRouter
  {
    /// <summary>
    /// The cached message and handler pairs.
    /// </summary>
    private Dictionary<UInt16, MessageClientCallback> m_cache = new Dictionary<UInt16, MessageClientCallback>();

    /// <summary>
    /// The logger instance to use
    /// </summary>
    protected ILogger m_log;

    /// <summary>
    /// Initializes the message map with no pre-cached messages
    /// </summary>
    /// <param name="log">The log instance to write to</param>
    public MessageHandlerRouter(ILogger log = null)
    {
      m_log = log;
    }

    /// <summary>
    /// Initializes the message map and caches all the messages for any HydraModules found in the module dictionary.
    /// </summary>
    /// <param name="modules">The collection of modules containing any HydraModules to cache the messages for</param>
    /// <param name="log">The logger instance to write messages to</param>
    public MessageHandlerRouter(IEnumerable<IModule> modules, ILogger log = null)
      : this(log)
    {
      CacheModules(modules);
    }

    /// <summary>
    /// Clears the cached message routing
    /// </summary>
    public void ClearCache()
    {
      lock (m_cache)
      {
        m_cache.Clear();
      }
    }

    /// <summary>
    /// Caches the message routing for the specified modules
    /// </summary>
    /// <param name="modules">The list of modules that contains <see cref="HydraModule"/> instances to cache message routing for</param>
    public void CacheModules(IEnumerable<IModule> modules)
    {
      foreach (IModule mod in modules)
      {
        HydraModule hydraModule = mod as HydraModule;
        if (hydraModule?.MessageHandlers != null)
        {
          foreach (HydraMessageHandler handlerPair in hydraModule.MessageHandlers)
          {
            Cache(handlerPair.CmdCode, handlerPair.Handler);
            if (!string.IsNullOrEmpty(handlerPair.MessageName))
            {
              MsgNames.Add(handlerPair.CmdCode, handlerPair.MessageName);
            }
          }
        }
      }
    }

    /// <summary>
    /// Routes a message to its handling HydraModule.
    /// </summary>
    /// <param name="modules">The collection of modules to search for handling HydraModules</param>
    /// <param name="msg">The message to route.</param>
    /// <param name="client">The ID of the client that received the message.</param>
    /// <returns>True if the message was routed, false if no handling HydraModule was found.</returns>
    public virtual bool RouteMessage(IEnumerable<IModule> modules, Message msg, HydraUuid client)
    {
      // Check cached messages
      bool? handled = RouteCached(msg, client);
      if (handled != null)
      {
        return (bool)handled;
      }

      // Message hasn't been cached - attempt to find a module that handles the message and add to cache
      UInt16 msgCode = msg.GetCmd();
      foreach (IModule mod in modules)
      {
        HydraModule hydraModule = mod as HydraModule;
        if (hydraModule != null)
        {
          if (hydraModule.MessageHandlers != null)
          {
            for (int i = 0; i < hydraModule.MessageHandlers.Count; ++i)
            {
              if (hydraModule.MessageHandlers[i].CmdCode == msgCode)
              {
                Cache(msg, hydraModule.MessageHandlers[i].Handler);
                hydraModule.MessageHandlers[i].Handler.Invoke(msg, client);
                return true;
              }
            }
          }
        }
      }

      // Cache the fact that this message is unhandled
      Cache(msg, null);
      return false;
    }

    /// <summary>
    /// Attempts to route the message to its handling module using the cache.
    /// </summary>
    /// <param name="msg">The message to route.</param>
    /// <param name="client">The UUID of the client the message came from.</param>
    /// <returns>
    /// True if the message was routed using the cache, false if the msg has been cached as unhandled
    /// and null if the route could not be determined.
    /// </returns>
    protected bool? RouteCached(Message msg, HydraUuid client)
    {
      if (m_cache.TryGetValue(msg.GetCmd(), out MessageClientCallback handler))
      {
        if (handler != null)
        {
          handler.Invoke(msg, client);
          return true;
        }
        else
        {
          return false;
        }
      }

      return null;
    }

    /// <summary>
    /// Caches the message so that it is routed to the specified module
    /// </summary>
    /// <param name="msg">The message to cache</param>
    /// <param name="handler">The handler the cached route should send messages to</param>
    protected void Cache(Message msg, MessageClientCallback handler)
    {
      Cache(msg.GetCmd(), handler);
    }

    /// <summary>
    /// Caches a message code so that it is routed to the specified module
    /// </summary>
    /// <param name="code">The message code to cache</param>
    /// <param name="handler">The handler the cached route should send messages to</param>
    protected void Cache(UInt16 code, MessageClientCallback handler)
    {
      if (!m_cache.ContainsKey(code))
      {
        m_cache.Add(code, handler);
      }
      else
      {
        m_log?.LogWarning("Message code: {0} is already cached, skipping", code);
      }
    }

    /// <summary>
    /// Removes the cached message code route if it exists
    /// </summary>
    /// <param name="code">The code to remove from the cache</param>
    /// <returns>True if the code was removed from the cache, otherwise false</returns>
    protected bool Uncache(UInt16 code)
    {
      if (m_cache.Remove(code))
      {
        m_log?.LogDebug("Removed cached message: {0}", code);
        return true;
      }
      else
      {
        m_log?.LogWarning("Attempted to remove handling for message code: {0}, but it already isn't handled.", code);
        return false;
      }
    }
  }
}
