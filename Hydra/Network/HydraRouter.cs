#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A class that performs routing operations to route messages through other physical profiles and connections to reach
  /// indirectly connected Hydra applications
  /// </summary>
  public class HydraRouter
  {
    /// <summary>
    /// The number of times the routing table should be cleaned and finding a route attempted after an initial failure
    /// </summary>
    private const int CLEAN_RETRY_ATTEMPTS = 1;

    private HydraProfileManager m_profileManager;
    private HydraRouteMap m_routeMap = new HydraRouteMap();

    /// <summary>
    /// The routing map instance used by this router
    /// </summary>
    public HydraRouteMap RouteMap { get => m_routeMap; }

    /// <summary>
    /// Initializes this router
    /// </summary>
    /// <param name="manager">The profile manager using this router</param>
    public HydraRouter(HydraProfileManager manager)
    {
      m_profileManager = manager;
      m_profileManager.OnConnectionAdded += OnConnectionAdded;
      m_profileManager.OnProfileRemoved += OnProfileRemoved;
    }

    /// <summary>
    /// Attempts to route a message to the specified destination
    /// </summary>
    /// <param name="message">The message to route</param>
    /// <param name="destination">The destination to send the message to</param>
    /// <returns>True if the message was successfully routed, false otherwise</returns>
    public bool Route(Message message, HydraUuid destination)
    {
      for (int i = 0; i < 1 + CLEAN_RETRY_ATTEMPTS; ++i)
      {
        // Use the cache to route messages
        HydraUuid route = m_routeMap.SearchRoute(m_profileManager.Parent.Uuid, destination);

        if (route != null)
        {
          Profile profile = m_profileManager.GetProfile(route);
          if (profile != null)
          {
            if (profile.SendMessage(message))
            {
              return true;
            }
            else
            {
              m_profileManager.Log?.LogWarning("Failed to route message over connection");
              Clean(route);
            }
          }
        }
        else
        {
          m_profileManager.Log?.LogDebug("No route to destination: {0} found.", destination);
          return false;
        }
      }
      return false;
    }

    /// <summary>
    /// Attempts to route a message to the specified destination
    /// </summary>
    /// <param name="message">The message to route</param>
    /// <param name="destination">The destination to send the message to</param>
    /// <param name="connectionType">The type of connection to send the message over</param>
    /// <returns>True if the message was successfully routed, false otherwise</returns>
    public bool Route(Message message, HydraUuid destination, ConnectionTypes connectionType)
    {
      for (int i = 0; i < 1 + CLEAN_RETRY_ATTEMPTS; ++i)
      {
        // Use the cache to route messages
        HydraUuid route = m_routeMap.SearchRoute(m_profileManager.Parent.Uuid, destination);

        if (route != null)
        {
          Profile profile = m_profileManager.GetProfile(route);
          if (profile != null)
          {
            if (profile.SendMessage(message, connectionType))
            {
              return true;
            }
            else
            {
              m_profileManager.Log?.LogWarning("Failed to route message over connection");
              Clean(route);
            }
          }
        }
        else
        {
          m_profileManager.Log?.LogDebug("No route to destination: {0} found.", destination);
          return false;
        }

        // Perform any known clean-up processes here
      }

      return false;
    }

    /// <summary>
    /// Sets or adds the specified routing table
    /// </summary>
    /// <param name="mapNode">The routing map node for the application</param>
    public void SetMapNode(HydraRouteMapNode mapNode)
    {
      m_profileManager.Log?.LogDebug("Updating routing map for UUID: {0}", mapNode.Uuid);
      m_routeMap.SetNode(mapNode);
    }

    /// <summary>
    /// Cleans the routing tables by removing the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID to remove from the tables</param>
    public void Clean(HydraUuid uuid)
    {
      //m_profileManager.Log?.LogTrace("Cleaning UUID: {0} from the route map", uuid);

      //m_routeMap.RemoveNode(uuid);
    }

    /// <summary>
    /// Handles the event when a new connection is added
    /// </summary>
    /// <param name="connection">The connection that was added</param>
    private void OnConnectionAdded(Connection connection)
    {
      if (connection as VirtualConnection == null)
      {
        m_profileManager.Router.SetMapNode(new HydraRouteMapNode(m_profileManager.Parent));

        // Send message requesting node routes
        if (connection.Profile.SendMessage(new Messages.HydraRouteEntryMsg()))
        {
          m_profileManager.Log?.LogDebug("Sent message to get map node for UUID: {0}", connection.Profile.Uuid);
        }
        else
        {
          m_profileManager.Log?.LogError("Failed to send map node request for UUID: {0}", connection.Profile.Uuid);
        }
      }
    }

    /// <summary>
    /// Handles the event where a profile is removed
    /// </summary>
    /// <param name="profile">The profile that was removed</param>
    private void OnProfileRemoved(Profile profile)
    {
      m_profileManager.Router.SetMapNode(new HydraRouteMapNode(m_profileManager.Parent));
      Clean(profile.Uuid);
    }
  }
}
