﻿#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.IO;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Contains data about the status of a Hydra stream
  /// </summary>
  public class HydraStreamStatus
  {
    /// <summary>
    /// The local stream ID
    /// </summary>
    private uint m_localId;

    /// <summary>
    /// The length of the stream in bytes
    /// </summary>
    private long m_length;

    /// <summary>
    /// The number of bytes that have been transferred
    /// </summary>
    private long m_count;

    /// <summary>
    /// The UUID of the remote application
    /// </summary>
    private HydraUuid m_remote;

    /// <summary>
    /// The stream transfer mode
    /// </summary>
    private StreamTransferModes m_transferMode;

    /// <summary>
    /// The direction of the stream transfer
    /// </summary>
    private HydraStreamDirections m_direction;

    /// <summary>
    /// The local stream involved in the transfer
    /// </summary>
    private Stream m_stream;

    /// <summary>
    /// The local ID used to identify this stream
    /// </summary>
    public uint LocalId { get => m_localId; set => m_localId = value; }

    /// <summary>
    /// The total length of the stream in bytes
    /// </summary>
    public long Length { get => m_length; set => m_length = value; }

    /// <summary>
    /// The total number of transferred bytes in the stream
    /// </summary>
    public long Count { get => m_count; set => m_count = value; }

    /// <summary>
    /// The UUID of the remote application involved in the transfer
    /// </summary>
    public HydraUuid Remote { get => m_remote; set => m_remote = value; }

    /// <summary>
    /// The mode used for transferring the stream data
    /// </summary>
    public StreamTransferModes TransferMode { get => m_transferMode; set => m_transferMode = value; }

    /// <summary>
    /// The direction of the stream transfer
    /// </summary>
    public HydraStreamDirections Direction { get => m_direction; set => m_direction = value; }

    /// <summary>
    /// The local stream involved in the transfer
    /// </summary>
    public Stream Stream { get => m_stream; set => m_stream = value; }
  }
}
