#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Class holding information describing a Hydra route
  /// </summary>
  public class HydraRouteInfo
  {
    private HydraRoutingEntry m_entry;
    private HydraUuid m_destination;

    /// <summary>
    /// The route entry information
    /// </summary>
    public HydraRoutingEntry Entry { get => m_entry; set => m_entry = value; }

    /// <summary>
    /// The route destination UUID
    /// </summary>
    public HydraUuid Destination { get => m_destination; set => m_destination = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraRouteInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this route with the specified values
    /// </summary>
    /// <param name="destination">The destination UUID of the route</param>
    /// <param name="entry">The entry information for the route</param>
    public HydraRouteInfo(HydraUuid destination, HydraRoutingEntry entry)
    {
      m_destination = destination;
      m_entry = entry;
    }
  }
}
