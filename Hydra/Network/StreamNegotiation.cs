﻿#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// An object defining the values used in negotiating the sending/receiving of a stream.
  /// </summary>
  public class StreamNegotiation : IHydralize
  {
    private bool m_isReply;
    private uint m_sourceId;
    private uint m_destinationId;
    private string m_source;
    private string m_destination;
    private HydraStreamDirections m_direction;
    private bool m_success;
    private object m_tag;

    /// <summary>
    /// Indicates whether this object describes a negotiation reply or request
    /// </summary>
    public bool IsReply { get => m_isReply; set => m_isReply = value; }

    /// <summary>
    /// The stream ID on the source application
    /// </summary>
    public uint SourceId { get => m_sourceId; set => m_sourceId = value; }

    /// <summary>
    /// The stream ID on the destination application
    /// </summary>
    public uint DestinationId { get => m_destinationId; set => m_destinationId = value; }

    /// <summary>
    /// A string specifying the source of the stream, typically this is a module type name
    /// </summary>
    public string Source { get => m_source; set => m_source = value; }

    /// <summary>
    /// A string specifying the destination of the stream, typically this is a module type name
    /// </summary>
    public string Destination { get => m_destination; set => m_destination = value; }

    /// <summary>
    /// The stream direction of the transfer
    /// </summary>
    public HydraStreamDirections Direction { get => m_direction; set => m_direction = value; }

    /// <summary>
    /// A flag indicating whether the negotiation was succesful/accepted
    /// </summary>
    public bool Success { get => m_success; set => m_success = value; }

    /// <summary>
    /// Custom tag object used to transfer case specific data related to the stream
    /// </summary>
    public object Tag { get => m_tag; set => m_tag = value; }

    /// <inheritdoc/>
    public void GetObjectData(HydralizeInfo info)
    {
      info.AddInfo(nameof(m_isReply), m_isReply);
      info.AddInfo(nameof(m_sourceId), m_sourceId);
      info.AddInfo(nameof(m_destinationId), m_destinationId);
      info.AddInfo(nameof(m_source), m_source);
      info.AddInfo(nameof(m_destination), m_destination);
      info.AddInfo(nameof(m_direction), m_direction);
      info.AddInfo(nameof(m_success), m_success);
      info.AddInfo(nameof(m_tag), m_tag, typeof(object));
    }

    /// <inheritdoc/>
    public void LoadObjectData(HydralizeInfo info)
    {
      int index = 0;
      m_isReply = info.Get<bool>(nameof(m_isReply), index++);
      m_sourceId = info.Get<uint>(nameof(m_sourceId), index++);
      m_destinationId = info.Get<uint>(nameof(m_destinationId), index++);
      m_source = info.Get<string>(nameof(m_source), index++);
      m_destination = info.Get<string>(nameof(m_destination), index++);
      m_direction = info.Get<HydraStreamDirections>(nameof(m_direction), index++);
      m_success = info.Get<bool>(nameof(m_success), index++);
      object tagObj = info.GetRaw(nameof(m_tag), index++);
      if (tagObj != null)
      {
        HydralizeInfo tagInfo = tagObj as HydralizeInfo;
        if (tagInfo != null)
        {
          m_tag = Hydralizer.Dehydralize(tagInfo);
        }
        else
        {
          m_tag = tagObj;
        }
      }
    }
  }
}
