#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.Hydralize.Encoding;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Enumeration of the types of connections 
  /// </summary>
  public enum ConnectionTypes
  {
    /// <summary>
    /// A connection that supports transferring message packets
    /// </summary>
    MESSAGE,

    /// <summary>
    /// A connection that streams data
    /// </summary>
    STREAM
  }

  /// <summary>
  /// An event callback involving a message
  /// </summary>
  /// <param name="message">The message involved</param>
  public delegate void MessageCallback(Message message);

  /// <summary>
  /// An event callback involving a stream
  /// </summary>
  /// <param name="streamData">The data describing the stream involved</param>
  public delegate void StreamEventCallback(HydraStreamInfo streamData);

  /// <summary>
  /// An event callback involving stream data
  /// </summary>
  /// <param name="streamData">The data describing the stream involved</param>
  /// <param name="buffer">The buffer of raw data involved in the event</param>
  public delegate void StreamDataCallback(HydraStreamInfo streamData, byte[] buffer);

  /// <summary>
  /// Base class that describes a connection
  /// </summary>
  public abstract class Connection : ThreadedApplication
  {
    /// <summary>
    /// Indicates whether this connection has ended
    /// </summary>
    volatile protected bool m_ended;

    /// <summary>
    /// Indicates whether this connection has been closed
    /// </summary>
    volatile protected bool m_closed;

    /// <summary>
    /// Indicates whether a close message should be sent before actually closing the socket
    /// </summary>
    volatile protected bool m_sendClosureMessage = true;

    /// <summary>
    /// The queue where messages to be sent are stored temporarily
    /// </summary>
    volatile protected Queue<Message> m_sendMessageQueue = new Queue<Message>();

    /// <summary>
    /// An ID used to identify unique messages sent on the connection. Primarily useful in identifying related extended messages.
    /// </summary>
    volatile protected ushort m_sendId = 0;

    /// <summary>
    /// The time in seconds of having no received messages before a keep alive should be sent
    /// </summary>
    protected double m_keepAlivePeriod;

    /// <summary>
    /// The manager to use when tracking and limiting the bandwidth on this connection
    /// </summary>
    protected BandwidthManager m_bandwidthManager;

    /// <summary>
    /// The time in seconds this connection has been inactive
    /// </summary>
    private double m_inactiveTime;

    /// <summary>
    /// Object used for locking access to the inactive time
    /// </summary>
    private object m_inactiveTimeLock = new object();

    /// <summary>
    /// Object used for locking access to the connection client instance
    /// </summary>
    private object m_clientLock = new object();

    /// <summary>
    /// The profile holding this connection
    /// </summary>
    private Profile m_profile;

    /// <summary>
    /// The ID of this connection on a profile
    /// </summary>
    private UInt32 m_id;

    /// <summary>
    /// The type of this connection
    /// </summary>
    private ConnectionTypes m_connectionType;

    /// <summary>
    /// The value indicating the priority level for using this connection with messages
    /// </summary>
    private int m_messagePriority;

    /// <summary>
    /// The value indicating the priority level for using this connection with streams
    /// </summary>
    private int m_streamPriority;

    /// <summary>
    /// The encoder to use on this connection
    /// </summary>
    private TranscoderPair m_encoder;

    /// <summary>
    /// List of streams this connection is sending
    /// </summary>
    private TimeoutList<ConnectionStreamInfo> m_streamBuffer = new TimeoutList<ConnectionStreamInfo>();

    /// <summary>
    /// The time in seconds a stream will be kept before timing out
    /// </summary>
    private double m_streamTimeout = 3600;

    /// <summary>
    /// The last measured latency in seconds on this connection
    /// </summary>
    private double m_latency = 0;

    /// <summary>
    /// A dictionary for temporarily holding extended message data.
    /// </summary>
    private TimeoutDictionary<ushort, PendingMessage> extendedMessageData = new TimeoutDictionary<ushort, PendingMessage>();

    /// <summary>
    /// The max time in seconds to wait for each segment of a pending extended message to be received.
    /// </summary>
    private double m_extendedMsgTimeout = 30;

    /// <summary>
    /// Gets a flag indicating whether this connection is currently connected
    /// </summary>
    public bool Ended { get => m_ended; }

    /// <summary>
    /// Gets a flag indicating whether this connection has been closed
    /// </summary>
    public bool Closed { get => m_closed; }

    /// <summary>
    /// The number of messages in the send queue
    /// </summary>
    public int SendQueueCount { get => m_sendMessageQueue.Count; }

    /// <summary>
    /// Indicates whether a close message should be sent before actually closing a connected socket
    /// </summary>
    public bool SendCloseMessage { get => m_sendClosureMessage; set => m_sendClosureMessage = value; }

    /// <summary>
    /// The period in seconds that keep alive messages should be sent out if no activity was seen on the connection
    /// </summary>
    public double KeepAlivePeriod { get => m_keepAlivePeriod; }

    /// <summary>
    /// Gets this connection's type
    /// </summary>
    public ConnectionTypes ConnectionType { get => m_connectionType; }

    /// <summary>
    /// Gets or sets the priority of this connection for messages
    /// </summary>
    public int MessagePriority { get => m_messagePriority; set => m_messagePriority = value; }

    /// <summary>
    /// Gets or sets the priority of this connection for streams
    /// </summary>
    public int StreamPriority { get => m_streamPriority; set => m_streamPriority = value; }

    /// <summary>
    /// Gets or sets the encoder to use on this connection
    /// </summary>
    public TranscoderPair Encoder { get => m_encoder; set => m_encoder = value; }

    /// <summary>
    /// Gets or sets the event callback invoked when a message is received on this connection
    /// </summary>
    public event ProfileMessageCallback OnMessageReceived;

    /// <summary>
    /// Event invoked when this connection is about to close
    /// </summary>
    public event ConnectionEventCallback OnConnectionClosing;

    /// <summary>
    /// Gets or sets the ID of this connection on the profile
    /// </summary>
    public UInt32 Id { get => m_id; set => m_id = value; }

    /// <summary>
    /// Gets a value indicating if this connection can stream data
    /// </summary>
    abstract public bool CanStream { get; }

    /// <summary>
    /// Gets a value indicating whether data can be written to this connection.
    /// </summary>
    abstract public bool CanWrite { get; }

    /// <summary>
    /// Gets a value indicating whether data can be read from this connection.
    /// </summary>
    abstract public bool CanRead { get; }

    /// <summary>
    /// Gets the object used to lock access to the connection client
    /// </summary>
    public object ClientLock { get => m_clientLock; }

    /// <summary>
    /// Gets or sets the Profile containing this connection
    /// </summary>
    public Profile Profile { get => m_profile; set => m_profile = value; }

    /// <summary>
    /// The list of streams sending on this connection
    /// </summary>
    public TimeoutList<ConnectionStreamInfo> StreamBuffer { get => m_streamBuffer; set => m_streamBuffer = value; }

    /// <summary>
    /// Gets or sets the timeout in seconds for sending streams
    /// </summary>
    public double StreamTimeout { get => m_streamTimeout; set => m_streamTimeout = value; }

    /// <summary>
    /// The last measured latency in seconds on this connection
    /// </summary>
    public double Latency { get => m_latency; set => m_latency = value; }

    /// <summary>
    /// Indicates whether this connection has a message guarantor instance
    /// </summary>
    public virtual bool HasGuarantor { get => false; }

    /// <summary>
    /// The max time in seconds to wait for each segment of a pending extended message. The message data is dropped if the timeout is reached.
    /// </summary>
    public double ExtendedMessageTimeout { get => this.m_extendedMsgTimeout; set => this.m_extendedMsgTimeout = value; }

    /// <summary>
    /// Initializes a new connection
    /// </summary>
    /// <param name="connectionType">The type of this connection</param>
    /// <param name="id">The ID of this connection on the profile</param>
    /// <param name="messagePriority">The priority of this connection for message data</param>
    /// <param name="streamPriority">The priority of this connection for stream data</param>
    /// <param name="encoder">The encoder to use on this connection</param>
    /// <param name="keepAlivePeriod">The time in seconds with no received messages before a keep alive message will be sent</param>
    /// <param name="maxRateBps">The max rate in bits per second to allow when sending data on this connection</param>
    public Connection(ConnectionTypes connectionType, UInt32 id, int messagePriority = 0, int streamPriority = 0, TranscoderPair encoder = null, double keepAlivePeriod = 10, double maxRateBps = -1)
    {
      m_connectionType = connectionType;
      m_messagePriority = messagePriority;
      m_streamPriority = streamPriority;
      m_encoder = encoder?.Clone();
      m_keepAlivePeriod = keepAlivePeriod;
      m_id = id;
      m_bandwidthManager = new BandwidthManager(maxRateBps);
    }

    /// <summary>
    /// Closes the connection interface
    /// </summary>
    /// <param name="failure">Indicates whether this close was called due to a socket failure</param>
    public virtual void Close(bool failure = false)
    {
      if (!m_closed)
      {
        m_closed = true;
        Profile?.Manager.Log.LogTrace("Closing connection, failure: {0}", failure);

        try
        {
          OnConnectionClosing?.Invoke(this);
        }
        catch (Exception ex)
        {
          Profile?.Manager.Log.LogError("Exception in connection OnClosing event: {0}", ex.Message);
          Profile?.Manager.Log.LogTrace("{0}", ex.StackTrace);
        }

        if (m_sendClosureMessage)
        {
          Write(new HydraCloseConnectionMsg());
        }

        if (failure)
        {
          // On failure remove the connection first
          Profile?.RemoveConnection(this);
          Profile?.TriggerFail(this);
        }
      }
      else
      {
        Profile?.Manager.Log.LogDebug("Ignoring redundant close, connection already closed");
      }
    }

    /// <summary>
    /// Sends a message on this connection
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <returns>True if this connection is currently able to send the message, false if this connection has become disconnected</returns>
    public virtual bool Send(Message message)
    {
      lock (m_inactiveTimeLock)
      {
        m_inactiveTime = 0;
      }

      return !m_ended;
    }

    /// <summary>
    /// Sends stream data on this connection
    /// </summary>
    /// <param name="stream">The stream to send on this connection</param>
    /// <param name="localId">The ID of the local sending stream</param>
    /// <param name="remoteId">The ID of the remote receiving stream</param>
    /// <param name="length">The total number of bytes to transfer from the stream</param>
    /// <param name="transferMode">The mode to use when transferring the stream data</param>
    /// <returns>True if this connection is currently able to send the stream, false if this connection has become disconnected</returns>
    public virtual bool Send(Stream stream, UInt32 localId, UInt32 remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
    {
      lock (m_streamBuffer)
      {
        HydraStreamInfo remoteInfo = new HydraStreamInfo()
        {
          Id = remoteId,
          Mode = transferMode,
          Length = length,
          Direction = HydraStreamDirections.READ
        };
        HydraStreamInfo localInfo = new HydraStreamInfo()
        {
          Id = localId,
          Mode = transferMode,
          Length = length,
          Direction = HydraStreamDirections.WRITE
        };

        m_streamBuffer.Add(new ConnectionStreamInfo(remoteInfo, localInfo, stream), m_streamTimeout);
        return !m_ended;
      }
    }

    /// <summary>
    /// Writes a message directly to the underlying hardware interface
    /// </summary>
    /// <param name="message">The message to write</param>
    /// <returns>True if the message was sent, false otherwise</returns>
    public virtual bool Write(Message message)
    {
      return false;
    }

    /// <summary>
    /// Resets the inactive time counter to 0
    /// </summary>
    public void ResetInactiveTime()
    {
      lock (m_inactiveTimeLock)
      {
        m_inactiveTime = 0;
      }
    }

    /// <summary>
    /// Sends a message to test the connection latency
    /// </summary>
    /// <param name="stopwatch">A stopwatch instance to start as the message is sent</param>
    /// <param name="checkId">The unique ID to use for this latency check</param>
    public void TestLatency(Stopwatch stopwatch, byte checkId)
    {
      HydraLatencyMsg latencyMsg = new HydraLatencyMsg((UInt16)(m_latency * 1000), checkId, HydraLatencyMsg.CheckType.INITIATED);
      Send(latencyMsg);
      stopwatch.Start();
    }

    /// <summary>
    /// Periodic update method for the connection
    /// </summary>
    /// <param name="time">The object containing update timing information</param>
    public virtual void Update(UpdateTime time)
    {
      lock (m_streamBuffer)
      {
        m_streamBuffer.Update(time);
      }

      lock (m_inactiveTimeLock)
      {
        m_inactiveTime += time.DeltaSeconds;
      }

      if (m_inactiveTime > m_keepAlivePeriod)
      {
        if (IsActive())
        {
          ResetInactiveTime();
          Profile.Manager.ResetTimeout(Profile.Uuid);
          Profile.Manager.Log?.LogDebug("Resetting UUID: {0} timeout.", Profile.Uuid);
        }
        else
        {
          // Send message
          Message msg = null;
          if (Profile?.Manager?.Router != null)
          {
            msg = new HydraRouteEntryMsg();
          }
          else
          {
            msg = new NoopMsg();
          }

          if (!Send(msg))
          {
            // Error sending; remove this connection
            Stop();
            Close();
            Profile.RemoveConnection(m_id);
          }
        }
      }

      this.extendedMessageData.Update(time);
    }

    /// <summary>
    /// Method that determines if this connection should be considered currently in an active state
    /// </summary>
    /// <remarks>
    /// This is used to determine if a keep alive message should be sent at a moment in time, return true if the connection
    /// is in a state where no keep alive message is necessary
    /// </remarks>
    /// <returns>True if the connection is in an active state, false otherwise</returns>
    public virtual bool IsActive()
    {
      return false;
    }

    /// <summary>
    /// Creates a new instance of the <see cref="ConnectionDiagnostics"/> class with the details of this connection.
    /// </summary>
    /// <returns>A new <see cref="ConnectionDiagnostics"/> instance detailing this connection.</returns>
    public virtual ConnectionDiagnostics GetDiagnostics()
    {
      int queueCount;
      lock (m_sendMessageQueue)
      {
        queueCount = m_sendMessageQueue.Count;
      }

      double inactiveTime;
      lock (m_inactiveTimeLock)
      {
        inactiveTime = m_inactiveTime;
      }

      int streamCount;
      lock (m_streamBuffer)
      {
        streamCount = m_streamBuffer.Count;
      }

      return new ConnectionDiagnostics()
      {
        Ended = m_ended,
        SendQueueCount = queueCount,
        KeepalivePeriod = m_keepAlivePeriod,
        InactiveTime = inactiveTime,
        Id = m_id,
        ConnectionType = m_connectionType,
        MessagePriority = m_messagePriority,
        StreamPriority = m_streamPriority,
        Encoder = m_encoder?.Encoder?.GetType().Name ?? "NULL",
        Decoder = m_encoder?.Decoder?.GetType().Name ?? "NULL",
        StreamCount = streamCount,
        StreamTimeout = m_streamTimeout,
        Latency = m_latency
      };
    }

    /// <summary>
    /// Base implementation of the connection main run thread.
    /// Triggers the connection added event.
    /// </summary>
    protected override void Run()
    {
      base.Run();
      // Setup variables
      m_ended = false;
      m_closed = false;

      // Trigger event
      Profile?.TriggerAdd(this);
    }

    /// <summary>
    /// Encodes the byte array if the encoder is set or returns the same byte array if it is null
    /// </summary>
    /// <param name="raw">The raw byte array to encode</param>
    /// <param name="length">The number of bytes to use in the raw array</param>
    /// <returns>The encoded array or the raw array if no encoder is set</returns>
    protected byte[] Encode(byte[] raw, int length = -1)
    {
      if (Encoder == null)
      {
        return raw;
      }
      else
      {
        return Encoder.Encode(raw, 0, length == -1 ? raw.Length : length);
      }
    }

    /// <summary>
    /// Receives a message handling any extended messages and invokes the <see cref="OnMessageReceived"/> event for each full message.
    /// </summary>
    /// <param name="message">The message or message segment to receive.</param>
    protected void ReceiveMessage(Message message)
    {
      try
      {
        if (message.IsExtended())
        {
          ushort index = message.GetIndex();
          ushort count = message.GetCount();

          if (count == 1)
          {
            // pass along without the extended flag
            message.SetFlags((ushort)(message.GetFlags() & ~(ushort)MessageFlagValues.EXTENDED));
            this.OnMessageReceived?.Invoke(message, this);
          }

          if (index == 0 || index > count)
          {
            // Umm, what?
            this.Profile.Manager.Log.LogError("Received extended message with invalid index: {0}/{1}, dropping {2} bytes", index, count, message.Payload?.Length ?? 0);
            return;
          }

          int offset = (int)((index - 1) * Message.MAX_PAYLOAD_SIZE);
          if (this.extendedMessageData.TryGetValue(message.GetId(), out PendingMessage pendingMsg))
          {
            this.extendedMessageData.Reset(message.GetId());
            pendingMsg.AddRange(index, message.Payload);

            if (pendingMsg.ReceiveCount == count)
            {
              _ = this.extendedMessageData.Remove(message.GetId());

              // Clear the extended flag
              message.SetFlags((ushort)(message.GetFlags() & ~(ushort)MessageFlagValues.EXTENDED));
              // Set payload to the full message contents
              message.SetPayload(pendingMsg.Data.ToArray());

              // Handle the message
              this.OnMessageReceived?.Invoke(message, this);
            }
          }
          else
          {
            PendingMessage newPendingMsg = new PendingMessage(count);
            newPendingMsg.AddRange(index, message.Payload);
            this.extendedMessageData.Add(message.GetId(), newPendingMsg, this.m_extendedMsgTimeout);
          }
        }
        else
        {
          this.OnMessageReceived?.Invoke(message, this);
        }
      }
      catch (Exception ex)
      {
        this.m_profile.Manager.Log.LogError("Exception in OnMessageReceived event: '{0}'\n{1}", ex.Message, ex.StackTrace);
      }
    }

    private class PendingMessage
    {
      private ushort receiveCount;
      private List<byte> data;

      public PendingMessage()
      {
        data = new List<byte>();
      }

      public PendingMessage(ushort count)
      {
        data = new List<byte>((int)(count * Message.MAX_PAYLOAD_SIZE));
      }

      /// <summary>
      /// The number of received segments.
      /// </summary>
      public ushort ReceiveCount { get => receiveCount; set => receiveCount = value; }

      /// <summary>
      /// The received message data.
      /// </summary>
      public List<byte> Data { get => data; set => data = value; }

      /// <summary>
      /// Adds the data for the specified segment index.
      /// </summary>
      /// <param name="index">The index of the segment data being added.</param>
      /// <param name="payload">The segment data to add.</param>
      public void AddRange(int index, byte[] payload)
      {
        int offset = (int)((index - 1) * Message.MAX_PAYLOAD_SIZE);
        for (int i = this.data.Count; i < offset; ++i)
        {
          this.data.Add(0);
        }

        if (offset >= this.data.Count)
        {
          this.data.AddRange(payload);
        }
        else
        {
          this.data.InsertRange(offset, payload);
        }

        ++this.receiveCount;
      }
    }
  }
}
