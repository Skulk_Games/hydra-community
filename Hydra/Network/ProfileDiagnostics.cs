#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Contains diagnostic information about a profile
  /// </summary>
  public class ProfileDiagnostics
  {
    private HydraModes m_mode;
    private HydraUuid m_uuid;
    private string m_name;
    private HydraAddress m_address;
    private ulong m_key;
    private ulong m_remoteKey;
    private uint m_idCounter;
    private int m_connectionCount;

    /// <summary>
    /// The mode of the application represented by the profile
    /// </summary>
    public HydraModes Mode { get => m_mode; set => m_mode = value; }

    /// <summary>
    /// The UUID of the application associated with the profile
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The name of the application associated with the profile
    /// </summary>
    public string Name { get => m_name; set => m_name = value; }

    /// <summary>
    /// The address of the application associated with the profile
    /// </summary>
    public HydraAddress Address { get => m_address; set => m_address = value; }

    /// <summary>
    /// The key for this profile
    /// </summary>
    public ulong Key { get => m_key; set => m_key = value; }

    /// <summary>
    /// The key on the remote application for the local application UUID
    /// </summary>
    public ulong RemoteKey { get => m_remoteKey; set => m_remoteKey = value; }

    /// <summary>
    /// The current value of the ID counter
    /// </summary>
    public uint IdCounter { get => m_idCounter; set => m_idCounter = value; }

    /// <summary>
    /// The number of connections on the profile
    /// </summary>
    public int ConnectionCount { get => m_connectionCount; set => m_connectionCount = value; }

    /// <summary>
    /// Gets the string representation of this object
    /// </summary>
    /// <returns>The string representation of this object's values</returns>
    public override string ToString()
    {
      return String.Format("Profile\nMode: {0}\nUUID: {1}\nName: {2}\nAddress: {3}\nKey: {4}\nRemoteKey: {5}\nID Counter: {6}\nConnections: {7}\n",
        Mode, Uuid, Name, Address, Key, RemoteKey, IdCounter, ConnectionCount);
    }
  }
}
