#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Generic;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A class for maintaining a network map of connected Hydra applications
  /// </summary>
  public class HydraRouteMap
  {
    private Dictionary<HydraUuid, HydraRouteMapNode> m_nodes = new Dictionary<HydraUuid, HydraRouteMapNode>();
    private Dictionary<HydraUuid, HydraUuid> m_cache = new Dictionary<HydraUuid, HydraUuid>();

    /// <summary>
    /// The map nodes in this route map. Access should be protected by locking this object
    /// </summary>
    public Dictionary<HydraUuid, HydraRouteMapNode> Nodes { get => m_nodes; }

    /// <summary>
    /// The cache of destination route paths for this application. Access should be protected by locking this object
    /// </summary>
    public Dictionary<HydraUuid, HydraUuid> Cache { get => m_cache; }

    /// <summary>
    /// Determines if this map contains a map node for the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the application map node to check for</param>
    /// <returns>True if a map node exists for the specified UUID</returns>
    public bool ContainsMapNode(HydraUuid uuid)
    {
      lock (m_nodes)
      {
        return m_nodes.ContainsKey(uuid);
      }
    }

    /// <summary>
    /// Gets a list of nodes that are referenced but missing in the map
    /// </summary>
    /// <returns>The list of referenced but missing nodes</returns>
    public List<HydraUuid> GetMissingNodes()
    {
      List<HydraUuid> missing = new List<HydraUuid>();
      lock (m_nodes)
      {
        foreach (KeyValuePair<HydraUuid, HydraRouteMapNode> node in m_nodes)
        {
          foreach (HydraUuid profileUuid in node.Value.Profiles)
          {
            if (!m_nodes.ContainsKey(profileUuid))
            {
              missing.Add(profileUuid);
            }
          }
        }
      }

      return missing;
    }

    /// <summary>
    /// Searches for the specified destination to determine the UUID of the application where routed messages should be sent
    /// </summary>
    /// <param name="origin">The UUID of the local application where routing begins</param>
    /// <param name="destination">The ultimate destination of a message</param>
    /// <returns>The UUID of the application to send routed messages to or null if no route was found</returns>
    public HydraUuid SearchRoute(HydraUuid origin, HydraUuid destination)
    {
      // Check for cached route first
      lock (m_cache)
      {
        if (m_cache.ContainsKey(destination))
        {
          return m_cache[destination];
        }
      }

      return SearchRoute(origin, destination, 0)?.Route;
    }

    private HydraRoutingEntry SearchRoute(HydraUuid origin, HydraUuid destination, int hops)
    {
      // Breadth first search of map nodes for destination
      Queue<HydraRouteInfo> nodeQueue = new Queue<HydraRouteInfo>();
      List<HydraUuid> visited = new List<HydraUuid>();
      nodeQueue.Enqueue(new HydraRouteInfo(origin, new HydraRoutingEntry(origin, hops)));

      lock (m_nodes)
      {
        if (m_nodes.ContainsKey(origin) && m_nodes[origin].Profiles.Contains(destination))
        {
          return new HydraRoutingEntry(destination, 0);
        }

        while (nodeQueue.Count > 0)
        {
          HydraRouteInfo info = nodeQueue.Dequeue();
          visited.Add(info.Entry.Route);
          if (m_nodes.ContainsKey(info.Entry.Route))
          {
            HydraRouteMapNode mapNode = m_nodes[info.Entry.Route];
            if (mapNode != null)
            {
              foreach (HydraUuid profileUuid in mapNode.Profiles)
              {
                if (profileUuid.Equals(destination))
                {
                  // Set original route as actual route
                  info.Entry.Route = info.Destination;
                  return info.Entry;
                }
                else if (!visited.Contains(profileUuid))
                {
                  if (info.Entry.Hops == 0)
                  {
                    info.Destination = profileUuid;
                  }

                  nodeQueue.Enqueue(new HydraRouteInfo(info.Destination, new HydraRoutingEntry(profileUuid, info.Entry.Hops + 1)));
                }
              }
            }
          }
          else
          {
            // Clean out UUID ?
          }
        }
      }

      return null;
    }

    /// <summary>
    /// Gets the map node information for the application with the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the map node information to get</param>
    /// <returns>The map node information for the application with the matching UUID, or null if no map node was found</returns>
    public HydraRouteMapNode GetNode(HydraUuid uuid)
    {
      lock (m_nodes)
      {
        if (m_nodes.TryGetValue(uuid, out HydraRouteMapNode mapNode))
        {
          return mapNode;
        }
      }

      return null;
    }

    /// <summary>
    /// Sets the map node information, overwrites any pre-existing node information
    /// </summary>
    /// <param name="mapNode">The class describing the map node to set</param>
    public void SetNode(HydraRouteMapNode mapNode)
    {
      bool clear = false;
      lock (m_nodes)
      {
        if (!m_nodes.ContainsKey(mapNode.Uuid))
        {
          m_nodes.Add(mapNode.Uuid, mapNode);
        }
        else
        {
          clear = true;
          m_nodes[mapNode.Uuid] = mapNode;
        }
      }

      if (clear)
      {
        ClearCache();
      }
    }

    /// <summary>
    /// Removes the map node with the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the map node to remove</param>
    public void RemoveNode(HydraUuid uuid)
    {
      bool clear = false;
      lock (m_nodes)
      {
        clear = m_nodes.Remove(uuid);
      }

      ClearCache();
    }

    /// <summary>
    /// Clears the cached routing results
    /// </summary>
    public void ClearCache()
    {
      lock (m_cache)
      {
        m_cache.Clear();
      }
    }
  }
}
