#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using System;
using System.IO;
using System.Threading;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A virtual connection that wraps a physical connection and sends routed messages through it
  /// </summary>
  public class VirtualConnection : Connection
  {
    private Connection m_physicalConnection;
    private byte[] m_tmpBuffer = new byte[32768];
    private ManualResetEvent m_sendEvent = new ManualResetEvent(true);

    /// <summary>
    /// Indicates whether this connection can continuously stream
    /// </summary>
    public override bool CanStream => false;

    /// <summary>
    /// Indicates whether data can be read from this connection.
    /// </summary>
    public override bool CanRead => m_physicalConnection?.CanRead ?? false;

    /// <summary>
    /// Indicates whether data can be written to this connection.
    /// </summary>
    public override bool CanWrite => m_physicalConnection?.CanWrite ?? false;

    /// <summary>
    /// Initializes this connection with the specified values
    /// </summary>
    /// <param name="physicalConnection">The physical connection to wrap</param>
    /// <param name="id">The ID of this virtual connection on the parent profile</param>
    /// <param name="messagePriority">The priority value to use for this connection when sending messages</param>
    /// <param name="streamPriority">The priority value to use for this connection when streaming</param>
    /// <param name="keepAlivePeriod">The period in seconds to send keep alive messages</param>
    public VirtualConnection(Connection physicalConnection, UInt32 id, int messagePriority, int streamPriority, double keepAlivePeriod)
      : base(physicalConnection.ConnectionType, id, messagePriority, streamPriority, null, keepAlivePeriod)
    {
      m_physicalConnection = physicalConnection;
      m_physicalConnection.OnConnectionClosing += Close;
    }

    /// <summary>
    /// Initializes this virtual connection using the values from the underlying physical connection
    /// </summary>
    /// <param name="physicalConnection">The underlying physical connection</param>
    /// <param name="id">The ID of this connection on the parent profile</param>
    public VirtualConnection(Connection physicalConnection, UInt32 id)
      : this(physicalConnection, id, physicalConnection.MessagePriority, physicalConnection.StreamPriority, physicalConnection.KeepAlivePeriod)
    {
      // Empty
    }

    /// <inheritdoc/>
    public override void Stop()
    {
      base.Stop();
      m_sendEvent.Set();
    }

    /// <summary>
    /// Sends a message directly to the physical connection
    /// </summary>
    /// <param name="message">The message to write</param>
    /// <returns>True if the message was written, false if the connection failed or stopped</returns>
    public override bool Write(Message message)
    {
      RoutedMessage routedMessage = new RoutedMessage(Profile.Manager.Parent.Uuid, Profile.Uuid, message);
      return m_physicalConnection?.Write(routedMessage) ?? false;
    }

    /// <summary>
    /// Queues a message to be sent over this connection
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <returns>True if the message was sent, false if the connection failed or stopped</returns>
    public override bool Send(Message message)
    {
      Profile.Manager.Log.LogTrace(
        "Sending routed message: {0}, with source: {1} and destination: {2} over: {3}",
        message.GetCmd(), Profile.Manager.Parent.Uuid, Profile.Uuid, m_physicalConnection.Profile.Uuid);
      RoutedMessage routedMessage = new RoutedMessage(Profile.Manager.Parent.Uuid, Profile.Uuid, message);
      return m_physicalConnection?.Send(routedMessage) ?? false;
    }

    /// <summary>
    /// Sends stream data over the connection.
    /// </summary>
    /// <param name="stream">The stream to send on this connection</param>
    /// <param name="localId">The ID of the local sending stream</param>
    /// <param name="remoteId">The ID of the remote receiving stream</param>
    /// <param name="length">The total number of bytes to transfer from the stream</param>
    /// <param name="transferMode">Ignored on a virtual connection, <see cref="StreamTransferModes.SEGMENTED"/> is enforced</param>
    /// <returns>True if this connection is currently able to send the stream, false if this connection has become disconnected</returns>
    public override bool Send(Stream stream, uint localId, uint remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.SEGMENTED)
    {
      if (base.Send(stream, localId, remoteId, length, StreamTransferModes.SEGMENTED))
      {
        lock (StreamBuffer)
        {
          m_sendEvent.Set();
        }
        return true;
      }

      return false;
    }

    /// <summary>
    /// Gets whether this connection should be considered active
    /// </summary>
    /// <returns>The active state of the underlying connection</returns>
    public override bool IsActive()
    {
      return m_physicalConnection.IsActive();
    }

    /// <summary>
    /// The connection running logic
    /// </summary>
    protected override void Run()
    {
      bool active = false;
      base.Run();

      while (!StopRun)
      {
        active = false;
        // Only process streams - direct messages are sent directly to physical connection
        // Also virtual connections don't receive data (it comes on the physical connection)
        lock (StreamBuffer)
        {
          for (int i = 0; i < StreamBuffer.Count; ++i)
          {
            active = true;
            bool endStream = false;

            if (StreamBuffer[i].TransferCount == 0)
            {
              Send(new StartStreamMsg(StreamBuffer[i].RemoteStreamInfo));
            }

            long left = StreamBuffer[i].LocalStreamInfo.Length - StreamBuffer[i].TransferCount;
            int size = (int)Math.Min(m_tmpBuffer.Length, left);
            int count = StreamBuffer[i].Stream.Read(m_tmpBuffer, 0, size);
            if (count > 0)
            {
              byte[] buffer = null;
              if (count != m_tmpBuffer.Length)
              {
                buffer = new byte[count];
                Array.Copy(m_tmpBuffer, buffer, count);
              }
              else
              {
                buffer = m_tmpBuffer;
              }
              Send(new StreamSegmentMsg(new HydraStreamSegment(StreamBuffer[i].RemoteStreamInfo.Id, StreamBuffer[i].Index++, buffer)));
              left -= count;
              StreamBuffer[i].TransferCount += count;
            }
            else
            {
              endStream = true;
              Profile.Manager.Log.LogError("Failed to read bytes from the read stream, ending stream early at {0}/{1} bytes", StreamBuffer[i].TransferCount, StreamBuffer[i].LocalStreamInfo.Length);
            }

            if (left <= 0 || endStream)
            {
              Send(new EndStreamMsg(StreamBuffer[i].RemoteStreamInfo));
              Profile.EndStream(StreamBuffer[i].LocalStreamInfo);
              StreamBuffer.RemoveAt(i--);
            }
          }

          // Only reset event if no more streams are in the buffer
          if (StreamBuffer.Count == 0)
          {
            m_sendEvent.Reset();
          }
        }

        if (!active)
        {
          m_sendEvent.WaitOne();
        }
      }
    }

    private void Close(Connection closingConnection)
    {
      base.Close();
      m_sendEvent.Set();
    }
  }
}
