#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Contains both <see cref="HydraUuid"/> and <see cref="HydraAddress"/> data
  /// </summary>
  public class HydraUuidAddress : HydraAddress
  {
    private HydraUuid m_uuid;

    /// <summary>
    /// The UUID of the application
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }
    
    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraUuidAddress()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new <see cref="HydraUuidAddress"/> instance with the specified values
    /// </summary>
    /// <param name="uuid">The UUID of the Hydra application</param>
    /// <param name="host">The hostname or IP of the application</param>
    /// <param name="messagePort">The UDP listening port of the application</param>
    /// <param name="streamPort">The TCP listening port of the application</param>
    public HydraUuidAddress(HydraUuid uuid, string host, int messagePort, int streamPort) : base(host, messagePort, streamPort)
    {
      m_uuid = uuid;
    }

    /// <summary>
    /// Determines if another <see cref="HydraUuidAddress"/> object has equal values
    /// </summary>
    /// <param name="obj">The object to compare</param>
    /// <returns>True if the object is a <see cref="HydraUuidAddress"/> with equal values to this instance, false otherwise</returns>
    public override bool Equals(object obj)
    {
      HydraUuidAddress uuidAddress = obj as HydraUuidAddress;
      if (uuidAddress != null)
      {
        if (base.Equals(obj))
        {
          if (m_uuid == null)
          {
            return uuidAddress.m_uuid == null;
          }
          else
          {
            return m_uuid.Equals(uuidAddress.m_uuid);
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Gets a hash code value for this UUID address instance
    /// </summary>
    /// <returns>The hash code value for this instance</returns>
    public override int GetHashCode()
    {
      return base.GetHashCode() + m_uuid?.GetHashCode() ?? 0;
    }
  }
}
