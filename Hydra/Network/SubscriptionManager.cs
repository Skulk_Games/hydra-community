#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Manages a list of <see cref="HydraUuid"/>s associated with subscribers
  /// </summary>
  public class SubscriptionManager : IDisposable
  {
    /// <summary>
    /// The list of subscribers
    /// </summary>
    protected List<HydraUuid> subscriptions = new List<HydraUuid>();

    /// <summary>
    /// The Hydra application this manager is on
    /// </summary>
    protected HydraCore hydra;

    /// <summary>
    /// Initializes this manager with the specified parent application
    /// </summary>
    /// <param name="hydra">The parent application for this sub manager</param>
    public SubscriptionManager(HydraCore hydra)
    {
      this.hydra = hydra;
      hydra.ProfileManager.OnProfileRemoved += HandleProfileRemoved;
    }

    /// <summary>
    /// Adds a subscriber to the tracked list of subscribers
    /// </summary>
    /// <param name="uuid">The UUID of the subscriber to add</param>
    /// <returns>True if the subscriber was added, false if they were already in the list</returns>
    public virtual bool Add(HydraUuid uuid)
    {
      lock (subscriptions)
      {
        if (!subscriptions.Contains(uuid))
        {
          subscriptions.Add(uuid);
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Removes a subscriber
    /// </summary>
    /// <param name="uuid">The UUID of the subscriber to remove</param>
    /// <returns>True if the subscriber was found and removed</returns>
    public virtual bool Remove(HydraUuid uuid)
    {
      lock (subscriptions)
      {
        return subscriptions.Remove(uuid);
      }
    }

    /// <summary>
    /// Gets a copied array containing the HydraUuids of the current subscribers
    /// </summary>
    /// <returns>A copied array containing the HydraUuids of the current subscribers</returns>
    public virtual HydraUuid[] GetSubscribers()
    {
      lock (subscriptions)
      {
        return subscriptions.ToArray();
      }
    }

    /// <summary>
    /// Sends the specified message to all subscribers
    /// </summary>
    /// <param name="message">The message to send</param>
    public void SendMessage(Message message)
    {
      lock (subscriptions)
      {
        for (int i = 0; i < subscriptions.Count; ++i)
        {
          hydra.SendMessage(message, subscriptions[i]);
        }
      }
    }

    /// <summary>
    /// Disposes this manager unsubscribing from all events.
    /// </summary>
    public void Dispose()
    {
      if (hydra != null)
      {
        hydra.ProfileManager.OnProfileRemoved -= HandleProfileRemoved;
      }
    }

    /// <summary>
    /// Handles the event invoked when a network profile is removed
    /// </summary>
    /// <param name="profile">The profile that was removed</param>
    protected virtual void HandleProfileRemoved(Profile profile)
    {
      lock (subscriptions)
      {
        subscriptions.Remove(profile.Uuid);
      }
    }
  }
}
