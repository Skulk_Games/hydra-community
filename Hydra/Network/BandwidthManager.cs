#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A class that helps manage bandwidth usage
  /// </summary>
  public class BandwidthManager
  {
    private double m_bytePeriod;
    private double m_bitRate;
    private DateTime m_initTime;
    private UInt64 m_totalBytes;
    private double m_sleepTime;

    /// <summary>
    /// The time in seconds the sending thread should sleep to compensate for (instantaneously) sent bytes.
    /// </summary>
    public double SleepTime { get => m_sleepTime; set => m_sleepTime = value; }

    /// <summary>
    /// The total number of bytes that have been added to this manager, this should equate to the number of bytes sent.
    /// </summary>
    public ulong TotalBytes { get => m_totalBytes; set => m_totalBytes = value; }

    /// <summary>
    /// The time this manager was last initialized.
    /// </summary>
    public DateTime InitTime { get => m_initTime; set => m_initTime = value; }

    /// <summary>
    /// The max rate in bits per second.
    /// </summary>
    public double BitRate { get => m_bitRate; set => m_bitRate = value; }

    /// <summary>
    /// Initializes this manager with the specified target bit rate.
    /// </summary>
    /// <param name="rateBps">The target bandwidth bit rate.</param>
    public BandwidthManager(double rateBps = 20000000)
    {
      m_bitRate = rateBps;
      if (rateBps <= 0)
      {
        m_bytePeriod = 0;
      }
      else
      {
        m_bytePeriod = 8 / rateBps;
      }

      Init();
    }

    /// <summary>
    /// Initializes this manager by setting the <see cref="InitTime"/> to UTC current time, <see cref="TotalBytes"/> to 0,
    /// and <see cref="SleepTime"/> to 0.
    /// </summary>
    public void Init()
    {
      m_initTime = DateTime.UtcNow;
      m_totalBytes = 0;
      m_sleepTime = 0;
    }

    /// <summary>
    /// Resets the <see cref="SleepTime"/> value.
    /// </summary>
    public void Reset()
    {
      m_sleepTime = 0;
    }

    /// <summary>
    /// Adds the specified number of bytes to the tracked total bytes and updates the <see cref="SleepTime"/>.
    /// </summary>
    /// <param name="bytes">The number of bytes that have been sent.</param>
    public void AddBytes(UInt64 bytes)
    {
      m_totalBytes += bytes;
      m_sleepTime += bytes * m_bytePeriod;
    }

    /// <summary>
    /// Synchronously sleeps for the <see cref="SleepTime"/> value determined by the number of bytes that have been sent
    /// and then resets the sleep time back to 0.
    /// </summary>
    public void Sleep()
    {
      if (m_bytePeriod == 0)
      {
        return;
      }
      else
      {
        TimeSpan span = new TimeSpan((long)(m_sleepTime / 0.0000001));
        System.Threading.Thread.Sleep(span);
        m_sleepTime = 0;
      }
    }
  }
}
