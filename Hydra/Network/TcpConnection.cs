#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.Hydralize.Encoding;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A connection that uses a TCP socket
  /// </summary>
  public class TcpConnection : Connection
  {
    private Message m_receivingMessage = new Message();

    /// <summary>
    /// The socket used on this connection
    /// </summary>
    protected Socket m_socket;

    /// <summary>
    /// The temporary buffer used to hold received bytes
    /// </summary>
    protected byte[] m_sendBuffer = new byte[Message.MAX_PAYLOAD_SIZE];

    /// <summary>
    /// Buffer used to hold bytes received from the socket
    /// </summary>
    protected byte[] m_receiveBuffer = new byte[Message.MAX_MESSAGE_SIZE];

    /// <summary>
    /// The offset in the receive buffer where bytes were last received
    /// </summary>
    protected int m_receiveOffset = 0;

    /// <summary>
    /// Reset event used to signal the sending thread
    /// </summary>
    protected ManualResetEvent m_sendEvent = new ManualResetEvent(true);

    /// <summary>
    /// The action stream information
    /// </summary>
    protected ConnectionStreamInfo m_activeReceiveStream = null;

    /// <summary>
    /// A message instance used for partial deserialization handling of the active receiving stream
    /// </summary>
    protected Message m_activeReceiveStreamMsg = null;

    /// <summary>
    /// The queue of continuous streams to send on this connection
    /// </summary>
    protected Queue<ConnectionStreamInfo> m_activeSendStreams = new Queue<ConnectionStreamInfo>();

    /// <summary>
    /// The max number of stream segments or messages to send before unlocking access to the queues
    /// </summary>
    protected UInt32 m_maxSendsPerLoop = 4096;

    /// <summary>
    /// The max number of messages to send before requiring the sending logic to unlock and loop around
    /// </summary>
    protected const int MAX_MESSAGES_PER_POLL = 1000;

    /// <summary>
    /// Indicates if this connection can stream
    /// </summary>
    public override bool CanStream => true;

    /// <summary>
    /// Indicates whether data can be written to this connection.
    /// </summary>
    public override bool CanWrite => true;

    /// <summary>
    /// Indicates whether data can be read from this connection.
    /// </summary>
    public override bool CanRead => true;

    /// <summary>
    /// The local port the TCP socket is using
    /// </summary>
    public int LocalPort { get => (m_socket.LocalEndPoint as IPEndPoint).Port; }

    /// <summary>
    /// The remote IPEndPoint of the underlying socket
    /// </summary>
    public IPEndPoint RemoteEndPoint { get => (m_socket.RemoteEndPoint as IPEndPoint); }

    /// <summary>
    /// The max number of times a buffer of data can be sent before unlocking queues and checking for received data
    /// </summary>
    public uint MaxSendsPerLoop { get => m_maxSendsPerLoop; set => m_maxSendsPerLoop = value; }

    /// <summary>
    /// Gets the underlying socket in use on this connection.
    /// </summary>
    /// <remarks>
    /// This is primarily intended to allow for socket option configuration, not for use with sending and receiving directly
    /// as it will throw exceptions if this connection is running.
    /// </remarks>
    public Socket Socket { get => m_socket; }

    /// <summary>
    /// Initializes this TCP connection with the specified settings
    /// </summary>
    /// <param name="socket">The underlying TCP socket to use</param>
    /// <param name="id">The ID of this connection on the parent profile</param>
    /// <param name="messagePriority">The priority value to use for this connection when sending messages</param>
    /// <param name="streamPriority">The priority value to use for this connection when streaming</param>
    /// <param name="encoder">The encoder to use on this connection, if null no encoder will be used</param>
    /// <param name="keepAlive">The period in seconds between sending keep alive messages</param>
    public TcpConnection(Socket socket, UInt32 id, int messagePriority = -1, int streamPriority = 1, TranscoderPair encoder = null, double keepAlive = 10)
      : base(ConnectionTypes.STREAM, id, messagePriority, streamPriority, encoder, keepAlive, -1)
    {
      m_socket = socket;
    }

    /// <inheritdoc/>
    public override void Stop()
    {
      Profile?.Manager.Log.LogDebug("TCP stop called");
      base.Stop();
      m_sendEvent.Set();
    }

    /// <inheritdoc/>
    public override void Join()
    {
      m_sendEvent.Set();
      base.Join();
    }

    /// <summary>
    /// Closes the connection
    /// </summary>
    /// <param name="failure">Indicates whether this close was called due to a socket failure</param>
    public override void Close(bool failure = false)
    {
      base.Close(failure);
      m_socket?.Close();
      m_socket?.Dispose();
      m_socket = null;
      m_sendEvent.Set();
    }

    /// <summary>
    /// Queues a message to be sent on this TCP connection
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <returns>True if the socket is still connected, false otherwise</returns>
    public override bool Send(Message message)
    {
      lock (m_sendMessageQueue)
      {
        message.SetId(this.m_sendId++);
        m_sendMessageQueue.Enqueue(message);
        ResetInactiveTime();
        m_sendEvent.Set();
        return !m_ended;
      }
    }

    /// <summary>
    /// Writes a message directly to the network interface
    /// </summary>
    /// <param name="message">The message to write</param>
    /// <returns>True if writing the message succeeded, false otherwise</returns>
    public override bool Write(Message message)
    {
      try
      {
        lock (m_socket)
        {
          message.SetId(this.m_sendId++);
          byte[] msg = message.Serialize();
          if (Encoder != null)
          {
            // make encoded message
            msg = Encoder.Encode(msg);
            msg = new EncodedMsg(msg).Serialize();
          }

          bool success = m_socket.Send(msg) == msg.Length;
          return success;
        }
      }
      catch
      {
        return false;
      }
    }

    /// <inheritdoc />
    public override bool Send(Stream stream, uint localId, uint remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
    {
      ResetInactiveTime();
      if (transferMode == StreamTransferModes.CONTINUOUS)
      {
        HydraStreamInfo localInfo = new HydraStreamInfo(localId, length, transferMode, HydraStreamDirections.WRITE);
        HydraStreamInfo remoteInfo = new HydraStreamInfo(remoteId, length, transferMode, HydraStreamDirections.READ);
        lock (m_activeSendStreams)
        {
          m_activeSendStreams.Enqueue(new ConnectionStreamInfo(remoteInfo, localInfo, stream));
        }

        m_sendEvent.Set();
        return !m_ended;
      }
      else
      {
        if (base.Send(stream, localId, remoteId, length, transferMode))
        {
          m_sendEvent.Set();
          return true;
        }
      }

      return false;
    }

    /// <inheritdoc/>
    public override ConnectionDiagnostics GetDiagnostics()
    {
      ConnectionDiagnostics summary = base.GetDiagnostics();
      summary.LocalPort = LocalPort;
      return summary;
    }

    /// <summary>
    /// Sets the active receive stream information
    /// </summary>
    /// <param name="info">The information describing the active receiving stream</param>
    public void SetActiveStream(HydraStreamInfo info)
    {
      if (m_activeReceiveStream != null)
      {
        Profile.Manager.Log.LogError("Setting a new active receive stream when the old one isn't null!");
      }
      m_activeReceiveStream = new ConnectionStreamInfo(null, info, null);
    }

    /// <summary>
    /// Receives a Handshake or HandshakeAck message
    /// </summary>
    /// <param name="timeoutMs">The max time in milliseconds to wait for a message</param>
    /// <param name="info">The Handshake information received in the message</param>
    /// <param name="log">The log instance to use in this method</param>
    /// <returns>True if a message was received, false otherwise</returns>
    public bool ReceiveHandshake(int timeoutMs, out HandshakeInfo info, ILogger log = null)
    {
      Message msg = new Message();
      for (int i = 0; i < timeoutMs; i += 100)
      {
        if (InternalReceiveMessage(msg))
        {
          switch (msg.GetCmd())
          {
            case HydraCodes.HydraHandshake:
            {
              HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(msg);
              info = handshakeMsg.GetObject<HandshakeInfo>();
              return true;
            }

            case HydraCodes.HydraHandshakeAck:
            {
              HydraHandshakeAckMsg ackMsg = new HydraHandshakeAckMsg(msg);
              info = ackMsg.GetObject<HandshakeInfo>();
              return true;
            }

            default:
            {
              // Unexpected message - fail connection
              log?.LogError("Received msg: {0}, expecting a handshake, failing the handshake", MsgNames.Get(msg.GetCmd()));
              info = null;
              return false;
            }
          }
        }
        Thread.Sleep(100);
      }

      log?.LogError("Timed out while waiting for a handshake message");
      info = null;
      return false;
    }

    /// <summary>
    /// Determines if this TCP connection is in an active state, prevents sending extra keep alive messages
    /// </summary>
    /// <returns>True if this connection is actively sending or receiving a stream</returns>
    public override bool IsActive()
    {
      return !m_ended && m_activeSendStreams.Count > 0;
    }

    /// <summary>
    /// The main connection run method
    /// </summary>
    protected override void Run()
    {
      try
      {
        bool continuousSending = false;
        string remoteHost = (m_socket.RemoteEndPoint as IPEndPoint)?.Address.ToString();
        if (remoteHost != null && (Profile.Address.Host == null || !Profile.Address.Host.Equals(remoteHost)))
        {
          Profile.Address.Host = remoteHost;
        }

        // Sets up some flags and triggers connection added event
        base.Run();

        // Start receiving
        SocketAsyncEventArgs socketAsyncArgs = new SocketAsyncEventArgs();
        socketAsyncArgs.SetBuffer(m_receiveBuffer, 0, m_receiveBuffer.Length);
        socketAsyncArgs.Completed += HandleReceive;
        if (!m_socket.ReceiveAsync(socketAsyncArgs))
        {
          Task.Run(() => this.HandleReceive(null, socketAsyncArgs));
        }

        while (!StopRun)
        {
          continuousSending = SendStreams();
          if (!continuousSending)
          {
            if (!SendMessages())
            {
              // Nothing to send so wait for activity
              m_sendEvent.WaitOne();
            }
          }
          else
          {
            // Keep processing the active stream so we don't reset the send event yet
          }
        }

        // Lock needed to prevent this call from beating an in progress exception with a failure
        lock (ClientLock)
        {
          Close(false);
        }
      }
      catch (Exception ex)
      {
        Profile.Manager.Log.LogError("Exception in TCP sending on connection: {0}, stopping...", Id);
        Profile.Manager.Log.LogError("Exception: {0}", ex.Message);
        Profile.Manager.Log.LogDebug(ex.ToString());
        // If the stop flag is already set then this shouldn't be counted as a failure since it was likely
        // intentionally triggered
        lock (ClientLock)
        {
          bool failed = !StopRun;
          Stop();
          Profile.Manager.Log.LogDebug("Send thread calling close with failure: {0}", failed);
          Close(failed);
        }
      }

      m_ended = true;
      Profile?.RemoveConnection(this);
      Profile.Manager.Log.LogDebug("TCP connection: {0} on profile: {1} ended", Id, Profile.Uuid);
    }

    /// <summary>
    /// Processes the active and segmented streams sending the next batch of data
    /// </summary>
    /// <returns>True if there is an active continuous stream in progress</returns>
    protected virtual bool SendStreams()
    {
      lock (m_activeSendStreams)
      {
        if (m_activeSendStreams.Count > 0)
        {
          ConnectionStreamInfo streamInfo = m_activeSendStreams.Peek();
          if (streamInfo.TransferCount == 0)
          {
            Write(new StartStreamMsg(streamInfo.RemoteStreamInfo));
          }

          long left = streamInfo.LocalStreamInfo.Length - streamInfo.TransferCount;

          if (Encoder != null)
          {
            Profile.Manager.Log.LogDebug("Using stream encoder");
            for (UInt32 i = 0; left > 0 && i < m_maxSendsPerLoop; ++i)
            {
              int size = (int)Math.Min(m_sendBuffer.Length, left);
              int count = streamInfo.Stream.Read(m_sendBuffer, 0, size);
              Profile.Manager.Log.LogDebug("Read {0} bytes for stream", count);

              if (count > 0)
              {
                // Wrap data in encoded messages
                byte[] crypto = Encoder.Encode(m_sendBuffer, 0, count);
                EncodedMsg encodedMsg = new EncodedMsg(crypto);
                byte[] raw = encodedMsg.Serialize();
                m_socket.Send(raw, 0, raw.Length, SocketFlags.None);
                Profile.Manager.Log.LogDebug("Sent {0} encoded bytes on socket", raw.Length);
                left -= count;
                streamInfo.TransferCount += count;
              }
              else
              {
                break;
              }
            }
          }
          else
          {
            for (UInt32 i = 0; left > 0 && i < m_maxSendsPerLoop; ++i)
            {
              int size = (int)Math.Min(m_sendBuffer.Length, left);
              int count = streamInfo.Stream.Read(m_sendBuffer, 0, size);
              if (count > 0)
              {
                m_socket.Send(m_sendBuffer, 0, count, SocketFlags.None);
                Profile.Manager.Log.LogDebug("Sent {0} bytes on socket", count);
                left -= count;
                streamInfo.TransferCount += count;
              }
            }
          }

          if (streamInfo.TransferCount == streamInfo.LocalStreamInfo.Length)
          {
            Write(new EndStreamMsg(streamInfo.RemoteStreamInfo));
            Profile.EndStream(streamInfo.LocalStreamInfo);
            ConnectionStreamInfo sentStream = m_activeSendStreams.Dequeue();
            ResetInactiveTime();
            Profile.Manager.Log.LogDebug("Ending active sending stream, transferred {0}/{1} bytes.", sentStream.TransferCount, sentStream.LocalStreamInfo.Length);
          }

          return m_activeSendStreams.Count > 0;
        }
      }

      lock (StreamBuffer)
      {
        for (int i = 0; i < StreamBuffer.Count; ++i)
        {
          if (StreamBuffer[i].TransferCount == 0)
          {
            Send(new StartStreamMsg(StreamBuffer[i].RemoteStreamInfo));
          }

          long left = StreamBuffer[i].LocalStreamInfo.Length - StreamBuffer[i].TransferCount;
          int size = (int)Math.Min(m_sendBuffer.Length, left);
          int count = StreamBuffer[i].Stream.Read(m_sendBuffer, 0, size);
          if (count > 0)
          {
            byte[] buffer = null;
            if (count != m_sendBuffer.Length)
            {
              buffer = new byte[count];
              Array.Copy(m_sendBuffer, buffer, count);
            }
            else
            {
              buffer = m_sendBuffer;
            }
            Send(new StreamSegmentMsg(new HydraStreamSegment(StreamBuffer[i].RemoteStreamInfo.Id, StreamBuffer[i].Index++, buffer)));
            left -= count;
            StreamBuffer[i].TransferCount += count;

            if (left <= 0)
            {
              Send(new EndStreamMsg(StreamBuffer[i].RemoteStreamInfo));
              Profile.EndStream(StreamBuffer[i].LocalStreamInfo);
              StreamBuffer.RemoveAt(i--);
            }
          }
          else
          {
            Profile.Manager.Log.LogError("Failed to read bytes from the source stream, ending prematurely with {0}/{1} bytes", StreamBuffer[i].TransferCount, StreamBuffer[i].LocalStreamInfo.Length);
            Send(new EndStreamMsg(StreamBuffer[i].RemoteStreamInfo));
            Profile.EndStream(StreamBuffer[i].LocalStreamInfo);
            StreamBuffer.RemoveAt(i--);
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Sends any messages waiting in the queue
    /// </summary>
    /// <returns>True if there were messages to send, false if nothing was sent</returns>
    protected virtual bool SendMessages()
    {
      bool active = false;
      lock (m_sendMessageQueue)
      {
        // Sending
        if (m_sendMessageQueue.Count > 0)
        {
          if (m_socket.Poll(1, SelectMode.SelectWrite))
          {
            active = true;
            for (int i = 0; m_sendMessageQueue.Count > 0 && i < MAX_MESSAGES_PER_POLL; ++i)
            {
              Message sendMsg = m_sendMessageQueue.Dequeue();
              byte[] msgBuffer = sendMsg.Serialize();
              if (Encoder != null)
              {
                msgBuffer = Encoder.Encode(msgBuffer);
                msgBuffer = new EncodedMsg(msgBuffer).Serialize();
              }

              Profile.Manager.Log.LogDebug("Sending TCP message: {0}", MsgNames.Get(sendMsg.GetCmd()));
              m_socket.Send(msgBuffer, 0, msgBuffer.Length, SocketFlags.None);
            }

            m_sendEvent.Reset();
          }
        }
      }

      return active;
    }

    /// <summary>
    /// Receives a raw stream if there is an active continuous stream
    /// </summary>
    /// <param name="buffer">The buffer containing the bytes to parse</param>
    /// <param name="size">The max number of bytes to use from the buffer</param>
    /// <param name="index">The index of the first byte to use, the value will be updated to the index of the first byte not parsed in the buffer</param>
    /// <returns>True if there is still an active continuous stream</returns>
    private bool ReceiveStream(byte[] buffer, int size, ref uint index)
    {
      if (m_activeReceiveStream != null)
      {
        long left = m_activeReceiveStream.LocalStreamInfo.Length - m_activeReceiveStream.TransferCount;
        if (left > 0)
        {
          // Decode the stream data
          if (Encoder != null)
          {
            // Attempt to deserialize an EncodedMsg
            if (m_activeReceiveStreamMsg == null)
            {
              m_activeReceiveStreamMsg = new Message();
            }

            // Use the whole size value to attempt to deserialize messages since we don't know how the stream data has been
            // segmented for encoding
            while (m_activeReceiveStream.TransferCount < m_activeReceiveStream.LocalStreamInfo.Length &&
              m_activeReceiveStreamMsg.Deserialize(buffer, index, (uint)(size - index), out index))
            {
              if (m_activeReceiveStreamMsg.Command == HydraCodes.HydraEncodedData)
              {
                byte[] raw = Encoder.Decode(new EncodedMsg(m_activeReceiveStreamMsg).GetEncodedBytes());
                Profile.Manager.HandleStream(m_activeReceiveStream.LocalStreamInfo.Id, raw, 0, Profile.Uuid, raw.Length);
                m_activeReceiveStream.TransferCount += raw.Length;
              }
              else
              {
                Profile.Manager.Log.LogError("Received {0} msg instead of expected encoded data for stream", MsgNames.Get(m_activeReceiveStreamMsg.Command));
              }
            }
          }
          else
          {
            int count = (int)Math.Min(left, (size - index));
            Profile.Manager.HandleStream(m_activeReceiveStream.LocalStreamInfo.Id, buffer, (int)index, Profile.Uuid, count);
            m_activeReceiveStream.TransferCount += count;
            index += (uint)count;
          }

          if (m_activeReceiveStream.TransferCount >= m_activeReceiveStream.LocalStreamInfo.Length)
          {
            Profile.Manager.Log.LogDebug("Ending active stream, received {0}/{1} bytes.", m_activeReceiveStream.TransferCount, m_activeReceiveStream.LocalStreamInfo.Length);
            m_activeReceiveStream = null;
            return false;
          }
          else
          {
            return true;
          }
        }
        else
        {
          Profile.Manager.Log.LogDebug("Ending active stream, received {0}/{1} bytes.", m_activeReceiveStream.TransferCount, m_activeReceiveStream.LocalStreamInfo.Length);
          m_activeReceiveStream = null;
        }
      }

      return m_activeReceiveStream != null;
    }

    /// <summary>
    /// Method used to receive a message without locking any objects
    /// </summary>
    /// <param name="msg">The message to load bytes into</param>
    /// <returns>True if the message was completed, false otherwise</returns>
    private bool InternalReceiveMessage(Message msg)
    {
      if (m_socket.Available > Message.HEADER_SIZE)
      {
        if (msg.Deserialize(m_socket))
        {
          if (Encoder != null && msg.GetCmd() == HydraCodes.HydraEncodedData)
          {
            EncodedMsg encodedMsg = new EncodedMsg(msg);
            byte[] raw = Encoder.Decode(encodedMsg.GetEncodedBytes());
            return msg.Deserialize(raw, 0, (uint)raw.Length, out uint index);
          }
          else
          {
            return true;
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Parses the data in the receive buffer.
    /// </summary>
    /// <param name="size">The size of the received data in the buffer.</param>
    /// <param name="index">The index of the first byte to use in the buffer.</param>
    /// <returns>True if more parsing can be done on the data in the received buffer, false if more data should be received before parsing can continue.</returns>
    private bool ParseBuffer(int size, ref uint index)
    {
      // Are we streaming?
      if (ReceiveStream(m_receiveBuffer, size, ref index))
      {
        // Still ready to parse if we have left over bytes
        return index < size;
      }
      else
      {
        if (m_receivingMessage.Deserialize(m_receiveBuffer, index, (uint)(size - index), out index))
        {
          if (Encoder != null && m_receivingMessage.GetCmd() == HydraCodes.HydraEncodedData)
          {
            EncodedMsg encodedMsg = new EncodedMsg(m_receivingMessage);
            byte[] crypto = encodedMsg.GetEncodedBytes();
            byte[] raw = Encoder.Decode(crypto);
            if (!m_receivingMessage.Deserialize(raw, 0, (uint)raw.Length, out uint cryptoIndex))
            {
              Profile.Manager.Log.LogError("Failed to deserialize encoded message data");
            }
          }

          Profile.Manager.Log.LogTrace("Received TCP message: {0}", MsgNames.Get(m_receivingMessage.GetCmd()));
          this.ReceiveMessage(m_receivingMessage);
          m_receivingMessage = new Message();
          return index < size;
        }
        else
        {
          // Couldn't parse a message go grab more bytes
          return false;
        }
      }
    }

    /// <summary>
    /// Callback handler for the asynchronous socket receive method.
    /// </summary>
    /// <param name="sender">Unused.</param>
    /// <param name="e">The socket async event arguments.</param>
    private void HandleReceive(object sender, SocketAsyncEventArgs e)
    {
      bool completed = false;
      try
      {
        while (!StopRun && !completed)
        {
          int readCount = e.BytesTransferred;
          int size = readCount + m_receiveOffset;
          uint index = 0;

          if (e.SocketError == SocketError.Success && readCount > 0)
          {
            while (ParseBuffer(size, ref index)) ;

            if (index > 0 && index < size)
            {
              // Move unused bytes to the beginning
              m_receiveOffset = (int)(size - index);
              for (int i = 0; i < m_receiveOffset; ++i)
              {
                m_receiveBuffer[i] = m_receiveBuffer[index + i];
              }
            }
            else
            {
              m_receiveOffset = 0;
            }
          }
          else
          {
            // Likely a disconnected socket so lets close it
            Profile.Manager.Log.LogWarning("Received returned 0 bytes, closing the connection");
            Stop();
            Close();
          }

          // Start receiving again
          if (!StopRun)
          {
            e.SetBuffer(m_receiveBuffer, m_receiveOffset, m_receiveBuffer.Length - m_receiveOffset);
            if (m_socket.ReceiveAsync(e))
            {
              completed = true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Profile.Manager.Log.LogError("Exception in TCP receiving on connection: {0}, stopping...", Id);
        Profile.Manager.Log.LogError("Exception: {0}", ex.Message);
        Profile.Manager.Log.LogDebug(ex.ToString());
        // If the stop flag is already set then this shouldn't be counted as a failure since it was likely
        // intentionally triggered
        lock (ClientLock)
        {
          bool failed = !StopRun;
          Stop();
          Profile.Manager.Log.LogDebug("Receiving calling close with failure: {0}", failed);
          Close(failed);
        }
      }
    }
  }
}
