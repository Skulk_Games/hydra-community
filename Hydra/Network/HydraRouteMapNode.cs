#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Describes a node within a <see cref="HydraRouteMap"/>
  /// </summary>
  public class HydraRouteMapNode
  {
    private HydraUuid m_uuid;
    private List<HydraUuid> m_profiles = new List<HydraUuid>();

    /// <summary>
    /// The UUID of this application
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The list of profiles this map node is connected with
    /// </summary>
    public List<HydraUuid> Profiles { get => m_profiles; set => m_profiles = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraRouteMapNode()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this map node with the profiles currently managed
    /// </summary>
    /// <param name="hydra">The Hydra application to describe with this map node</param>
    public HydraRouteMapNode(HydraCore hydra)
    {
      m_uuid = hydra.Uuid;

      Profile[] profiles = hydra.GetProfiles();
      if (profiles != null && profiles.Length > 0)
      {
        for (int i = 0; i < profiles.Length; ++i)
        {
          m_profiles.Add(profiles[i].Uuid);
        }
      }
    }

    /// <summary>
    /// Creates a string representation of this object
    /// </summary>
    /// <returns>The string representation of this object</returns>
    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendFormat("Node: {0}\n", m_uuid);
      for (int i = 0; i < m_profiles.Count; ++i)
      {
        sb.AppendFormat("  Profile: {0}\n", m_profiles[i]);
      }

      return sb.ToString();
    }
  }
}
