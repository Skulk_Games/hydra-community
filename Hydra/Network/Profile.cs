#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System;
using System.Collections.Generic;
using System.IO;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Event callback for messages received on a profile
  /// </summary>
  /// <param name="message">The message received</param>
  /// <param name="connection">The connection that received the message</param>
  public delegate void ProfileMessageCallback(Message message, Connection connection);

  /// <summary>
  /// Describes the profile of a connection
  /// </summary>
  public class Profile
  {
    /// <summary>
    /// The max latency (in seconds) allowed before a latency check response will be discarded
    /// </summary>
    private const double MAX_LATENCY = 60;

    /// <summary>
    /// The manager for this profile
    /// </summary>
    private HydraProfileManager m_manager;

    /// <summary>
    /// The Hydra mode of the application this profile describes
    /// </summary>
    private HydraModes m_mode;

    /// <summary>
    /// The UUID of the remote Hydra application
    /// </summary>
    private HydraUuid m_uuid;

    /// <summary>
    /// The name of the remote application
    /// </summary>
    private string m_name;

    /// <summary>
    /// Address information for connecting to the application associated with this profile
    /// </summary>
    private HydraAddress m_address;

    /// <summary>
    /// An authentication value associated with this profile
    /// </summary>
    private UInt64 m_key;

    /// <summary>
    /// The authentication value used on the remote endpoint for this profile
    /// </summary>
    private UInt64 m_remoteKey;

    /// <summary>
    /// The counter used for assigning connection IDs
    /// </summary>
    private UInt32 m_idCounter;

    /// <summary>
    /// The object that manages the connections on this profile
    /// </summary>
    private ConnectionManager m_connections = new ConnectionManager();

    /// <summary>
    /// The stopwatch instance used to time connection latency
    /// </summary>
    private System.Diagnostics.Stopwatch m_latencyStopwatch = new System.Diagnostics.Stopwatch();

    /// <summary>
    /// The queue of IDs that are in the queue for triggering a latency check
    /// </summary>
    private TimeoutQueue<LatencyCheck> m_latencyCheckIds = new TimeoutQueue<LatencyCheck>(3);

    /// <summary>
    /// A counter used to generate the IDs used in latency checks
    /// </summary>
    private byte m_latencyIdCounter = 1;

    /// <summary>
    /// Indicates whether the <see cref="HydraCloseProfileMsg"/> should be sent when shutting down
    /// </summary>
    private bool m_sendCloseMessage = true;

    /// <summary>
    /// The max time in milliseconds to delay closing for the close message
    /// </summary>
    private int m_closeDelayMs = 100;

    /// <summary>
    /// Gets or sets the authentication key value for this profile
    /// </summary>
    public ulong Key { get => m_key; }

    /// <summary>
    /// The key used on the remote end point for this profile
    /// </summary>
    public ulong RemoteKey { get => m_remoteKey; set => m_remoteKey = value; }

    /// <summary>
    /// Gets the number of connections on this profile
    /// </summary>
    public int ConnectionCount { get => m_connections.Count; }

    /// <summary>
    /// The UUID of the remote Hydra application
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; }

    /// <summary>
    /// The name of the remote application
    /// </summary>
    public string Name { get => m_name; }

    /// <summary>
    /// Address information for connecting to the application associated with this profile
    /// </summary>
    public HydraAddress Address { get => m_address; }

    /// <summary>
    /// Gets the mode that the application this profile is associated with is in
    /// </summary>
    public HydraModes Mode { get => m_mode; }

    /// <summary>
    /// Gets the manager holding this profile
    /// </summary>
    public HydraProfileManager Manager { get => m_manager; }

    /// <summary>
    /// Gets the object managing the connections on this profile
    /// </summary>
    public ConnectionManager ConnectionManager { get => m_connections; }

    /// <summary>
    /// A value indicating whether this profile will send a <see cref="HydraCloseProfileMsg"/> before shutting down
    /// </summary>
    public bool SendCloseMessage { get => m_sendCloseMessage; set => m_sendCloseMessage = value; }

    /// <summary>
    /// The max time in milliseconds to wait for the <see cref="HydraCloseProfileMsg"/> to send before shutting down
    /// </summary>
    public int CloseDelayMs { get => m_closeDelayMs; set => m_closeDelayMs = value; }

    /// <summary>
    /// Event invoked when a connection on this profile has its latency updated
    /// </summary>
    public event ConnectionEventCallback OnLatencyUpdated;

    /// <summary>
    /// Event invoked when a connection has been added and started running
    /// </summary>
    public event ConnectionEventCallback ConnectionAdded;

    /// <summary>
    /// Event invoked when a connection is removed from this profile
    /// </summary>
    public event ConnectionEventCallback ConnectionRemoved;

    /// <summary>
    /// Event invoked when a connection fails on this profile
    /// </summary>
    public event ConnectionEventCallback ConnectionFailed;

    /// <summary>
    /// Initializes a new profile with the specified ID
    /// </summary>
    /// <param name="manager">The manager of this profile</param>
    /// <param name="uuid">The UUID to assign to this profile</param>
    /// <param name="name">The name of the remote application</param>
    /// <param name="mode">The mode of the remote application</param>
    /// <param name="remoteKey">The key value used on the remote application for the local application profile</param>
    public Profile(HydraProfileManager manager, HydraUuid uuid, string name, HydraModes mode, UInt64 remoteKey)
    {
      m_manager = manager;
      m_uuid = uuid;
      m_name = name;
      m_mode = mode;

      // TODO: improve this to be more 'secure' maybe use cryptographically random value. Currently this
      //       will only provide 1000 different possible sequence variations since it is seeded by milliseconds and
      //       follows the C# random class algorithm. We could simply call reseed giving us theoretically int32 variations.
      m_key = ChanceUtils.RandUInt64(1, UInt64.MaxValue);
      m_remoteKey = remoteKey;
      m_latencyCheckIds.OnItemExpired += OnLatencyCheckExpired;
    }

    /// <summary>
    /// Initializes a new profile with the specified ID and associated connections
    /// </summary>
    /// <param name="manager">The manager of this profile</param>
    /// <param name="uuid">The UUID to assign to this profile</param>
    /// <param name="name">The name of the remote application</param>
    /// <param name="mode">The mode of the remote application</param>
    /// <param name="remoteKey">The key used for this profile on the remote application</param>
    /// <param name="address">The address information of the profiled application</param>
    /// <param name="connections">The connections to associate with this profile</param>
    public Profile(HydraProfileManager manager, HydraUuid uuid, string name, HydraModes mode, UInt64 remoteKey, HydraAddress address, params Connection[] connections)
      : this(manager, uuid, name, mode, remoteKey)
    {
      m_address = address;
      manager.Log.LogDebug("Created profile with address: {0}", address);
      if (connections != null)
      {
        for (int i = 0; i < connections.Length; ++i)
        {
          AddConnection(connections[i]);
        }
      }
    }

    /// <summary>
    /// Adds a connection to this profile
    /// </summary>
    /// <param name="connection">The connection to add</param>
    public void AddConnection(Connection connection)
    {
      if (connection != null)
      {
        lock (m_connections)
        {
          connection.Id = m_idCounter++;
          connection.Profile = this;
          m_connections.Add(connection);
          connection.OnMessageReceived += OnMessageReceived;
        }
      }
    }

    /// <summary>
    /// Removes the connection
    /// </summary>
    /// <param name="connection">The connection to remove</param>
    public void RemoveConnection(Connection connection)
    {
      connection.OnMessageReceived -= OnMessageReceived;
      if (m_connections.Remove(connection))
      {
        try
        {
          ConnectionRemoved?.Invoke(connection);
        }
        catch (Exception ex)
        {
          Manager.Log.LogError("Exception in connection removed event: {0}", ex.Message);
          Manager.Log.LogTrace("{0}", ex.StackTrace);
        }
      }
    }

    /// <summary>
    /// Removes the connection with the specified ID
    /// </summary>
    /// <param name="id">The ID of the connection to remove</param>
    public void RemoveConnection(UInt32 id)
    {
      Connection connection = m_connections.Pop(id);
      if (connection != null)
      {
        connection.OnMessageReceived -= OnMessageReceived;
        try
        {
          ConnectionRemoved?.Invoke(connection);
        }
        catch (Exception ex)
        {
          Manager.Log.LogError("Exception in connection removed event: {0}", ex.Message);
          Manager.Log.LogTrace("{0}", ex.StackTrace);
        }
      }
    }

    /// <summary>
    /// Determines if this profile has a connection of a specified type
    /// </summary>
    /// <param name="type">The type of connection to check for</param>
    /// <returns>True if the specified type of connection exists, false otherwise</returns>
    public bool HasConnection(ConnectionTypes type)
    {
      return m_connections.HasConnection(type);
    }

    /// <summary>
    /// Removes all the connections from this profile
    /// </summary>
    public void ClearConnections()
    {
      m_connections.Clear();
    }

    /// <summary>
    /// Stops, closes, and removes all connections on this profile
    /// </summary>
    public void Shutdown()
    {
      bool closeSent = !m_sendCloseMessage;
      Connection[] connections = m_connections.GetConnections(ConnectionTypes.MESSAGE);
      for (int i = 0; i < connections.Length; ++i)
      {
        if (connections[i] != null)
        {
          if (!closeSent)
          {
            if (connections[i].Send(new HydraCloseProfileMsg()))
            {
              closeSent = true;
              int sleeps = 0;
              while (connections[i].SendQueueCount > 0 && sleeps < m_closeDelayMs)
              {
                System.Threading.Thread.Sleep(1);
                ++sleeps;
              }
            }
          }

          connections[i].Stop();
          connections[i].Close();
        }
      }
    }

    /// <summary>
    /// Gets the connection with the specified ID value
    /// </summary>
    /// <param name="id">The ID of the connection to get</param>
    /// <returns>The connection that matched the ID or null if no matching connection was found</returns>
    public Connection GetConnection(UInt32 id)
    {
      return m_connections.GetConnection(id);
    }

    /// <summary>
    /// Gets the first connection of the specified type on this profile
    /// </summary>
    /// <param name="connectionType">The type of connection to get</param>
    /// <returns>The first connection of the specified type or null if no connection of that type exists</returns>
    public Connection GetConnection(ConnectionTypes connectionType)
    {
      return m_connections.GetConnection(connectionType);
    }

    /// <summary>
    /// Sends a message to this profile, this utilizes the highest priority message connection
    /// </summary>
    /// <param name="message">The message to send to this profile</param>
    /// <returns>True if the message was sent, false otherwise</returns>
    public bool SendMessage(Message message)
    {
      bool sending = true;
      while (sending)
      {
        Connection connection = m_connections.GetPriorityMessenger();
        if (connection?.Send(message) != true)
        {
          if (connection == null)
          {
            Manager.Log?.LogError("Unable to find a connection, can't send message on profile: {0}", Uuid);
            sending = false;
          }
          else
          {
            Manager.Log?.LogError("Failed to send message, closing and removing connection: {0} on profile: {1}", connection.Id, Uuid);
            connection.Stop();
            connection.Close();
            RemoveConnection(connection);
          }
        }
        else
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Sends a message to this profile utilizing a connection of the specified type
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="connectionType">The type of the connection to send the message over</param>
    /// <returns>True if the message was sent, false otherwise</returns>
    public bool SendMessage(Message message, ConnectionTypes connectionType)
    {
      List<Connection> connections = m_connections.GetConnections(connectionType, ConnectionTypes.MESSAGE);
      for (int i = 0; i < connections.Count; ++i)
      {
        if (connections[i].Send(message))
        {
          return true;
        }
        else
        {
          connections[i].Stop();
          connections[i].Close();
          RemoveConnection(connections[i]);
        }
      }
      return false;
    }

    /// <summary>
    /// Sets up a stream to be sent over the preferred connection
    /// </summary>
    /// <param name="stream">The stream containing the data to send</param>
    /// <param name="localId">The locally reserved ID value for the stream</param>
    /// <param name="remoteId">The remotely reserved ID value for the stream</param>
    /// <param name="length">The total length in bytes of the stream</param>
    /// <param name="transferMode">The mode that should be used while transferring the stream</param>
    /// <returns>True if the stream was enqueued to be sent, false if there is no connection able to send the stream</returns>
    public bool SendStream(Stream stream, UInt32 localId, UInt32 remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
    {
      Connection[] connections = m_connections.GetConnections(ConnectionTypes.STREAM);
      for (int i = 0; i < connections.Length; ++i)
      {
        if (connections[i].Send(stream, localId, remoteId, length, transferMode))
        {
          return true;
        }
        else
        {
          Manager.Log?.LogError("Failed to send stream on connection: {0} for profile: {1}. stopping and removing connection", connections[i].Id, Uuid);
          RemoveConnection(connections[i]);
        }
      }
      return false;
    }

    /// <summary>
    /// Method used to end the local sending side of a stream transfer
    /// </summary>
    /// <param name="info">The information describing the local stream to end</param>
    public void EndStream(HydraStreamInfo info)
    {
      m_manager.StreamAuthority.EndStream(info, m_manager.Parent.Uuid);
    }

    /// <summary>
    /// Performs real time periodic updates
    /// </summary>
    /// <param name="time">The object containing update timing information</param>
    public void Update(UpdateTime time)
    {
      m_connections.Update(time);

      // Latency Check Handling
      lock (m_latencyCheckIds)
      {
        if (m_latencyCheckIds.Count > 0)
        {
          if (!m_latencyStopwatch.IsRunning)
          {
            SendLatencyCheck(m_latencyCheckIds.Peek());
          }

          m_latencyCheckIds.Update(time);
        }
      }
    }

    /// <summary>
    /// Queues a latency check for the connection with the specified ID
    /// </summary>
    /// <param name="connectionId">The ID of the connection to test the latency on</param>
    public void StartLatencyCheck(UInt32 connectionId)
    {
      Connection connection = GetConnection(connectionId);
      if (connection != null)
      {
        lock (m_latencyCheckIds)
        {
          m_latencyCheckIds.Enqueue(new LatencyCheck(m_latencyIdCounter, connectionId), MAX_LATENCY);
          if (m_latencyIdCounter < byte.MaxValue)
          {
            ++m_latencyIdCounter;
          }
          else
          {
            m_latencyIdCounter = 1;
          }
        }
      }
    }

    /// <summary>
    /// Creates a new <see cref="ProfileDiagnostics"/> instance with values taken from this profile.
    /// </summary>
    /// <returns>A new <see cref="ProfileDiagnostics"/> with values taken from this profile.</returns>
    public ProfileDiagnostics GetDiagnostics()
    {
      return new ProfileDiagnostics()
      {
        Mode = m_mode,
        Uuid = m_uuid,
        Name = m_name,
        Address = m_address,
        Key = m_key,
        RemoteKey = m_remoteKey,
        IdCounter = m_idCounter,
        ConnectionCount = m_connections.Count
      };
    }

    /// <summary>
    /// Invokes the <see cref="ConnectionFailed"/> event for the specified connection on this profile
    /// </summary>
    /// <param name="connection">The connection on this profile to </param>
    public void TriggerFail(Connection connection)
    {
      try
      {
        ConnectionFailed?.Invoke(connection);
      }
      catch (Exception eventEx)
      {
        Manager.Log.LogError("Exception in connection failure event: {0}", eventEx.Message);
        Manager.Log.LogTrace("{0}", eventEx.StackTrace);
      }
    }

    /// <summary>
    /// Invokes the <see cref="ConnectionAdded"/> event for the specified connection on this profile
    /// </summary>
    /// <param name="connection">The connection on this profile to invoke the add event for</param>
    public void TriggerAdd(Connection connection)
    {
      try
      {
        ConnectionAdded?.Invoke(connection);
      }
      catch (Exception eventEx)
      {
        Manager.Log.LogError("Exception in connection failure event: {0}", eventEx.Message);
        Manager.Log.LogTrace("{0}", eventEx.StackTrace);
      }
    }

    /// <summary>
    /// Handles latency check response messages
    /// </summary>
    /// <param name="checkId">The ID of the latency check received</param>
    /// <param name="connectionId">The ID of the connection that received the response</param>
    public void HandleLatencyCheckResponse(byte checkId, UInt32 connectionId)
    {
      bool match = false;
      double latency = 0;
      lock (m_latencyCheckIds)
      {
        if (m_latencyCheckIds.Peek().CheckId == checkId)
        {
          match = true;
          m_latencyStopwatch.Stop();

          // Our tracked latency is round trip time (RTT)
          latency = m_latencyStopwatch.Elapsed.TotalSeconds;
          m_latencyCheckIds.Dequeue();
        }
      }

      if (match)
      {
        if (latency <= MAX_LATENCY)
        {
          Connection connection = GetConnection(connectionId);
          if (connection != null)
          {
            connection.Latency = latency;
            Manager.Log.LogDebug("Connection: {0} latency set to: {1}", connectionId, latency);
            try
            {
              OnLatencyUpdated?.Invoke(connection);
            }
            catch (Exception ex)
            {
              Manager.Log.LogError("Exception in latency updated callback: {0}", ex.Message);
              Manager.Log.LogTrace("{0}", ex.StackTrace);
            }
          }
        }
      }
    }

    /// <summary>
    /// Event handler for when a connection receives a message
    /// </summary>
    /// <param name="message">The message received on the connection</param>
    /// <param name="connection">The connection that received the message</param>
    private void OnMessageReceived(Message message, Connection connection)
    {
      switch (message.GetCmd())
      {
        case HydraCodes.HydraLatencyTest:
        {
          HydraLatencyMsg latencyMsg = new HydraLatencyMsg(message);
          switch (latencyMsg.GetCheckType())
          {
            case HydraLatencyMsg.CheckType.INITIATED:
            {
              if (connection != null)
              {
                connection.Send(new HydraLatencyMsg((UInt16)(connection.Latency * 1000), latencyMsg.GetCheckId(), HydraLatencyMsg.CheckType.RESPONSE));
              }
              else
              {
                Manager.Log?.LogError("Unable to get the connection with ID: {0}, can't send a latency response.", connection.Id);
              }
            }
            break;

            case HydraLatencyMsg.CheckType.RESPONSE:
            {
              HandleLatencyCheckResponse(latencyMsg.GetCheckId(), connection.Id);
              Manager.Log.LogDebug("{0}: Received latency response", Uuid);
            }
            break;
          }
        }
        break;

        default:
        {
          m_manager.HandleMessage(message, m_uuid, connection.Id);
        }
        break;
      }
    }

    /// <summary>
    /// Initiates the sending of a latency check message and starts the latency timer
    /// </summary>
    /// <param name="latencyCheck">The latency check information</param>
    private void SendLatencyCheck(LatencyCheck latencyCheck)
    {
      Connection connection = GetConnection(latencyCheck.ConnectionId);
      if (connection != null)
      {
        m_latencyStopwatch.Reset();
        connection.TestLatency(m_latencyStopwatch, latencyCheck.CheckId);
      }
    }

    /// <summary>
    /// Method called when a latency check times out
    /// </summary>
    /// <param name="item">The ID of the latency check that timed out</param>
    private void OnLatencyCheckExpired(LatencyCheck item)
    {
      Manager.Log?.LogError("The latency check {0} on profile: {1} connection: {2} has timed out.", item.CheckId, Uuid, item.ConnectionId);
    }

    /// <summary>
    /// Simple class used to store latency check information
    /// </summary>
    private class LatencyCheck
    {
      public byte CheckId;
      public UInt32 ConnectionId;

      /// <summary>
      /// Initializes this object with the specified latency check ID and connection ID
      /// </summary>
      /// <param name="checkId">The ID of the latency check</param>
      /// <param name="connectionId">The ID of the connection the latency check is to be sent on</param>
      public LatencyCheck(byte checkId, UInt32 connectionId)
      {
        CheckId = checkId;
        ConnectionId = connectionId;
      }
    }
  }
}
