#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A class holding information for reserving a stream
  /// </summary>
  public class StreamProfile
  {
    /// <summary>
    /// The module that is handling the stream
    /// </summary>
    private HydraModule m_module;

    /// <summary>
    /// Status information for the stream
    /// </summary>
    private HydraStreamStatus m_streamStatus;

    /// <summary>
    /// The current index of received stream messages/blocks
    /// </summary>
    private UInt32 m_index;

    /// <summary>
    /// The module interested in receiving the stream
    /// </summary>
    public HydraModule Module { get => m_module; set => m_module = value; }

    /// <summary>
    /// The information describing the stream to be sent/received
    /// </summary>
    public HydraStreamStatus StreamStatus { get => m_streamStatus; set => m_streamStatus = value; }

    /// <summary>
    /// The current index of received stream messages/blocks
    /// </summary>
    public uint Index { get => m_index; set => m_index = value; }

    /// <summary>
    /// Initializes this profile with the specified module
    /// </summary>
    /// <param name="module">The module that is managing the stream</param>
    /// <param name="id">The local ID of the stream transfer</param>
    public StreamProfile(HydraModule module, uint id)
    {
      m_module = module;
      m_streamStatus = new HydraStreamStatus()
      {
        LocalId = id
      };
    }
  }
}
