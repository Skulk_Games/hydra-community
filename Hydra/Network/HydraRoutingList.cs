#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Generic;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Class holding a list of routing entries
  /// </summary>
  public class HydraRoutingList
  {
    private List<HydraRoutingEntry> m_entries = new List<HydraRoutingEntry>();

    /// <summary>
    /// Gets the number of entries in this routing list
    /// </summary>
    public int Count { get => m_entries.Count; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraRoutingList()
    {
      // Empty
    }

    /// <summary>
    /// Copies the entries to an array and returns them
    /// </summary>
    /// <returns>The copied array of entries in this list</returns>
    public HydraRoutingEntry[] GetEntries()
    {
      return m_entries.ToArray();
    }

    /// <summary>
    /// Gets the route with the least amount of hops
    /// </summary>
    /// <returns>The route with the least amount of hops or null if no route exists</returns>
    public HydraRoutingEntry GetBestRoute()
    {
      if (m_entries.Count > 0)
      {
        return m_entries[0];
      }

      return null;
    }

    /// <summary>
    /// Attempts to add the specified routing entry to this list
    /// </summary>
    /// <param name="entry">The entry to add</param>
    /// <returns>True if the entry was added, false if it was already present</returns>
    public bool Add(HydraRoutingEntry entry)
    {
      if (!m_entries.Contains(entry))
      {
        m_entries.Add(entry);
        m_entries.Sort();
        return true;
      }

      return false;
    }

    /// <summary>
    /// Attempts to remove the specified entry from the list
    /// </summary>
    /// <param name="entry">The entry to remove</param>
    /// <returns>True if the entry was present and was removed, false otherwise</returns>
    public bool Remove(HydraRoutingEntry entry)
    {
      return m_entries.Remove(entry);
    }

    /// <summary>
    /// Attempts to remove all entries in the list that have the specified routing UUID
    /// </summary>
    /// <param name="route">The routing UUID to remove from this list</param>
    /// <returns>True if any routes have the specified UUID and were removed, false otherwise</returns>
    public bool RemoveRoute(HydraUuid route)
    {
      bool removed = false;
      for (int i = m_entries.Count - 1; i >= 0; --i)
      {
        if (m_entries[i].Route.Equals(route))
        {
          m_entries.RemoveAt(i);
          removed = true;
        }
      }

      return removed;
    }

    /// <summary>
    /// Clears all the entries in this list
    /// </summary>
    public void Clear()
    {
      m_entries.Clear();
    }

    /// <summary>
    /// Increments the hop count by 1 on all entries
    /// </summary>
    public void IncrementHops()
    {
      for (int i = 0; i < m_entries.Count; ++i)
      {
        m_entries[i].Hops += 1;
      }
    }
  }
}
