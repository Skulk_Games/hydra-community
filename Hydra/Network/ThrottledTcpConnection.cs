#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize.Encoding;
using System;
using System.Net.Sockets;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A TCP connection that allows for throttling the send throughput in software
  /// </summary>
  public class ThrottledTcpConnection : TcpConnection
  {
    /// <summary>
    /// Initializes this throttled TCP connection with the specified settings
    /// </summary>
    /// <remarks>
    /// The throttling is not perfect and will be affected by the temporary buffer size used with streams, message size, and the keepalive period.
    /// The throttling will never be allowed to sleep longer than the keepalive period and for very low bandwidths with a large amount of throttling
    /// this can cause the specified max rate to be exceeded.
    /// Currently this will happen if 131,072 / max bps > poll period.
    /// </remarks>
    /// <param name="socket">The underlying TCP socket to use</param>
    /// <param name="id">The ID of this connection on the parent profile</param>
    /// <param name="messagePriority">The priority value to use for this connection when sending messages</param>
    /// <param name="streamPriority">The priority value to use for this connection when streaming</param>
    /// <param name="encoder">The encoder to use on this connection, if null no encoder will be used</param>
    /// <param name="keepAlive">The period in seconds between sending keep alive messages</param>
    /// <param name="maxRateBps">The max rate in bits per second to send data over this connection, the default is unlimited</param>
    public ThrottledTcpConnection(Socket socket, UInt32 id, int messagePriority = -1, int streamPriority = 1, TranscoderPair encoder = null, double keepAlive = 10, int maxRateBps = -1)
      : base(socket, id, messagePriority, streamPriority, encoder, keepAlive)
    {
      // Reduce the buffer size used when sending to improve throttling accuracy
      m_sendBuffer = new byte[16384];

      // Re-initialize the bandwidth manager since the base TcpConnection will initialize it with unlimited bandwidth
      m_bandwidthManager = new BandwidthManager(maxRateBps);

      // Send max of the temp buffer size (16KB) at a time before sleeping to throttle bandwidth
      m_maxSendsPerLoop = 1;
    }

    /// <summary>
    /// Processes the active and segmented streams sending the next batch of data
    /// </summary>
    /// <returns>True if there is an active continuous stream in progress</returns>
    protected override bool SendStreams()
    {
      lock (m_activeSendStreams)
      {
        if (m_activeSendStreams.Count > 0)
        {
          ConnectionStreamInfo streamInfo = m_activeSendStreams.Peek();
          if (streamInfo.TransferCount == 0)
          {
            // I'm going to ignore the start and end message sizes in bandwidth limiting, they're fairly small and
            // it's probably more effort to compute their size than it's worth
            Write(new StartStreamMsg(streamInfo.RemoteStreamInfo));
          }

          long left = streamInfo.LocalStreamInfo.Length - streamInfo.TransferCount;
          if (Encoder != null)
          {
            for (UInt32 i = 0; left > 0 && i < m_maxSendsPerLoop; ++i)
            {
              int size = (int)Math.Min(m_sendBuffer.Length, left);
              int count = streamInfo.Stream.Read(m_sendBuffer, 0, size);
              if (count > 0)
              {
                // Wrap data in encoded messages
                byte[] crypto = Encoder.Encode(m_sendBuffer, 0, count);
                EncodedMsg encodedMsg = new EncodedMsg(crypto);
                byte[] raw = encodedMsg.Serialize();
                m_socket.Send(raw, 0, raw.Length, SocketFlags.None);
                left -= count;
                streamInfo.TransferCount += count;
                m_bandwidthManager.AddBytes((ulong)raw.Length);
              }
            }
          }
          else
          {
            for (UInt32 i = 0; left > 0 && i < m_maxSendsPerLoop; ++i)
            {
              int size = (int)Math.Min(m_sendBuffer.Length, left);
              int count = streamInfo.Stream.Read(m_sendBuffer, 0, size);
              if (count > 0)
              {
                m_socket.Send(m_sendBuffer, 0, count, SocketFlags.None);
                left -= count;
                streamInfo.TransferCount += count;
                m_bandwidthManager.AddBytes((ulong)count);
              }
            }
          }

          if (streamInfo.TransferCount == streamInfo.LocalStreamInfo.Length)
          {
            Write(new EndStreamMsg(streamInfo.RemoteStreamInfo));
            Profile.EndStream(streamInfo.LocalStreamInfo);
            m_activeSendStreams.Dequeue();
            ResetInactiveTime();
          }

          if (m_bandwidthManager.SleepTime > 0)
          {
            if (m_bandwidthManager.SleepTime > KeepAlivePeriod)
            {
              // Max sleep time is the keep alive period, otherwise
              // the connection could be dropped from inactivity
              m_bandwidthManager.SleepTime = KeepAlivePeriod;
            }

            m_bandwidthManager.Sleep();
          }

          return m_activeSendStreams.Count > 0;
        }
      }

      lock (StreamBuffer)
      {
        for (int i = 0; i < StreamBuffer.Count; ++i)
        {
          if (StreamBuffer[i].TransferCount == 0)
          {
            Send(new StartStreamMsg(StreamBuffer[i].RemoteStreamInfo));
          }

          long left = StreamBuffer[i].LocalStreamInfo.Length - StreamBuffer[i].TransferCount;
          int size = (int)Math.Min(m_sendBuffer.Length, left);
          int count = StreamBuffer[i].Stream.Read(m_sendBuffer, 0, size);
          if (count > 0)
          {
            byte[] buffer = null;
            if (count != m_sendBuffer.Length)
            {
              buffer = new byte[count];
              Array.Copy(m_sendBuffer, buffer, count);
            }
            else
            {
              buffer = m_sendBuffer;
            }
            Send(new StreamSegmentMsg(new HydraStreamSegment(StreamBuffer[i].RemoteStreamInfo.Id, StreamBuffer[i].Index++, buffer)));
            left -= count;
            StreamBuffer[i].TransferCount += count;
          }

          if (left <= 0)
          {
            Send(new EndStreamMsg(StreamBuffer[i].RemoteStreamInfo));
            Profile.EndStream(StreamBuffer[i].LocalStreamInfo);
            StreamBuffer.RemoveAt(i--);
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Sends any messages waiting in the queue
    /// </summary>
    /// <returns>True if there were messages to send, false if nothing was sent</returns>
    protected override bool SendMessages()
    {
      bool active = false;
      lock (m_sendMessageQueue)
      {
        // Sending
        if (m_sendMessageQueue.Count > 0)
        {
          active = true;
          for (int i = 0; m_sendMessageQueue.Count > 0 && i < MAX_MESSAGES_PER_POLL; ++i)
          {
            Message sendMsg = m_sendMessageQueue.Dequeue();
            byte[] msgBuffer = sendMsg.Serialize();
            if (Encoder != null)
            {
              msgBuffer = Encoder.Encode(msgBuffer);
              msgBuffer = new EncodedMsg(msgBuffer).Serialize();
            }

            m_socket.Send(msgBuffer, 0, msgBuffer.Length, SocketFlags.None);
            m_bandwidthManager.AddBytes((ulong)msgBuffer.Length);
          }
        }
      }

      if (m_bandwidthManager.SleepTime > 0)
      {
        if (m_bandwidthManager.SleepTime > KeepAlivePeriod)
        {
          // Max sleep time is the keep alive period, otherwise
          // the connection could be dropped from inactivity
          m_bandwidthManager.SleepTime = KeepAlivePeriod;
        }

        m_bandwidthManager.Sleep();

        // Return as active to avoid sleeping again
        return true;
      }

      return active;
    }
  }
}
