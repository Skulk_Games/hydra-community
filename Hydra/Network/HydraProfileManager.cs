#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// An event callback involving a profile
  /// </summary>
  /// <param name="profile">The profile involved in the event</param>
  public delegate void ProfileEventCallback(Profile profile);

  /// <summary>
  /// An event callback involving a connection
  /// </summary>
  /// <param name="connection">The connection involved in the event</param>
  public delegate void ConnectionEventCallback(Connection connection);

  /// <summary>
  /// Manages a collection of <see cref="Profile"/> instances
  /// </summary>
  public class HydraProfileManager : GenericModularApplication<HydraProfileManager>
  {
    private HydraCore m_parent;
    private TimeoutDictionary<HydraUuid, Profile> m_profiles = new TimeoutDictionary<HydraUuid, Profile>();
    private StreamManager m_streamAuthority;
    private HydraRouter m_router;
    private double m_routerUpdateTime = 0;

    private double m_keepAlivePeriod;
    private double m_connectionTimeout;

    private int m_maxProfiles = int.MaxValue;

    /// <summary>
    /// The number of profiles
    /// </summary>
    public int Count { get => m_profiles.Count; }

    /// <summary>
    /// Event invoked when any message is received
    /// </summary>
    public event MessageClientCallback OnMessageReceived;

    /// <summary>
    /// Event invoked an a profile is added
    /// </summary>
    public event ProfileEventCallback OnProfileAdded;

    /// <summary>
    /// Event invoked when a profile is removed
    /// </summary>
    public event ProfileEventCallback OnProfileRemoved;

    /// <summary>
    /// Event invoked when a connection is added
    /// </summary>
    public event ConnectionEventCallback OnConnectionAdded;

    /// <summary>
    /// Event invoked when a connection is removed
    /// </summary>
    public event ConnectionEventCallback OnConnectionRemoved;

    /// <summary>
    /// Callback invoked when a connection fails
    /// </summary>
    public event ConnectionEventCallback OnConnectionFailed;

    /// <summary>
    /// Gets or sets the StreamManager instance used by the profiles
    /// </summary>
    public StreamManager StreamAuthority { get => m_streamAuthority; set => m_streamAuthority = value; }

    /// <summary>
    /// Gets the router instance in use
    /// </summary>
    public HydraRouter Router { get => m_router; }

    /// <summary>
    /// Gets or sets the parent Hydra application
    /// </summary>
    public HydraCore Parent { get => m_parent; set => m_parent = value; }

    /// <summary>
    /// The max number of profiles to allow
    /// </summary>
    public int MaxProfiles { get => m_maxProfiles; set => m_maxProfiles = value; }

    /// <summary>
    /// Initializes a <see cref="HydraProfileManager"/> instance
    /// </summary>
    /// <param name="parent">The parent application holding this profile manager</param>
    /// <param name="streamManager">The stream manager to use</param>
    /// <param name="messageHandler">The callback to invoke for received messages</param>
    /// <param name="keepAlivePeriod">The period in seconds to send keep alive messages on inactive connections</param>
    /// <param name="profileTimeout">The time in seconds before a profile should be removed after becoming unresponsive</param>
    /// <param name="enableRouting">Indicates whether this manager should support routing messages</param>
    public HydraProfileManager(HydraCore parent, StreamManager streamManager, MessageClientCallback messageHandler, double keepAlivePeriod = 10, double profileTimeout = 30, bool enableRouting = false)
    {
      m_parent = parent;
      Log = m_parent.Log;
      m_streamAuthority = streamManager;
      m_router = enableRouting ? new HydraRouter(this) : null;

      m_keepAlivePeriod = keepAlivePeriod;
      m_connectionTimeout = profileTimeout;
      OnMessageReceived += messageHandler;
      OnUpdate += Update;
      m_profiles.OnItemExpired += OnProfileTimeout;
    }

    /// <summary>
    /// Stops the application, module, and connection threads and clears all profiles
    /// </summary>
    public override void Stop()
    {
      base.Stop();
      ClearProfiles();
    }

    /// <summary>
    /// Adds the connection under a new profile
    /// </summary>
    /// <param name="connection">The connection to add</param>
    /// <param name="uuid">The UUID of the profile created for the connection</param>
    /// <param name="name">The name of the application the new profile describes</param>
    /// <param name="mode">The mode of the application the new profile describes</param>
    /// <param name="remoteKey">The key used on the remote application for the profile</param>
    /// <param name="address">The address information for the new application profile</param>
    /// <returns>True if the connection was added, false otherwise</returns>
    public bool AddConnection(Connection connection, HydraUuid uuid, string name, HydraModes mode, UInt64 remoteKey, HydraAddress address)
    {
      bool result = false;
      Profile profile = null;
      lock (m_profiles)
      {
        connection.Id = 0;
        if (!m_profiles.ContainsKey(uuid))
        {
          if (m_profiles.Count < m_maxProfiles)
          {
            profile = new Profile(this, uuid, name, mode, remoteKey, address, connection);
            profile.ConnectionAdded += HandleAddedConnections;
            profile.ConnectionRemoved += HandleRemovedConnections;
            profile.ConnectionFailed += HandleFailedConnections;
            m_profiles.Add(uuid, profile, m_connectionTimeout);
            result = true;
          }
          else
          {
            Log.LogWarning("The max number of profiles: {0} has been reached can't add a profile for: {1}", m_maxProfiles, uuid);
          }
        }
        else
        {
          Log.LogError("Can't add a new profile for UUID: {0}, it already exists", uuid);
        }
      }

      if (result)
      {
        try
        {
          OnProfileAdded?.Invoke(profile);
        }
        catch (Exception ex)
        {
          Log.LogError("Exception in profile added event: {0}", ex.Message);
          Log.LogTrace("{0}", ex.StackTrace);
        }
      }

      return result;
    }

    /// <summary>
    /// Attempts to add the connection to a matching profile
    /// </summary>
    /// <param name="connection">The connection to add</param>
    /// <param name="uuid">The UUID of the profile to add the connection to</param>
    /// <param name="key">The key of the profile to add the connection to</param>
    /// <returns>True if the connection was added, false if the connection was unable to be added</returns>
    public bool AddConnection(Connection connection, HydraUuid uuid, UInt64 key)
    {
      bool result = false;
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out Profile profile))
        {
          if (profile.Key == key)
          {
            profile.AddConnection(connection);
            result = true;
          }
          // Specifically handle the race condition where both apps have sent initial handshakes and both expect each other to use their key
          // we deconflict by detecting this specific case of a single uninitialized UDP connection and removing our old connection and accepting
          // the new one if our UUID is lower than theirs.
          else if (key == 0 && profile.ConnectionCount == 1 && (profile.GetConnection((uint)0) as UdpConnection) != null &&
            (profile.GetConnection((uint)0) as UdpConnection).ClientEp == null && Parent.Uuid < uuid)
          {
            // We're in an uninitialized state and we're the lesser UUID so let it through even though the key isn't right
            Log.LogWarning("Dropping uninitialized connection 0 since we've detected a likely handshake race and {0} < {1}", Parent.Uuid, uuid);
            // Remove the old conflicting one
            profile.RemoveConnection(0);
            // Add this new one
            profile.AddConnection(connection);
            result = true;
          }
          else
          {
            Log.LogError("Invalid key: {0} for profile: {1}, expected: {2}", key, uuid, profile.Key);
            Parent.SendError(HydraErrors.INVALID_HANDSHAKE_KEY, uuid);
          }
        }
        else
        {
          Log.LogError("Can't find profile with UUID: {0}, can't add connection", uuid);
          Parent.SendError(HydraErrors.INVALID_CLIENT_ID, uuid);
        }
      }

      return result;
    }

    /// <summary>
    /// Attempts to add the connection to a matching profile without authenticating a key value
    /// </summary>
    /// <param name="connection">The connection to add</param>
    /// <param name="uuid">The UUID of the profile to add the connection to</param>
    /// <returns>True if the connection was added, false if the connection was unable to be added</returns>
    public bool AddConnection(Connection connection, HydraUuid uuid)
    {
      bool result = false;
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out Profile profile))
        {
          profile.AddConnection(connection);
          result = true;
        }
      }

      return result;
    }

    /// <summary>
    /// Removes all profiles from this manager
    /// </summary>
    public void ClearProfiles()
    {
      HydraUuid[] uuids = null;
      lock (m_profiles)
      {
        uuids = new HydraUuid[m_profiles.Count];
        m_profiles.Keys.CopyTo(uuids, 0);
      }

      if (uuids != null)
      {
        for (int i = 0; i < uuids.Length; ++i)
        {
          RemoveProfile(uuids[i]);
        }
      }
    }

    /// <summary>
    /// Gets the profile with the matching ID
    /// </summary>
    /// <param name="uuid">The UUID of the profile to get</param>
    /// <returns>The profile with the specified ID or null if no profile was found</returns>
    public Profile GetProfile(HydraUuid uuid)
    {
      if (uuid == null)
      {
        return null;
      }

      if (m_profiles.TryGetValue(uuid, out Profile profile))
      {
        return profile;
      }
      else
      {
        return null;
      }
    }

    /// <summary>
    /// Gets the profile that matches the specified address
    /// </summary>
    /// <param name="address">The address information of the profile to get</param>
    /// <returns>The profile with a matching address or null if no matching profile was found</returns>
    public Profile GetProfile(HydraAddress address)
    {
      if (address == null)
      {
        return null;
      }

      Profile[] profiles = m_profiles?.Values;
      if (profiles != null)
      {
        for (int i = 0; i < profiles.Length; ++i)
        {
          if (profiles[i]?.Address?.Equals(address) == true)
          {
            Log?.LogTrace("Profile[{0}] matches {1}", i, address);
            return profiles[i];
          }
        }
      }
      else
      {
        Log?.LogDebug("GetProfile(HydraAddress) Profiles null");
      }

      return null;
    }

    /// <summary>
    /// Gets the UUIDs of the managed profiles
    /// </summary>
    /// <returns>An array of the UUIDs of the profiles</returns>
    public HydraUuid[] GetUuids()
    {
      return m_profiles.Keys;
    }

    /// <summary>
    /// Determines if the specified UUID is in the managed profiles
    /// </summary>
    /// <param name="uuid">The UUID to check for</param>
    /// <returns>True if there is a profile with the specified UUID, false otherwise</returns>
    public bool Contains(HydraUuid uuid)
    {
      return m_profiles.ContainsKey(uuid);
    }

    /// <summary>
    /// Gets the UUID of the first profile found in SERVER mode
    /// </summary>
    /// <returns>The UUID of the first profile found in SERVER mode</returns>
    public HydraUuid GetServerUuid()
    {
      List<Profile> profiles = GetProfiles(HydraModes.SERVER);
      if (profiles.Count > 0)
      {
        return profiles[0].Uuid;
      }

      return null;
    }

    /// <summary>
    /// Gets an array of the managed profiles
    /// </summary>
    /// <returns>An array of the profiles</returns>
    public Profile[] GetProfiles()
    {
      return m_profiles.Values;
    }

    /// <summary>
    /// Gets profiles in a specified mode
    /// </summary>
    /// <param name="mode">The profile mode to search for</param>
    /// <returns>The list of profiles in the specified mode</returns>
    public List<Profile> GetProfiles(HydraModes mode)
    {
      List<Profile> profiles = new List<Profile>();
      lock (m_profiles)
      {
        foreach (Profile profile in m_profiles.Values)
        {
          if (profile.Mode == mode)
          {
            profiles.Add(profile);
          }
        }
      }

      return profiles;
    }

    /// <summary>
    /// Gets the profiles with the specified name and use the specified mode
    /// </summary>
    /// <remarks>
    /// Names are not guaranteed to be unique, so this method returns all matching profiles 
    /// </remarks>
    /// <param name="name">The name of the remote applications to get a profile for</param>
    /// <param name="mode">The mode of the applications to get, if NULL then all applications will match</param>
    /// <returns>The list of profiles with a matching name and mode</returns>
    public List<Profile> GetProfiles(string name, HydraModes mode = HydraModes.NULL)
    {
      List<Profile> results = new List<Profile>();
      if (name != null)
      {
        Profile[] profiles = m_profiles.Values;
        for (int i = 0; i < profiles.Length; ++i)
        {
          if (profiles[i].Name.Equals(name) && (mode == HydraModes.NULL || profiles[i].Mode == mode))
          {
            results.Add(profiles[i]);
          }
        }
      }

      return results;
    }

    /// <summary>
    /// Attempts to remove a profile
    /// </summary>
    /// <param name="uuid">The UUID of the profile to remove</param>
    /// <returns>True if the profile was found and removed, false if it was not found</returns>
    public bool RemoveProfile(HydraUuid uuid)
    {
      Profile profile = null;
      bool result = false;
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out profile))
        {
          profile.Shutdown();
        }
        result = m_profiles.Remove(uuid);
      }

      if (result && profile != null)
      {
        Connection[] connections = profile.ConnectionManager.GetConnections(ConnectionTypes.MESSAGE);
        for (UInt32 i = 0; i < connections.Length; ++i)
        {
          profile.RemoveConnection(connections[i]);
        }

        profile.ConnectionAdded -= HandleAddedConnections;
        profile.ConnectionRemoved -= HandleRemovedConnections;
        profile.ConnectionFailed -= HandleFailedConnections;
        try
        {
          OnProfileRemoved?.Invoke(profile);
        }
        catch (Exception ex)
        {
          Log.LogError("Exception in profile removed event: {0}", ex.Message);
          Log.LogTrace("{0}", ex.StackTrace);
        }
      }
      return result;
    }

    /// <summary>
    /// Attempts to send a message to the profile indicated
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="uuid">The UUID of the profile to send the message to</param>
    /// <returns>True if the message was sent, false otherwise</returns>
    public bool SendMessage(Message message, HydraUuid uuid)
    {
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out Profile profile))
        {
          if (profile.SendMessage(message))
          {
            return true;
          }
        }
        else
        {
          Log.LogError("Failed to find a profile for UUID: {0}, can't send message: {1}", uuid, MsgNames.Get(message.GetCmd()));
        }
      }
      return false;
    }

    /// <summary>
    /// Sends a message to all profiles on this manager
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <returns>True if the message was successfully sent to every profile, false otherwise</returns>
    public bool SendMessageAll(Message message)
    {
      bool result = true;
      lock (m_profiles)
      {
        foreach (Profile profile in m_profiles.Values)
        {
          result &= profile.SendMessage(message);
        }
      }

      return result;
    }

    /// <summary>
    /// Sends a stream to another application
    /// </summary>
    /// <param name="stream">The stream containing the data to send</param>
    /// <param name="uuid">The UUID of the application to send the stream to</param>
    /// <param name="localId">The locally reserved stream ID</param>
    /// <param name="remoteId">The remotely reserved stream ID</param>
    /// <param name="length">The total length in bytes of the stream</param>
    /// <param name="transferMode">The mode to use when transferring the stream</param>
    /// <returns>True if the stream was enqueued to be sent, false if there was an error enqueueing the stream</returns>
    public bool SendStream(Stream stream, HydraUuid uuid, UInt32 localId, UInt32 remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
    {
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out Profile profile))
        {
          if (profile.SendStream(stream, localId, remoteId, length, transferMode))
          {
            return true;
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Determines if there is an active connection to the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the application to check for a connection with</param>
    /// <returns>True if there is a connection to the specified UUID, false otherwise</returns>
    public bool IsConnected(HydraUuid uuid)
    {
      lock (m_profiles)
      {
        if (m_profiles.TryGetValue(uuid, out Profile profile))
        {
          return profile.ConnectionCount > 0;
        }
      }

      return false;
    }

    /// <summary>
    /// Message handler for messages received on a profile connection
    /// </summary>
    /// <param name="message">The message received</param>
    /// <param name="uuid">The UUID of the profile that received the message</param>
    /// <param name="connectionId">The ID of the connection on the profile that received the message</param>
    public void HandleMessage(Message message, HydraUuid uuid, UInt32 connectionId)
    {
      if (uuid != null)
      {
        m_profiles.Reset(uuid);
      }

      // Handle profile messages here
      switch (message.GetCmd())
      {
        case HydraCodes.HydraStreamStart:
        {
          StartStreamMsg startStreamMsg = new StartStreamMsg(message);
          HydraStreamInfo streamInfo = startStreamMsg.GetObject<HydraStreamInfo>();
          Log?.LogDebug("Starting a new stream with ID: {0}", streamInfo.Id);
          if (streamInfo.Mode == StreamTransferModes.CONTINUOUS)
          {
            Profile profile = GetProfile(uuid);
            TcpConnection tcpConnection = profile?.GetConnection(connectionId) as TcpConnection;
            if (tcpConnection != null)
            {
              tcpConnection.SetActiveStream(streamInfo);
            }
          }
        }
        break;

        case HydraCodes.HydraStreamSegment:
        {
          StreamSegmentMsg segmentMsg = new StreamSegmentMsg(message);
          HydraStreamSegment streamSegment = segmentMsg.GetObject<HydraStreamSegment>();
          m_streamAuthority.HandleStream(streamSegment.Id, streamSegment.Data, 0, streamSegment.Index, uuid);
        }
        break;

        case HydraCodes.HydraStreamEnd:
        {
          EndStreamMsg endStreamMsg = new EndStreamMsg(message);
          HydraStreamInfo streamInfo = endStreamMsg.GetObject<HydraStreamInfo>();
          m_streamAuthority.EndStream(streamInfo, uuid);
          Log?.LogDebug("Finished stream with ID: {0}", streamInfo.Id);
        }
        break;

        case HydraCodes.HydraCloseConnection:
        {
          Profile profile = GetProfile(uuid);
          if (profile != null)
          {
            Log.LogDebug("Closing connection: {0} on Profile: {1}", connectionId, uuid);
            Connection connection = profile.GetConnection(connectionId);
            connection?.Stop();
            connection?.Close();
            profile?.RemoveConnection(connectionId);
          }
        }
        break;

        case HydraCodes.HydraCloseProfile:
        {
          Profile profile = GetProfile(uuid);
          if (profile != null)
          {
            Log.LogDebug("Closing profile: {0}", uuid);
            profile?.Shutdown();
            RemoveProfile(uuid);
          }
        }
        break;

        case HydraCodes.HydraRouteEntry:
        {
          if (m_router != null)
          {
            HydraRouteEntryMsg routeEntryMsg = new HydraRouteEntryMsg(message);
            Log.LogDebug("Route entry message received with object ID: {0}", routeEntryMsg.GetObjectId());
            switch (routeEntryMsg.GetObjectId())
            {
              case HydraRouteEntryMsg.REQUEST_ID:
              {
                // This is a message requesting route entries
                Log.LogDebug("Received route entry request");
                HydraRouteMapNode mapNode = new HydraRouteMapNode(Parent);
                Log.LogDebug("Sending map node with {0} profiles", mapNode.Profiles.Count);
                m_router.Route(new RoutedMessage(Parent.Uuid, uuid, new HydraRouteEntryMsg(mapNode)), uuid);
              }
              break;

              case HydraRouteEntryMsg.ENTRY_ID:
              {
                HydraRouteMapNode mapNode = routeEntryMsg.GetObject<HydraRouteMapNode>();
                if (mapNode != null)
                {
                  Log.LogDebug("Application: {0} received map node\n{1}", Parent.Uuid, mapNode.ToString());
                  m_router.SetMapNode(mapNode);
                  if (mapNode.Profiles != null)
                  {
                    foreach (HydraUuid profileUuid in mapNode.Profiles)
                    {
                      if (!profileUuid.Equals(Parent.Uuid))
                      {
                        if (!m_router.RouteMap.ContainsMapNode(profileUuid))
                        {
                          Log.LogTrace("Sending map node request for: {0}", profileUuid);
                          m_router.Route(new RoutedMessage(Parent.Uuid, profileUuid, new HydraRouteEntryMsg()), profileUuid);
                        }
                      }
                    }
                  }
                }
                else
                {
                  Log.LogError("Failed to unmarshal map node from message");
                }
              }
              break;
            }
          }
          else
          {
            Log.LogWarning("Received a HydraRouteEntryMsg, but this application is not configured to support routing, dropping message.");
          }
        }
        break;

        case HydraCodes.HydraRouteMessage:
        {
          if (m_router != null)
          {
            RoutedMessage routedMessage = new RoutedMessage(message);

            if (routedMessage.GetDestination().Equals(Parent.Uuid))
            {
              // I am the destination
              Message subMsg = routedMessage.GetMessage();
              if (subMsg != null)
              {
                HandleRoutedMessage(uuid, routedMessage.GetSource(), subMsg, connectionId);
              }
              else
              {
                Log.LogError("Unable to unmarshal message contained in the routed message, from: {0} with source: {1} and destination: {2}", uuid, routedMessage.GetSource(), routedMessage.GetDestination());
              }
            }
            else
            {
              if ((Log.LogLevel & (byte)LogLevels.TRACE) != 0)
              {
                Message subMsg = routedMessage.GetMessage();
                if (subMsg != null)
                {
                  Log.LogTrace("Routing sub message: {0}", MsgNames.Get(subMsg.Command));
                }
                Log.LogTrace("Routing message from: {0} with source: {1} to destination: {2}", uuid, routedMessage.GetSource(), routedMessage.GetDestination());
              }

              Profile profile = GetProfile(routedMessage.GetDestination());
              if (profile != null)
              {
                profile.SendMessage(routedMessage);
              }
              else if (m_router.Route(routedMessage, routedMessage.GetDestination()))
              {
                Log.LogTrace("Routing message succeeded using routing table");
              }
              else
              {
                Log.LogError("Unable to find a profile with UUID: {0} and failed to route message.", routedMessage.GetDestination());
              }
            }
          }
          else
          {
            Log.LogWarning("Received a routed message, but this application does not support routed messages. Dropping message...");
          }
        }
        break;

        default:
        {
          // Pre-handling to be added here
          //  this should be a good place to add certain profile modules

          // Action handling
          OnMessageReceived?.Invoke(message, uuid);
        }
        break;
      }
    }

    private void HandleRoutedMessage(HydraUuid path, HydraUuid source, Message message, UInt32 connectionId)
    {
      switch (message.GetCmd())
      {
        case HydraCodes.HydraHandshake:
        {
          if (m_router != null)
          {
            // Handle the virtual connection
            HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(message);
            HandshakeInfo info = handshakeMsg.GetObject<HandshakeInfo>();
            VirtualConnection virtualConnection = new VirtualConnection(GetProfile(path).GetConnection(connectionId), 0);

            if (info.RemoteKey == 0)
            {
              HydraAddress profileAddress = new HydraAddress(info.Address);
              profileAddress.Host = null;
              if (AddConnection(virtualConnection, info.Uuid, info.Name, info.Mode, info.RemoteKey, profileAddress))
              {
                Profile profile = GetProfile(info.Uuid);
                if (profile != null)
                {
                  Log.LogDebug("New {1} virtual connection added under profile with UUID: {0}", info.Uuid, info.Mode);
                  virtualConnection.Start(true);
                  SendMessage(new HydraHandshakeAckMsg(new HandshakeInfo(Parent.Uuid, Parent.ApplicationName, Parent.HydraMode, Parent.Address, profile.Key)), source);
                }
                else
                {
                  Log.LogError("Failed to add virtual connection requesting a new profile");
                  virtualConnection.Close();
                }
              }
            }
            else if (AddConnection(virtualConnection, info.Uuid, info.RemoteKey))
            {
              Log.LogDebug("New virtual connection assigned to profile with UUID: {0}", info.Uuid);
              virtualConnection.Start(true);
              SendMessage(new HydraHandshakeAckMsg(new HandshakeInfo(Parent.Uuid, Parent.ApplicationName, Parent.HydraMode, Parent.Address, info.RemoteKey, info.LocalKey)), source);
            }
            else
            {
              Log.LogError("Failed to add virtual connection requesting UUID: {0}", info.Uuid);
            }
          }
        }
        break;

        case HydraCodes.HydraHandshakeAck:
        {
          // Create a new connection to handle
          HydraHandshakeAckMsg ackMsg = new HydraHandshakeAckMsg(message);
          HandshakeInfo info = ackMsg.GetObject<HandshakeInfo>();
          if (info != null)
          {
            VirtualConnection virtualConnection = new VirtualConnection(GetProfile(path).GetConnection(connectionId), 0);
            Log.LogDebug("Adding new {0} virtual connection for profile: {1} and key: {2} from address: {3}", info.Mode, info.Uuid, info.RemoteKey, info.Address);
            bool success;
            if (!Contains(info.Uuid))
            {
              HydraAddress profileAddress = new HydraAddress(info.Address);
              profileAddress.Host = null;
              success = AddConnection(virtualConnection, info.Uuid, info.Name, info.Mode, info.RemoteKey, profileAddress);
            }
            else
            {
              success = AddConnection(virtualConnection, info.Uuid, info.RemoteKey);
            }

            if (!success)
            {
              Log.LogError("Failed to add virtual connection for profile: {0}", info.Uuid);
            }
            else
            {
              virtualConnection.Start(true);
            }
          }
        }
        break;

        case HydraCodes.HydraLatencyTest:
        {
          HydraLatencyMsg latencyMsg = new HydraLatencyMsg(message);
          Log.LogDebug("Received routed latency check message");
          switch (latencyMsg.GetCheckType())
          {
            case HydraLatencyMsg.CheckType.INITIATED:
            {
              Profile profile = GetProfile(source);
              if (profile != null)
              {
                Connection connection = profile.GetConnection(connectionId);
                if (connection != null)
                {
                  connection.Send(new HydraLatencyMsg((UInt16)(connection.Latency * 1000), latencyMsg.GetCheckId(), HydraLatencyMsg.CheckType.RESPONSE));
                  Log.LogDebug("Sending latency check message response");
                }
                else
                {
                  Log.LogError("Unable to get the connection with ID: {0}, can't send a latency response.", connectionId);
                }
              }
              else
              {
                Log.LogError("Unable to get profile for source: {0}, can't send a latency response", source);
              }
            }
            break;

            case HydraLatencyMsg.CheckType.RESPONSE:
            {
              Profile profile = GetProfile(source);
              if (profile != null)
              {
                profile.HandleLatencyCheckResponse(latencyMsg.GetCheckId(), connectionId);
                Log.LogDebug("{0}: Received latency response", source);
              }
            }
            break;
          }
        }
        break;

        default:
        {
          // Handle other routed messages normally
          HandleMessage(message, source, connectionId);
        }
        break;
      }
    }

    /// <summary>
    /// Handles received stream data
    /// </summary>
    /// <param name="streamId">The ID of the stream that received data</param>
    /// <param name="buffer">The buffer of raw stream bytes received</param>
    /// <param name="offset">The index of the first byte in the buffer that contains stream data</param>
    /// <param name="uuid">The UUID of the application sending the stream</param>
    /// <param name="count">The number of bytes in the buffer that contain stream data</param>
    public void HandleStream(UInt32 streamId, byte[] buffer, int offset, HydraUuid uuid, int count = -1)
    {
      m_profiles.Reset(uuid);
      m_streamAuthority.HandleStream(streamId, buffer, offset, uuid, count);
    }

    /// <summary>
    /// Resets the timeout for the profile with the indicated UUID
    /// </summary>
    /// <param name="uuid">The UUID of the profile whose timeout should be reset</param>
    public void ResetTimeout(HydraUuid uuid)
    {
      m_profiles.Reset(uuid);
    }

    /// <summary>
    /// Periodic update method
    /// </summary>
    /// <param name="updateTime">The object containing application timing info</param>
    private void Update(UpdateTime updateTime)
    {
      if (m_connectionTimeout > 0)
      {
        m_profiles.Update(updateTime);
      }

      foreach (Profile profile in m_profiles.Values)
      {
        profile.Update(updateTime);
      }

      if (m_router != null)
      {
        m_routerUpdateTime += updateTime.DeltaSeconds;
        if (m_routerUpdateTime >= m_keepAlivePeriod)
        {
          RequestRouteEntries(m_router.RouteMap.GetMissingNodes());
          m_routerUpdateTime = 0;
        }
      }
    }

    private void RequestRouteEntries(List<HydraUuid> missing)
    {
      foreach (HydraUuid uuid in missing)
      {
        if (!uuid.Equals(Parent.Uuid))
        {
          m_router.Route(new RoutedMessage(Parent.Uuid, uuid, new HydraRouteEntryMsg()), uuid);
        }
      }
    }

    /// <summary>
    /// Event method called when a profile is removed due to a timeout
    /// </summary>
    /// <param name="profile">The profile that has timed out</param>
    private void OnProfileTimeout(Profile profile)
    {
      Log?.LogWarning("Profile with UUID: {0} has timed out due to inactivity", profile.Uuid);
      profile.Shutdown();
      profile.ConnectionAdded -= HandleAddedConnections;
      profile.ConnectionRemoved -= HandleRemovedConnections;
      profile.ConnectionFailed -= HandleFailedConnections;
      try
      {
        OnProfileRemoved?.Invoke(profile);
      }
      catch (Exception ex)
      {
        Log.LogError("Exception in profile removed event: {0}", ex.Message);
        Log.LogTrace("{0}", ex.StackTrace);
      }
    }

    /// <summary>
    /// Handles the connection added event at the profile level and invokes the event on this manager
    /// </summary>
    /// <param name="connection">The connection that was added</param>
    private void HandleAddedConnections(Connection connection)
    {
      OnConnectionAdded?.Invoke(connection);
    }

    /// <summary>
    /// Handles the connection removed event at the profile level and invokes the event on this manager
    /// </summary>
    /// <param name="connection">The connection that was removed</param>
    private void HandleRemovedConnections(Connection connection)
    {
      OnConnectionRemoved?.Invoke(connection);
    }

    /// <summary>
    /// Handles the connection failed event at the profile level and invokes the event on this manager
    /// </summary>
    /// <param name="connection">The connection that failed</param>
    private void HandleFailedConnections(Connection connection)
    {
      OnConnectionFailed?.Invoke(connection);
    }
  }
}
