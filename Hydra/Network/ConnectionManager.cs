#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL;
using System;
using System.Collections.Generic;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A class that manages a collection of connections
  /// </summary>
  public class ConnectionManager
  {
    private List<Connection> m_master = new List<Connection>(2);
    private List<Connection> m_streaming = new List<Connection>(2);
    private List<Connection> m_messaging = new List<Connection>(2);

    /// <summary>
    /// The number of connections managed
    /// </summary>
    public int Count { get => GetCount(); }

    /// <summary>
    /// Default constructor
    /// </summary>
    public ConnectionManager()
    {
      // Empty
    }

    /// <summary>
    /// Adds a connection to the collection of managed connections
    /// </summary>
    /// <param name="connection">The connection to add</param>
    public void Add(Connection connection)
    {
      lock (m_master)
      {
        m_master.Add(connection);

        // Don't add unwritable connections
        if (connection.CanWrite)
        {
          m_messaging.Add(connection);
          m_messaging.Sort((a, b) => -1 * a.MessagePriority.CompareTo(b.MessagePriority));
          m_streaming.Add(connection);
          m_streaming.Sort((a, b) => -1 * a.StreamPriority.CompareTo(b.StreamPriority));
        }
      }
    }

    /// <summary>
    /// Removes a connection from this manager
    /// </summary>
    /// <param name="connection">The connection to remove</param>
    /// <returns>True if the connection was successfully removed, otherwise false</returns>
    public bool Remove(Connection connection)
    {
      lock (m_master)
      {
        return m_master.Remove(connection) &&
        m_messaging.Remove(connection) &&
        m_streaming.Remove(connection);
      }
    }

    /// <summary>
    /// Removes the connection with the specified ID
    /// </summary>
    /// <param name="Id">The ID of the connection to remove</param>
    public void Remove(UInt32 Id)
    {
      lock (m_master)
      {
        Connection connection = m_master.Find(x => x.Id == Id);
        if (connection != null)
        {
          m_master.Remove(connection);
          m_messaging.Remove(connection);
          m_streaming.Remove(connection);
        }
      }
    }

    /// <summary>
    /// Gets the connection with the specified ID
    /// </summary>
    /// <param name="id">The ID of the connection to get</param>
    /// <returns>The connection with the matching ID or null if no matching connection was found</returns>
    public Connection GetConnection(UInt32 id)
    {
      lock (m_master)
      {
        return m_master.Find(x => x.Id == id);
      }
    }

    /// <summary>
    /// Gets the first connection of the specified type
    /// </summary>
    /// <param name="type">The type of connection to get</param>
    /// <returns>The connection fouond of the specified type or null if no connection was found</returns>
    public Connection GetConnection(ConnectionTypes type)
    {
      lock (m_master)
      {
        return m_master.Find(x => x.ConnectionType == type);
      }
    }

    /// <summary>
    /// Gets all the connections of the specified type sorted by the connection type priority
    /// </summary>
    /// <param name="type">The type of connections to get</param>
    /// <param name="priorityType">The type to use for priority</param>
    /// <returns>The list of connections of the specified type</returns>
    public List<Connection> GetConnections(ConnectionTypes type, ConnectionTypes priorityType)
    {
      lock (m_master)
      {
        switch(priorityType)
        {
          case ConnectionTypes.MESSAGE:
          {
            return m_messaging.FindAll(x => x.ConnectionType == type);
          }

          case ConnectionTypes.STREAM:
          {
            return m_streaming.FindAll(x => x.ConnectionType == type);
          }
        }
      }
      return null;
    }

    /// <summary>
    /// Gets an array of connections ordered by the specified priority
    /// </summary>
    /// <param name="priority">The priority order to get the connections in</param>
    /// <returns>The array of ordered connections</returns>
    public Connection[] GetConnections(ConnectionTypes priority)
    {
      lock (m_master)
      {
        switch(priority)
        {
          case ConnectionTypes.MESSAGE:
          {
            return m_messaging.ToArray();
          }

          case ConnectionTypes.STREAM:
          {
            return m_streaming.ToArray();
          }
        }
      }
      return null;
    }

    /// <summary>
    /// Gets the highest priority message based connection
    /// </summary>
    /// <returns>The highest priority message connection</returns>
    public Connection GetPriorityMessenger()
    {
      lock (m_master)
      {
        if (m_messaging.Count > 0)
        {
          return m_messaging[0];
        }
      }
      return null;
    }

    /// <summary>
    /// Gets the highest priority stream based connection
    /// </summary>
    /// <returns>The highest priority stream connection</returns>
    public Connection GetPriorityStreamer()
    {
      lock (m_master)
      {
        if (m_streaming.Count > 0)
        {
          return m_streaming[0];
        }
      }
      return null;
    }

    /// <summary>
    /// Determines if there is a connection of the specified type
    /// </summary>
    /// <param name="type">The type of connection to check for</param>
    /// <returns>True if a connection of the specified type exists</returns>
    public bool HasConnection(ConnectionTypes type)
    {
      lock (m_master)
      {
        return m_master.Find(x => x.ConnectionType == type) != null;
      }
    }

    /// <summary>
    /// Removes all the connections
    /// </summary>
    public void Clear()
    {
      lock (m_master)
      {
        m_master.Clear();
        m_messaging.Clear();
        m_streaming.Clear();
      }
    }

    /// <summary>
    /// Retrieves and removes the first connection in the master list
    /// </summary>
    /// <returns>The connection that was removed or null if no connections are left</returns>
    public Connection Pop()
    {
      lock (m_master)
      {
        if (m_master.Count > 0)
        {
          Connection connection = m_master[0];
          m_master.Remove(connection);
          m_messaging.Remove(connection);
          m_streaming.Remove(connection);
          return connection;
        }
      }
      return null;
    }

    /// <summary>
    /// Attempts to remove a connection with the specified ID
    /// </summary>
    /// <param name="id">The ID of the connection to remove</param>
    /// <returns>The connection removed or null if no connection was removed</returns>
    public Connection Pop(UInt32 id)
    {
      lock (m_master)
      {
        Connection connection = m_master.Find(x => x.Id == id);
        if (connection != null)
        {
          m_master.Remove(connection);
          m_messaging.Remove(connection);
          m_streaming.Remove(connection);
        }
        return connection;
      }
    }

    /// <summary>
    /// Performs periodic real time updates
    /// </summary>
    /// <param name="time">The object containing update timing information</param>
    public void Update(UpdateTime time)
    {
      lock (m_master)
      {
        for (int i = 0; i < m_master.Count; ++i)
        {
          m_master[i].Update(time);
        }
      }
    }

    /// <summary>
    /// Gets the number of connections managed
    /// </summary>
    /// <returns>The number of connections managed</returns>
    private int GetCount()
    {
      lock (m_master)
      {
        return m_master.Count;
      }
    }
  }
}
