#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Generic;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Holds the information describing routes to all known nodes in a network
  /// </summary>
  public class HydraRoutingTable
  {
    Dictionary<HydraUuid, HydraRoutingList> m_table = new Dictionary<HydraUuid, HydraRoutingList>();

    /// <summary>
    /// Initializes this routing table
    /// </summary>
    public HydraRoutingTable()
    {
      // Empty
    }

    /// <summary>
    /// Gets the UUID of the best route to get to the destination UUID
    /// </summary>
    /// <param name="destination">The UUID of the destination application to get a route for</param>
    /// <returns>The UUID of the profile to send the routed message through</returns>
    public HydraRoutingEntry GetBestRoute(HydraUuid destination)
    {
      lock (m_table)
      {
        if (m_table.TryGetValue(destination, out HydraRoutingList routingList))
        {
          return routingList.GetBestRoute();
        }
      }

      return null;
    }

    /// <summary>
    /// Gets the destinations in this routing table
    /// </summary>
    /// <returns>The array of destinations in this routing table</returns>
    public HydraUuid[] GetDestinations()
    {
      lock (m_table)
      {
        HydraUuid[] destinations = new HydraUuid[m_table.Count];
        m_table.Keys.CopyTo(destinations, 0);
        return destinations;
      }
    }

    /// <summary>
    /// Increments the hop count by 1 on all entries
    /// </summary>
    public void IncrementHops()
    {
      lock (m_table)
      {
        foreach (KeyValuePair<HydraUuid, HydraRoutingList> pair in m_table)
        {
          pair.Value.IncrementHops();
        }
      }
    }

    /// <summary>
    /// Adds the specified route to this table
    /// </summary>
    /// <param name="routingInfo">The route information to add</param>
    /// <returns>True if the entry was added, false if it was already present</returns>
    public bool Add(HydraRouteInfo routingInfo)
    {
      lock (m_table)
      {
        if (!m_table.ContainsKey(routingInfo.Destination))
        {
          m_table.Add(routingInfo.Destination, new HydraRoutingList());
        }

        return m_table[routingInfo.Destination].Add(routingInfo.Entry);
      }
    }

    /// <summary>
    /// Removes all routes that lead to the specified destination UUID
    /// </summary>
    /// <param name="uuid">The destination UUID to remove</param>
    /// <returns>True if the routes were removed, false if they already don't exist</returns>
    public bool RemoveDestination(HydraUuid uuid)
    {
      lock (m_table)
      {
        return m_table.Remove(uuid);
      }
    }

    /// <summary>
    /// Removes all entries that route to the specified UUID
    /// </summary>
    /// <param name="uuid">The UUID of the route to remove</param>
    /// <returns>True if any entries with the specified route were found and removed, false otherwise</returns>
    public bool RemoveRoutes(HydraUuid uuid)
    {
      bool removed = false;
      lock (m_table)
      {
        foreach (KeyValuePair<HydraUuid, HydraRoutingList> pairs in m_table)
        {
          removed |= pairs.Value.RemoveRoute(uuid);
        }
      }

      return removed;
    }

    /// <summary>
    /// Cleans a UUID from the routing table removing all entries using that UUID
    /// </summary>
    /// <param name="uuid">The UUID to remove from the routing table</param>
    public void Clean(HydraUuid uuid)
    {
      RemoveDestination(uuid);
      RemoveRoutes(uuid);
    }
  }
}
