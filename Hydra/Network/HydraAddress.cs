#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Class containing details for connecting to an application
  /// </summary>
  public class HydraAddress
  {
    private List<string> m_hosts = new List<string>();
    private int m_messagePort;
    private int m_streamPort;

    /// <summary>
    /// The main IP address that can refer to an application
    /// </summary>
    public string Host
    {
      get
      {
        return m_hosts?[0] ?? null;
      }
      
      set
      {
        if (m_hosts.Count > 0)
        {
          m_hosts[0] = value;
        }
        else
        {
          m_hosts.Add(value);
        }
      }
    }

    /// <summary>
    /// The list of hostnames this application may be on.
    /// </summary>
    public List<string> Hosts { get => m_hosts; set => m_hosts = value; }

    /// <summary>
    /// The port used to listen for UDP connections
    /// </summary>
    public int MessagePort { get => m_messagePort; set => m_messagePort = value; }

    /// <summary>
    /// The port used to listen for TCP connections
    /// </summary>
    public int StreamPort { get => m_streamPort; set => m_streamPort = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraAddress()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="HydraAddress"/> with the specified values
    /// </summary>
    /// <param name="host">The IP address as a string</param>
    /// <param name="messagePort">The port being used to listen for UDP connections</param>
    /// <param name="streamPort">The port being used to listen for TCP connections</param>
    public HydraAddress(string host, int messagePort, int streamPort)
    {
      Host = host;
      m_messagePort = messagePort;
      m_streamPort = streamPort;
    }

    /// <summary>
    /// Initializes this <see cref="HydraAddress"/> with the specified values
    /// </summary>
    /// <param name="hosts">The list of IP addresses/hostnames as strings</param>
    /// <param name="messagePort">The port being used to listen for UDP connections</param>
    /// <param name="streamPort">The port being used to listen for TCP connections</param>
    public HydraAddress(List<string> hosts, int messagePort, int streamPort)
    {
      m_hosts = hosts;
      m_messagePort = messagePort;
      m_streamPort = streamPort;
    }

    /// <summary>
    /// Initializes this <see cref="HydraAddress"/> with data copied from another instance
    /// </summary>
    /// <param name="other">The address to copy data from</param>
    public HydraAddress(HydraAddress other)
    {
      if (other != null)
      {
        if (other.Host != null)
        {
          Host = other.Host;
        }
        m_messagePort = other.MessagePort;
        m_streamPort = other.StreamPort;
      }
    }

    /// <summary>
    /// Creates an IP endpoint from the host and port specified.
    /// </summary>
    /// <param name="host">The hostname or IP.</param>
    /// <param name="port">The socket port.</param>
    /// <returns>A new valid IP endpoint for the host and port specified.</returns>
    public static IPEndPoint GetEndPoint(string host, int port)
    {
      if (IPAddress.TryParse(host, out IPAddress ip))
      {
        return new IPEndPoint(ip, port);
      }

      IPAddress[] addresses = Dns.GetHostAddresses(host);
      for (int i = 0; i < addresses.Length; i++)
      {
        if (addresses[i].AddressFamily == AddressFamily.InterNetwork)
        {
          return new IPEndPoint(addresses[i], port);
        }
      }

      return new IPEndPoint(IPAddress.Any, port);
    }

    /// <summary>
    /// Creates an IP endpoint from the host and port specified.
    /// </summary>
    /// <param name="hosts">The list of hostnames or IP addresses.</param>
    /// <param name="port">The socket port.</param>
    /// <returns>A new valid IP endpoint for the host and port specified.</returns>
    public static List<IPEndPoint> GetEndPoints(List<string> hosts, int port)
    {
      List<IPEndPoint> endPoints = new List<IPEndPoint>();
      if (hosts != null)
      {
        for (int hostIndex = 0; hostIndex < hosts.Count; ++hostIndex)
        {
          if (IPAddress.TryParse(hosts[hostIndex], out IPAddress ip))
          {
            endPoints.Add(new IPEndPoint(ip, port));
          }
          else
          {
            IPAddress[] addresses = Dns.GetHostAddresses(hosts[hostIndex]);
            for (int i = 0; i < addresses.Length; i++)
            {
              if (addresses[i].AddressFamily == AddressFamily.InterNetwork)
              {
                endPoints.Add(new IPEndPoint(addresses[i], port));
              }
            }
          }
        }
      }

      return endPoints;
    }

    /// <summary>
    /// Creates an IP endpoint for the host and UDP port in this instance
    /// </summary>
    /// <returns>A new IP endpoint for the UDP connection</returns>
    public IPEndPoint GetUdpEndPoint()
    {
      if (MessagePort != -1 && !string.IsNullOrEmpty(Host))
      {
        return GetEndPoint(Host, MessagePort);
      }
      return null;
    }

    /// <summary>
    /// Creates IP endpoints for all hosts on the UDP port for this address.
    /// </summary>
    /// <returns>An array containing the addresses for all hosts on the UDP port.</returns>
    public IPEndPoint[] GetUdpEndPoints()
    {
      if (MessagePort != -1 && Hosts != null && Hosts.Count > 0)
      {
        return GetEndPoints(Hosts, MessagePort).ToArray();
      }

      return null;
    }

    /// <summary>
    /// Creates an IP endpoint for the host and TCP port in this instance
    /// </summary>
    /// <returns>A new IP endpoint for the TCP connection or null if this endpoint has none</returns>
    public IPEndPoint GetTcpEndPoint()
    {
      if (StreamPort != -1 && !string.IsNullOrEmpty(Host))
      {
        return GetEndPoint(Host, StreamPort);
      }
      return null;
    }

    /// <summary>
    /// Creates IP endpoints for all hosts on the TCP port for this address.
    /// </summary>
    /// <returns>An array containing the addresses for all hosts on the TCP port.</returns>
    public IPEndPoint[] GetTcpEndPoints()
    {
      if (StreamPort != -1 && Hosts != null && Hosts.Count > 0)
      {
        return GetEndPoints(Hosts, StreamPort).ToArray();
      }

      return null;
    }

    /// <summary>
    /// Determines if another <see cref="HydraAddress"/> object has equal values
    /// </summary>
    /// <param name="obj">The <see cref="HydraAddress"/> object to compare to this instance</param>
    /// <returns>True if the two objects have equivalent values, false otherwise</returns>
    public override bool Equals(object obj)
    {
      HydraAddress address = obj as HydraAddress;
      if (address != null)
      {
        if (m_hosts == null)
        {
          if (address.m_hosts == null)
          {
            return (MessagePort == address.MessagePort && StreamPort == address.StreamPort);
          }
        }
        else
        {
          if (address.m_hosts != null && m_hosts.Count == address.m_hosts.Count)
          {
            for (int i = 0; i < m_hosts.Count; ++i)
            {
              if (!m_hosts[i].Equals(address.m_hosts[i]))
              {
                return false;
              }
            }

            return (MessagePort == address.MessagePort && StreamPort == address.StreamPort);
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Gets a hash code value for this instance
    /// </summary>
    /// <returns>The hash code for this instance</returns>
    public override int GetHashCode()
    {
      // Unfortunately likely to collide, can't use hosts because any match in a host returns equal
      return MessagePort + StreamPort;
    }

    /// <summary>
    /// Gets a string that represents this object
    /// </summary>
    /// <returns>The string representation of this object</returns>
    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      if (m_hosts != null)
      {
        for (int i = 0; i < m_hosts.Count; ++i)
        {
          if (i > 0)
          {
            sb.Append(",");
          }
          sb.Append(m_hosts[i] ?? "NULL");
        }
      }

      sb.AppendFormat(":{0}:{1}", m_messagePort, m_streamPort);
      return sb.ToString();
    }
  }
}
