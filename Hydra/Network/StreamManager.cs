#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL;
using Skulk.MEL.Utils;
using System;
using System.Collections.Generic;
using System.IO;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A delegate for handling events involving receved stream negotiations and replies
  /// </summary>
  /// <param name="uuid">The UUID of the application that sent the negotiation object</param>
  /// <param name="negotiation">The negotiation details</param>
  public delegate void StreamNegotiationHandler(HydraUuid uuid, StreamNegotiation negotiation);

  /// <summary>
  /// Class that manages the active streams and the responsible <see cref="HydraModule"/>
  /// </summary>
  public class StreamManager
  {
    /// <summary>
    /// The logger instance to use
    /// </summary>
    private ILogger m_log;

    /// <summary>
    /// A counter used to quickly find the next available stream ID
    /// </summary>
    private UInt32 m_idCounter;

    /// <summary>
    /// The core hydra application being managed
    /// </summary>
    private HydraCore m_hydra;

    /// <summary>
    /// The collection of reserved stream IDs and their handling module
    /// </summary>
    private TimeoutDictionary<UInt32, StreamProfile> m_activeStreams = new TimeoutDictionary<uint, StreamProfile>();

    /// <summary>
    /// A dictionary pairing destination names with their negotiation handlers
    /// </summary>
    private Dictionary<string, StreamNegotiationHandler> m_negotiationHandlers = new Dictionary<string, StreamNegotiationHandler>();

    /// <summary>
    /// Event invoked when a stream status changes
    /// </summary>
    public event EventHandler<HydraStreamStatus> OnStreamUpdated;

    /// <summary>
    /// Event invoked when a stream transfer completes
    /// </summary>
    public event EventHandler<HydraStreamStatus> OnStreamCompleted;

    /// <summary>
    /// Initializes a new <see cref="StreamManager"/> instance
    /// </summary>
    /// <param name="hydra">The hydra instance being managed</param>
    public StreamManager(HydraCore hydra)
    {
      m_hydra = hydra;
      m_log = hydra.Log;
      m_activeStreams.OnItemExpired += OnStreamExpired;
    }

    /// <summary>
    /// Periodic update that handles timing out old stream requests
    /// </summary>
    /// <param name="time">The object containing update timing information</param>
    public void Update(UpdateTime time)
    {
      lock (m_activeStreams)
      {
        m_activeStreams.Update(time);
      }
    }

    /// <summary>
    /// Registers a handler as a negotiation destination
    /// </summary>
    /// <param name="destination">The name of the negotiation destination</param>
    /// <param name="handler">The method that handles negotiations</param>
    public void RegisterNegotiationDestination(string destination, StreamNegotiationHandler handler)
    {
      lock (m_negotiationHandlers)
      {
        if (m_negotiationHandlers.ContainsKey(destination))
        {
          m_negotiationHandlers[destination] += handler;
        }
        else
        {
          m_negotiationHandlers.Add(destination, handler);
        }
      }
    }

    /// <summary>
    /// Removes the specified registered negotiation handler
    /// </summary>
    /// <param name="destination">The name of the negotiation destination</param>
    /// <param name="handler">The handler method to remove</param>
    public void DeregisterNegotiationHandler(string destination, StreamNegotiationHandler handler)
    {
      lock (m_negotiationHandlers)
      {
        if (m_negotiationHandlers.TryGetValue(destination, out StreamNegotiationHandler oldHandler))
        {
          oldHandler -= handler;
          if (oldHandler == null)
          {
            m_negotiationHandlers.Remove(destination);
          }
        }
      }
    }

    /// <summary>
    /// A method that sends an affirmative response to a received stream negotiation message
    /// </summary>
    /// <param name="negotiation">The negotiation to accept</param>
    /// <param name="uuid">The UUID of the application that send the negotiation</param>
    /// <param name="stream">The stream to associate with the transfer. This may be null if the data writing handling is implemented elsewhere</param>
    /// <param name="id">The local ID assigned to the stream transfer</param>
    /// <param name="timeout">The max time in seconds to track the stream transfer</param>
    /// <returns>True if the negotiation was accepted successfully, otherwise false</returns>
    public bool AcceptNegotiation(StreamNegotiation negotiation, HydraUuid uuid, Stream stream, out uint id, double timeout = 3600)
    {
      if (ReserveStreamId(null, out id, timeout))
      {
        negotiation.IsReply = true;
        if (m_activeStreams.TryGetValue(id, out StreamProfile profile))
        {
          // We are opposite to our received negotiation
          profile.StreamStatus.Direction = negotiation.Direction == HydraStreamDirections.WRITE ? HydraStreamDirections.READ : HydraStreamDirections.WRITE;
          profile.StreamStatus.Remote = uuid;
          profile.StreamStatus.Stream = stream;
        }

        negotiation.Success = true;

        switch (negotiation.Direction)
        {
          case HydraStreamDirections.WRITE:
          {
            negotiation.DestinationId = id;
            m_hydra.SendMessage(new Messages.HydraStreamNegotiationMsg(negotiation, true), uuid);
          }
          break;

          case HydraStreamDirections.READ:
          {
            negotiation.SourceId = id;
            m_hydra.SendMessage(new Messages.HydraStreamNegotiationMsg(negotiation, true), uuid);

            // Send the stream itself as well
            return m_hydra.SendStream(stream, uuid, id, negotiation.DestinationId, stream.Length, profile.StreamStatus.TransferMode);
          }
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Rejects a stream negotiation and sends the rejection to the remote application
    /// </summary>
    /// <param name="negotiation">The negotiation to reject</param>
    /// <param name="uuid">The UUID of the remote application</param>
    public void RejectNegotiation(StreamNegotiation negotiation, HydraUuid uuid)
    {
      negotiation.IsReply = true;
      negotiation.Success = false;
      m_hydra.SendMessage(new Messages.HydraStreamNegotiationMsg(negotiation, true), uuid);
    }

    /// <summary>
    /// Initiates a negotiation with a remote application to send/receive a stream
    /// </summary>
    /// <param name="source">The name of the source negotiating the transfer, typically this should be the type name of the module</param>
    /// <param name="uuid">The UUID of the application to negotiate with</param>
    /// <param name="direction">The transfer mode to initiate</param>
    /// <param name="id">The local ID assigned to the stream</param>
    /// <param name="destination">The name of the destination, typically the type name of the module is used</param>
    /// <param name="timeout">The max time in seconds to reserve stream IDs</param>
    /// <param name="tag">A user specified tag object that can be used to transfer further details about the stream transfer</param>
    /// <returns>True if a local ID was reserved and the negotiation message was sent</returns>
    public bool NegotiateStream(string source, string destination, HydraUuid uuid, HydraStreamDirections direction, out uint id, double timeout = 3600, object tag = null)
    {
      if (ReserveStreamId(null, out id, timeout))
      {
        StreamNegotiation negotiation = new StreamNegotiation()
        {
          Source = source,
          Destination = destination,
          Direction = direction,
          Tag = tag
        };

        switch (direction)
        {
          case HydraStreamDirections.WRITE:
          {
            negotiation.SourceId = id;
          }
          break;

          case HydraStreamDirections.READ:
          {
            negotiation.DestinationId = id;
          }
          break;
        }

        m_hydra.SendMessage(new Messages.HydraStreamNegotiationMsg(negotiation), uuid);
        return true;
      }

      return false;
    }

    /// <summary>
    /// Negotiates and internally sends a stream to a remote application
    /// </summary>
    /// <param name="source">The name of the source sending the stream</param>
    /// <param name="destination">The name of the destination</param>
    /// <param name="stream">The stream to send</param>
    /// <param name="uuid">The UUID of the application to send the stream to</param>
    /// <param name="localId">The locally reserved ID for the stream</param>
    /// <param name="transferMode">The transfer mode to use with the stream</param>
    /// <param name="timeout">The max time in seconds to track the stream</param>
    /// <param name="tag">User defined tag object to send with the negotiation</param>
    /// <returns>True if the stream was successfully initialized for a negotiation, otherwise false</returns>
    public bool SendStream(string source, string destination, Stream stream, HydraUuid uuid, out uint localId, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS, double timeout = 3600, object tag = null)
    {
      if (NegotiateStream(source, destination, uuid, HydraStreamDirections.WRITE, out localId, timeout, tag))
      {
        if (m_activeStreams.TryGetValue(localId, out StreamProfile streamProfile))
        {
          streamProfile.StreamStatus.Direction = HydraStreamDirections.WRITE;
          streamProfile.StreamStatus.Length = stream.Length;
          streamProfile.StreamStatus.Remote = uuid;
          streamProfile.StreamStatus.Stream = stream;
          streamProfile.StreamStatus.TransferMode = transferMode;
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Negotiates and internally receives a stream from a remote application
    /// </summary>
    /// <param name="source">The name of the source the stream is being requested from</param>
    /// <param name="destination">The name of the stream destination</param>
    /// <param name="stream">The stream that will store received data</param>
    /// <param name="uuid">The UUID of the remote application to request the stream from</param>
    /// <param name="localId">The locally reserved ID for the stream</param>
    /// <param name="transferMode">The transfer mode to use with the stream</param>
    /// <param name="timeout">The max time in seconds to track the stream</param>
    /// <param name="tag">User defined tag object to send with the negotiation, typically this is used to specify a specific stream to request from the destination</param>
    /// <returns>True if the stream was successfully initialized for a negotiation, otherwise false</returns>
    public bool ReceiveStream(string source, string destination, Stream stream, HydraUuid uuid, out uint localId, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS, double timeout = 3600, object tag = null)
    {
      if (NegotiateStream(source, destination, uuid, HydraStreamDirections.READ, out localId, timeout, tag))
      {
        if (m_activeStreams.TryGetValue(localId, out StreamProfile streamProfile))
        {
          streamProfile.StreamStatus.Direction = HydraStreamDirections.READ;
          streamProfile.StreamStatus.Remote = uuid;
          streamProfile.StreamStatus.Stream = stream;
          streamProfile.StreamStatus.TransferMode = transferMode;
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Attempts to reserve a unique ID for a stream transfer to send to a HydraModule
    /// </summary>
    /// <param name="module">The module requesting the stream transfer</param>
    /// <param name="id">The reserved stream ID</param>
    /// <param name="timeout">The max time in seconds to reserve the stream ID</param>
    /// <returns>True if a stream ID was successfully reserved, false otherwise</returns>
    public bool ReserveStreamId(HydraModule module, out UInt32 id, double timeout = 3600)
    {
      lock (m_activeStreams)
      {
        for (UInt32 i = 0; m_activeStreams.ContainsKey(++m_idCounter) && i < UInt32.MaxValue; ++i)
        {
          // Empty
        }

        id = m_idCounter;
        return m_activeStreams.TryAdd(m_idCounter, new StreamProfile(module, id), timeout);
      }
    }

    /// <summary>
    /// Releases a reserved stream ID after the stream finishes
    /// </summary>
    /// <param name="id">The stream ID to release</param>
    public void ReleaseStreamId(UInt32 id)
    {
      lock (m_activeStreams)
      {
        m_activeStreams.Remove(id);
      }
    }

    /// <summary>
    /// Handles the received stream data by routing it to the associated module
    /// </summary>
    /// <param name="streamId">The stream ID</param>
    /// <param name="buffer">The raw stream data</param>
    /// <param name="offset">The index of the first byte in the buffer that contains stream data</param>
    /// <param name="uuid">The UUID of the application sending the stream</param>
    /// <param name="count">The number of bytes actually set in the buffer or -1 to use the full length of the buffer</param>
    public void HandleStream(UInt32 streamId, byte[] buffer, int offset, HydraUuid uuid, int count = -1)
    {
      if (m_activeStreams.TryGetValue(streamId, out StreamProfile streamProfile))
      {
        int length = count == -1 ? buffer.Length - offset : count;
        streamProfile.StreamStatus.Count += length;

        if (streamProfile.Module != null)
        {
          streamProfile.Module.HandleStream(streamId, buffer, offset, uuid, count);
        }
        else
        {
          if (streamProfile.StreamStatus.Direction == HydraStreamDirections.READ && streamProfile.StreamStatus.Stream != null)
          {
            streamProfile.StreamStatus.Stream.Write(buffer, offset, length);
          }

          OnStreamUpdated?.Invoke(this, streamProfile.StreamStatus);
        }
      }
    }

    /// <summary>
    /// Handles the received stream data by routing it to the associated module
    /// </summary>
    /// <param name="streamId">The stream ID</param>
    /// <param name="buffer">The raw stream data</param>
    /// <param name="offset">The index of the first byte in the buffer that contains stream data</param>
    /// <param name="index">The index of the received stream segment</param>
    /// <param name="uuid">The UUID of the application sending the stream</param>
    public void HandleStream(UInt32 streamId, byte[] buffer, int offset, UInt32 index, HydraUuid uuid)
    {
      if (m_activeStreams.TryGetValue(streamId, out StreamProfile streamProfile))
      {
        if (index != streamProfile.Index)
        {
          m_log?.LogError("Received segment with index: {0} instead of expected index: {1}", index, streamProfile.Index);
        }
        streamProfile.Index = index + 1;
        int length = buffer.Length - offset;
        streamProfile.StreamStatus.Count += length;

        if (streamProfile.Module != null)
        {
          streamProfile.Module.HandleStream(streamId, buffer, offset, uuid);
        }
        else
        {
          if (streamProfile.StreamStatus.Direction == HydraStreamDirections.READ && streamProfile.StreamStatus.Stream != null)
          {
            streamProfile.StreamStatus.Stream.Write(buffer, offset, length);
          }

          OnStreamUpdated?.Invoke(this, streamProfile.StreamStatus);
        }
      }
      else
      {
        m_log?.LogError("Received stream segment for unknown stream with ID: {0} and index: {1}", streamId, index);
      }
    }

    /// <summary>
    /// Ends the stream with the specified ID
    /// </summary>
    /// <param name="streamInfo">The info describing the stream to end</param>
    /// <param name="uuid">The UUID of the application sending the stream</param>
    public void EndStream(HydraStreamInfo streamInfo, HydraUuid uuid)
    {
      if (m_activeStreams.TryGetValue(streamInfo.Id, out StreamProfile streamProfile))
      {
        if (streamProfile.Module != null)
        {
          streamProfile.Module.EndStream(streamInfo, uuid);
        }
        else
        {
          OnStreamCompleted?.Invoke(this, streamProfile.StreamStatus);
        }
      }

      ReleaseStreamId(streamInfo.Id);
    }

    /// <summary>
    /// Handles received messages specific to the <see cref="StreamManager"/>
    /// </summary>
    /// <param name="message">The received message instance</param>
    /// <param name="uuid">The UUID of the application that sent the message</param>
    public void HandleMessage(Message message, HydraUuid uuid)
    {
      switch (message.GetCmd())
      {
        case HydraCodes.HydraStreamNegotiation:
        {
          Messages.HydraStreamNegotiationMsg msg = new Messages.HydraStreamNegotiationMsg(message);
          StreamNegotiation negotiation = msg.GetObject<StreamNegotiation>();
          if (negotiation != null)
          {
            switch (msg.GetObjectId())
            {
              case Messages.HydraStreamNegotiationMsg.NEGOTIATION_ID:
              {
                string key = negotiation.Direction == HydraStreamDirections.WRITE ? negotiation.Destination : negotiation.Source;
                if (m_negotiationHandlers.TryGetValue(key, out StreamNegotiationHandler handler))
                {
                  // Invoke the event, A relevant handler should Accept or Reject the negotiation
                  try
                  {
                    handler?.Invoke(uuid, negotiation);
                  }
                  catch (Exception ex)
                  {
                    m_hydra.Log.LogError("Exception in {0} negotiation handler: '{1}'", negotiation.Destination, ex.Message);
                  }
                }
                else
                {
                  // Default to a rejection for unknown destinations
                  RejectNegotiation(negotiation, uuid);
                }
                break;
              }

              case Messages.HydraStreamNegotiationMsg.REPLY_ID:
              {
                // Find the source module
                if (negotiation.Success)
                {
                  // The negotiation was accepted, start the transfer process
                  switch (negotiation.Direction)
                  {
                    case HydraStreamDirections.WRITE:
                    {
                      if (m_activeStreams.TryGetValue(negotiation.SourceId, out StreamProfile streamProfile))
                      {
                        if (streamProfile.Module == null && streamProfile.StreamStatus.Stream != null)
                        {
                          // This transfer is managed by this class since there is no module set
                          m_hydra.ProfileManager.SendStream(
                            streamProfile.StreamStatus.Stream,
                            streamProfile.StreamStatus.Remote,
                            streamProfile.StreamStatus.LocalId,
                            negotiation.DestinationId,
                            streamProfile.StreamStatus.Stream.Length,
                            streamProfile.StreamStatus.TransferMode);
                        }
                      }
                      break;
                    }

                    case HydraStreamDirections.READ:
                    {
                      // Not much to do here either everything is set up (by ReceiveStream) or it isn't and we're dropping stream bytes
                      break;
                    }
                  }
                }
                else
                {
                  uint localId = negotiation.Direction == HydraStreamDirections.WRITE ? negotiation.SourceId : negotiation.DestinationId;
                  m_hydra.Log.LogInfo("Failed to negotiate stream with {0}, releasing ID: {1}", uuid, localId);
                  // The negotiation failed, release the stream ID
                  ReleaseStreamId(localId);
                }

                // Call back to the source to report the status
                string key = negotiation.Direction == HydraStreamDirections.WRITE ? negotiation.Source : negotiation.Destination;
                if (m_negotiationHandlers.TryGetValue(key, out StreamNegotiationHandler handler))
                {
                  handler?.Invoke(uuid, negotiation);
                }
                break;
              }
            }
          }
          break;
        }
      }
    }

    /// <summary>
    /// Method invoked when a stream expires from the manager dictionary
    /// </summary>
    /// <param name="streamProfile">The profile information describing the expired stream</param>
    private void OnStreamExpired(StreamProfile streamProfile)
    {
      m_log?.LogError("Stream {0} expired, ending now...", streamProfile.StreamStatus?.LocalId.ToString() ?? "-");
      OnStreamCompleted?.Invoke(this, streamProfile.StreamStatus);
      ReleaseStreamId(streamProfile.StreamStatus.LocalId);
    }
  }
}
