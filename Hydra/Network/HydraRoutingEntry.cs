#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Contains information describing an entry in a routing table
  /// </summary>
  public class HydraRoutingEntry : IComparable<HydraRoutingEntry>
  {
    private HydraUuid m_route;
    private int m_hops;

    /// <summary>
    /// The UUID of the profile/application that can route to the destination
    /// </summary>
    public HydraUuid Route { get => m_route; set => m_route = value; }

    /// <summary>
    /// The number of hops it takes to reach the destination by going through this route
    /// </summary>
    public int Hops { get => m_hops; set => m_hops = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraRoutingEntry()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this routing entry with the specified values
    /// </summary>
    /// <param name="route">The route UUID</param>
    /// <param name="hops">The number of hops it takes to reach the destination</param>
    public HydraRoutingEntry(HydraUuid route, int hops)
    {
      m_route = route;
      m_hops = hops;
    }

    /// <summary>
    /// Initializes this routing entry with copies of the data from another entry
    /// </summary>
    /// <param name="other">The entry containing the data to copy</param>
    public HydraRoutingEntry(HydraRoutingEntry other)
    {
      m_hops = other.m_hops;
      m_route = new HydraUuid(other.m_route);
    }

    /// <summary>
    /// Compares this routing entry to another by the number of hops
    /// </summary>
    /// <param name="other">The routing entry to compare against this entry</param>
    /// <returns>0 when equal, 1 if this entry has greater hops and -1 if this entry has fewer hops</returns>
    public int CompareTo(HydraRoutingEntry other)
    {
      if (other == null)
      {
        return 1;
      }

      return m_hops.CompareTo(other.m_hops);
    }

    /// <summary>
    /// Determines if another object is a <see cref="HydraRoutingEntry"/> and equivalent
    /// </summary>
    /// <param name="obj">The object to compare</param>
    /// <returns>True if the other object is equivalent, false otherwise</returns>
    public override bool Equals(object obj)
    {
      HydraRoutingEntry entry = obj as HydraRoutingEntry;
      if (entry != null)
      {
        return m_route.Equals(entry.m_route) && m_hops == entry.m_hops;
      }

      return false;
    }

    /// <summary>
    /// Gets a hash code for this entry instance
    /// </summary>
    /// <returns>The hash code for this entry</returns>
    public override int GetHashCode()
    {
      return m_route.GetHashCode() ^ m_hops;
    }
  }
}
