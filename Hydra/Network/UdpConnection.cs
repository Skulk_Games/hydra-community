#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.Hydralize.Encoding;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// A connection that interracts with a UDP socket
  /// </summary>
  public class UdpConnection : Connection
  {
    private IPEndPoint m_clientEp;
    private UdpClient m_client;
    private byte[] m_tmpStreamBuffer = new byte[Message.MAX_PAYLOAD_SIZE];
    private byte[] m_receiveBuffer = new byte[Message.MAX_MESSAGE_SIZE];

    /// <summary>
    /// Flag indicating if the client end point has been initialized
    /// </summary>
    private bool m_endPointInitialized = false;

    /// <summary>
    /// The time in seconds to wait for the client endpoint to be set before closing the connection
    /// </summary>
    private double m_endPointTimeout = 10;

    /// <summary>
    /// The <see cref="PeriodicUpdateGenerator"/> instance used to monitor the client endpoint status
    /// </summary>
    private PeriodicUpdateGenerator m_endPointUpdate;

    /// <summary>
    /// The reset event used to signal when messages are ready to be sent
    /// </summary>
    private ManualResetEvent m_sendEvent = new ManualResetEvent(true);

    /// <summary>
    /// The task used to run the receiving handling.
    /// </summary>
    private Task m_receiveTask;

    /// <summary>
    /// The port this UDP connection is listening on
    /// </summary>
    public virtual int ListenPort { get => (((IPEndPoint)m_client?.Client?.LocalEndPoint)?.Port ?? 0); }

    /// <summary>
    /// Gets the value indicating this connection can not stream
    /// </summary>
    public override bool CanStream => false;

    /// <summary>
    /// Indicates whether data can be written to this connection.
    /// </summary>
    public override bool CanWrite => true;

    /// <summary>
    /// Indicates whether data can be read from this connection.
    /// </summary>
    public override bool CanRead => true;

    /// <summary>
    /// The endpoint of the client
    /// </summary>
    public IPEndPoint ClientEp { get => m_clientEp; set => SetClientEp(value); }

    /// <summary>
    /// The time in seconds to wait for a client endpoint before timing out and closing this connection
    /// </summary>
    public double EndPointTimeout { get => m_endPointTimeout; set => m_endPointTimeout = value; }

    /// <summary>
    /// Minimal constructor for derived classes.
    /// </summary>
    /// <param name="id">The ID of this connection on the parent profile</param>
    /// <param name="messagePriority">The priority value for sending messages</param>
    /// <param name="streamPriority">The priority value for sending streams</param>
    /// <param name="keepalive">The period in seconds between sending keepalive messages</param>
    protected UdpConnection(uint id, int messagePriority = 1, int streamPriority = -1, double keepalive = 10) : base(ConnectionTypes.MESSAGE, id, messagePriority, streamPriority, null, keepalive)
    {
      // Empty
    }

    /// <summary>
    /// Initializes this UDP connection with the specified values
    /// </summary>
    /// <param name="bindAddress">The IP to bind this connection to</param>
    /// <param name="endPoint">The end point of the remote client</param>
    /// <param name="id">The ID of this connection on the parent profile</param>
    /// <param name="port">The local UDP port to use when constructing this connection</param>
    /// <param name="messagePriority">The priority value to use for this connection when sending messages</param>
    /// <param name="streamPriority">The priority value to use for this connection when streaming</param>
    /// <param name="encoder">The encoder to use on this connection, if null no encoder will be used</param>
    /// <param name="keepAlive">The period in seconds between sending keep alive messages</param>
    /// <param name="maxRateBps">The max rate in bits per second to send data over this connection, the default is unlimited</param>
    public UdpConnection(string bindAddress, IPEndPoint endPoint, UInt32 id, ushort port = 0, int messagePriority = 1, int streamPriority = -1, TranscoderPair encoder = null, double keepAlive = 10, double maxRateBps = -1)
      : base(ConnectionTypes.MESSAGE, id, messagePriority, streamPriority, encoder, keepAlive, maxRateBps)
    {
      m_clientEp = endPoint;

      try
      {
        IPEndPoint ep = new IPEndPoint(IPAddress.Parse(bindAddress), port);
        m_client = new UdpClient(ep);
        m_ended = !m_client.Client.Connected;
      }
      catch (Exception ex)
      {
        Profile?.Manager?.Log?.LogError("Error constructing UDP connection: {0}", ex.Message);
        m_ended = true;
      }
    }

    /// <inheritdoc/>
    public override void Stop()
    {
      base.Stop();
      m_sendEvent.Set();
    }

    /// <inheritdoc/>
    public override void Join()
    {
      m_sendEvent.Set();
      base.Join();
    }

    /// <inheritdoc />
    public override bool Send(Message message)
    {
      ResetInactiveTime();

      // If message is null the connection could still be valid, but do nothing
      if (message != null)
      {
        lock (m_sendMessageQueue)
        {
          message.SetId(this.m_sendId++);
          m_sendMessageQueue.Enqueue(message);
          m_sendEvent.Set();
        }
      }

      return !m_ended;
    }

    /// <summary>
    /// Writes a message directly to the UDP socket
    /// </summary>
    /// <param name="message">The message to write</param>
    /// <returns>True if the message was sent, false if an error occurred</returns>
    public override bool Write(Message message)
    {
      try
      {
        lock (ClientLock)
        {
          message.SetId(this.m_sendId++);
          byte[] raw = message.Serialize();
          raw = Encode(raw);
          m_client?.Send(raw, raw.Length, m_clientEp);
          return !m_ended;
        }
      }
      catch
      {
        return false;
      }
    }

    /// <summary>
    /// Closes the UDP socket on this connection
    /// </summary>
    public override void Close(bool failure = false)
    {
      base.Close(failure);
      lock (ClientLock)
      {
        m_client?.Close();
        m_client = null;
      }

      m_sendEvent.Set();
    }

    /// <summary>
    /// The run loop logic for this UDP connection
    /// </summary>
    protected override void Run()
    {
      m_ended = false;
      m_closed = false;

      // Start an update thread that will stop this connection if the client EP is not set
      m_endPointUpdate = new PeriodicUpdateGenerator(1);
      m_endPointUpdate.OnUpdate += UpdateStatus;
      m_endPointUpdate.Start(true);

      if (m_clientEp != null)
      {
        // Trigger first initialization and connection added event
        SetClientEp(m_clientEp);
      }

      // Start receiving
      bool receiving = false;
      if (m_client != null)
      {
        receiving = this.StartReceiving();
      }

      try
      {
        while (!StopRun)
        {
          // Send
          // Check for stream data
          SendStreams();

          // Check for pending messages
          Message message = null;
          if (m_clientEp != null)
          {
            if (!receiving)
            {
              receiving = this.StartReceiving();
            }

            lock (m_sendMessageQueue)
            {
              if (m_sendMessageQueue.Count > 0)
              {
                lock (ClientLock)
                {
                  while (m_sendMessageQueue.Count > 0)
                  {
                    ResetInactiveTime();
                    message = m_sendMessageQueue.Dequeue();
                    Profile.Manager.Log?.LogTrace("Sending message: {0} to: {1}", MsgNames.Get(message.GetCmd()), Profile.Uuid);
                    byte[][] bufferSequence = message.SerializeSequence();
                    for (int i = 0; i < bufferSequence.Length; ++i)
                    {
                      bufferSequence[i] = Encode(bufferSequence[i]);
                      m_bandwidthManager.AddBytes((ulong)bufferSequence[i].Length);
                      m_client.Send(bufferSequence[i], bufferSequence[i].Length, m_clientEp);
                      if (m_bandwidthManager.SleepTime > KeepAlivePeriod)
                      {
                        // Ensure we never sleep longer than the keepalive period
                        m_bandwidthManager.SleepTime = KeepAlivePeriod;
                      }

                      m_bandwidthManager.Sleep();
                    }
                  }
                }
              }

              // We've cleared the send queue so reset the signal
              m_sendEvent.Reset();
            }
          }
          else
          {
            m_sendEvent.Reset();
            Profile.Manager.Log?.LogDebug("Client endpoint has not been set, skipping processing of the send queue");
          }

          m_sendEvent.WaitOne();
        }

        // Lock so that once a stop & run pair starts we don't beat it to the close
        lock (ClientLock)
        {
          Close(false);
        }
      }
      catch (Exception ex)
      {
        if (!StopRun)
        {
          Profile.Manager.Log?.LogError("Exception in {0}: {1} profile:{2}, stopping...", GetType().Name, Id, Profile?.Uuid.ToString() ?? "null");
          Profile.Manager.Log?.LogError("Exception: {0}", ex.Message);
          Profile.Manager.Log?.LogDebug(ex.ToString());
        }

        // If the stop flag is already set then this shouldn't be counted as a failure since it was likely
        // intentionally triggered
        lock (ClientLock)
        {
          bool failed = !StopRun;
          Stop();
          Close(failed);
        }
      }

      m_ended = true;
      m_endPointUpdate?.Stop();
      m_endPointUpdate?.Join();

      Profile?.Manager.Log.LogDebug("UDP connection ended with {0} messages in the queue", m_sendMessageQueue.Count);

      // Trigger removal
      Profile?.RemoveConnection(Id);
    }

    /// <inheritdoc/>
    public override ConnectionDiagnostics GetDiagnostics()
    {
      ConnectionDiagnostics summary = base.GetDiagnostics();
      summary.LocalPort = ListenPort;
      return summary;
    }

    /// <summary>
    /// Handles sending any messages for active stream connections.
    /// </summary>
    /// <returns>True if a stream message was sent, otherwise false.</returns>
    protected virtual bool SendStreams()
    {
      bool messageSent = false;
      lock (StreamBuffer)
      {
        Queue<ConnectionStreamInfo> finishedStreams = new Queue<ConnectionStreamInfo>();
        for (int i = 0; i < StreamBuffer.Count; ++i)
        {
          if (StreamBuffer[i].TransferCount == 0)
          {
            Send(new StartStreamMsg(StreamBuffer[i].RemoteStreamInfo));
            messageSent = true;
          }

          long left = StreamBuffer[i].RemoteStreamInfo.Length - StreamBuffer[i].TransferCount;
          if (left > 0)
          {
            int count = StreamBuffer[i].Stream.Read(m_tmpStreamBuffer, 0, (int)Math.Min(m_tmpStreamBuffer.Length, left));
            if (count > 0)
            {
              byte[] data = m_tmpStreamBuffer;
              if (count < m_tmpStreamBuffer.Length)
              {
                data = new byte[count];
                Array.Copy(m_tmpStreamBuffer, data, count);
              }

              HydraStreamSegment segment = new HydraStreamSegment()
              {
                Id = StreamBuffer[i].RemoteStreamInfo.Id,
                Data = data,
                Index = StreamBuffer[i].Index++
              };

              messageSent = true;
              Send(new StreamSegmentMsg(segment));
              StreamBuffer[i].TransferCount += count;
            }
          }
          else
          {
            // Finish stream
            messageSent = true;
            Send(new EndStreamMsg(StreamBuffer[i].RemoteStreamInfo));
            finishedStreams.Enqueue(StreamBuffer[i]);
            Profile.EndStream(StreamBuffer[i].LocalStreamInfo);
          }
        }

        while (finishedStreams.Count > 0)
        {
          StreamBuffer.Remove(finishedStreams.Dequeue());
        }
      }

      return messageSent;
    }

    /// <summary>
    /// A periodic update callback used to monitor the realtime state of the connection.
    /// </summary>
    /// <param name="updateTime">The object holding timing information for the connection thread.</param>
    private void UpdateStatus(UpdateTime updateTime)
    {
      if (m_clientEp == null)
      {
        if (updateTime.TotalSeconds >= m_endPointTimeout)
        {
          Profile.Manager.Log?.LogError("UDP connection failed to set the client endpoint before the timeout. Stopping this connection.");
          Stop();
        }
      }
      else
      {
        // EndPoint set so stop monitoring
        m_endPointUpdate.Stop();
      }
    }

    private void SetClientEp(IPEndPoint endPoint)
    {
      m_clientEp = endPoint;
      Profile?.Manager?.Log?.LogDebug("Client endpoint set to: {0}", endPoint);
      if (Profile.Address.Host == null || !Profile.Address.Host.Equals(endPoint.Address.ToString()))
      {
        Profile.Address.Host = endPoint.Address.ToString();
      }

      if (!m_endPointInitialized)
      {
        Profile?.Manager?.Log?.LogDebug("First client endpoint initialization, calling OnConnectionAdded");
        m_endPointInitialized = true;
        Profile?.TriggerAdd(this);
      }
    }

    /// <summary>
    /// Starts a receiving task.
    /// </summary>
    /// <returns>True if the receiving task was started, otherwise false.</returns>
    private bool StartReceiving()
    {
      if (m_receiveTask == null || m_receiveTask.IsCompleted)
      {
        m_receiveTask = Task.Run(ReceiveLoop);
        return true;
      }

      return false;
    }

    /// <summary>
    /// A method that runs an async receiving loop on the socket. This continously receives data until the stop flag is set or an exception is caught.
    /// </summary>
    private void ReceiveLoop()
    {
      try
      {
        UdpReceiveResult result;
        while (!StopRun)
        {
          // Start receiving again
          if (!StopRun)
          {
            Task<UdpReceiveResult> task = m_client.ReceiveAsync();
            task.Wait();
            result = task.Result;
          }
          else
          {
            return;
          }

          IPEndPoint source = result.RemoteEndPoint;
          byte[] buffer = result.Buffer;
          if (m_client == null)
          {
            // We've closed
            return;
          }

          if (Encoder != null)
          {
            buffer = Encoder.Decode(buffer);
          }

          List<Message> messages = Message.Deserialize(buffer);
          if (source.Equals(m_clientEp))
          {
            for (int i = 0; i < messages.Count; ++i)
            {
              Profile.Manager.Log.LogTrace("Received message: {0}", MsgNames.Get(messages[i].GetCmd()));
              this.ReceiveMessage(messages[i]);
            }
          }
          else
          {
            for (int i = 0; i < messages.Count; ++i)
            {
              switch (messages[i].GetCmd())
              {
                case HydraCodes.HydraSetUdpSource:
                {
                  SetUdpSourceMsg setSourceMsg = new SetUdpSourceMsg(messages[i]);
                  ProfileInfo profileInfo = setSourceMsg.GetObject<ProfileInfo>();
                  if (profileInfo != null && profileInfo.Uuid.Equals(Profile.Uuid) && profileInfo.Key == Profile.Key)
                  {
                    SetClientEp(source);
                  }
                  else
                  {
                    Profile.Manager.Log?.LogError("Set UDP source message for UUID: {0} with key: {1} did not match profile information with UUID: {2} and key: {3}", profileInfo.Uuid, profileInfo.Key, Profile.Uuid, Profile.Key);
                  }
                }
                break;

                default:
                {
                  // Check again because client EP may have changed
                  if (source.Equals(m_clientEp))
                  {
                    this.ReceiveMessage(messages[i]);
                  }
                  else
                  {
                    // Message attempting to impersonate client, dropping
                  }
                }
                break;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (!StopRun)
        {
          Profile.Manager.Log?.LogError("Exception in UDP connection receiving: {0} on profile:{1}, stopping...", Id, Profile?.Uuid.ToString() ?? "null");
          Profile.Manager.Log?.LogError("Exception: {0}", ex.Message);
          Profile.Manager.Log?.LogDebug(ex.ToString());
        }

        // If the stop flag is already set then the exception is likely intentional and not a failure
        lock (ClientLock)
        {
          bool failed = !StopRun;
          Stop();
          Close(failed);
        }
        return;
      }
    }
  }
}
