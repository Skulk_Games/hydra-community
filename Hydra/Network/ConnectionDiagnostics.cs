#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydra.Network
{
  /// <summary>
  /// Contains diagnostic information for a connection
  /// </summary>
  public class ConnectionDiagnostics
  {
    /// <summary>
    /// Indicates whether this connection has ended
    /// </summary>
    private bool m_ended;
    private int m_sendQueueCount;
    private double m_keepalivePeriod;
    private double m_inactiveTime;
    private uint m_id;
    private ConnectionTypes m_connectionType;
    private int m_messagePriority;
    private int m_streamPriority;
    private string m_encoder;
    private string m_decoder;
    private int m_streamCount;
    private double m_streamTimeout;
    private double m_latency;
    private int m_localPort;

    /// <summary>
    /// Indicates whether the connection thread has ended
    /// </summary>
    public bool Ended { get => m_ended; set => m_ended = value; }

    /// <summary>
    /// The number of messages in the send queue
    /// </summary>
    public int SendQueueCount { get => m_sendQueueCount; set => m_sendQueueCount = value; }

    /// <summary>
    /// The configured keepalive period for the connection
    /// </summary>
    public double KeepalivePeriod { get => m_keepalivePeriod; set => m_keepalivePeriod = value; }

    /// <summary>
    /// The time the connection has been inactive
    /// </summary>
    public double InactiveTime { get => m_inactiveTime; set => m_inactiveTime = value; }

    /// <summary>
    /// The ID of the connection
    /// </summary>
    public uint Id { get => m_id; set => m_id = value; }

    /// <summary>
    /// The type of the connection
    /// </summary>
    public ConnectionTypes ConnectionType { get => m_connectionType; set => m_connectionType = value; }

    /// <summary>
    /// The message priority of the connection
    /// </summary>
    public int MessagePriority { get => m_messagePriority; set => m_messagePriority = value; }

    /// <summary>
    /// The stream priority of the connection
    /// </summary>
    public int StreamPriority { get => m_streamPriority; set => m_streamPriority = value; }

    /// <summary>
    /// The encoder in use on the connection
    /// </summary>
    public string Encoder { get => m_encoder; set => m_encoder = value; }

    /// <summary>
    /// The decoder in use on the connection
    /// </summary>
    public string Decoder { get => m_decoder; set => m_decoder = value; }

    /// <summary>
    /// The number of streams on the connection
    /// </summary>
    public int StreamCount { get => m_streamCount; set => m_streamCount = value; }

    /// <summary>
    /// The stream timeout setting on the connection
    /// </summary>
    public double StreamTimeout { get => m_streamTimeout; set => m_streamTimeout = value; }

    /// <summary>
    /// The latency of the connection
    /// </summary>
    public double Latency { get => m_latency; set => m_latency = value; }

    /// <summary>
    /// The local port used by the connection
    /// </summary>
    public int LocalPort { get => m_localPort; set => m_localPort = value; }

    /// <summary>
    /// Gets the string representation of this object
    /// </summary>
    /// <returns>The string representation of the values in this object</returns>
    public override string ToString()
    {
      return String.Format("Connection\nID: {0}\nEnded: {1}\nSend Queue Count: {2}\nKeepalivePeriod: {4}\nInactiveTime: {5}\n" +
        "Type: {6}\nMessage Priority: {7}\nStream Priority: {8}\nEncoder: {9}\nDecoder: {10}\n" +
        "StreamCount: {11}\nStreamTimeout: {12}\nLatency: {13}\nLocalPort: {14}\n",
        Id, Ended, SendQueueCount, KeepalivePeriod, InactiveTime, ConnectionType, MessagePriority, StreamPriority,
        Encoder, Decoder, StreamCount, StreamTimeout, Latency, LocalPort);
    }
  }
}
