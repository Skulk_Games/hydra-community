#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.MapRegistration.Data;
using Skulk.Hydra.Messages;
using Skulk.MEL;
using System;
using System.Collections.Generic;

namespace Skulk.Hydra
{
  /// <summary>
  /// Abstract class for a message handling module in the Hydra framework
  /// </summary>
  abstract public class HydraModule : Module, IMessageModule
  {
    private List<HydraMessageHandler> m_messageHandlers;
    private HydraCore m_hydra;

    /// <summary>
    /// The message IDs that this module handles
    /// </summary>
    public List<HydraMessageHandler> MessageHandlers { get => m_messageHandlers; set => m_messageHandlers = value; }

    /// <summary>
    /// The parent Hydra application holding this module
    /// </summary>
    new public HydraCore Parent { get => m_hydra; set => m_hydra = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraModule"/> with the specified parent application
    /// </summary>
    /// <param name="hydra">The parent Hydra application that holds this module</param>
    public HydraModule(HydraCore hydra)
      : base(hydra)
    {
      m_hydra = hydra;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraModule"/> with the specified values
    /// </summary>
    /// <param name="hydra">The parent Hydra application that holds this module</param>
    /// <param name="messages">The message IDs that this module handles</param>
    public HydraModule(HydraCore hydra, params UInt16[] messages)
      : this(hydra)
    {
      if (messages != null)
      {
        m_messageHandlers = new List<HydraMessageHandler>(messages.Length);
        for (int i = 0; i < messages.Length; ++i)
        {
          m_messageHandlers.Add(new HydraMessageHandler(messages[i], HandleMessage));
        }
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HydraModule"/> with the specified values
    /// </summary>
    /// <param name="hydra">The parent Hydra application that holds this module</param>
    /// <param name="messages">The message IDs and names that this module handles</param>
    public HydraModule(HydraCore hydra, params Tuple<ushort,string>[] messages)
      : this(hydra)
    {
      if (messages != null)
      {
        m_messageHandlers = new List<HydraMessageHandler>(messages.Length);
        for (int i = 0; i < messages.Length; ++i)
        {
          m_messageHandlers.Add(new HydraMessageHandler(messages[i].Item1, HandleMessage, messages[i].Item2));
        }
      }
    }

    /// <summary>
    /// Handles received messages
    /// </summary>
    /// <param name="msg">The message received</param>
    /// <param name="uuid">The UUID of the client that sent the message</param>
    public virtual void HandleMessage(Message msg, HydraUuid uuid)
    {
      Parent.Log?.LogWarning("{0}: received unhandled message: {1} from uuid: {2}", GetType().Name, MsgNames.Get(msg.GetCmd()), uuid);
    }

    /// <summary>
    /// Handles received stream data
    /// </summary>
    /// <param name="streamId">The ID of the stream receiving the data</param>
    /// <param name="buffer">The buffer of raw bytes received on the stream</param>
    /// <param name="offset">The index of the first byte in the buffer that contains stream data</param>
    /// <param name="uuid">The UUID of the application sending the stream</param>
    /// <param name="count">The number of bytes actually set in the buffer</param>
    public virtual void HandleStream(UInt32 streamId, byte[] buffer, int offset, HydraUuid uuid, int count = -1)
    {
      // Empty
    }

    /// <summary>
    /// Method called when a stream handled by this module ends
    /// </summary>
    /// <param name="streamInfo">The info describing the stream that ended</param>
    /// <param name="uuid">The UUID of the application that sent the stream</param>
    public virtual void EndStream(HydraStreamInfo streamInfo, HydraUuid uuid)
    {
      // Empty
    }

    /// <summary>
    /// Performs the registration of objects used by the module on a <see cref="MapRegistrationModule"/> if present
    /// </summary>
    public virtual void RegisterHydraObjects()
    {
      List<MapDefinition> maps = GetHydraObjects();
      if (maps != null && maps.Count > 0)
      {
        MapRegistrationModule mapModule = Parent.GetModule<MapRegistrationModule>();
        if (mapModule != null)
        {
          for (int i = 0; i < maps.Count; ++i)
          {
            mapModule.Add(Parent.Uuid, maps[i]);
          }
        }
      }
    }

    /// <summary>
    /// Method called to get the Hydralizeable objects this module uses to be registered on
    /// a <see cref="MapRegistrationModule"/>, if one is present on the parent application.
    /// Any objects registered can generically be understood by any Hydra application.
    /// </summary>
    protected virtual List<MapDefinition> GetHydraObjects()
    {
      return null;
    }

    /// <summary>
    /// Specifies the message handler for a specific command code
    /// </summary>
    /// <param name="cmdCode">The code of the message the handler will receive</param>
    /// <param name="handler">The method that will handle the received messages</param>
    /// <param name="messageName">The name of the message associated with the command code</param>
    protected void SetMessageHandler(UInt16 cmdCode, MessageClientCallback handler, string messageName = null)
    {
      HydraMessageHandler hydraMsgHandler = m_messageHandlers?.Find(x => x.CmdCode == cmdCode);
      if (hydraMsgHandler != null)
      {
        hydraMsgHandler.Handler = handler;
      }
      else
      {
        if (m_messageHandlers == null)
        {
          m_messageHandlers = new List<HydraMessageHandler>();
        }

        m_messageHandlers.Add(new HydraMessageHandler(cmdCode, handler, messageName));
      }
    }
  }
}
