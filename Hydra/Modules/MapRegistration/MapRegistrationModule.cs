#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;
using Skulk.Hydra.MapRegistration.Data;
using Skulk.Hydra.MapRegistration.Messages;
using Skulk.Hydralize;
using System;
using System.Collections.Generic;

namespace Skulk.Hydra
{
  /// <summary>
  /// A hydra module that requests and receives <see cref="ObjectMap"/> objects
  /// </summary>
  public class MapRegistrationModule : HydraModule
  {
    /// <summary>
    /// The max number of maps that will be transmitted per update call
    /// </summary>
    private const int MAPS_PER_UPDATE = 10;
    private Dictionary<HydraUuid, HydraMapDefinitions> m_applicationDefinitions = new Dictionary<HydraUuid, HydraMapDefinitions>();
    private MapDefinitionCallback m_onDefinitionAdded;

    /// <summary>
    /// Event invoked when a definition is added
    /// </summary>
    public MapDefinitionCallback OnDefinitionAdded { get => m_onDefinitionAdded; set => m_onDefinitionAdded = value; }

    // Future implementation - limit map bandwidth by queueing maps to be sent
    //private Queue<HydraObjectMap> m_mapQueue = new Queue<HydraObjectMap>();

    /// <summary>
    /// Initializes a new instance of the <see cref="MapRegistrationModule"/> class
    /// </summary>
    /// <param name="parent">The parent application holding this module</param>
    public MapRegistrationModule(HydraCore parent) : base(parent)
    {
      SetMessageHandler(HydraCodes.HydraRequestMap, HandleMapRequest, "HydraRequestMap");
      SetMessageHandler(HydraCodes.HydraObjectMap, HandleObjectMap, "HydraObjectMap");
      parent.ProfileManager.OnProfileAdded += OnProfileAdded;
    }

    /// <summary>
    /// Sends a message to the specified client requesting an object map
    /// </summary>
    /// <param name="mapName">The name of the object map to request</param>
    /// <param name="uuid">The UUID of the application to request the map from</param>
    public void RequestMap(string mapName, HydraUuid uuid)
    {
      Parent.SendMessage(new HydraRequestMapMsg(new MapRequestInfo(mapName)), uuid);
    }

    /// <summary>
    /// Registers an object into the stored map definitions
    /// </summary>
    /// <param name="objType">The type of the object to store the map for</param>
    /// <param name="categoryIds">The IDs indicating the categories associated with this type</param>
    public void RegisterObject(Type objType, params int[] categoryIds)
    {
      ObjectMap map = new ObjectMap(objType);
      Add(Parent.Uuid, new MapDefinition(map, categoryIds));
    }

    /// <summary>
    /// Registers a <see cref="HydraObjMsg"/> object into the stored map definitions
    /// </summary>
    /// <typeparam name="T">The class type of the object the message contains</typeparam>
    /// <param name="objMsg">An instance of the message to map</param>
    /// <param name="categoryIds">The IDs indicating the categories associated with this message type</param>
    public void RegisterObjectMessage<T>(HydraObjMsg objMsg, params int[] categoryIds)
    {
      // Ensure MESSAGE_OBJECT is one of the categories
      if (categoryIds == null)
      {
        categoryIds = new int[1];
        categoryIds[0] = (int)MapDefinitionCategories.MESSAGE_OBJECT;
      }
      else
      {
        bool found = false;
        for (int i = 0; i < categoryIds.Length; ++i)
        {
          if (categoryIds[i] == (int)MapDefinitionCategories.MESSAGE_OBJECT)
          {
            found = true;
            break;
          }
        }

        if (!found)
        {
          int[] newCats = new int[categoryIds.Length + 1];
          categoryIds.CopyTo(newCats, 0);
          newCats[newCats.Length - 1] = (int)MapDefinitionCategories.MESSAGE_OBJECT;
          categoryIds = newCats;
        }
      }

      HydraMessageMap map = new HydraMessageMap(objMsg, typeof(T));
      Add(Parent.Uuid, new MapDefinition(map, categoryIds));
      ObjectMap objMap = new ObjectMap(typeof(T));
      Add(Parent.Uuid, new MapDefinition(objMap));
    }

    /// <summary>
    /// Attempts to add the specified map definition
    /// </summary>
    /// <param name="uuid">The UUID of the application to add the map for</param>
    /// <param name="definition">The object map definition to add</param>
    /// <returns>True if new objects were added, false if they were already present</returns>
    public bool Add(HydraUuid uuid, MapDefinition definition)
    {
      lock (m_applicationDefinitions)
      {
        if (!m_applicationDefinitions.ContainsKey(uuid))
        {
          HydraMapDefinitions maps = new HydraMapDefinitions(uuid);
          maps.OnMapAdded += OnMapAdded;
          maps.OnMapRemoved += OnMapRemoved;
          m_applicationDefinitions.Add(uuid, maps);
        }

        return m_applicationDefinitions[uuid].Add(definition);
      }
    }

    /// <summary>
    /// Registers the specified type into the stored map definitions
    /// </summary>
    /// <typeparam name="T">The class type of the object to register</typeparam>
    /// <param name="categoryIds">The IDs indicating the categories associated with this type</param>
    public void RegisterObject<T>(params int[] categoryIds)
    {
      RegisterObject(typeof(T));
    }

    /// <summary>
    /// Attempts to get an object map for the type name specified
    /// </summary>
    /// <param name="uuid">The UUID of the application to get the map for</param>
    /// <param name="name">The name of the object map to get</param>
    /// <returns>The object map for the type indicated or null if not found</returns>
    public MapDefinition GetMap(HydraUuid uuid, string name)
    {
      lock (m_applicationDefinitions)
      {
        if (m_applicationDefinitions.TryGetValue(uuid, out HydraMapDefinitions maps))
        {
          return maps.Get(name);
        }
      }

      return null;
    }

    /// <summary>
    /// Gets a collection of the UUID key values for maps currently stored in this module
    /// </summary>
    /// <returns>The list of UUID key values for maps currently in this module</returns>
    public List<HydraUuid> GetKeys()
    {
      List<HydraUuid> keys = new List<HydraUuid>();
      lock (m_applicationDefinitions)
      {
        keys.AddRange(m_applicationDefinitions.Keys);
      }

      return keys;
    }

    /// <summary>
    /// Gets a list of all the object maps stored
    /// </summary>
    /// <param name="uuid">The UUID of the application to get the maps for</param>
    /// <param name="categoryFilters">The category ID values indicating the maps that should be returned, if null all maps will be returned</param>
    /// <returns>The list of all stored object maps that match the filter categories</returns>
    public List<MapDefinition> GetMaps(HydraUuid uuid, params int[] categoryFilters)
    {
      lock (m_applicationDefinitions)
      {
        if (m_applicationDefinitions.TryGetValue(uuid, out HydraMapDefinitions maps))
        {
          return maps.GetMaps(categoryFilters);
        }
      }

      return null;
    }

    /// <summary>
    /// Handles a received <see cref="HydraMapMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="HydraMapMsg"/></param>
    /// <param name="uuid">The UUID of the Hydra application that sent the message</param>
    private void HandleObjectMap(Message msg, HydraUuid uuid)
    {
      HydraMapMsg mapMsg = new HydraMapMsg(msg);
      MapDefinition definition = mapMsg.GetObject<MapDefinition>();
      Add(uuid, definition);
    }

    /// <summary>
    /// Handles a received <see cref="HydraRequestMapMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="HydraRequestMapMsg"/></param>
    /// <param name="uuid">The UUID of the Hydra application that sent the message</param>
    private void HandleMapRequest(Message msg, HydraUuid uuid)
    {
      HydraRequestMapMsg requestMapMsg = new HydraRequestMapMsg(msg);
      MapRequestInfo requestInfo = requestMapMsg.GetObject<MapRequestInfo>();
      if (requestInfo?.MapName != null)
      {
        Parent.Log?.LogDebug("Received a map request for map with name: {0}", requestInfo.MapName);
        switch (requestInfo.MapName)
        {
          case "ALL":
          case "all":
          {
            List<MapDefinition> maps = GetMaps(Parent.Uuid);
            if (maps != null)
            {
              Parent.Log.LogDebug("There are {0} stored maps", maps.Count);
              foreach (MapDefinition definition in maps)
              {
                Parent.SendMessage(new HydraMapMsg(definition), uuid);
              }
            }
            else
            {
              Parent.Log.LogDebug("There are no maps stored");
            }
          }
          break;

          default:
          {
            MapDefinition definition = GetMap(Parent.Uuid, requestInfo.MapName);
            if (definition != null)
            {
              Parent.SendMessage(new HydraMapMsg(definition), uuid);
            }
          }
          break;
        }
      }
      else
      {
        Parent.Log?.LogError("Received a map request msg with null info");
      }
    }

    /// <summary>
    /// Event invoked when a map is added to any applications map definitions
    /// </summary>
    /// <param name="uuid">The UUID of the application the definition was added for</param>
    /// <param name="definition">The map definition that was added</param>
    private void OnMapAdded(HydraUuid uuid, MapDefinition definition)
    {
      Parent.Log?.LogDebug("Added map: {0} to application: {1}", definition.Name, uuid);
      m_onDefinitionAdded?.Invoke(uuid, definition);
    }

    /// <summary>
    /// Event invoked when a map is removed from any applications map definitions
    /// </summary>
    /// <param name="uuid">The UUID of the application the definition was removed from</param>
    /// <param name="definition">The map definition that was removed</param>
    private void OnMapRemoved(HydraUuid uuid, MapDefinition definition)
    {
      Parent.Log?.LogDebug("Removed map: {0} from application: {1}", definition.Name, uuid);
    }

    private void OnProfileAdded(Profile profile)
    {
      lock (m_applicationDefinitions)
      {
        if (!m_applicationDefinitions.ContainsKey(profile.Uuid))
        {
          HydraMapDefinitions definitions = new HydraMapDefinitions(profile.Uuid);
          definitions.OnMapAdded += OnMapAdded;
          definitions.OnMapRemoved += OnMapRemoved;
          m_applicationDefinitions.Add(profile.Uuid, definitions);
        }
      }
    }

    /// <summary>
    /// Event invoked when a profile is removed
    /// </summary>
    /// <param name="profile">The profile that was removed</param>
    private void OnProfileRemoved(Profile profile)
    {
      lock (m_applicationDefinitions)
      {
        m_applicationDefinitions.Remove(profile.Uuid);
      }
      Parent.Log.LogDebug("Removed application definitions for profile with UUID: {0}", profile.Uuid);
    }
  }
}
