#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;
using System.Collections.Generic;

namespace Skulk.Hydra.MapRegistration.Data
{
  /// <summary>
  /// Enumeration of built in <see cref="MapDefinition"/> category ID values
  /// </summary>
  public enum MapDefinitionCategories : int
  {
    /// <summary>
    /// A reserved value indicating an unset category
    /// </summary>
    NULL,

    /// <summary>
    /// A category for maps that describe an object deriving from <see cref="HydraObjMsg"/>
    /// </summary>
    MESSAGE_OBJECT,

    /// <summary>
    /// A category for maps that contain information data
    /// </summary>
    TELEMETRY,

    /// <summary>
    /// A category for maps that describe data for commanding
    /// </summary>
    COMMAND
  }

  /// <summary>
  /// Contains an object map and information describing and categorizing the map
  /// </summary>
  public class MapDefinition
  {
    private List<int> m_categoryIds = new List<int>();
    private ObjectMap m_map;

    /// <summary>
    /// A collection of IDs that categorize this map
    /// </summary>
    public List<int> CategoryIds { get => m_categoryIds; set => m_categoryIds = value; }

    /// <summary>
    /// The object map information
    /// </summary>
    public ObjectMap Map { get => m_map; set => m_map = value; }

    /// <summary>
    /// The name of this object map
    /// </summary>
    public string Name { get => m_map?.Name; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public MapDefinition()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="MapDefinition"/> with the specified map and category IDs
    /// </summary>
    /// <param name="map">The object map</param>
    /// <param name="categoryIds">The IDs of the categories this map should be associated with</param>
    public MapDefinition(ObjectMap map, params int[] categoryIds)
    {
      m_map = map;
      if (categoryIds != null)
      {
        m_categoryIds.AddRange(categoryIds);
      }
    }

    /// <summary>
    /// Initializes this <see cref="MapDefinition"/> with the specified map and category IDs
    /// </summary>
    /// <param name="map">The object map</param>
    /// <param name="categoryIds">A list of IDs of the categories this map should be associated with</param>
    public MapDefinition(ObjectMap map, List<int> categoryIds)
    {
      m_map = map;
      if (categoryIds != null)
      {
        m_categoryIds.AddRange(categoryIds);
      }
    }

    /// <summary>
    /// Initializes this <see cref="MapDefinition"/> for a HydraObjMsg with the specified values
    /// </summary>
    /// <param name="message">The type of the message class</param>
    /// <param name="code">The message command code</param>
    /// <param name="objectId">The message object ID used for the object type indicated</param>
    /// <param name="messageObject">The type of the object contained in the message</param>
    /// <param name="flags">The message flags (automatically sets the hydralized object bit)</param>
    /// <param name="categoryIds">Any custom category IDs used to identify the message</param>
    public MapDefinition(Type message, int code, int objectId, Type messageObject, UInt16 flags = 0, params int[] categoryIds)
    {
      HydraMessageMap messageMap = new HydraMessageMap(message, code, objectId, messageObject, (UInt16)(flags | (UInt16)MessageFlagValues.HYDRALIZED_OBJECT));
      m_map = messageMap;

      // Ensure the MESSAGE_OBJECT category is set
      m_categoryIds.Add((int)MapDefinitionCategories.MESSAGE_OBJECT);

      if (categoryIds != null)
      {
        for (int i = 0; i < categoryIds.Length; ++i)
        {
          if (!m_categoryIds.Contains(categoryIds[i]))
          {
            m_categoryIds.Add(categoryIds[i]);
          }
        }
      }
    }

    /// <summary>
    /// Initializes this <see cref="MapDefinition"/> for a HydraObjMsg with the specified values
    /// </summary>
    /// <param name="objectMessage">The <see cref="HydraObjMsg"/> to describe</param>
    /// <param name="messageObject">The type of the object being sent in this message</param>
    /// <param name="categoryIds">The custom category IDs for this message map</param>
    public MapDefinition(HydraObjMsg objectMessage, Type messageObject, params int[] categoryIds)
      : this(objectMessage.GetType(), (int)objectMessage.GetCmd(), objectMessage.GetObjectId(), messageObject, objectMessage.GetFlags(), categoryIds)
    {
      // Empty
    }

    /// <summary>
    /// Checks if this definition is part of a specified category
    /// </summary>
    /// <param name="categoryId">The ID of the category to check for</param>
    /// <returns>True if this definition is part of the specified category, false otherwise</returns>
    public bool HasCategory(int categoryId)
    {
      if (m_categoryIds != null)
      {
        for (int i = 0; i < m_categoryIds.Count; ++i)
        {
          if (m_categoryIds[i] == categoryId)
          {
            return true;
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Checks if this definition is part of a specified category
    /// </summary>
    /// <param name="category">The category to check for</param>
    /// <returns>True if this definition is part of the specified category, false otherwise</returns>
    public bool HasCategory(MapDefinitionCategories category)
    {
      return HasCategory((int)category);
    }
  }
}
