#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;

namespace Skulk.Hydra.MapRegistration.Data
{
  /// <summary>
  /// Describes a <see cref="HydraObjMsg"/> message and its associated value types
  /// </summary>
  public class HydraMessageMap : ObjectMap
  {
    private int m_code;
    private int m_objectId;
    private UInt16 m_flags;

    /// <summary>
    /// The unique message code
    /// </summary>
    public int Code { get => m_code; set => m_code = value; }

    /// <summary>
    /// The object Id used in the message that corresponds to the map
    /// </summary>
    public int ObjectId { get => m_objectId; set => m_objectId = value; }

    /// <summary>
    /// The flags for this message
    /// </summary>
    public ushort Flags { get => m_flags; set => m_flags = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraMessageMap()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new <see cref="HydraMessageMap"/> with the values specified
    /// </summary>
    /// <param name="messageType">The type of this message object</param>
    /// <param name="code">The message code</param>
    /// <param name="objectId">The object ID used in the message</param>
    /// <param name="objectType">The type of the object used in the corresponding message</param>
    /// <param name="flags">The flags used in the message</param>
    public HydraMessageMap(Type messageType, int code, int objectId, Type objectType, UInt16 flags = 0) : base(objectType)
    {
      Name = messageType.Name;
      m_code = code;
      m_objectId = objectId;
      m_flags = flags;
    }

    /// <summary>
    /// Initializes a new <see cref="HydraMessageMap"/> with values from the message indicated
    /// </summary>
    /// <param name="objMsg">The message to map</param>
    /// <param name="objectType">The type of the object the message holds</param>
    public HydraMessageMap(HydraObjMsg objMsg, Type objectType)
      : this (objMsg.GetType(), (int)objMsg.GetCmd(), objMsg.GetObjectId(), objectType, objMsg.GetFlags())
    {
      // Empty
    }

    /// <summary>
    /// Uses the specified map values to create a new instance of a <see cref="HydraObjMsg"/> class
    /// </summary>
    /// <param name="paths">The full paths to field values to set for the map entries in the message object</param>
    /// <param name="values">The field values to set for the map entries in the message object</param>
    /// <returns>The message created with the specified values or null if creating the message failed</returns>
    public Message CreateMessage(string[] paths, object[] values)
    {
      if (paths == null || values == null || paths.Length != values.Length)
      {
        return null;
      }

      HydralizeInfo info = new HydralizeInfo();
      for (int i = 0; i < paths.Length; ++i)
      {
        if (!AddObjectInfo(paths[i], values[i], ref info))
        {
          return null;
        }
      }

      return new HydraObjMsg((UInt16)m_code, m_objectId, info, m_flags);
    }
  }
}
