#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System.Collections.Generic;

namespace Skulk.Hydra.MapRegistration.Data
{
  /// <summary>
  /// Event involving a <see cref="MapDefinition"/> object and Hydra application
  /// </summary>
  /// <param name="uuid">The UUID of the application involved in the event</param>
  /// <param name="definition">The map definition involved in the event</param>
  public delegate void MapDefinitionCallback(HydraUuid uuid, MapDefinition definition);

  /// <summary>
  /// Manages Hydra object map definitions
  /// </summary>
  public class HydraMapDefinitions
  {
    /// <summary>
    /// The collection of stored map definitions
    /// </summary>
    private Dictionary<string, MapDefinition> m_definitions = new Dictionary<string, MapDefinition>();
    private HydraUuid m_uuid;
    private MapDefinitionCallback m_onMapAdded;
    private MapDefinitionCallback m_onMapRemoved;

    /// <summary>
    /// Gets or sets the object map definition
    /// </summary>
    /// <param name="key">The name of the object map</param>
    /// <returns>The object map or null if it was not found</returns>
    public MapDefinition this[string key]
    {
      get { return Get(key); }
      set { Set(key, value); }
    }

    /// <summary>
    /// The UUID of the application owning these definitions
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The number of definitions stored
    /// </summary>
    public int Count { get => m_definitions.Count; }

    /// <summary>
    /// The event triggered when a new map is added
    /// </summary>
    public MapDefinitionCallback OnMapAdded { get => m_onMapAdded; set => m_onMapAdded = value; }

    /// <summary>
    /// The event triggered when a map is removed
    /// </summary>
    public MapDefinitionCallback OnMapRemoved { get => m_onMapRemoved; set => m_onMapRemoved = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraMapDefinitions()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this object with the specified application UUID
    /// </summary>
    /// <param name="uuid">The UUID of the application owning these definitions</param>
    public HydraMapDefinitions(HydraUuid uuid)
    {
      m_uuid = uuid;
    }

    /// <summary>
    /// Attempts to add the map to the stored definitions
    /// </summary>
    /// <param name="definition">The map definition to add</param>
    /// <returns>True if the map was added, false it was already present</returns>
    public bool Add(MapDefinition definition)
    {
      bool added = false;
      lock (m_definitions)
      {
        if (!m_definitions.ContainsKey(definition.Name))
        {
          m_definitions.Add(definition.Name, definition);
          added = true;
        }
      }

      if (added)
      {
        m_onMapAdded?.Invoke(m_uuid, definition);
        return true;
      }
      else
      {
        return false;
      }
    }

    /// <summary>
    /// Attempts to remove the map for the specified object
    /// </summary>
    /// <param name="name">The name of the object to remove the map for</param>
    /// <returns>True if the map was removed, false otherwise</returns>
    public bool Remove(string name)
    {
      bool removed = false;
      MapDefinition definition = null;
      lock (m_definitions)
      {
        if (m_definitions.TryGetValue(name, out definition))
        {
          removed = m_definitions.Remove(name);
        }
      }

      if (removed)
      {
        m_onMapRemoved?.Invoke(m_uuid, definition);
        return true;
      }
      else
      {
        return false;
      }
    }

    /// <summary>
    /// Attempts to get the object map with the specified name
    /// </summary>
    /// <param name="name">The name of the object to get the map for</param>
    /// <returns>The map for the specified object or null if the was not found</returns>
    public MapDefinition Get(string name)
    {
      lock (m_definitions)
      {
        if (m_definitions.TryGetValue(name, out MapDefinition definition))
        {
          return definition;
        }
      }
      return null;
    }

    /// <summary>
    /// Attempts to set the map for an object already in the definitions
    /// </summary>
    /// <param name="name">The name to store the map definition under, usually should match the name of the map</param>
    /// <param name="map">The map value to set</param>
    /// <returns>True if the map value was set, false if the object was not found in the dictionary</returns>
    public bool Set(string name, MapDefinition map)
    {
      lock (m_definitions)
      {
        if (m_definitions.ContainsKey(name))
        {
          m_definitions[name] = map;
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Gets a list of all the map definitions currently stored
    /// </summary>
    /// <returns>All the map definitions stored in this object</returns>
    public List<MapDefinition> GetMaps(params int[] categoryFilters)
    {
      List<MapDefinition> maps = new List<MapDefinition>();
      lock (m_definitions)
      {
        foreach (MapDefinition definition in m_definitions.Values)
        {
          if (categoryFilters == null || categoryFilters.Length == 0)
          {
            maps.Add(definition);
          }
          else
          {
            for (int i = 0; i < categoryFilters.Length; ++i)
            {
              if (definition.CategoryIds.Contains(categoryFilters[i]))
              {
                maps.Add(definition);
                break;
              }
            }
          }
        }
      }
      return maps;
    }
  }
}
