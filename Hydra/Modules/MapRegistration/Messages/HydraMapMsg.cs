#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.MapRegistration.Data;

namespace Skulk.Hydra.MapRegistration.Messages
{
  /// <summary>
  /// A message that transmits information describing an object type
  /// </summary>
  public class HydraMapMsg : HydraObjMsg
  {
    /// <summary>
    /// The ID used in this message when it contains an object map
    /// </summary>
    public const int OBJECT_MAP_ID = 0;

    /// <summary>
    /// The ID used in this message when it contains a message map
    /// </summary>
    public const int MSG_MAP_ID = 1;

    /// <summary>
    /// Initializes a new instance of this message with the object map specified
    /// </summary>
    /// <param name="definition">The object map data to send</param>
    public HydraMapMsg(MapDefinition definition) : base (HydraCodes.HydraObjectMap, OBJECT_MAP_ID, definition, true, true)
    {
      if (definition.Map.GetType() == typeof(HydraMessageMap))
      {
        SetObjectId(MSG_MAP_ID);
      }
    }

    /// <summary>
    /// Initializes a new instance of this message with the values from the message specified
    /// </summary>
    /// <param name="other">The message containing the values to copy into this instance</param>
    public HydraMapMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
