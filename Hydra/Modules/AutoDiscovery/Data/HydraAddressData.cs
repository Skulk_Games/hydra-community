#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Network;

namespace Skulk.Hydra.AutoDiscovery.Data
{
  /// <summary>
  /// Holds the information describing how to connect to a Hydra application
  /// </summary>
  public class HydraAddressData : HydraUuidAddress
  {
    private HydraModes m_mode;

    /// <summary>
    /// The mode thie Hydra application is running in
    /// </summary>
    public HydraModes Mode { get => m_mode; set => m_mode = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraAddressData()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new <see cref="HydraAddressData"/> class with the specified values
    /// </summary>
    /// <param name="uuid">The UUID of the application</param>
    /// <param name="host">The hostname or IP address of an application</param>
    /// <param name="port">The UDP listen port of an application</param>
    /// <param name="streamPort">The TCP listen port of an application</param>
    /// <param name="mode">The mode an application is running in</param>
    public HydraAddressData(HydraUuid uuid, string host, int port, int streamPort, HydraModes mode)
      : base(uuid, host, port, streamPort)
    {
      m_mode = mode;
    }

    /// <summary>
    /// Initializes the address data with the specified application's data
    /// </summary>
    /// <param name="hydra">The application to describe</param>
    public HydraAddressData(HydraCore hydra)
    {
      Uuid = hydra.Uuid;
      Hosts = hydra.Address.Hosts;
      MessagePort = hydra.Address.MessagePort;
      StreamPort = hydra.Address.StreamPort;
      m_mode = hydra.HydraMode;
    }

    /// <summary>
    /// Initializes the address data with the specified profile's data
    /// </summary>
    /// <param name="profile">The profile for the application address to describe</param>
    public HydraAddressData(Profile profile) : this(profile.Uuid, profile.Address.Host, profile.Address.MessagePort, profile.Address.StreamPort, profile.Mode)
    {
      // Empty
    }

    /// <summary>
    /// Determines if another address has equal values to this address instance
    /// </summary>
    /// <param name="obj">The address object to compare with this instance</param>
    /// <returns>True if all values are equal, false otherwise</returns>
    public override bool Equals(object obj)
    {
      HydraAddressData address = obj as HydraAddressData;
      if (address == null)
      {
        return false;
      }

      if (base.Equals(address))
      {
        return m_mode == address.m_mode;
      }

      return false;
    }

    /// <summary>
    /// Gets a hash code value for this address instance
    /// </summary>
    /// <returns>The hash code for this instance</returns>
    public override int GetHashCode()
    {
      return base.GetHashCode() + (int)m_mode;
    }
  }
}
