#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;

namespace Skulk.Hydra.AutoDiscovery.Data
{
  /// <summary>
  /// Describes statically defined server connection information
  /// </summary>
  public class StaticNodeEndPoint : HydralizeObject
  {
    private HydraAddressData m_address;
    private bool m_publish;
    private bool m_active;

    /// <summary>
    /// The server end point
    /// </summary>
    public HydraAddressData AddressData { get => m_address; set => m_address = value; }

    /// <summary>
    /// Indicates if this server data should be published to others
    /// </summary>
    public bool Publish { get => m_publish; set => m_publish = value; }

    /// <summary>
    /// Indicates if the connection to this server is currently active
    /// </summary>
    public bool Active { get => m_active; set => m_active = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public StaticNodeEndPoint()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new <see cref="StaticNodeEndPoint"/> class with the specified values
    /// </summary>
    /// <param name="address">The address information of the static node</param>
    /// <param name="publish">Indicates if this node should be published to other nodes</param>
    /// <param name="active">Indicates if this node is currently active</param>
    public StaticNodeEndPoint(HydraAddressData address, bool publish = true, bool active = false)
    {
      m_address = address;
      m_publish = publish;
      m_active = active;
    }
  }
}
