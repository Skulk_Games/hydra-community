#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.AutoDiscovery.Data;
using System;

namespace Skulk.Hydra.AutoDiscovery.Messages
{
  /// <summary>
  /// A message sent to respond to discovery pings with connection information for a server
  /// </summary>
  public class DiscoveryPongMsg : HydraObjMsg
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="DiscoveryPongMsg"/> with the address data specified
    /// </summary>
    /// <param name="data">The address data to send in the message</param>
    public DiscoveryPongMsg(HydraAddressData data) : base(HydraCodes.HydraDiscoveryPong, 0, data, true, true, (UInt16)MessageFlagValues.ANONYMOUS)
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DiscoveryPongMsg"/> with data from another message
    /// </summary>
    /// <param name="other">The message containing the data to initialize this instance with</param>
    public DiscoveryPongMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
