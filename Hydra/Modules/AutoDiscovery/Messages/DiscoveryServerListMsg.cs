#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.AutoDiscovery.Data;

namespace Skulk.Hydra.AutoDiscovery.Messages
{
  /// <summary>
  /// A message containing a collection of server information
  /// </summary>
  public class DiscoveryServerListMsg : HydraObjMsg
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="DiscoveryServerListMsg"/> class with the data specified
    /// </summary>
    /// <param name="data">The object containing the server data to transmit</param>
    public DiscoveryServerListMsg(KnownServerData data) : base(HydraCodes.HydraDiscoveryList, 0, data, true)
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DiscoveryServerListMsg"/> class with data from another message
    /// </summary>
    /// <param name="other">The message containing the values to initialize this message with</param>
    public DiscoveryServerListMsg(Message other) : base(other)
    {
      // Empty
    }
  }
}
