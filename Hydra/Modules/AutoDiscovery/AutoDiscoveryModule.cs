#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.AutoDiscovery.Data;
using Skulk.Hydra.AutoDiscovery.Messages;
using Skulk.Hydra.MapRegistration.Data;
using Skulk.Hydra.Network;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Skulk.Hydra
{
  /// <summary>
  /// An event callback involving a <see cref="HydraAddressData"/> instance
  /// </summary>
  /// <param name="addressData">The address data involved</param>
  public delegate void HydraAddressCallback(HydraAddressData addressData);

  /// <summary>
  /// A HydraModule that attempts to automatically discover Hydra servers on the same network
  /// </summary>
  public class AutoDiscoveryModule : HydraModule
  {
    /// <summary>
    /// The period in seconds this module will automatically send a ping message
    /// </summary>
    private double m_autoPing = 600;

    /// <summary>
    /// The time since a ping was sent
    /// </summary>
    private double m_pingTime = 0;

    /// <summary>
    /// The period in seconds a server may stay in the list without a pong update
    /// </summary>
    private double m_serverTimeout = 1200;

    /// <summary>
    /// Indicates if UDP connections should automatically be attempted to discovered servers.
    /// </summary>
    private bool m_autoConnect = false;

    /// <summary>
    /// The max time to wait for a profile to be established in between handshake attempts on each host for an automatic connection.
    /// </summary>
    /// <remarks>
    /// Defaulted to 0 to preserve previous behavior, but it is recommended to be ~1 normally.
    /// </remarks>
    private double m_autoConnectTimeout = 0;

    /// <summary>
    /// Indicates whether discovery messages should be sent to the loopback interface.
    /// </summary>
    private bool m_broadcastLocal = false;

    /// <summary>
    /// Indicates whether discovery message should be broadcast to the remote broadcast IP.
    /// </summary>
    private bool m_broadcastRemote = true;

    /// <summary>
    /// The address to send remote broadcast messages to.
    /// </summary>
    private string m_remoteBroadcastAddress = "255.255.255.255";

    /// <summary>
    /// The UDP port numbers to use when discovering applications
    /// </summary>
    private List<int> m_searchPorts = new List<int>();

    /// <summary>
    /// The list of servers that have responded to pings
    /// </summary>
    private TimeoutList<HydraAddressData> m_servers = new TimeoutList<HydraAddressData>();

    /// <summary>
    /// The list of statically defined servers
    /// </summary>
    private List<StaticNodeEndPoint> m_staticServers = new List<StaticNodeEndPoint>();

    /// <summary>
    /// Event invoked when a new server has been added to the list of known servers
    /// </summary>
    public event HydraAddressCallback OnServerAdded;

    /// <summary>
    /// Indicates if UDP connections should automatically be attempted for discovered servers
    /// </summary>
    public bool AutoConnect { get => m_autoConnect; set => m_autoConnect = value; }

    /// <summary>
    /// The max time in seconds to wait for an autoconnect to establish a profile before the next host in the address list will be attempted.
    /// </summary>
    /// <remarks>
    /// There is no real problem with this being left as 0, but it will reduce handshake errors in the log if set higher.
    /// The recommended value is 1.
    /// </remarks>
    public double AutoConnectTimeout { get => m_autoConnectTimeout; set => m_autoConnectTimeout = value; }

    /// <summary>
    /// Indicates whether discovery pings should be sent to the loopback interface.
    /// </summary>
    public bool BroadcastLocal { get => m_broadcastLocal; set => m_broadcastLocal = value; }

    /// <summary>
    /// Indicates whether discovery pings should be sent to the remote broadcast address.
    /// </summary>
    public bool BroadcastRemote { get => m_broadcastRemote; set => m_broadcastRemote = value; }

    /// <summary>
    /// The IP to send remote broadcast messages to.
    /// </summary>
    public string RemoteBroadcastAddress { get => m_remoteBroadcastAddress; set => m_remoteBroadcastAddress = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="AutoDiscoveryModule"/> class
    /// </summary>
    /// <param name="hydra">The parent Hydra instance holding this module</param>
    /// <param name="autoPingPeriod">The period in seconds that this module will automatically send a discovery ping</param>
    /// <param name="serverTimeout">The period in seconds a server will remain in the list unless a discovery response is received</param>
    /// <param name="searchPorts">The UDP port numbers to use when discovering applications, if null this application's UDP port will be used</param>
    /// <param name="staticServers">Statically defined servers to connect to</param>
    public AutoDiscoveryModule(HydraCore hydra, double autoPingPeriod = 600, double serverTimeout = 900, int[] searchPorts = null, StaticNodeEndPoint[] staticServers = null)
      : base(hydra)
    {
      // Set custom handlers
      SetMessageHandler(HydraCodes.HydraDiscoveryPing, HandlePingMessage, "HydraDiscoveryPing");
      SetMessageHandler(HydraCodes.HydraDiscoveryPong, HandlePongMessage, "HydraDiscoveryPong");
      SetMessageHandler(HydraCodes.HydraDiscoveryRequest, HandleDiscoveryRequest, "HydraDiscoveryRequest");
      SetMessageHandler(HydraCodes.HydraDiscoveryList, HandleDiscoveryList, "HydraDiscoveryList");

      m_autoPing = autoPingPeriod;
      m_serverTimeout = serverTimeout;
      m_servers.OnItemExpired += OnExpired;
      if (searchPorts != null && searchPorts.Length > 0)
      {
        m_searchPorts.AddRange(searchPorts);
      }
      if (staticServers != null && staticServers.Length > 0)
      {
        m_staticServers.AddRange(staticServers);
      }
    }

    /// <summary>
    /// Registers this modules messages and objects with a map module
    /// </summary>
    protected override List<MapDefinition> GetHydraObjects()
    {
      List<MapDefinition> maps = new List<MapDefinition>();

      maps.Add(new MapDefinition(new DiscoveryPingMsg((HydraAddressData)null), typeof(HydraAddressData), (int)MapDefinitionCategories.TELEMETRY));
      maps.Add(new MapDefinition(new DiscoveryPongMsg((HydraAddressData)null), typeof(HydraAddressData), (int)MapDefinitionCategories.TELEMETRY));
      maps.Add(new MapDefinition(new DiscoveryRequestMsg((HydraAddressData)null), typeof(HydraAddressData), (int)MapDefinitionCategories.COMMAND));
      maps.Add(new MapDefinition(new DiscoveryServerListMsg((KnownServerData)null), typeof(KnownServerData), (int)MapDefinitionCategories.TELEMETRY));

      return maps;
    }

    /// <summary>
    /// Periodic update callback
    /// </summary>
    /// <param name="updateTime">The object containing update timing information</param>
    public override void Update(UpdateTime updateTime)
    {
      lock (m_servers)
      {
        m_servers.Update(updateTime);
      }

      m_pingTime += updateTime.DeltaSeconds;
      if (m_pingTime >= m_autoPing)
      {
        m_pingTime = 0;
        Ping();
        RequestServerListAll();
        for (int i = 0; i < m_staticServers.Count; ++i)
        {
          if (!Parent.Uuid.Equals(m_staticServers[i].AddressData.Uuid) && (m_staticServers[i].AddressData.Uuid == null || Parent.GetProfile(m_staticServers[i].AddressData.Uuid) == null))
          {
            Parent.SendHandshake(m_staticServers[i].AddressData.GetUdpEndPoint());
          }
        }
      }
    }

    /// <summary>
    /// Performs module startup
    /// </summary>
    public override void ModuleStartup()
    {
      base.ModuleStartup();
      // Add self to static servers if not already present
      if (m_staticServers.Find(x => Parent.Uuid.Equals(x.AddressData.Uuid)) == null)
      {
        AddStaticNode(new StaticNodeEndPoint(new HydraAddressData(Parent)));
      }

      // Have at least one search port
      if (m_searchPorts.Count == 0)
      {
        switch (Parent.HydraMode)
        {
          case HydraModes.CLIENT:
          {
            m_searchPorts.Add(Parent.ServerPort);
          }
          break;

          default:
          {
            m_searchPorts.Add(Parent.LocalPort);
          }
          break;
        }
      }

      Parent.ProfileManager.OnProfileAdded += OnProfileAssigned;
      Ping();
    }

    /// <summary>
    /// Performs module cleanup on shutdown
    /// </summary>
    public override void ModuleShutdown()
    {
      base.ModuleShutdown();
      Parent.ProfileManager.OnProfileAdded -= OnProfileAssigned;
    }

    /// <summary>
    /// Handles a received <see cref="DiscoveryPingMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="DiscoveryPingMsg"/></param>
    /// <param name="client">The client that sent the message</param>
    private void HandlePingMessage(Message msg, HydraUuid client)
    {
      DiscoveryPingMsg pingMsg = new DiscoveryPingMsg(msg);
      HydraAddressData pingData = pingMsg.GetObject<HydraAddressData>();
      Parent.Log.LogDebug("Received discovery ping from: {0} on port: {1}", pingData.Uuid, Parent.Settings.UdpPort);
      // Only servers respond to pings
      if (Parent.HydraMode != HydraModes.CLIENT && !pingData.Uuid.Equals(Parent.Uuid))
      {
        this.Pong(pingData.MessagePort);
      }
    }

    /// <summary>
    /// Handles a received <see cref="DiscoveryPongMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="DiscoveryPongMsg"/></param>
    /// <param name="client">The client that sent the message</param>
    private void HandlePongMessage(Message msg, HydraUuid client)
    {
      // Update the list of servers
      Parent.Log.LogDebug("{0} received a discovery pong msg.", Parent.Uuid);
      DiscoveryPongMsg pong = new DiscoveryPongMsg(msg);
      HydraAddressData data = pong.GetObject<HydraAddressData>();

      Parent.Log.LogTrace("Pong data, UUID: {0}, Address: {1}, mode: {2}", data.Uuid, data.ToString(), data.Mode);
      if (data != null)
      {
        if (data.Mode == HydraModes.SERVER || data.Mode == HydraModes.NODE)
        {
          AddServer(data);
        }
      }
    }

    /// <summary>
    /// Handles a received <see cref="DiscoveryRequestMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="DiscoveryRequestMsg"/></param>
    /// <param name="client">The client that sent the message</param>
    private void HandleDiscoveryRequest(Message msg, HydraUuid client)
    {
      DiscoveryRequestMsg requestMsg = new DiscoveryRequestMsg(msg);
      HydraAddressData requestData = requestMsg.GetObject<HydraAddressData>();
      KnownServerData serverData;

      lock (m_servers)
      {
        serverData = new KnownServerData(m_servers.ToArray());
      }

      for (int i = 0; i < m_staticServers.Count; ++i)
      {
        if (m_staticServers[i].Publish && m_staticServers[i].Active)
        {
          serverData.Servers.Add(m_staticServers[i].AddressData);
        }
      }

      Parent.Log?.LogDebug("Received a discovery request, transmitting list of {0} known servers", serverData.Servers.Count);
      Parent.SendMessage(new DiscoveryServerListMsg(serverData), client);
    }

    /// <summary>
    /// Handles a received <see cref="DiscoveryServerListMsg"/>
    /// </summary>
    /// <param name="msg">The received <see cref="DiscoveryServerListMsg"/></param>
    /// <param name="client">The client that sent the message</param>
    private void HandleDiscoveryList(Message msg, HydraUuid client)
    {
      Parent.Log?.LogDebug("Received a discovery list message");
      DiscoveryServerListMsg serverListMsg = new DiscoveryServerListMsg(msg);
      KnownServerData serverData = serverListMsg.GetObject<KnownServerData>();
      if (serverData != null)
      {
        for (int i = 0; i < serverData.Servers.Count; ++i)
        {
          AddServer(serverData.Servers[i]);
        }
      }
    }

    /// <summary>
    /// Gets the server data entries
    /// </summary>
    /// <param name="includeStatic">Indicates if static servers should be included</param>
    /// <returns>The list of server data entries</returns>
    public List<HydraAddressData> GetServers(bool includeStatic = false)
    {
      lock (m_servers)
      {
        List<HydraAddressData> servers = new List<HydraAddressData>(m_servers.Count + m_staticServers.Count);
        servers.AddRange(m_servers.ToArray());
        for (int i = 0; i < m_staticServers.Count; ++i)
        {
          servers.Add(m_staticServers[i].AddressData);
        }

        return servers;
      }
    }

    /// <summary>
    /// Adds the node to the list of statically defined end points
    /// </summary>
    /// <param name="staticNode">The static node endpoint to add</param>
    public void AddStaticNode(StaticNodeEndPoint staticNode)
    {
      m_staticServers.Add(staticNode);
    }

    /// <summary>
    /// Sends out a ping message with this Hydra instances connection details that triggers responses from servers on the network segment
    /// </summary>
    public void Ping()
    {
      for (int i = 0; i < m_searchPorts.Count; ++i)
      {
        Message pingMsg = new DiscoveryPingMsg(new HydraAddressData(Parent));
        if (this.m_broadcastRemote)
        {
          Parent.SendMessage(pingMsg, this.m_remoteBroadcastAddress, m_searchPorts[i]);
        }
        if (this.m_broadcastLocal)
        {
          Parent.SendMessage(pingMsg, new IPEndPoint(IPAddress.Loopback, m_searchPorts[i]));
        }
      }
    }

    /// <summary>
    /// Sends out a pong message on the specified port to the loopback interface and/or the remote broadcast IP depending on configuration.
    /// </summary>
    /// <param name="port">The port to send the pong message on.</param>
    public void Pong(int port)
    {
      // Build the pong data
      HydraAddressData data = new HydraAddressData(Parent);

      if (this.m_broadcastLocal)
      {
        Parent.SendMessage(new DiscoveryPongMsg(data), new IPEndPoint(IPAddress.Loopback, port));
      }

      if (this.m_broadcastRemote)
      {
        Parent.SendMessage(new DiscoveryPongMsg(data), this.m_remoteBroadcastAddress, port);
      }
    }

    /// <summary>
    /// Sends a message that requests the list of known servers from the specified client
    /// </summary>
    /// <param name="uuid">The UUID of the application to request the server list from</param>
    public void RequestServerList(HydraUuid uuid)
    {
      Parent.SendMessage(new DiscoveryRequestMsg(new HydraAddressData(Parent)), uuid);
    }

    /// <summary>
    /// Sends a <see cref="DiscoveryRequestMsg"/> to all clients
    /// </summary>
    private void RequestServerListAll()
    {
      Parent.SendMessageAll(new DiscoveryRequestMsg(new HydraAddressData(Parent)));
    }

    /// <summary>
    /// Attempts to add the server to the list of known servers or resets the timeout if it is already present
    /// </summary>
    /// <param name="address">The address data of the server to add</param>
    /// <returns>True if the server was added to the list, false if it was already present</returns>
    private bool AddServer(HydraAddressData address)
    {
      // Data validation
      if (address.Host == null || address.MessagePort == 0 || address.StreamPort == 0)
      {
        return false;
      }

      bool added = false;
      lock (m_servers)
      {
        int index = m_servers.FindIndex(x => x.Uuid?.Equals(address.Uuid) == true);
        if (index == -1)
        {
          m_servers.Add(address, m_serverTimeout);
          Parent.Log?.LogDebug("Added server entry on {0} for {1}, there are now {2} entries.", Parent.Uuid, address, m_servers.GetCount());
          added = true;
        }
        else
        {
          if (address.Hosts.Count > m_servers[index].Hosts.Count)
          {
            Parent.Log.LogWarning("Replacing old address with fewer hostnames");
            m_servers[index] = address;
          }

          m_servers.Reset(index);
        }
      }

      if (added)
      {
        try
        {
          this.OnServerAdded?.Invoke(address);
        }
        catch (Exception ex)
        {
          this.Parent.Log.LogError("Exception in AutoDiscoveryModule.OnServerAdded event: '{0}'\n{1}", ex.Message, ex.StackTrace);
        }
      }

      // Autoconnect to any unconnected new servers if not a client
      if (Parent.HydraMode != HydraModes.CLIENT)
      {
        if (!Parent.Uuid.Equals(address.Uuid))
        {
          Profile profile = Parent.GetProfile(address.Uuid);
          if (profile == null && m_autoConnect)
          {
            // Start a task to try each endpoint in the address
            Task.Run(() => this.AttemptAutoConnection(address));
          }
        }
      }

      return added;
    }

    /// <summary>
    /// Attempts to connect to the application with the specified address data.
    /// </summary>
    /// <remarks>
    /// This method is potentially blocking as it will sleep between handshake attempts on each host specified in the address when configured to do so.
    /// </remarks>
    /// <param name="addressData">The address data describing the application endpoint to connect with.</param>
    private void AttemptAutoConnection(HydraAddressData addressData)
    {
      // The server is not connected as a client - send handshake
      IPEndPoint[] eps = addressData.GetUdpEndPoints();
      if (eps != null)
      {
        for (int i = 0; i < eps.Length; ++i)
        {
          if (this.Parent.GetProfile(addressData.Uuid) != null)
          {
            // Already connected
            return;
          }

          Parent.Log.LogDebug("Sending autoconnect handshake to: {0}", eps[i].ToString());
          Parent.SendHandshake(eps[i]);

          // Wait for the autoconnect timeout
          if (i < eps.Length && this.m_autoConnectTimeout > 0.001)
          {
            Thread.Sleep((int)m_autoConnectTimeout * 1000);
          }
        }
      }
    }

    /// <summary>
    /// Event invoked when a server expires from the list
    /// </summary>
    /// <param name="address">The address of the server that expired</param>
    private void OnExpired(HydraAddressData address)
    {
      Parent.Log?.LogDebug("Server data expired, address: {0}, UUID: {1}", address, address.Uuid);
    }

    /// <summary>
    /// Event callback invoked when a new application profile is added on the parent application
    /// </summary>
    /// <param name="profile">The profile that was added</param>
    private void OnProfileAssigned(Profile profile)
    {
      // Add the server address information if it's not already known
      AddServer(new HydraAddressData(profile));

      // Update UUID values for any assigned static servers
      for (int i = 0; i < m_staticServers.Count; ++i)
      {
        if (HydraAddress.Equals(m_staticServers[i], profile.Address))
        {
          m_staticServers[i].AddressData.Uuid = profile.Uuid;
        }
      }
    }
  }
}
