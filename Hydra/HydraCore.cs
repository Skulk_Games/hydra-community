#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra.Messages;
using Skulk.Hydra.Network;
using Skulk.Hydralize;
using Skulk.Hydralize.Encoding;
using Skulk.MEL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace Skulk.Hydra
{
  /// <summary>
  /// The different modes for a Hydra instance
  /// </summary>
  public enum HydraModes : byte
  {
    /// <summary>
    /// Indicates an unset mode value
    /// </summary>
    NULL,

    /// <summary>
    /// The Hydra instance is running as a server and listens for client connections
    /// </summary>
    SERVER,

    /// <summary>
    /// The Hydra instance runs as a client and connects to a server
    /// </summary>
    CLIENT,

    /// <summary>
    /// The Hydra instance runs as a node within a mesh network
    /// </summary>
    NODE
  }

  /// <summary>
  /// A callback for an event involving a message and a client UUID.
  /// </summary>
  /// <param name="msg">The message involved.</param>
  /// <param name="client">The UUID of the client involved</param>
  public delegate void MessageClientCallback(Message msg, HydraUuid client);

  /// <summary>
  /// A class that provides a dual channel communication path between a server and client.
  /// 
  /// A UDP connection is used for commanding over messages and a TCP connection can be
  /// utilized in response to commands that trigger high bandwidth responses.
  /// </summary>
  public class HydraCore : LoggedApp
  {
    /// <summary>
    /// The ID value used to indicated an unassigned profile
    /// </summary>
    public const int UNASSIGNED_ID = -1;

    /// <summary>
    /// The settings used by this Hydra application
    /// </summary>
    private HydraSettings m_settings;
    private HydraUuid m_uuid = new HydraUuid();
    private HydraAddress m_address;
    private Queue<UdpConnection> m_pendingUdp = new Queue<UdpConnection>();
    private string m_localAppDir;
    private HydraModule m_defaultModule;

    /// <summary>
    /// The object that manages active streams
    /// </summary>
    protected StreamManager m_streamAuthority;

    /// <summary>
    /// The profile manager
    /// </summary>
    protected HydraProfileManager m_profileManager;

    /// <summary>
    /// The handler the routes messages to their handling modules
    /// </summary>
    private MessageHandlerRouter m_messageHandler;

    /// <summary>
    /// The message handler that supports dynamic handling of messages
    /// and attempts to route any messages that don't have static handling
    /// </summary>
    private DynamicMessageHandler m_dynamicMessageHandler;

    /// <summary>
    /// The main UDP connection that listens for new handshakes
    /// </summary>
    private UdpClient m_udpListener;

    /// <summary>
    /// The object used to signal the send thread to start processing the send queue
    /// </summary>
    private ManualResetEvent m_sendEvent = new ManualResetEvent(true);

    /// <summary>
    /// The queue where messages to be sent on the core UDP connection are stored
    /// </summary>
    private Queue<MessageInfo> m_sendMessageQueue = new Queue<MessageInfo>();

    /// <summary>
    /// Gets or sets the default module which is sent messages unhandled by any other module
    /// </summary>
    /// <remarks>
    /// This value is only valid after the application has been started
    /// </remarks>
    public HydraModule DefaultModule { get => m_defaultModule; set => m_defaultModule = value; }

    /// <summary>
    /// Indicates the mode this Hydra instance is running in.
    /// </summary>
    public HydraModes HydraMode { get => m_settings.Mode; }

    /// <summary>
    /// The encoder to use for sending and receiving data on connections that only support messages.
    /// </summary>
    public TranscoderPair MessageEncoder { get => m_settings.MessageEncoder; set => m_settings.MessageEncoder = value; }

    /// <summary>
    /// The encoder to use when sending and receiving data on a streaming connection
    /// </summary>
    public TranscoderPair StreamEncoder { get => m_settings.StreamEncoder; set => m_settings.StreamEncoder = value; }

    /// <summary>
    /// The max number of clients the Server will allow to connect.
    /// </summary>
    public int MaxClients { get => m_settings.MaxClients; }

    /// <summary>
    /// The local port number that is used by the UDP socket.
    /// </summary>
    public int LocalPort { get => m_address.MessagePort; }

    /// <summary>
    /// The UDP listen port of the server
    /// </summary>
    public int ServerPort { get => m_settings.UdpPort; }

    /// <summary>
    /// The UUID of the server this application is connected to
    /// </summary>
    public HydraUuid ServerUuid { get => m_profileManager.GetServerUuid(); }

    /// <summary>
    /// The universal unique identifier value for this application
    /// </summary>
    public HydraUuid Uuid { get => m_uuid; set => m_uuid = value; }

    /// <summary>
    /// The address information for this application
    /// </summary>
    public HydraAddress Address { get => m_address; set => m_address = value; }

    /// <summary>
    /// Gets the stream handler that controls stream routing and assignment
    /// </summary>
    public StreamManager StreamAuthority { get => m_streamAuthority; }

    /// <summary>
    /// Gets the profile manager for this application
    /// </summary>
    public HydraProfileManager ProfileManager { get => m_profileManager; }

    /// <summary>
    /// The name of this application
    /// </summary>
    public string ApplicationName { get => m_settings.ApplicationName; set => m_settings.ApplicationName = value; }

    /// <summary>
    /// Gets the number of active profiles in this application
    /// </summary>
    public int ProfileCount { get => m_profileManager.Count; }

    /// <summary>
    /// The settings used by the core Hydra application
    /// </summary>
    public HydraSettings Settings { get => m_settings; set => m_settings = value; }

    /// <summary>
    /// Initializes a new instance of the Hydra class with an uninitialized UDP connection.
    /// </summary>
    /// <param name="settings">The application configuration settings</param>
    public HydraCore(HydraSettings settings)
      : base((byte)settings.LogLevel, settings.LogFile, settings.EnableConsoleLog, settings.MaxUpdateHz)
    {
      m_settings = settings;
      m_localAppDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), settings.ApplicationName);

      if (m_settings.Uuid == null)
      {
        // Attempt loading UUID from file
        LoadUuid();
      }
      else
      {
        m_uuid = m_settings.Uuid;
      }

      if (m_address == null)
      {
        // Try to automatically determine IPs
        IPAddress[] addresses = GetHostIps(settings.UdpBindAddress);
        List<string> hosts = new List<string>(addresses.Length);
        for (int i = 0; i < addresses.Length; ++i)
        {
          hosts.Add(addresses[i].ToString());
        }

        HydraServerSettings serverSettings = settings as HydraServerSettings;
        if (serverSettings != null)
        {
          if (serverSettings.TcpBindAddress != settings.UdpBindAddress)
          {
            addresses = GetHostIps(serverSettings.TcpBindAddress);
            for (int i = 0; i < addresses.Length; ++i)
            {
              if (!hosts.Contains(addresses.ToString()))
              {
                hosts.Add(addresses[i].ToString());
              }
            }
          }
        }

        if (hosts.Count == 0)
        {
          IPAddress address = ResolveHostname(Dns.GetHostName());
          hosts.Add(address?.ToString() ?? "127.0.0.1");
        }

        m_address = new HydraAddress(hosts, settings.UdpPort, (settings as HydraServerSettings)?.TcpPort ?? -1);
      }

      Log.LogDebug("My address is: {0}", m_address);

      m_streamAuthority = new StreamManager(this);
      OnUpdate += m_streamAuthority.Update;
      m_profileManager = new HydraProfileManager(this, m_streamAuthority, HandleMessage, m_settings.KeepAlivePeriod, m_settings.ProfileTimeout, m_settings.UseRouting)
      {
        MaxProfiles = m_settings.MaxClients
      };

      m_messageHandler = new MessageHandlerRouter(Log);
      m_dynamicMessageHandler = new DynamicMessageHandler(Log);
      OnUpdate += Update;

      // Attempt setup of hydralizer indexed assemblies
      // 0 is already reserved for the Hydralize assembly
      if (!Hydralizer.AddSupportedAssembly(1, System.Reflection.Assembly.GetAssembly(typeof(ModularApplication))))
      {
        throw new Exception("Unable to add MEL assembly to the Hydralizer with ID: 1");
      }
      if (!Hydralizer.AddSupportedAssembly(2, System.Reflection.Assembly.GetAssembly(typeof(HydraCore))))
      {
        throw new Exception("Unable to add Hydra assembly to the Hydralizer with ID: 2");
      }

      // Load message codes
      if (!string.IsNullOrEmpty(Settings.MessageCodeFile) && File.Exists(Settings.MessageCodeFile))
      {
        HydraCodes.Load<HydraCodes>(Settings.MessageCodeFile);
      }

      // Make sure the core Hydra messages are in the name lookup
      MsgNames.AddConstants(typeof(HydraCodes));
    }

    /// <summary>
    /// Starts the thread(s) that run a Hydra instance
    /// </summary>
    /// <param name="background">Indicates whether the run thread should be a background thread</param>
    public override void Start(bool background = false)
    {
      IEnumerable<IModule> modules = GetModules();
      m_messageHandler.ClearCache();
      m_messageHandler.CacheModules(modules);

      foreach (IModule mod in modules)
      {
        HydraModule hydraModule = mod as HydraModule;
        hydraModule?.RegisterHydraObjects();
      }

      if (!m_settings.DisableUdp)
      {
        StartUdpListener(m_settings.UdpBindAddress, m_address.MessagePort);
      }

      m_profileManager.Start(true);
      base.Start(background);
    }

    /// <summary>
    /// Stops the thread(s) that run a Hydra instance
    /// </summary>
    public override void Stop()
    {
      base.Stop();
      m_sendEvent.Set();
      m_profileManager.Stop();
      m_udpListener?.Close();
      m_udpListener?.Dispose();
    }

    /// <summary>
    /// Blocks waiting for the application thread to exit.
    /// </summary>
    public override void Join()
    {
      m_sendEvent.Set();
      base.Join();
      m_profileManager.Join();
    }

    /// <summary>
    /// Blocks waiting for application threads to exit up to the specified timeout for each one
    /// </summary>
    /// <param name="millisecondTimeout">The time in milliseconds to wait for each thread</param>
    /// <returns>True if all threads ended, false otherwise</returns>
    public override bool Join(int millisecondTimeout)
    {
      m_sendEvent.Set();
      bool ended = base.Join(millisecondTimeout);
      return m_profileManager.Join(millisecondTimeout) && ended;
    }

    /// <summary>
    /// Sends a message to the application with the specified UUID
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="uuid">The UUID of the destination</param>
    public void SendMessage(Message message, HydraUuid uuid)
    {
      if (uuid != null)
      {
        if (Uuid.Equals(uuid))
        {
          // Directly handle messages destined for myself.
          HandleMessage(message, uuid);
        }
        else
        {
          m_profileManager.SendMessage(message, uuid);
        }
      }
      else
      {
        Log.LogWarning("UUID is null, not sending message");
      }
    }

    /// <summary>
    /// Sends a message to the indicated IP and port over UDP.
    /// This should only be used with anonymous messages.
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="ip">The destination IP address of the message</param>
    /// <param name="port">The destination port of the message</param>
    /// <returns>True if the message was enqueued; otherwise, false.</returns>
    public bool SendMessage(Message message, string ip, int port)
    {
      if (!m_settings.DisableUdp)
      {
        lock (m_sendMessageQueue)
        {
          m_sendMessageQueue.Enqueue(new MessageInfo(message, GetEndPoint(ip, port)));
          m_sendEvent.Set();
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Sends a message on the core listener
    /// </summary>
    /// <param name="message">The message to send</param>
    /// <param name="destination">The destination end point</param>
    /// <returns>True if the message was enqueued; otherwise, false.</returns>
    public bool SendMessage(Message message, IPEndPoint destination)
    {
      if (!m_settings.DisableUdp)
      {
        lock (m_sendMessageQueue)
        {
          m_sendMessageQueue.Enqueue(new MessageInfo(message, destination));
          m_sendEvent.Set();
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Sends an error message to the connection with the specified ID
    /// </summary>
    /// <param name="error">The error to send</param>
    /// <param name="uuid">The UUID of the connection to send the error to</param>
    public bool SendError(HydraErrors error, HydraUuid uuid)
    {
      return m_profileManager.SendMessage(new HydraErrorMsg(new HydraErrorInfo(error)), uuid);
    }

    /// <summary>
    /// Sends a stream to the application with the specified UUID
    /// </summary>
    /// <param name="stream">The stream to send</param>
    /// <param name="uuid">The UUID of the destination application</param>
    /// <param name="localId">The ID of the local sending stream</param>
    /// <param name="remoteId">The ID to use when sending the stream</param>
    /// <param name="length">The total length in bytes to transfer</param>
    /// <param name="transferMode">The mode to use when transferring</param>
    /// <returns>True if the stream was setup for transferring, false otherwise</returns>
    public bool SendStream(Stream stream, HydraUuid uuid, UInt32 localId, UInt32 remoteId, long length, StreamTransferModes transferMode = StreamTransferModes.CONTINUOUS)
    {
      return m_profileManager.SendStream(stream, uuid, localId, remoteId, length, transferMode);
    }

    /// <summary>
    /// Sends a message to the hardware broadcast address 255.255.255.255 and the local port used by this Hydra instance
    /// </summary>
    /// <param name="message">The message to broadcast</param>
    /// <param name="port">The port to send the broadcast on</param>
    /// <returns>True if the broadcast was enqueued; otherwise false.</returns>
    public bool NetworkBroadcast(Message message, int port)
    {
      if (!m_settings.DisableUdp)
      {
        lock (m_sendMessageQueue)
        {
          m_sendMessageQueue.Enqueue(new MessageInfo(message, GetEndPoint("255.255.255.255", port)));
          m_sendEvent.Set();
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Sends the message to every connected client.
    /// </summary>
    /// <param name="message">The message to broadcast.</param>
    public void SendMessageAll(Message message)
    {
      m_profileManager.SendMessageAll(message);
    }

    /// <summary>
    /// A method for sending a handshake to a Hydra server.
    /// </summary>
    /// <param name="uuid">The UUID of the client to send the handshake to</param>
    /// <returns>True if the handshake was sent; otherwise, false.</returns>
    public virtual bool SendHandshake(HydraUuid uuid)
    {
      Profile profile = m_profileManager.GetProfile(uuid);
      HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(new HandshakeInfo(m_uuid, ApplicationName, HydraMode, m_address, profile?.Key ?? 0, profile?.RemoteKey ?? 0));
      if (profile != null)
      {
        return profile.SendMessage(handshakeMsg);
      }
      else if (m_profileManager.Router != null)
      {
        RoutedMessage routeMsg = new RoutedMessage(Uuid, uuid, handshakeMsg);
        if (m_profileManager.Router.Route(routeMsg, uuid))
        {
          Log.LogTrace("Sent routed handshake message");
          return true;
        }
        else
        {
          Log.LogError("Unable to find a profile for or route to UUID: {0}, can't send handshake", uuid);
        }
      }
      else
      {
        Log.LogError("Unable to find a profile for UUID: {0}, can't send message", uuid);
      }

      return false;
    }

    /// <summary>
    /// Sends a handshake message to the specified end point
    /// </summary>
    /// <param name="ep">The IP endpoint destination for the handshake</param>
    /// <returns>True if the handshake was enqueued; otherwise, false.</returns>
    public virtual bool SendHandshake(IPEndPoint ep)
    {
      return SendHandshake(ep, 0, 0);
    }

    /// <summary>
    /// Sends a handshake message to the specified end point with a specific profile key.
    /// </summary>
    /// <param name="ep">The IP endpoint destination for the handshake</param>
    /// <param name="localKey">The key for the local profile associated with this endpoint</param>
    /// <param name="remoteKey">The key to use for the remote profile</param>
    /// <returns>True if the handshake was enqueued; otherwise, false.</returns>
    public virtual bool SendHandshake(IPEndPoint ep, ulong localKey, ulong remoteKey)
    {
      return SendMessage(new HydraHandshakeMsg(new HandshakeInfo(m_uuid, ApplicationName, HydraMode, m_address, localKey, remoteKey)), ep);
    }

    /// <summary>
    /// Adds dynamic handling for the specified message code
    /// </summary>
    /// <param name="messageCode">The message code to add handling for</param>
    /// <param name="handler">The method that will handle received messages</param>
    public virtual void AddDynamicHandling(UInt16 messageCode, MessageClientCallback handler)
    {
      m_dynamicMessageHandler.AddHandling(messageCode, handler);
    }

    /// <summary>
    /// Removes dynamic handling for the specified message code
    /// </summary>
    /// <param name="messageCode">The message code to remove dynamic handling for</param>
    /// <returns>True if the handling was removed, false if the code was already unhandled</returns>
    public virtual bool RemoveDynamicHandling(UInt16 messageCode)
    {
      return m_dynamicMessageHandler.RemoveHandling(messageCode);
    }

    /// <summary>
    /// Stops and removes the assigned client profile, freeing the ID.
    /// </summary>
    /// <param name="uuid">The UUID of the profile to remove</param>
    /// <returns>True if the profile was removed, otherwise false</returns>
    public virtual bool RemoveProfile(HydraUuid uuid)
    {
      return m_profileManager.RemoveProfile(uuid);
    }

    /// <summary>
    /// Gets the profile with the matching UUID
    /// </summary>
    /// <param name="uuid">The UUID of the profile to get</param>
    /// <returns>The profile with a matching UUID or null if no matching profile was found</returns>
    public virtual Profile GetProfile(HydraUuid uuid)
    {
      return m_profileManager.GetProfile(uuid);
    }

    /// <summary>
    /// Gets an array of the managed profiles
    /// </summary>
    /// <returns>The array of managed profiles</returns>
    public virtual Profile[] GetProfiles()
    {
      return m_profileManager.GetProfiles();
    }

    /// <summary>
    /// Gets the array of UUIDs known to this application
    /// </summary>
    /// <returns>The array of known UUIDs</returns>
    public virtual HydraUuid[] GetUuids()
    {
      return m_profileManager.GetUuids();
    }

    /// <summary>
    /// Gets the IP address for a hostname or IP string
    /// </summary>
    /// <param name="hostname">The hostname or IP string to resolve</param>
    /// <returns>The IP address of the hostname or IP string or null if the string wasn't resolved</returns>
    public virtual IPAddress ResolveHostname(string hostname)
    {
      try
      {
        IPAddress[] addresses = Dns.GetHostAddresses(hostname);
        if (addresses != null)
        {
          for (int i = 0; i < addresses.Length; ++i)
          {
            if (addresses[i].AddressFamily == AddressFamily.InterNetwork)
            {
              return addresses[i];
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.LogError("DNS Failed to resolve hostname: {0} with error: {1}", hostname, ex.Message);
        Log.LogTrace(ex.ToString());
      }

      return null;
    }

    /// <summary>
    /// Gets all host IPs that relate to the specified bind address.
    /// </summary>
    /// <param name="bindAddress">The address we're binding to, IPs should only include those relevant to this bind address.</param>
    /// <returns>An array of IP addresses relevant to the bind address, may be an empty array if none are relevant.</returns>
    public virtual IPAddress[] GetHostIps(string bindAddress)
    {
      List<IPAddress> ipAddrList = new List<IPAddress>();
      foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
      {
        if (item.OperationalStatus == OperationalStatus.Up)
        {
          foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
          {
            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
            {
              ipAddrList.Add(ip.Address);
            }
          }
        }
      }

      // Filter out using the bind address
      if (bindAddress != null && !bindAddress.Equals("0.0.0.0"))
      {
        // Look for an exact match
        for (int i = 0; i < ipAddrList.Count; ++i)
        {
          if (!ipAddrList[i].ToString().Equals(bindAddress))
          {
            ipAddrList.RemoveAt(i--);
          }
        }
      }

      return ipAddrList.ToArray();
    }

    /// <summary>
    /// Attempts to load the UUID value from an existing UUID file
    /// </summary>
    /// <returns>True if the UUID was loaded from a file, false otherwise</returns>
    public virtual bool LoadUuid()
    {
      try
      {
        string uuidPath = Path.Combine(m_localAppDir, "uuid.bin");
        Log.LogInfo("UUID file path: {0}", uuidPath);
        if (Directory.Exists(m_localAppDir))
        {
          if (File.Exists(uuidPath))
          {
            byte[] uuidBytes = File.ReadAllBytes(uuidPath);
            int offset = 0;
            Hydralizer.Dehydralize(m_uuid, uuidBytes, ref offset);
            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.LogError(ex.Message);
      }
      return false;
    }

    /// <summary>
    /// Saves the application UUID to a file
    /// </summary>
    public virtual bool SaveUuid()
    {
      try
      {
        if (!Directory.Exists(m_localAppDir))
        {
          Directory.CreateDirectory(m_localAppDir);
        }

        string uuidPath = Path.Combine(m_localAppDir, "uuid.bin");
        using (FileStream fout = File.OpenWrite(uuidPath))
        {
          byte[] uuidBytes = Hydralizer.Hydralize(m_uuid);
          fout.Write(uuidBytes, 0, uuidBytes.Length);
        }
        return true;
      }
      catch (Exception ex)
      {
        Log.LogError(ex.Message);
        return false;
      }
    }

    /// <summary>
    /// Clears the persisted UUID value by deleting the uuid.bin file if it exists
    /// </summary>
    /// <returns>True if the persisted UUID is cleared, otherwise false</returns>
    public virtual bool ClearUuid()
    {
      try
      {
        if (Directory.Exists(m_localAppDir))
        {
          string uuidPath = Path.Combine(m_localAppDir, "uuid.bin");
          if (File.Exists(uuidPath))
          {
            File.Delete(uuidPath);
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.LogError(ex.Message);
        return false;
      }
    }

    /// <summary>
    /// Adds a connection to a new or existing profile
    /// </summary>
    /// <param name="connection">The connection to add</param>
    /// <param name="info">The handshake information</param>
    /// <param name="createNew">If true a new profile will be created to hold the connection, otherwise the connection will be added to an existing profile</param>
    /// <returns>True if the connection was added, false otherwise</returns>
    protected virtual bool AddConnection(Connection connection, HandshakeInfo info, bool createNew)
    {
      if (createNew)
      {
        HydraAddress profileAddress = new HydraAddress(info.Address);
        return m_profileManager.AddConnection(connection, info.Uuid, info.Name, info.Mode, info.LocalKey, profileAddress);
      }
      else
      {
        return m_profileManager.AddConnection(connection, info.Uuid, info.RemoteKey);
      }
    }

    /// <summary>
    /// Adds the connection to an existing profile without authenticating the key value
    /// </summary>
    /// <param name="connection">The connection to add</param>
    /// <param name="uuid">The UUID of the profile to add the connection to</param>
    /// <returns>True if the connection was added, false otherwise</returns>
    protected virtual bool AddConnection(Connection connection, HydraUuid uuid)
    {
      return m_profileManager.AddConnection(connection, uuid);
    }

    /// <summary>
    /// Handles received messages by either acting on native Hydra messages or routing to the appropriate HydraModule.
    /// </summary>
    /// <param name="msg">The message received.</param>
    /// <param name="client">The ID of the client that received the message.</param>
    protected virtual void HandleMessage(Message msg, HydraUuid client)
    {
      if (msg != null)
      {
        Log.LogTrace("Received a {0} message from client {1}", MsgNames.Get(msg.GetCmd()), client?.ToString() ?? "NULL");

        switch (msg.GetCmd())
        {
          case HydraCodes.Noop:
            Log.LogDebug("Received NOOP from {0}", client);
            SendMessage(new NoopAckMsg(), client);
            break;

          case HydraCodes.NoopAck:
          {
            // Do nothing
          }
          break;

          case HydraCodes.HydraError:
          {
            HydraErrorMsg errorMsg = new HydraErrorMsg(msg);
            HydraErrorInfo errorInfo = errorMsg.GetObject<HydraErrorInfo>();
            HandleHydraError(errorInfo, client);
            break;
          }

          case HydraCodes.HydraTransfer:
          {
            HydraTransferMsg transferMsg = new HydraTransferMsg(msg);
            HydraTransferData transferData = transferMsg.GetObject<HydraTransferData>();

            // Start new thread to prevent deadlock when stopping and joining this thread in the transfer
            Thread transferThread = new Thread(new ParameterizedThreadStart(HandleHydraTransfer));
            transferThread.Start(transferData);
          }
          break;

          case HydraCodes.HydraStreamNegotiation:
          {
            m_streamAuthority.HandleMessage(msg, client);
          }
          break;

          default:
          {
            IEnumerable<IModule> modules = GetModules();
            if (m_messageHandler.RouteMessage(modules, msg, client) || m_dynamicMessageHandler.RouteMessage(msg, client))
            {
              return;
            }
            else if (m_defaultModule != null)
            {
              Log.LogTrace("No handling for message: {0}, sending to default module", MsgNames.Get(msg.GetCmd()));
              m_defaultModule.HandleMessage(msg, client);
            }
            else
            {
              Log.LogError("Unhandled message {0}.", MsgNames.Get(msg.GetCmd()));
            }
          }
          break;
        }
      }
      else
      {
        Log.LogError("Received a null message to handle from client {0}, can't process.", client);
      }
    }

    /// <summary>
    /// Runs the main Hydra UDP thread
    /// </summary>
    protected override void Run()
    {
      if (m_settings.DisableUdp)
      {
        // Exit if not running UDP
        return;
      }

      // Start listening
      m_udpListener.BeginReceive(HandleReceive, null);

      while (!StopRun)
      {
        try
        {
          lock (m_sendMessageQueue)
          {
            for (int i = 0; m_sendMessageQueue.Count > 0; i++)
            {
              MessageInfo msgInfo = null;
              if (m_sendMessageQueue.Count > 0)
              {
                msgInfo = m_sendMessageQueue.Dequeue();
              }

              if (msgInfo != null && msgInfo.Message != null && msgInfo.Ep != null)
              {
                byte[] aMsg;
                if (MessageEncoder != null)
                {
                  aMsg = MessageEncoder.Encode(msgInfo.Message.Serialize());
                }
                else
                {
                  aMsg = msgInfo.Message.Serialize();
                }

                m_udpListener.Send(aMsg, aMsg.Length, msgInfo.Ep);
              }
              else
              {
                string msgName = null;
                if (msgInfo?.Message == null)
                {
                  msgName = "null";
                }
                else
                {
                  msgName = MsgNames.Get(msgInfo.Message.GetCmd());
                }

                Log.LogError("Unable to send message, invalid information. Message: {0} EndPoint: {1}", msgName, msgInfo?.Ep);
              }
            }

            m_sendEvent.Reset();
          }

          m_sendEvent.WaitOne();
        }
        catch (Exception ex)
        {
          if (!StopRun)
          {
            Log.LogError("Error: {0}", ex.Message);
            Log.LogTrace(ex.ToString());
            Log.LogInfo("Sleeping {0}ms and continuing...", 100);
            Thread.Sleep(100);
          }
        }
      }
    }

    /// <summary>
    /// The method used to create new UdpConnection instances for connected clients
    /// </summary>
    /// <param name="handshake">The handshake information for the application this connection will be created for</param>
    /// <param name="endPoint">The remote endpoint the connection will communicate with</param>
    /// <returns>A newly created UdpConnection</returns>
    protected virtual UdpConnection CreateUdpConnection(HandshakeInfo handshake, IPEndPoint endPoint = null)
    {
      UdpConnection connection = new UdpConnection(m_settings.UdpBindAddress, endPoint, 0, 0, 1, -1, m_settings.MessageEncoder, m_settings.KeepAlivePeriod);
      connection.StreamTimeout = Settings.StreamTimeout;
      return connection;
    }

    /// <summary>
    /// The method used to create new TcpConnection instances
    /// </summary>
    /// <param name="socket">The socket to wrap in the new connection</param>
    /// <returns>A newly created TcpConnection</returns>
    protected virtual TcpConnection CreateTcpConnection(Socket socket)
    {
      TcpConnection connection = new TcpConnection(socket, 0, -1, 1, m_settings.StreamEncoder, m_settings.KeepAlivePeriod);
      connection.StreamTimeout = m_settings.StreamTimeout;
      return connection;
    }

    /// <summary>
    /// Asynchronous callback handler for the UDP listener receiving
    /// </summary>
    /// <param name="result">The receive result object</param>
    protected virtual void HandleReceive(IAsyncResult result)
    {
      try
      {
        IPEndPoint source = new IPEndPoint(IPAddress.Any, 0);
        byte[] buffer = m_udpListener.EndReceive(result, ref source);
        if (buffer.Length > 0)
        {
          if (MessageEncoder != null)
          {
            buffer = MessageEncoder.Decode(buffer);
          }

          List<Message> messages = Message.Deserialize(buffer);
          for (int m = 0; m < messages.Count; ++m)
          {
            switch (messages[m].GetCmd())
            {
              case HydraCodes.HydraHandshake:
              {
                HydraHandshakeMsg handshakeMsg = new HydraHandshakeMsg(messages[m]);
                HandshakeInfo info = handshakeMsg.GetObject<HandshakeInfo>();
                UdpConnection udpConnection = CreateUdpConnection(info, null);

                if (info.RemoteKey == 0)
                {
                  HydraAddress profileAddress = new HydraAddress(info.Address);
                  profileAddress.Host = source.Address.ToString();
                  if (m_profileManager.AddConnection(udpConnection, info.Uuid, info.Name, info.Mode, info.LocalKey, profileAddress))
                  {
                    Profile profile = m_profileManager.GetProfile(info.Uuid);
                    if (profile != null)
                    {
                      Log.LogDebug("New {1} UDP connection added under profile with UUID: {0}", info.Uuid, info.Mode);
                      udpConnection.Start(true);
                      SendMessage(new HydraHandshakeAckMsg(new HandshakeInfo(m_uuid, ApplicationName, HydraMode, m_address, profile.Key, profile.RemoteKey, udpConnection.ListenPort)), source);
                    }
                    else
                    {
                      Log.LogError("Failed to add UDP connection requesting a new profile");
                      udpConnection.Close();
                    }
                  }
                }
                else if (m_profileManager.AddConnection(udpConnection, info.Uuid, info.RemoteKey))
                {
                  Log.LogDebug("New connection assigned to profile with UUID: {0}", info.Uuid);
                  udpConnection.Start(true);
                  Profile profile = m_profileManager.GetProfile(info.Uuid);
                  profile.RemoteKey = info.LocalKey;
                  SendMessage(new HydraHandshakeAckMsg(new HandshakeInfo(m_uuid, ApplicationName, HydraMode, m_address, profile.Key, profile.RemoteKey, udpConnection.ListenPort)), source);
                }
                else
                {
                  Log.LogError("Failed to add connection requesting UUID: {0}", info.Uuid);
                  udpConnection.Close();
                }
              }
              break;

              case HydraCodes.HydraHandshakeAck:
              {
                // Create a new connection to handle
                HydraHandshakeAckMsg ackMsg = new HydraHandshakeAckMsg(messages[m]);
                HandshakeInfo info = ackMsg.GetObject<HandshakeInfo>();
                if (info != null)
                {
                  info.Address.Host = source.Address.ToString();
                  UdpConnection udpConnection = CreateUdpConnection(info, GetEndPoint(source.Address.ToString(), info.Port));
                  Log.LogDebug("Adding new {3} UDP connection for profile: {0} and key: {1} on port: {2} from address: {4}", info.Uuid, info.RemoteKey, info.Port, info.Mode, info.Address);
                  bool success = AddConnection(udpConnection, info, !m_profileManager.Contains(info.Uuid));
                  ProfileInfo profileInfo = new ProfileInfo(Uuid, info.LocalKey);

                  if (!success)
                  {
                    Log.LogError("Failed to add UDP connection for profile: {0}", info.Uuid);
                    udpConnection.Stop();
                    udpConnection.Close();
                  }
                  else
                  {
                    Log.LogDebug("Writing {0} message to newly established UDP connection", HydraCodes.HydraSetUdpSource);
                    udpConnection.Write(new SetUdpSourceMsg(profileInfo));
                    udpConnection.Start(true);
                  }
                }
              }
              break;

              default:
              {
                if (messages[m].IsAnonymous())
                {
                  HandleMessage(messages[m], null);
                }
                else
                {
                  Log.LogError("Received a {0} message from an unknown source, dropping...", MsgNames.Get(messages[m].GetCmd()));
                }
              }
              break;
            }
          }
        }

        if (!StopRun)
        {
          m_udpListener.BeginReceive(HandleReceive, null);
        }
      }
      catch (Exception ex)
      {
        if (!StopRun)
        {
          Log.LogError("Error receiving UDP messages: {0}", ex.Message);
          Log.LogTrace(ex.ToString());
          Log.LogInfo("Sleeping {0}ms and continuing...", 100);
          Thread.Sleep(100);

          if (!StopRun)
          {
            m_udpListener.BeginReceive(HandleReceive, null);
          }
        }
      }
    }

    /// <summary>
    /// Creates an IP endpoint from the host and port specified.
    /// </summary>
    /// <param name="host">The hostname or IP.</param>
    /// <param name="port">The socket port.</param>
    /// <returns>A new valid IP endpoint for the host and port specified or null if the host wasn't found</returns>
    protected virtual IPEndPoint GetEndPoint(string host, int port)
    {
      try
      {
        IPAddress[] addresses = Dns.GetHostAddresses(host);
        for (int i = 0; i < addresses.Length; i++)
        {
          if (addresses[i].AddressFamily == AddressFamily.InterNetwork)
          {
            return new IPEndPoint(addresses[i], port);
          }
        }
      }
      catch (Exception ex)
      {
        Log.LogError("Exception: {0}, Failed to get host addresses for host: {1}", ex.Message, host);
        Log.LogTrace(ex.ToString());
      }

      return null;
    }

    /// <summary>
    /// Performs automatic error handling for Hydra error messages received
    /// </summary>
    /// <param name="error">The error received</param>
    /// <param name="uuid">The ID of the connection that sent the error</param>
    /// <returns>True if the error was handled, false otherwise</returns>
    protected virtual bool HandleHydraError(HydraErrorInfo error, HydraUuid uuid)
    {
      Log.LogError("Received {0} hydra error", error.Error);

      // No error handling at this level
      return false;
    }

    /// <summary>
    /// Handles transferring a client to a new server
    /// This should NOT be called from any application thread to prevent a deadlock when join is called
    /// </summary>
    /// <param name="transferData">The transfer information</param>
    protected virtual void HandleHydraTransfer(object transferData)
    {
      HydraTransferData data = transferData as HydraTransferData;
      if (data != null && !string.IsNullOrEmpty(data.HostName))
      {
        switch (HydraMode)
        {
          case HydraModes.CLIENT:
          {
            Log.LogInfo("Beginning transfer process, stopping threads...");
            // Stop the processing
            Stop();
            Join();
            Log.LogInfo("Setting new server values and starting, host: {0} port: {1}", data.HostName, data.Port);
            // Set new values
            m_profileManager.ClearProfiles();
            Start();
          }
          break;

          default:
          {
            Log.LogError("Transferring is not implemented for HydraMode: {0}, can't transfer.", HydraMode);
          }
          break;
        }
      }
      else
      {
        Log.LogError("Received transfer data object was invalid, can't transfer.");
      }
    }

    /// <summary>
    /// Handles core hydra timing based responses and actions
    /// </summary>
    /// <param name="deltaTime">Object holding timing information</param>
    protected virtual void Update(UpdateTime deltaTime)
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of a UDP connection listening on the specified port
    /// </summary>
    /// <param name="host">The IP of the interface to listen on</param>
    /// <param name="port">The port to listen for UDP connections on</param>
    protected virtual void StartUdpListener(string host, int port)
    {
      Log.LogInfo("Starting UDP listener on port: {0}", port);
      m_udpListener?.Close();
      IPEndPoint ep = new IPEndPoint(IPAddress.Parse(host), port);
      m_udpListener = new UdpClient(ep);
      m_address.MessagePort = ((IPEndPoint)m_udpListener.Client.LocalEndPoint).Port;
    }
  }
}
