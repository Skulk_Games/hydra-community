#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL;
using Skulk.MEL.Utils;

namespace Skulk.Hydra
{
  /// <summary>
  /// Basic argument parser for a Hydra application.
  /// </summary>
  public class HydraArgParser : CommandLineParser
  {
    /// <summary>
    /// The name of the application
    /// </summary>
    [Option('n', "name", "The application name")]
    public virtual string Name { get; set; } = "Hydra";

    /// <summary>
    /// The server hostname or IP address.
    /// </summary>
    [Option('a', "host", "The server hostname or IP address")]
    public virtual string ServerHost { get; set; } = "localhost";

    /// <summary>
    /// The server UDP port number.
    /// </summary>
    [Option('p', "udp-port", "The server UDP port to use")]
    public virtual ushort ServerPort { get; set; } = 52505;

    /// <summary>
    /// The local port number to use with the local UDP client.
    /// </summary>
    [Option('l', "local-port", "The local port to use for UDP")]
    public virtual ushort LocalPort { get; set; } = 0;

    /// <summary>
    /// The streaming port number for the TCP connection.
    /// </summary>
    [Option('s', "stream-port", "The server TCP port to use")]
    public virtual ushort StreamPort { get; set; } = 52600;

    /// <summary>
    /// The IP address to bind to on UDP connections.
    /// </summary>
    [Option('U', "udp-bind", "The IP address to bind UDP listeners to")]
    public virtual string UdpBindAddress { get; set; } = "0.0.0.0";

    /// <summary>
    /// The IP address to bind to on TCP connections.
    /// </summary>
    [Option('T', "tcp-bind", "The IP address to bind TCP listeners to")]
    public virtual string TcpBindAddress {get; set;} = "0.0.0.0";

    /// <summary>
    /// The log levels to use.
    /// </summary>
    [Option('v', "log-level", "The bitmask indicating which logging levels are enabled")]
    public virtual byte LogLevel { get; set; } = (byte)(LogLevels.ERROR | LogLevels.WARNING | LogLevels.INFO);

    /// <summary>
    /// The log file to use.
    /// </summary>
    [Option('f', "log-file", "The file where log messages will be written")]
    public virtual string LogFile { get; set; } = "hydra.log";

    /// <summary>
    /// Enables logging to the console.
    /// </summary>
    [Option('e', "enable-console-logging", "Enables writing log messages to the console in addition to writing to the file")]
    public virtual bool EnableConsoleLogging { get; set; } = false;

    /// <summary>
    /// The max number of clients to allow to connect.
    /// </summary>
    [Option('c', "max-clients", "The max number of clients to allow to connect")]
    public virtual int MaxClients { get; set; } = 1;

    /// <summary>
    /// The period in seconds at which keepalive messages should be sent.
    /// </summary>
    [Option('k', "keepalive-period", "The period in seconds at which keepalive messages should be sent")]
    public virtual double KeepalivePeriod { get; set; } = 10;

    /// <summary>
    /// The max time in seconds a profile may be inactive (not receive messages) before being removed.
    /// </summary>
    [Option('t', "profile-timeout", "The max time in seconds a profile may be inactive before being removed")]
    public virtual double ProfileTimeout { get; set; } = 30;

    /// <summary>
    /// Indicates whether the application should allow routing messages through other applications.
    /// </summary>
    [Option('r', "use-routing", "Indicates whether this application will allow messages to be routed through to other applications")]
    public virtual bool UseRouting { get; set; } = false;

    /// <summary>
    /// The max rate in Hz that the realtime update method will be called.
    /// </summary>
    [Option('x', "max-update-hz", "The max rate in Hz that the application update method will be called")]
    public virtual double MaxUpdateHz { get; set; } = 10;

    /// <summary>
    /// The max time in seconds a stream may be pending before being removed.
    /// </summary>
    [Option('S', "stream-timeout", "The max time in seconds a stream may be pending before being removed")]
    public double StreamTimeout { get; set; } = 3600;

    /// <summary>
    /// Disables the use of UDP connections on this application.
    /// </summary>
    [Option('d', "disable-udp", "Disables UDP connections on this application")]
    public bool DisableUdp { get; set; } = false;

    /// <summary>
    /// A manually specified UUID to use on this application.
    /// </summary>
    [Option('u', "uuid", "A manually specified UUID to use for this application")]
    public HydraUuid Uuid { get; set; }

    /// <summary>
    /// The path to the file to load message codes from.
    /// </summary>
    [Option('m', "message-codes", "The path to a file that specifies the message codes to use")]
    public string MessageCodeFile { get; set; }

    /// <summary>
    /// Sets the values in the specified <see cref="HydraSettings"/> object from the values on this parser
    /// </summary>
    /// <param name="settings">The settings object to set</param>
    public void UpdateSettings(ref HydraSettings settings)
    {
      settings.ApplicationName = Name;
      settings.MaxClients = MaxClients;
      settings.LogLevel = (LogLevels)LogLevel;
      settings.LogFile = LogFile;
      settings.EnableConsoleLog = EnableConsoleLogging;
      settings.KeepAlivePeriod = KeepalivePeriod;
      settings.ProfileTimeout = ProfileTimeout;
      settings.UseRouting = UseRouting;
      settings.MaxUpdateHz = MaxUpdateHz;
      settings.StreamTimeout = StreamTimeout;
      settings.DisableUdp = DisableUdp;
      settings.Uuid = Uuid;
      settings.MessageCodeFile = MessageCodeFile;
      settings.UdpBindAddress = UdpBindAddress;
    }

    /// <summary>
    /// Sets the values in the specified <see cref="HydraServerSettings"/> object from values on this parser
    /// </summary>
    /// <param name="settings">The server settings object to set</param>
    public void UpdateSettings(ref HydraServerSettings settings)
    {
      HydraSettings baseSettings = settings;
      UpdateSettings(ref baseSettings);
      settings.UdpPort = ServerPort;
      settings.TcpPort = StreamPort;
      settings.TcpBindAddress = TcpBindAddress;
    }

    /// <summary>
    /// Sets the values in the specified <see cref="HydraClientSettings"/> object from values on this parser
    /// </summary>
    /// <param name="settings">The client settings object to set</param>
    public void UpdateSettings(ref HydraClientSettings settings)
    {
      HydraSettings baseSettings = settings;
      UpdateSettings(ref baseSettings);
      settings.ServerHost = ServerHost;
      settings.ServerUdpPort = ServerPort;
      settings.ServerTcpPort = StreamPort;
      settings.UdpPort = LocalPort;
    }
  }
}
