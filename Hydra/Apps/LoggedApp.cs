#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL;

namespace Skulk.Hydra
{
  /// <summary>
  /// A modular application with a local log module.
  /// </summary>
  public class LoggedApp : ModularApplication
  {
    /// <summary>
    /// Constructor for the logged app.
    /// </summary>
    /// <param name="logLevels">The log levels to use.</param>
    /// <param name="logFile">The log file to use.</param>
    /// <param name="enableConsoleOutput">Enables logging output to the console.</param>
    /// <param name="maxUpdateHz">The maximum update frequency in Hertz.</param>
    public LoggedApp(byte logLevels, string logFile = "app.log", bool enableConsoleOutput = true, double maxUpdateHz = 10) : base(maxUpdateHz)
    {
      AppLog log = new AppLog(this);
      log.SetLogLevels(logLevels);
      log.SetLogFile(logFile);
      log.EnableConsoleOutput(enableConsoleOutput);
      AddModule(log);
    }
  }
}
