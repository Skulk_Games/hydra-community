# Hydra Community

A networking library and framework designed for ease of use with mostly self-healing connections.

# Quickstart

Most applications simply need to instantiate and configure either the `HydraClient` or `HydraServer` classes. The `HydraNode` class may also be used in cases where something closer to a mesh network is desired.

## Client

Create a client and configure it to connect to the server address (IP and ports).

```
HydraClientSettings clientSettings = new HydraClientSettings()
{
  ApplicationName = "HydraTestClient",
  UdpPort = 0, // The port the client binds to, set to 0 to let the OS choose
  ServerHost = "localhost",
  ServerUdpPort = 52505,
  ServerTcpPort = 52600,
  LogLevel = MEL.LogLevels.ALL,
  LogFile = "hydraClient.log",
  EnableConsoleLog = true,
  KeepAlivePeriod = 10,
  ProfileTimeout = 30,
  UseRouting = true,
  MaxClients = 2
};

HydraClient client = new HydraClient(clientSettings);

// Add any custom modules here

client.Start();
```

## Server

Create a server and configure the ports to listen on.

```
HydraServerSettings serverSettings = new HydraServerSettings()
{
  ApplicationName = "HydraTestServer",
  MaxClients = m_clientCount + m_serverCount,
  UdpPort = 52505,
  TcpPort = 52600,
  LogLevel = MEL.LogLevels.ALL,
  LogFile = "hydraServer.log",
  EnableConsoleLog = true,
  KeepAlivePeriod = 10,
  ProfileTimeout = 30,
  UseRouting = true,
};

HydraServer server = new HydraServer(serverSettings);

// Add any custom modules here
// i.e. server.AddModule(new CustomModule());

server.Start();

```
