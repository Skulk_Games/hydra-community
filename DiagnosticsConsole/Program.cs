#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydra;
using Skulk.Hydra.Diagnostics;
using System;

namespace DiagnosticsConsole
{
  /// <summary>
  /// Main application class.
  /// </summary>
  /// <remarks>
  /// This application is designed as a simple client program that will subscribe to network events on a server that has a <see cref="DiagnosticsModule"/>.
  /// </remarks>
  class Program
  {
    private static HydraClient ms_client;

    /// <summary>
    /// Main program entry point.
    /// </summary>
    /// <param name="args">Command line argument values.</param>
    static void Main(string[] args)
    {
      ArgParser parser = new ArgParser();
      if (!parser.Parse(args))
      {
        return;
      }

      HydraClientSettings settings = new HydraClientSettings()
      {
        ServerHost = parser.Host,
        ServerUdpPort = parser.UdpPort,
        ServerTcpPort = parser.TcpPort,
        ApplicationName = "DiagnosticsConsole",
        LogFile = "diagnosticsConsole.log"
      };

      Console.CancelKeyPress += HandleCancelKey;
      AppDomain.CurrentDomain.ProcessExit += HandleExit;

      ms_client = new HydraClient(settings);
      ms_client.AddModule(new DiagnosticsClientModule(ms_client, parser.LogDiagnostics, parser.DiagnosticLog));
      ms_client.GetModule<DiagnosticsClientModule>().NetworkEventReceived += HandleNetworkEvent;
      ms_client.MaintainUdp = true;
      ms_client.ProfileManager.OnConnectionAdded += HandleNewConnection;
      ms_client.Start();
      ms_client.SendHandshake();
      ms_client.Join();
    }

    private static void HandleNetworkEvent(object sender, Skulk.Hydra.Diagnostics.Data.NetworkEvent e)
    {
      // Do nothing - the module already logs these
    }

    private static void HandleExit(object sender, EventArgs e)
    {
      ms_client?.Stop();
    }

    private static void HandleCancelKey(object sender, ConsoleCancelEventArgs e)
    {
      ms_client?.Stop();
    }

    private static void HandleNewConnection(Skulk.Hydra.Network.Connection connection)
    {
      ms_client.GetModule<DiagnosticsClientModule>()?.Subscribe(Skulk.Hydra.Diagnostics.Data.SubscriptionType.Network, connection.Profile.Uuid);
    }
  }
}
