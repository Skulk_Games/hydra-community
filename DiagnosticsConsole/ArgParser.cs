#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.MEL.Utils;

namespace DiagnosticsConsole
{
  /// <summary>
  /// Argument parser for the DiagnosticsConsole application.
  /// </summary>
  public class ArgParser : CommandLineParser
  {
    [Option('H', "host", "The hostname or IP address of the server")]
    public string Host { get; set; } = "127.0.0.1";

    [Option('u', "udp-port", "The UDP port to connect on")]
    public int UdpPort { get; set; } = 52505;

    [Option('t', "tcp-port", "The TCP port to connect on")]
    public int TcpPort { get; set; } = 52600;

    [Option('D', "log-diagnostics", "Indicates whether the diagnostics should be written to a log file", PrintValue = true)]
    public bool LogDiagnostics { get; set; } = true;

    [Option('d', "diagnostic-log", "The log where diagnostic information is written")]
    public string DiagnosticLog { get; set; } = "diagnostics.log";
  }
}
